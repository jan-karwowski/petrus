#!/bin/bash

for g in $1/*.sgame; do
    tsp ./run_bosanksy.sh $g ./mixed-configs/bosansky-searchgame-config.json
done
