package pl.edu.pw.mini.sg.gamegen;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BuildingCoordsHelpersTest {

    private static final BuildingDimensions dim = new BuildingDimensions(5, 4, 3); 
    
    @Test
    public void testGetXConnectionIndexUpLeftCorner() {
        assertEquals(0, BuildingCoordsHelpers.getXConnectionIndex(0, 1, 0, 0, dim));
    }
    
    @Test
    public void testGetXCIBottomRightCorner() {
        assertEquals(15, BuildingCoordsHelpers.getXConnectionIndex(3, 4, 3, 0, dim));
    }

    @Test
    public void testGetYConnectionIndex() {
        assertEquals(0, BuildingCoordsHelpers.getYConnectionIndex(0, 0, 1, 0, dim));
    }
    
    @Test
    public void testGetYConnectionIndexBRC() {
        assertEquals(14, BuildingCoordsHelpers.getYConnectionIndex(4, 2, 3, 0, dim));
    }

}
