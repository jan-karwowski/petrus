package pl.edu.pw.mini.sg.gamegen;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BuildingDimensions {
    private final int width;
    private final int height;
    private final int floorCount;

    @JsonCreator
    public BuildingDimensions(@JsonProperty("width") int width, @JsonProperty("height") int height,
            @JsonProperty("floorCount") int floorCount) {
        super();
        this.width = width;
        this.height = height;
        this.floorCount = floorCount;
    }

    public int getHeight() {
        return height;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public int getWidth() {
        return width;
    }
}