package pl.edu.pw.mini.sg.gamegen;

public enum GameType {
	SIMPLE, ER, SSMD, ERLIM, BUILDINGLIKE;
}
