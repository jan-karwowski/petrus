package pl.edu.pw.mini.sg.gamegen;

public enum NodeRole {
    ELEVATOR(true, 'E'), CORRIDOR(true, 'C'), OFFICE(false, 'O'), ENTRANCE(true, 'I');

    private final boolean mainRoute;
    private final char symbol;
    
    private NodeRole(boolean mainRoute, char symbol) {
        this.mainRoute = mainRoute;
        this.symbol = symbol;
    }

    public boolean isMainRoute() {
        return mainRoute;
    }
    
    public char getSymbol() {
        return symbol;
    }

}
