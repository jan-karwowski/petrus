package pl.edu.pw.mini.sg.gamegen;

import java.io.File;

import com.lexicalscope.jewel.cli.Option;

public interface GamegenOptions {
	@Option(longName="output-file")
	File getOutFile();
	
	@Option(longName="output-files-prefix")
	File getOutputFilesPrefix();
	
	boolean isOutputFilesPrefix();
	
	@Option(longName="game-type")
	GameType getGraphType();
	
	@Option(longName="target-count", defaultToNull=true)
	Integer getTargetCount();
	
	@Option(longName="defender-count", defaultToNull=true)
	Integer getDefenderCount();
	
	@Option(longName="vertex-count", defaultToNull=true)
	Integer getVertexCount();
	
	@Option(longName="edge-probability", defaultToNull=true)
	Double getEdgeProbability();
	
	@Option(longName="method-config")
	File getMethodConfig();
}
