package pl.edu.pw.mini.sg.gamegen;

import com.google.common.base.Preconditions;

public class Door {
    private final BuildingCoord corridor;
    private final BuildingCoord office;
    private final boolean xAxis;
    public BuildingCoord getCorridor() {
        return corridor;
    }
    
    public BuildingCoord getOffice() {
        return office;
    }
    
    public boolean isXAxis() {
        return xAxis;
    }
    
    public Door(BuildingCoord v1, BuildingCoord v2) {
        Preconditions.checkArgument(BuildingCoord.areYNeighbours(v1, v2) || BuildingCoord.areXNeighbours(v1, v2));
        this.corridor = v1;
        this.office = v2;
        this.xAxis = BuildingCoord.areXNeighbours(v1, v2);
    }
    
}
