package pl.edu.pw.mini.sg.gamegen;

import java.util.Arrays;
import java.util.Optional;

import com.google.common.collect.ImmutableList;

public class BuildingCoord {
    private final int x;
    private final int y;
    private final int floor;

    public BuildingCoord(int x, int y, int floor) {
        super();
        this.x = x;
        this.y = y;
        this.floor = floor;
    }

    public int getX() {
        return x;
    }

    public int getFloor() {
        return floor;
    }

    public int getY() {
        return y;
    }
    
    public int to3dIndex(final BuildingDimensions dimensions) {
        return BuildingCoordsHelpers.convert3dCoordsToIndex(this, dimensions);
    }

    public static boolean areSameFloor(BuildingCoord p1, BuildingCoord p2) {
        return p1.floor == p2.floor;
    }

    public static boolean areSameXY(BuildingCoord p1, BuildingCoord p2) {
        return p1.x == p2.x && p1.y == p2.y;
    }

    public static boolean areXNeighbours(BuildingCoord p1, BuildingCoord p2) {
        return p1.floor == p2.floor && p1.y == p2.y && Math.abs(p1.x - p2.x) == 1;
    }

    public static boolean areYNeighbours(BuildingCoord p1, BuildingCoord p2) {
        return p1.floor == p2.floor && p1.x == p2.x && Math.abs(p1.y - p2.y) == 1;
    }

    public Optional<BuildingCoord> advanceX(final BuildingDimensions dimensions) {
        return advancePoint(Direction.EAST, dimensions);
    }

    public Optional<BuildingCoord> advanceY(final BuildingDimensions dimensions) {
        return advancePoint(Direction.SOUTH, dimensions);
    }

    public ImmutableList<BuildingCoord> getNeighbours(final BuildingDimensions dimensions) {
        return ImmutableList.copyOf(Arrays.stream(Direction.values()).map(d -> advancePoint(d, dimensions))
                .filter(Optional::isPresent).map(Optional::get).iterator());
    }

    public Optional<BuildingCoord> advancePoint(final Direction direction, final BuildingDimensions dimensions) {
        final int newX = getX() + direction.getXShift();
        final int newY = getY() + direction.getYShift();
        if (newX >= 0 && newY >= 0 && newX < dimensions.getWidth() && newY < dimensions.getHeight()) {
            return Optional.of(new BuildingCoord(newX, newY, getFloor()));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + floor;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BuildingCoord other = (BuildingCoord) obj;
        if (floor != other.floor)
            return false;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "BuildingCoord [x=" + x + ", y=" + y + ", floor=" + floor + "]";
    }
}
