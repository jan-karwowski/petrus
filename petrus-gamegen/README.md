# Random game generator

## SIMPLE

A game that mimics an one-step game with one defender starting vertex, one attacker starting vertex and n targets. The defender has an edge to every target, the same is true for the attacker. The payoffs are random from [-19, +19].

## ER

Generates a random ER(d) graph. The attacker and defender starts and the targets are selected randomly (avoiding collusion, neither the defender, nor the attacker start in a target vertex). The payoffs are random from [-19, +19].


## SSMD
Single Step -- Multiple Defenders

Based on the SIMPLE variant, but the defender has more than one unit as their disposal.

## ERLIM
An ER variant, where the defender has more than one unit as their disposal. 

## BUILDINGLIKE

The most complicated (so far) generator, the generated games mimic office buildings. It is described more precisely in buildinglike-rgg.pdf