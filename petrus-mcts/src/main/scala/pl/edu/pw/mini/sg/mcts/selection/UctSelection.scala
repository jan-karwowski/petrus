package pl.edu.pw.mini.sg.mcts.selection

import pl.edu.pw.mini.sg.mcts.tree.MctsNode
import pl.edu.pw.mini.sg.mcts.tree.stat.MoveEstimate

final case class UctSelection(
  c: Double 
) extends MctsSelection {
  override def apply[M](node: MctsNode[M, _]) : Option[M] = {
    if(node.unvisitedMoves.nonEmpty || node.isEstimatesEmpty)
      None
    else {
      val (move, _) = node.estimates.view
        .map { case MoveEstimate(move, q, n) => (move, q+c*Math.sqrt(Math.log(node.visitCounter.toDouble)/n))}
        .maxBy(_._2)
      Some(move)
    }
  }
}
