package pl.edu.pw.mini.sg.mcts.tree.stat

trait StatCalculator {
  type StatStorage

  def registerPlayout(oldval: StatStorage, reward: Double) : StatStorage
  def estimate(stat: StatStorage, sampleCount: Int) : Double
  val emptyStat : StatStorage
}

