package pl.edu.pw.mini.sg.mcts

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.single.SinglePlayerGame



trait McMove {
  def apply[M](state: SinglePlayerGame[M, _, _], rng: RandomGenerator) : M
  def apply[M](state: SinglePlayerGame[M, _, _], allowedMoves: Seq[M],  rng: RandomGenerator) : M
}
