package pl.edu.pw.mini.sg.mcts.tree.stat

final object MeanStatCalculator extends StatCalculator {
  override type StatStorage = Double

  override def registerPlayout(old: Double, reward: Double) = old+reward
  override def estimate(sum: Double, visits: Int) = sum/visits.toDouble
  override val emptyStat : Double = 0.0
}
