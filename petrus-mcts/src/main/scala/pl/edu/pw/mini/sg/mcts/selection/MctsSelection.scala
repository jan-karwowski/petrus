package pl.edu.pw.mini.sg.mcts.selection

import pl.edu.pw.mini.sg.mcts.tree.MctsNode


trait MctsSelection {
  def apply[M](node: MctsNode[M, _]) : Option[M]
}
