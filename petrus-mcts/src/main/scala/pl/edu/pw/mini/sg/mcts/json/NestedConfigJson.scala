package pl.edu.pw.mini.sg.mcts.json

import argonaut.CodecJson
import pl.edu.pw.mini.sg.mcts.NestedMcsConfig

object NestedConfigJson {
  implicit val nestedMcsConfigCodec : CodecJson[NestedMcsConfig] =
    CodecJson.casecodec2(NestedMcsConfig.apply, NestedMcsConfig.unapply)("depth", "samples")
}
