package pl.edu.pw.mini.sg.mcts.tree.stat

final case class MoveEstimate[M](
  move: M,
  estimate: Double,
  visits: Int
)
