package pl.edu.pw.mini.sg.mcts

import pl.edu.pw.mini.sg.mcts.tree.MctsTree
import scala.language.higherKinds

import pl.edu.pw.mini.sg.mcts.selection.MctsSelection
import pl.edu.pw.mini.sg.mcts.propagation.Propagation
import pl.edu.pw.mini.sg.mcts.tree.simple.SimpleTreeFactory

final case class MctsParameters(
  simulationsInRoot: Int,
  mctsSelection: MctsSelection,
  mcPhaseMove: McMove,
  propagation: Propagation,
  treeFactory: SimpleTreeFactory
)
