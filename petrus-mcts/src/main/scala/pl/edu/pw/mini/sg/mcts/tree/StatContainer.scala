package pl.edu.pw.mini.sg.mcts.tree

import pl.edu.pw.mini.sg.mcts.tree.stat.MoveEstimate
import pl.edu.pw.mini.sg.mcts.tree.stat.StatCalculator

trait StatContainer[M,QS] {
  def visitCounter: Int
  def registerPlayout(result: Double, move: M)(implicit sc: StatCalculator{type StatStorage = QS}) : Unit
  def unvisitedMoves: Vector[M]

//  def statistics: Seq[(M, Double, Int)]
  def estimates(implicit sc: StatCalculator{type StatStorage = QS}): Seq[MoveEstimate[M]]
  def isEstimatesEmpty : Boolean
  def isUnvisitedMovesEmpty : Boolean
}
