package pl.edu.pw.mini.sg.mcts

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.single.SinglePlayerGame




object UniformRandomMove extends McMove {
  override def apply[M](state: SinglePlayerGame[M, _, _], rng: RandomGenerator) : M = {
    val possibleMoves = state.possibleMoves
    return possibleMoves(rng.nextInt(possibleMoves.length))
  }

  override def apply[M](state: SinglePlayerGame[M, _, _], allowedMoves: Seq[M],  rng: RandomGenerator) : M = {
    val allowedMoves2 = allowedMoves.toVector
    return allowedMoves2(rng.nextInt(allowedMoves2.length))
  }
}
