package pl.edu.pw.mini.sg.mcts.tree.stat

final object MaxStatCalculator extends StatCalculator {
  override type StatStorage = Double

  override def registerPlayout(old: Double, reward: Double) = Math.max(old, reward)
  override def estimate(sum: Double, visits: Int) = sum
  override val emptyStat : Double = Double.NegativeInfinity
}
