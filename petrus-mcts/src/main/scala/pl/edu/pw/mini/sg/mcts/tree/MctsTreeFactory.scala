package pl.edu.pw.mini.sg.mcts.tree

import scala.language.higherKinds

trait MctsTreeFactory[+T[M,S] <: MctsTree[M, S]] {
  // Co ze stanem startowym? 
  def create[M, S](state: S, possibleMoves: Seq[M]): T[M, S]
}
