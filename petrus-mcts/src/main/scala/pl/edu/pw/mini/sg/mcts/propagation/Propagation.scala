package pl.edu.pw.mini.sg.mcts.propagation

import scala.language.existentials

import pl.edu.pw.mini.sg.mcts.tree.MctsNode



trait Propagation {
  def propagate(reward: Double, path: List[(MctsNode[M,_],M) forSome {type M}]) : Unit 
}
