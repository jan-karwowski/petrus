package pl.edu.pw.mini.sg.mcts.tree.simple

import pl.edu.pw.mini.sg.mcts.tree.stat.{MapContainer, StatCalculator}
import pl.edu.pw.mini.sg.mcts.tree.{ MctsTreeFactory }

import scala.collection.mutable


case class SimpleTreeFactory(
  statCalculator: StatCalculator
) extends MctsTreeFactory[SimpleTree] {
  override def create[M,S](state: S, possibleMoves: Seq[M]) : SimpleTree[M, S] =
    new SimpleTree(new SimpleNode[M, S, statCalculator.StatStorage](state, mutable.Map(), MapContainer.create(possibleMoves, statCalculator.emptyStat), statCalculator))
}
