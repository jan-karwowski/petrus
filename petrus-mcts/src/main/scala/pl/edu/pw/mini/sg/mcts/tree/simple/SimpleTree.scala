package pl.edu.pw.mini.sg.mcts.tree.simple

import pl.edu.pw.mini.sg.mcts.tree.MctsTree



final class SimpleTree[M, S](
  override val root: SimpleNode[M, S, _]
) extends MctsTree[M, S] {
}
