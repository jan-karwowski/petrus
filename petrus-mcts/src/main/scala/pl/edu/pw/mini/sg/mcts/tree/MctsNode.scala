package pl.edu.pw.mini.sg.mcts.tree

import pl.edu.pw.mini.sg.mcts.tree.stat.MoveEstimate

trait MctsNode[M,S] {
  def successor(move: M, state: S): Option[MctsNode[M,S]]
  def addSuccessor(move: M, newState: S, possibleMoves: Vector[M]) : MctsNode[M, S]

  def unvisitedMoves : Seq[M]

  def state: S
  //  def statContainer: StatContainer[M]
  def estimates: Seq[MoveEstimate[M]]
  def isEstimatesEmpty : Boolean
  def isUnvisitedMovesEmpty : Boolean
  def registerPlayout(result: Double, move: M) : Unit
  def visitCounter: Int
  def nodeCount: Int
}
