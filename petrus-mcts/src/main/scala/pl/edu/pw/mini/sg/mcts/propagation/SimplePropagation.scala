package pl.edu.pw.mini.sg.mcts.propagation

import scala.language.existentials

import pl.edu.pw.mini.sg.mcts.tree.MctsNode



object SimplePropagation extends Propagation {
  override def propagate(result: Double, path: List[(MctsNode[M,_],M) forSome {type M}]) : Unit = {
    path.foreach{case (node, move) => node.registerPlayout(result, move)}
  }
}
