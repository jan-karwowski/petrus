#!/bin/bash

GAME=$1
OUTDIR=columngen-out

mkdir -p $OUTDIR

export GUROBI_HOME=/home/jan/Pobrane/gurobi751/linux64/
export LD_LIBRARY_PATH=$GUROBI_HOME/lib

JAVA_OPTS="-Djava.library.path=$GUROBI_HOME/lib" ./target/pack/bin/petrus-column-lp  -g $GAME -o $OUTDIR/$(basename $GAME .sgmt).colgen.trmt > $OUTDIR/$(basename $GAME .sgmt).colgen.out 2> $OUTDIR/$(basename $GAME .sgmt).colgen.err

