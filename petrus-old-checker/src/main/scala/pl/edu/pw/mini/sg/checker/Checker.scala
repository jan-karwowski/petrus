package pl.edu.pw.mini.sg.checker

import better.files.File
import org.rogach.scallop.ScallopConf
import pl.edu.pw.mini.sg.game.boundedrationality.BoundedRationality
import pl.edu.pw.mini.sg.game.graph.GraphGamePlugin
import pl.edu.pw.mini.sg.game.graph.sud.{ DeterministicSingleUnitMoveGraphGame, DeterministicSingleUnitMoveGraphGameFactory, SingleUnitAttackerState, SingleUnitDefenderState }
import pl.edu.pw.mini.sg.game.util.attacker.OptimalAttacker


object Checker {
  final class Options(args: Seq[String]) extends ScallopConf(args) {
    val gameFile = opt[String](name = "game-file", short = 'g').map(x => File(x))
    val strategyFile = opt[String](name = "strategy-file", short = 's').map(x => File(x))

    verify()
  }

  def main(args: Array[String]) : Unit = {
    implicit val br = BoundedRationality.fullyRational
    val options = new Options(args);

    val gameConfig = GraphGamePlugin.readConfig(options.gameFile().toJava).get
    val game = DeterministicSingleUnitMoveGraphGameFactory.create(gameConfig)
    val strategy = MixedStrategy.readFromFile(options.strategyFile()).toBehavioralStrategy(game.defenderState)
    val (optimalAttacker, optPayoff) = OptimalAttacker.calculate[Int, Int, SingleUnitDefenderState, SingleUnitAttackerState, DeterministicSingleUnitMoveGraphGame](game , strategy)

    println(optimalAttacker)
    println(optPayoff)
  }
}
