#!/bin/sh

while [ $# -gt 0 ]; do
    sed -e 's/DCM \[\([0-9]*\)\]/\1/g' -e 's/=\([0-9][0-9]*\.[0-9][0-9][0-9][0-9]\)[0-9]*/=\1/g' \
	-e 's/Iteration:/It:/g' -e 's/AveragedAttackerStrategy/AAS/' -e 's/AttackerStrategy/AS/' \
	-e 's/\([0-9]\), /\1,/g' -i  "$1"
    shift
done

