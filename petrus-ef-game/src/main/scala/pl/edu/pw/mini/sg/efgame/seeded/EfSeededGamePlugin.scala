package pl.edu.pw.mini.sg.efgame.seeded

import pl.edu.pw.mini.sg.game.deterministic.plugin.{ GameFactory, GamePlugin }


object EfSeededGamePlugin extends GamePlugin {
  override type GameConfig = SeededEfConfig

  val fileExtension: String = "seededef"
  val configJsonDecode = SeededEfConfigJson.seededEfConfigCodec
  val singleCoordFactory : GameFactory[GameConfig] = EfSeededGameFactory
}
