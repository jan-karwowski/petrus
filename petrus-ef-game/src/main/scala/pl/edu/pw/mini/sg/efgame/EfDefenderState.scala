package pl.edu.pw.mini.sg.efgame

import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState


final class EfDefenderState(
  val isId: Int,
  val possibleMoves : List[Int]
) extends PlayerVisibleState[Int] {
  override def equals(other: Any) : Boolean = other match {
    case eds : EfDefenderState => eds.isId == isId
    case _ => false
  }

  override def hashCode: Int = isId.hashCode()

  override def toString = s"D${isId}"
}


