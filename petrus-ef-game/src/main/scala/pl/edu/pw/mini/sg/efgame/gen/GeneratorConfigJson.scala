package pl.edu.pw.mini.sg.efgame.gen

import argonaut.CodecJson



object GeneratorConfigJson {
  implicit val generatorConfigCodec = CodecJson.casecodec14(GeneratorConfig.apply, GeneratorConfig.unapply)(
    "defenderMovesMin",
    "defenderMovesMax",
    "attackerMovesMin",
    "attackerMovesMax",
    "gameDepthMin",
    "gameDepthMax",
    "defenderLambda",
    "attackerLambda",
    "defenderEqualIsBias",
    "attackerEqualIsBias",
    "payoffDriftVariance",
    "defenderPayoffVariance",
    "attackerPayoffVariance",
    "playersPayoffCovariance"
  )
}
