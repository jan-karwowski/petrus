package pl.edu.pw.mini.sg.efgame.gen

final case class GeneratorConfig (
  defenderMovesMin: Int,
  defenderMovesMax: Int,
  attackerMovesMin: Int,
  attackerMovesMax: Int,
  gameDepthMin: Int,
  gameDepthMax: Int,
  defenderLambda: Double,
  attackerLambda: Double,
  defenderEqualIsBias: Double,
  attackerEqualIsBias: Double,
  payoffDriftVariance: Double,
  defenderPayoffVariance: Double,
  attackerPayoffVariance: Double,
  playersPayoffCovaciance: Double
)
