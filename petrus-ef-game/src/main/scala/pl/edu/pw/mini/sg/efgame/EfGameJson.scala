package pl.edu.pw.mini.sg.efgame

import argonaut.EncodeJson
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson


object EfGameJson extends DeterministicGameJson[Int, Int, EfDefenderState, EfAttackerState]{
  override val defenderMoveEncode: EncodeJson[Int] = EncodeJson.IntEncodeJson
  override val attackerMoveEncode: EncodeJson[Int] = EncodeJson.IntEncodeJson
  override val defenderStateEncode: EncodeJson[EfDefenderState] = EncodeJson.IntEncodeJson.contramap[EfDefenderState](_.isId)
  override val attackerStateEncode: EncodeJson[EfAttackerState] = EncodeJson.IntEncodeJson.contramap[EfAttackerState](_.isId)
}
