package pl.edu.pw.mini.sg.efgame

import pl.edu.pw.mini.sg.game.deterministic.DeterministicGame


sealed trait EfGame extends DeterministicGame[Int, Int, EfDefenderState, EfAttackerState, EfGame] 


final class EfGameInner (
  gameNode: InnerGameNode,
  efGameConfig: EfGameConfig
) extends EfGame {

  override def attackerPayoff : Double = 0
  override def defenderPayoff : Double = 0
  override val attackerState: EfAttackerState = new EfAttackerStateInner(
    gameNode.attackerIS, efGameConfig.attackerContinuations, efGameConfig.attackerIS
  )
  override val defenderState: EfDefenderState =
    new EfDefenderState(gameNode.defenderIS, efGameConfig.defenderIS(gameNode.defenderIS))

  override val isLeaf : Boolean = false

  override def makeMove(defenderMove: Int,attackerMove: Int): EfGame = gameNode.successors(defenderMove)(attackerMove) match {
    case in: InnerGameNode => new EfGameInner(in, efGameConfig)
    case leaf: LeafGameNode => new EfGameLeaf(leaf)
  }
}

final class EfGameLeaf (
  gameNode: LeafGameNode
) extends EfGame {
  override def attackerPayoff : Double = gameNode.defenderPayoff
  override def defenderPayoff : Double = gameNode.attackerPayoff

  override val defenderState: EfDefenderState = new EfDefenderState(-1, Nil)
  override def attackerState: EfAttackerState = EfAttackerLeaf
  override val isLeaf : Boolean = true

  override def makeMove(defenderMove: Int,attackerMove: Int): EfGame = throw new IllegalStateException("Leaf state")
}
