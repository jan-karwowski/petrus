package pl.edu.pw.mini.sg.efgame

import argonaut.DecodeJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.{GameFactory, GamePlugin}


object EfGamePlugin extends GamePlugin {
  override type GameConfig = EfGameConfig

  override val fileExtension = "efgame"
  override val configJsonDecode: DecodeJson[EfGameConfig] = EfGameConfigJson.efGameConfigCodec
  override val singleCoordFactory : GameFactory[GameConfig] = EfGameFactory
}
