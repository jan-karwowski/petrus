package pl.edu.pw.mini.sg.efgame

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.AdvanceablePlayerVisibleState
import pl.edu.pw.mini.sg.game.util.RandomHelpers._


sealed trait EfAttackerState extends AdvanceablePlayerVisibleState[Int, EfAttackerState] {
  val isId: Int
}


final class EfAttackerStateInner(
  val currentIs: Int,
  val continuations: Map[(Int,Int), List[Int]],
  val attackerIs : Map[Int, List[Int]]
) extends EfAttackerState {
  override def possibleMoves: List[Int] = attackerIs(currentIs).toList

  override val isId = currentIs

  override def possibleContinuations(move: Int) : List[EfAttackerState] = {
    continuations((currentIs, move)).map(is => if(is == -1) EfAttackerLeaf else new EfAttackerStateInner(is, continuations, attackerIs))
  }
  override def sampleContinuations(move: Int)(implicit rng: RandomGenerator) : EfAttackerState =
    continuations((currentIs, move)).toVector.randomElement match {
      case -1 => EfAttackerLeaf
      case x =>  new EfAttackerStateInner(x, continuations, attackerIs)
    }


  override def equals(other: Any) : Boolean = other match {
    case eas: EfAttackerStateInner => eas.currentIs == currentIs
    case _ => false
  }

  override def hashCode : Int = currentIs.hashCode()

  override def toString = s"A${currentIs}"
}

object EfAttackerLeaf extends EfAttackerState {
  override def possibleMoves: List[Int] = Nil

  override def possibleContinuations(move: Int) = throw new IllegalStateException("Leaf state")
  override def sampleContinuations(move: Int)(implicit rng: RandomGenerator) = throw new IllegalStateException("Leaf state")

  override def toString = "AL"
  override val isId = -1
}
