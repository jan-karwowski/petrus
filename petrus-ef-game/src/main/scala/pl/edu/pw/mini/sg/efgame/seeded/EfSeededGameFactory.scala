package pl.edu.pw.mini.sg.efgame.seeded

import org.apache.commons.math3.random.MersenneTwister
import pl.edu.pw.mini.sg.efgame.EfGameFactory
import pl.edu.pw.mini.sg.efgame.gen.Generator
import pl.edu.pw.mini.sg.efgame.{ EfAttackerState, EfDefenderState, EfGame, EfGameInner, EfGameJson }
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactory


object EfSeededGameFactory extends GameFactory[SeededEfConfig] {
  override type DefenderMove = Int
  override type AttackerMove = Int
  override type DefenderState = EfDefenderState
  override type AttackerState = EfAttackerState
  override type Game = EfGame

  override def create(seededConfig: SeededEfConfig) : EfGame = {
    val generator = new MersenneTwister(seededConfig.seed)
    val gameConfig = Generator.randomGame(seededConfig.generatorConfig)(generator)
    new EfGameInner(gameConfig.gameTree, gameConfig)
  }

  def maxDefenderPayoff(seededConfig: SeededEfConfig): Double = {
    val generator = new MersenneTwister(seededConfig.seed)
    val gameConfig = Generator.randomGame(seededConfig.generatorConfig)(generator)
    EfGameFactory.maxDefenderPayoff(gameConfig)
  }
  def minDefenderPayoff(seededConfig: SeededEfConfig): Double = {
    val generator = new MersenneTwister(seededConfig.seed)
    val gameConfig = Generator.randomGame(seededConfig.generatorConfig)(generator)
    EfGameFactory.minDefenderPayoff(gameConfig)
  }
  def maxAttackerPayoff(seededConfig: SeededEfConfig): Double = {
    val generator = new MersenneTwister(seededConfig.seed)
    val gameConfig = Generator.randomGame(seededConfig.generatorConfig)(generator)
    EfGameFactory.maxAttackerPayoff(gameConfig)
  }
  def minAttackerPayoff(seededConfig: SeededEfConfig): Double = {
    val generator = new MersenneTwister(seededConfig.seed)
    val gameConfig = Generator.randomGame(seededConfig.generatorConfig)(generator)
    EfGameFactory.minAttackerPayoff(gameConfig)
  }

  override val json: DeterministicGameJson[DefenderMove, AttackerMove, DefenderState, AttackerState] = EfGameJson
}
