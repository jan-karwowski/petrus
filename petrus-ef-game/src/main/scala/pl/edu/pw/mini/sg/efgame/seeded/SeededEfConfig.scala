package pl.edu.pw.mini.sg.efgame.seeded

import java.util.UUID
import pl.edu.pw.mini.sg.efgame.gen.GeneratorConfig
import pl.edu.pw.mini.sg.game.deterministic.plugin.config.GameConfig

final case class SeededEfConfig(
  generatorConfig: GeneratorConfig,
  seed: Int ,
  id: UUID
) extends GameConfig
