package pl.edu.pw.mini.sg.efgame.seeded

import argonaut.CodecJson
import pl.edu.pw.mini.sg.efgame.gen.GeneratorConfigJson



object SeededEfConfigJson {
  import GeneratorConfigJson.generatorConfigCodec
  implicit val seededEfConfigCodec : CodecJson[SeededEfConfig] =
    CodecJson.casecodec3(SeededEfConfig.apply, SeededEfConfig.unapply)("generatorConfig", "seed", "id")
}
