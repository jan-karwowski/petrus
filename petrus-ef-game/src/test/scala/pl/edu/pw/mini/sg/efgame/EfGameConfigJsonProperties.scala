package pl.edu.pw.mini.sg.efgame

import argonaut.DecodeResult
import org.scalacheck.{ Arbitrary, Gen, Properties }
import org.scalacheck.Prop.forAll


object EfGameConfigJsonProperties extends Properties("EfGameConfigJson") {

  implicit val arbIgn : Gen[InnerGameNode] = for {
    dMoves <- Gen.chooseNum(1,4)
    aMoves <- Gen.chooseNum(1,4)
    aIs <- Gen.chooseNum(0, 200)
    dIs <- Gen.chooseNum(0, 200)
    successors <- Gen.listOfN(dMoves, Gen.listOfN(aMoves, arbGn))
  } yield InnerGameNode(dIs, aIs, successors.map(_.zipWithIndex.map(_.swap).toMap).zipWithIndex.map(_.swap).toMap)

  implicit val arbLgn : Gen[LeafGameNode] = for {
    dp <- Gen.chooseNum(0.0, 1.0)
    ap <- Gen.chooseNum(0.0, 1.0)
  } yield LeafGameNode(dp, ap)

  val arbGn : Gen[GameNode] = Gen.frequency((1, arbIgn), (5, arbLgn))
  val arbGnFF : Gen[GameNode] = Gen.frequency((3, arbIgn), (1, arbLgn))
  implicit val arbit : Arbitrary[GameNode] = Arbitrary(arbGnFF)

  property("encode-decode should be identity function") = forAll{efgc: GameNode => {
    EfGameConfigJson.gameNodeDecode.decodeJson(EfGameConfigJson.gameNodeEncode.apply(efgc)) == DecodeResult.ok(efgc)
  }}
}
