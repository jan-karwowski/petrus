package pl.edu.pw.mini.sg.efgame

import argonaut.{ DecodeResult, Json, Parse }
import org.scalatest.{ Matchers, WordSpec }


class EfGameConfigJsonSpec extends WordSpec with Matchers {
  "game node decoder" should {
    "decode example json nested" in {
      val json = """{"defenderIs":3, "attackerIs":5,
          "3": {
             "4": {"defenderPayoff": 0.2, "attackerPayoff": 0.5}, 
             "5": {"defenderPayoff": 0.3, "attackerPayoff": 0.1}
          }
      }"""
      val parsed = Parse.parse(json).right.get

      EfGameConfigJson.gameNodeDecode.decodeJson(parsed) shouldEqual(
        DecodeResult.ok(InnerGameNode(3,5, Map(3 -> Map(4 -> LeafGameNode(0.2, 0.5), 5 -> LeafGameNode(0.3, 0.1)))))
      )
    }

    "decode example leaf" in {
      val json = Json("defenderPayoff" -> Json.jNumber(0.3), "attackerPayoff" -> Json.jNumber(0.5))
      EfGameConfigJson.gameNodeDecode.decodeJson(json).shouldEqual(DecodeResult.ok(LeafGameNode(0.3, 0.5)))
    }
  }
}
