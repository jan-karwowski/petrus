package pl.edu.pw.mini.sg.matrix.deterministic

import argonaut.DecodeJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.{ GameFactory, GamePlugin }
import pl.edu.pw.mini.sg.matrix.{ MatrixGameConfig, MatrixGameConfigJson }



object MatrixGamePlugin extends GamePlugin {
  override type GameConfig = MatrixGameConfig

  override val fileExtension: String = "nfgame"
  override val configJsonDecode: DecodeJson[GameConfig] = MatrixGameConfigJson.matrixGameConfigDecodeJson
  val singleCoordFactory : GameFactory[GameConfig] = MatrixGameFactory
}
