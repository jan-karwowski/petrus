package pl.edu.pw.mini.sg.matrix.deterministic

import pl.edu.pw.mini.sg.game.deterministic.{ AdvanceablePlayerVisibleState }


sealed trait MatrixGameState extends AdvanceablePlayerVisibleState[Int, MatrixGameState]

final case class ColumnChoiceState(columnCount: Int)
 extends MatrixGameState
{
  override def possibleMoves : List[Int] = List.tabulate(columnCount)(identity)
  override def possibleContinuations(move: Int): List[MatrixGameState] = List(AfterChoiceState)
  override def sampleContinuations(move: Int)
    (implicit rng: pl.edu.pw.mini.sg.rng.RandomGenerator): MatrixGameState = AfterChoiceState 
}

final object AfterChoiceState extends MatrixGameState {
  override def possibleMoves : List[Int] = List()
  override def possibleContinuations(move: Int) =
    throw new IllegalStateException("This state does not allow continuations")
  override def sampleContinuations(move: Int)
    (implicit rng: pl.edu.pw.mini.sg.rng.RandomGenerator): MatrixGameState =
    throw new IllegalStateException("This state does not allow continuations")
}

