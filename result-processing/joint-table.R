#!/usr/bin/env Rscript

require(jsonlite)

sgmt.file.parser <- function(file.name) {
    wo.extension <- gsub("\\.trmt$", "", gsub("^eval-", "", file.name))
    dotsplit <- strsplit(wo.extension, "\\.")
    sgmtsplit <- strsplit(dotsplit[1], "\\.sgmt-0-")
    data.frame(repetition=as.integer(dotsplit[2]),
               game=sgmtsplit[1], method=sgmtsplit[2])
}

read.results <- function(file.names, file.name.parser) {
    Map(function(file.name) {
        json <- fromJSON(file.name)
        data.frame(time=json$trainingTime, attackerPayoff = json$attackerPayoff,
                   defenderPayoff = json$defenderPayoff, file.name.parser(file.name))
    }. file.names)
}
