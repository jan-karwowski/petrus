#!/bin/sh

OUT_FILE=$2
AGG_FILE=$1

if [ ! -f $AGG_FILE ] ; then
    echo "Agg file does not exist"
    exit 1
fi

if echo $AGG_FILE | grep -Ev '\.csv$' ; then
    echo "Input must have csv extension"
    exit 1
fi

if echo $OUT_FILE | grep -Ev '\.tex$' ; then
    echo "Output must have .tex extension"
    exit 1
fi

cat <<EOF > $OUT_FILE
\documentclass[a4paper]{article}

\usepackage[landscape,margin=1cm]{geometry}

\begin{document}
\begin{tabular}{c|ccccc|cc|c|cc|c|c}
game&\multicolumn{5}{c|}{New}&\multicolumn{2}{c|}{MixedUCT}&R&\multicolumn{2}{c|}{MILP}&Sc&Sc\\\\
&payoff&payoff max&sd&time&sd&payoff&time&payoff&payoff&time&MU&new\\\\
EOF

csvtool namedcol game,defenderPayoff.mean,defenderPayoff.max,defenderPayoff.sd,training.time.mean,training.time.sd,uctDefenderPayoff.mean,uctTimeMs.mean,randomDefenderPayoff.mean,gtoptDefenderPayoff.mean,gtoptTime.mean,score,score.new $AGG_FILE | tail -n +1 | csv2latex --nohlines --nohead | tail -n +3 >> $OUT_FILE

cat <<EOF >> $OUT_FILE
\end{document}
EOF


