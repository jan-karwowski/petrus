#!/usr/bin/env Rscript

args <- commandArgs(trailingOnly = TRUE)

out <- args[[1]]
new.results <- args[[2]]
sb.results <- args[[3]]
basic.results <- args[[4]]

aggregate.results <- function(data) {
    rr <- aggregate(. ~ game, data, function(x) c(mean = round(mean(x), 2), sd = round(sd(x), 2), max = round(max(x), 2)))
    rr <- do.call(data.frame, rr)
}

read.old.results <- function(file) {
    data <- read.csv(file, header = TRUE)
    data$game <- paste0(data$game, "-5")
    data
}


new.data <- read.csv(new.results)
new.agg <- aggregate.results(new.data)

old.data <- rbind(read.old.results(basic.results), read.old.results(sb.results))

d <- merge(new.agg, old.data)

d$score.new <-
    round(
    (d$defenderPayoff.mean - d$randomDefenderPayoff.mean)/(d$gtoptDefenderPayoff.mean - d$randomDefenderPayoff.mean),
    2
    )

write.csv(d, out, row.names = FALSE)
