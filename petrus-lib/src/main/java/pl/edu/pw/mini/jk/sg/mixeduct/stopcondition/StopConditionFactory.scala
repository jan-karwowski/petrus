package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove


trait StopConditionFactory {
  def create[DM <: DefenderMove, DS] : StopCondition[DM, DS]
}
