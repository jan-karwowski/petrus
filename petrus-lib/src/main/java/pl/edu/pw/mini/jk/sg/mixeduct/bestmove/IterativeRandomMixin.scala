package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator

trait IterativeRandomMixin[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
] {
  this: AbstractBestImprove[DM, AM, DS, AS, G] =>

  def randomIteration(count: Int, step: RandomGenerator => Map[ImmutableList[DM], Double])(
      implicit rng: RandomGenerator
  ): Stream[Map[ImmutableList[DM], Double]] = Stream.tabulate(count)(_ => step(rng))

  def findGood(
      currPayoff: Double
  )(stream: Stream[Map[ImmutableList[DM], Double]]): Option[Map[ImmutableList[DM], Double]] =
    stream.find(dir => isImprovementDirection(currPayoff, dir))

  def findGoodIteration(
      currPayoff: Double,
      count: Int,
      step: RandomGenerator => Map[ImmutableList[DM], Double]
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] =
    findGood(currPayoff)(randomIteration(count, step))

}
