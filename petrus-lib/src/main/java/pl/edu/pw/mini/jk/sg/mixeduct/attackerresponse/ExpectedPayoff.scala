package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse

import java.util.stream.Collectors
import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.opt.util.GamePayoff

case class ExpectedPayoff(attackerPayoff: Double, defenderPayoff: Double) {
  def this (payoff: GamePayoff, probability : Double) =
        this(probability * payoff.getAttackerPayoff, probability * payoff.getDefenderPayoff)
}


object ExpectedPayoff {
  def sum(s: java.util.stream.Stream[ExpectedPayoff]) : ExpectedPayoff = {
    val l = s.collect(Collectors.toList[ExpectedPayoff]).asScala
    ExpectedPayoff(l.map(_.attackerPayoff).sum, l.map(_.defenderPayoff).sum)
  }

  def sum(s: Seq[ExpectedPayoff]) : ExpectedPayoff = {
    ExpectedPayoff(s.map(_.attackerPayoff).sum, s.map(_.defenderPayoff).sum)
  }

}
