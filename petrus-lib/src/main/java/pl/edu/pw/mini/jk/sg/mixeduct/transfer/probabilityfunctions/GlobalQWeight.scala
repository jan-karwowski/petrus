package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

object GlobalQWeight extends NodeProbabilityWeightFunction {
  override type GlobalStat = Double
  override def calculateGlobalStat(stats: => Seq[UnmodifiableDecisionStatisticsView]): Double =
    stats.map(_.averagePayoff).min
  override def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _], stat: Double): Double = statistics.averagePayoff-stat

  override val leafOnlyWeight = false
}
