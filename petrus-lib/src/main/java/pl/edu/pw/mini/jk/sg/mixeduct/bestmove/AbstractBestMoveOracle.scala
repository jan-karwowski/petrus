package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import java.{util => ju}
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracle
import com.google.common.collect.ImmutableList
import scala.collection.JavaConverters._
import com.google.common.collect.ImmutableMap
import org.apache.commons.math3.random.RandomGenerator

abstract class AbstractBestMoveOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
](
    gameMatrices: GameMatrices[AM, DM, AS, DS, G]
) extends AbstractBestImprove[DM, AM, DS, AS, G](gameMatrices) {

  protected def heuristicMoveOrder(attStr: ImmutableMap[ImmutableList[AM], java.lang.Double]) =
    gameMatrices
      .getDefenderSequences()
      .asScala
      .sortBy(str => {
        val defStr = bumpMove(str, 1)
        -gameMatrices.getExpectedPayoff(defStr, attStr).defenderPayoff
      })

  protected def bestMoveWithStep(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      sortedMoves: Seq[ImmutableList[DM]],
      step: Double,
      currPayoff: Double
  ) = {
    logger.debug(s"Trying step ${step}, moves: ${sortedMoves.length}, currP: ${currPayoff}")
    sortedMoves
      .find(move => {
        val ds = bumpMove(move, step)
        val bestResponse = OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, ds)
        val payoff =
          gameMatrices.getExpectedPayoff(ds, AbstractBestImprove.strategyListToMap(bestResponse))
        payoff.defenderPayoff > currPayoff
      })
  }

  protected def bestMoveProcedure(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      sortedMoves: Seq[ImmutableList[DM]],
      currPayoff: Double
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]]

  def bestImprovementStep(
      attackerProbabilities: AttackerStrategy[AM]
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] = {
    val attStr = AbstractBestImprove.strategyListToMap(attackerProbabilities.getPmf())

    val moves = heuristicMoveOrder(attStr)
    val currPayoff = payoff

    val ret = bestMoveProcedure(attStr, moves, currPayoff)
    logger.debug(s"Best move found: ${ret}")
    ret
  }
}
