package pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping

final case class ExponentialDamping(
  alpha: Double
) extends WeightDamping {
  override type GlobalStat = Unit
  def calcGlobalStat(allWeights: Seq[Double]): GlobalStat = ()
  def modifyWeight(stat: Unit)(weight: Double): Double = weight*alpha
}
