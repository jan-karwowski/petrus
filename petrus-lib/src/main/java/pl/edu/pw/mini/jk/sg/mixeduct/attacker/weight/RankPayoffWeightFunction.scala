package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight

trait RankPayoffWeightFunction {
  def apply(payoff: Double, maxPayoff: Double, minPayoff: Double, rank: Int): Double
}
