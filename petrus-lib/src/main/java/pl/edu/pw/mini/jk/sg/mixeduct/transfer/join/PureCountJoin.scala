package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoinerFunctions }


object PureCountJoin extends TreeJoinerFunctions {
  override type CS = Unit
  override type GS = Unit

  val quantizationLevel = 100000

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit) : UCTDecisionStatistics = {
    val q = stats.count(_.averagePayoff > 0).toDouble/treeCount
    new UCTDecisionStatistics((q*quantizationLevel).toInt, (q*quantizationLevel).toInt, q)
  }
  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) = Unit
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 
}
