package pl.edu.pw.mini.jk.sg.gteval;

import com.google.common.collect.ImmutableList;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;

case class PlayScenario[AM, DM <: DefenderMove] (
  defenderSequence: ImmutableList[DM],
  attackerSequence: ImmutableList[AM]
)
