package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult;

public interface StrategyChooser<DM extends DefenderMove,AM> {

    public void registerStrategy(IterationResult<DM, AM, ?, ?, ?> iterationResult);
    public default  ImmutableMap<ImmutableList<DM>, Double> getBestStrategy() {
	return getBestIterationResult().defenderStrategy();
    }
    public IterationResult<DM, AM, ?, ?, ?> getBestIterationResult();
}
