package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator

final class ReducingBestMoveOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
](
    stepEps: Double,
    gameMatrices: GameMatrices[AM, DM, AS, DS, G]
) extends AbstractBestMoveOracle[DM, AM, DS, AS, G](
      gameMatrices
    ) {
  protected def bestMoveProcedure(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      sortedMoves: Seq[ImmutableList[DM]],
      currPayoff: Double
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] = {
    def singleStep(step: Double): Option[(ImmutableList[DM], Double)] = {
      if (step < stepEps) None
      else
        bestMoveWithStep(attStr, sortedMoves, step, currPayoff)
          .map((_, step))
          .orElse(singleStep(step / 2))
    }

    singleStep(1.0).map(Map(_))
  }
}
