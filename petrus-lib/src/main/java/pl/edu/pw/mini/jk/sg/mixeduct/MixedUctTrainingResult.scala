package pl.edu.pw.mini.jk.sg.mixeduct

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.gteval.result.{TimedTrainingResult}

final class MixedUctTrainingResult[DM <: DefenderMove, AM](
  iterationResult: IterationResult[DM, AM, _, _, _],
  timeMs: Long,
  iterations: Int,
  restarts: Int
) extends TimedTrainingResult[DM, AM](
  iterationResult.defenderStrategy,
  iterationResult.optimalAttacker,
  iterationResult.gameMatrices,
  timeMs
) {
  override def methodSpecificStatistics() = super.methodSpecificStatistics() + "Iterations: " + iterations + "\nRestarts: " + restarts + "\n"

  def getIterationResult = iterationResult
}
