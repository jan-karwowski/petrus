package pl.edu.pw.mini.jk.sg.mixeduct.transfer.reduction

import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

final case class VisitCounterDiscountingFloor(coeff: Double) extends StatisticsReduction {
  override def apply(stats: UCTDecisionStatistics): UCTDecisionStatistics =
    new UCTDecisionStatistics(
      Math.max(1,(stats.visitsCount*coeff).floor.toInt),
      stats.realVisitsCount, stats.averagePayoff)
}
