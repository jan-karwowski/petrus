package pl.edu.pw.mini.jk.sg.mixeduct.transfer.strategyfilter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.OptionalDouble;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class BestGapStrategyFilter<M> implements StrategyFilter<M> {
    private final double bestPortion;

    public BestGapStrategyFilter(double bestPortion) {
        super();
        Preconditions.checkArgument(bestPortion > 0 && bestPortion <= 1);
        this.bestPortion = bestPortion;
    }

    @Override
    public ImmutableMap<ImmutableList<M>, Double> filterStrategy(Map<ImmutableList<M>, Double> strategy) {
        OptionalDouble x = strategy.values().stream().mapToDouble(i -> i).max();
	if(x.isPresent()) {
	    double bestProb = x.getAsDouble();
	    return ImmutableMap.copyOf(strategy.entrySet().stream().filter(e -> e.getValue() >= bestPortion * bestProb)
				       .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
	} else {
	    return ImmutableMap.of();
	}
    }
}
