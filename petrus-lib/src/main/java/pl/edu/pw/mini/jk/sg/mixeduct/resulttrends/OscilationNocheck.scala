package pl.edu.pw.mini.jk.sg.mixeduct.resulttrends


class OscilationNocheck extends Oscilation {
  override def registerResult(result: Double) = Unit
  override def isOscilating(iteration: Int) : Boolean = false
}