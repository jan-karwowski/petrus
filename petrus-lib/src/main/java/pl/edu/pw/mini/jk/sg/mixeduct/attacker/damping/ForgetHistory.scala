package pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping


final object ForgetHistory extends WeightDamping {
  override type GlobalStat = Unit
  def calcGlobalStat(allWeights: Seq[Double]): GlobalStat = Unit
  def modifyWeight(stat: Unit)(weight: Double): Double = 0
}
