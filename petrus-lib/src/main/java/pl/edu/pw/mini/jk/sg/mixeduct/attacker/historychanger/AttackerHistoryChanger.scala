package pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import scala.collection.immutable.Map


trait AttackerHistoryChanger {
  def newHistoryLength(initialHist: Int, currHist: Int, maxHist: Int, iteration: Int, oscillating: Boolean) : Int 
}

class AttackerHistoryDecode(decodes: Map[String, DecodeJson[_ <: AttackerHistoryChanger]]) extends TraitJsonDecoder[AttackerHistoryChanger](decodes)

object AttackerHistoryDecode {
  val defaultDecoder = new AttackerHistoryDecode(Map(
    "constant" -> ConstantAttackerHistory.decoder,
    "reducing" -> ReducingAttackerHistory.decoder,
    "oscillationExtending" -> OscilationExtendingHistoryChanger.decoder
  )).decoder
}
