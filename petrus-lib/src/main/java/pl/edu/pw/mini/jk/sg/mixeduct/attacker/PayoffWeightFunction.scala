package pl.edu.pw.mini.jk.sg.mixeduct.attacker

trait PayoffWeightFunction {
  def apply(payoff: Double, maxPayoff: Double): Double
}
