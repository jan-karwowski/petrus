package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder


object StrategyChooserFactoryDecode {
  def decoder(decoders: Map[String, DecodeJson[_ <: StrategyChooserFactory]]) : DecodeJson[StrategyChooserFactory] =
    TraitJsonDecoder[StrategyChooserFactory](decoders).decoder

  implicit val bseDecoder : DecodeJson[BestStrategyEverFactory] = DecodeJson.UnitDecodeJson.map(x => new BestStrategyEverFactory())
  
  implicit val defaultDecoder = decoder(Map("bestStrategyEver" -> bseDecoder))
}
