package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse

import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.lib.CollectionHelpers
import scala.collection.JavaConverters._
import scala.Vector


//java interop
class OptimalAttacker2


object OptimalAttacker2 {

  def moveSequenceHelper[AM](
    gameView : ClonableDefenderGameView[AM, _, _, _],
    payoffMap : java.util.Map[ImmutableList[AM], ExpectedPayoff],
        roundLimit: Int,
    multiplier: Double
  ) : Unit =
    moveSequenceHelper(gameView, payoffMap, Vector(), roundLimit, multiplier)


  def moveSequenceHelper[AM](
    gameView : ClonableDefenderGameView[AM, _, _, _],
    payoffMap : java.util.Map[ImmutableList[AM], ExpectedPayoff],
    moveSequence : Vector[AM],
    roundLimit: Int,
    multiplier: Double
  ) : Unit =
    if(roundLimit == gameView.getTimeStep) {
      val ilms = CollectionHelpers.collectionToIL(moveSequence.asJava)
      payoffMap.getOrDefault(ilms, ExpectedPayoff(0, 0)) match {
        case ExpectedPayoff(attackerP, defenderP) =>
          payoffMap.put(ilms, ExpectedPayoff(attackerP+multiplier*gameView.getAttackerPayoff, defenderP+ multiplier* gameView.getDefenderPayoff))
      }
    }
    else
      gameView.possibleMoves().asScala.foreach { m => 
        val gameClone = gameView.typedClone()
        gameClone.makeMove(m)
        moveSequenceHelper(gameClone, payoffMap, moveSequence :+ m, roundLimit, multiplier)
      }
}

