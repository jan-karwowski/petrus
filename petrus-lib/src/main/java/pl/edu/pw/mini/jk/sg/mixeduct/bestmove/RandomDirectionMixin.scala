package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.random.RandomDataGenerator

trait RandomDirectionMixin[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
] {
  this: AbstractBestImprove[DM, AM, DS, AS, G] =>

  
  def randomDirection(
      step: Double
  )(implicit rng: RandomGenerator): Map[ImmutableList[DM], Double] = {
    val size = rng.nextInt(gameMatrices.getDefenderSequences().size()) + 1
    val sample = (new RandomDataGenerator(rng))
      .nextSample(gameMatrices.getDefenderSequences(), size)
      .view
      .map(_.asInstanceOf[ImmutableList[DM]])
      .map(str => (str, rng.nextDouble()))
      .toMap

    if (sample.values.sum > 0)
      RandomDirectionMixin.normalize(sample, step)
    else
      randomDirection(step)
  }

  def randomIteration(count: Int, step: Double)(
      implicit rng: RandomGenerator
  ): Stream[Map[ImmutableList[DM], Double]] = Stream.tabulate(count)(_ => randomDirection(step))

  def findGood(
      currPayoff: Double
  )(stream: Stream[Map[ImmutableList[DM], Double]]): Option[Map[ImmutableList[DM], Double]] =
    stream.find(dir => isImprovementDirection(currPayoff, dir))

}


object RandomDirectionMixin {
  def normalize[T](map: Map[T, Double], len: Double): Map[T, Double] = {
    val ll = len / Math.sqrt(map.values.view.map(x => x * x).sum)
    map.mapValues(_ * ll)
  }
}
