package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult


trait StopCondition[DM <: DefenderMove, AM] {
  def isStopCondition : Boolean
  def registerTrainingResult(result: IterationResult[DM, AM, _, _, _]) : Unit
}

