package pl.edu.pw.mini.jk.lib;

import java.util.function.Supplier;

import org.slf4j.Logger;

public class Logging {	
	public static void logIfDebug(Logger logger, Supplier<String> message) {
		if (logger.isDebugEnabled()) {
			logger.debug(message.get());
		}
	}
}
