package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoinerFunctions }

case object GlobalMeanCountJoin extends TreeJoinerFunctions {
  private val quantizationLevel = 100000

  override type CS = Unit
  override type GS = Double

  override def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalMean : Double) : UCTDecisionStatistics = {
    val q = stats.count(_.averagePayoff >= globalMean).toDouble/treeCount.toDouble
    new UCTDecisionStatistics((q * quantizationLevel).toInt, (q * quantizationLevel).toInt, q)
}
  override def calculateContextStats(stats: => Iterable[UctReadableStatistics]) : Unit = Unit
  override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) : Double =  stats.map(_.averagePayoff).sum/stats.size  

}
