package pl.edu.pw.mini.jk.sg.mixeduct.attacker;

import com.google.common.collect.ImmutableList
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.random.RandomGenerator


class EqualProbableStrategyJoin[AM](
  distributions: ImmutableList[EnumeratedDistribution[ImmutableList[AM]]],
  rng: RandomGenerator) extends AttackerStrategy[AM] {

  override def sample() = distributions.get(rng.nextInt(distributions.size())).sample() 
  override def getPmf = ProbabilisticStrategyAttackerWithHistoryFactory.joinDistributions(distributions, 1, rng).getPmf()
}
