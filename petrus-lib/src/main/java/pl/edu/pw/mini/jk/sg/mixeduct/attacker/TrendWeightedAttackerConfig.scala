package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator

import scala.collection.JavaConverters._
import scala.collection.mutable

final case class TrendWeightedAttackerConfig(
    baseWeight: Double,
    damping: Double,
    cutoff: Double,
    weightBias: Double
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[
        org.apache.commons.math3.util.Pair[ImmutableList[AM], java.lang.Double]
      ]
  ): AttackerWithHistory[AM] = new TrendWeightedHistoryAttacker[AM](
    baseWeight,
    damping,
    cutoff,
    weightBias,
    mutable.HashMap(initialStrategy.asScala.toSeq.map(p => (p.getFirst, p.getSecond.toDouble)): _*),
    rng
  )
}
