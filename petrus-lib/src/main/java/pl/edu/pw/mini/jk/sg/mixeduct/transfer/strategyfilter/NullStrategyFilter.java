package pl.edu.pw.mini.jk.sg.mixeduct.transfer.strategyfilter;

import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class NullStrategyFilter<M> implements StrategyFilter<M> {

    @Override
    public ImmutableMap<ImmutableList<M>, Double> filterStrategy(Map<ImmutableList<M>, Double> strategy) {
        return ImmutableMap.copyOf(strategy);
    }

}
