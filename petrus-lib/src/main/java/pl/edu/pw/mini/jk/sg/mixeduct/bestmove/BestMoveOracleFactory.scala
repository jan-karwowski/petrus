package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.{DefenderMove, SecurityGame}
import pl.edu.pw.mini.jk.sg.game.common.advanceable.{
  AdvanceableAttackerState,
  AdvanceableDefenderState
}
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracleFactory
import pl.edu.pw.mini.jk.sg.mixeduct.TreeLogConfig
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracle

object BestMoveOracleFactory extends DefenderOracleFactory {
  def apply[DM <: DefenderMove, AM, DS <: AdvanceableDefenderState[DM], AS <: AdvanceableAttackerState[
    AM
  ], G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
    G
  ]](
      gameMatrices: GameMatrices[AM, DM, AS, DS, G],
      treeLogConfig: TreeLogConfig
  ): DefenderOracle[DM, AM, DS, AS, G] = new BestMoveOracle[DM, AM, DS, AS, G](gameMatrices)
}
