package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.PayoffWeightFunction

object ExponentialWeight extends PayoffWeightFunction {
  override def apply(payoff: Double, maxPayoff: Double): Double = Math.exp(payoff-maxPayoff)
}
