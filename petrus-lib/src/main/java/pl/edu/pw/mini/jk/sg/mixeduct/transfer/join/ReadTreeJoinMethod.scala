package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.JsonHelpers
import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder

object ReadTreeJoinMethod {
  implicit val decodeFunctions : DecodeJson[TreeJoinerFunctions] =
    JsonHelpers.readOneFromSet[TreeJoinerFunctions]{
      case "VISIT_WEIGHTED_JOIN" => VisitWeightedJoin
      case "MEDIAN_JOIN" => MedianJoin
      case "MEAN_JOIN" => AveragePayoffJoin
      case "COUNT_QUALITY_JOIN" => CountQualityJoin
      case "POSITIVE_COUNT_QUALITY_JOIN" => PositiveCountQualityJoin
      case "PURE_COUNT_JOIN" => PureCountJoin
      case "MEAN_COUNT_JOIN" => MeanCountJoin
      case "GLOBAL_MEAN_COUNT_JOIN" => GlobalMeanCountJoin
      case "MAX_Q_JOIN" => MaxPayoffJoin
      case "SUM_Q_JOIN" => SumQJoiner
      case "SUM_Q_JOIN_FILL_MIN" => GlobalMinFillSumJoin
      case "SUM_Q_DIVIDED_JOIN" => SumQDividedJoiner
    } ||| (TraitJsonDecoder[TreeJoinerFunctions](Map(
      "onlyMostVisited" -> DecodeJson.jdecode1L(OnlyMostVisitedJoiner.apply)("portion"),
      "onlyVisitPercentage" -> DecodeJson.jdecode1L(OnlyVisitPercentageJoiner.apply)("percentage")
    )).decoder)
}

