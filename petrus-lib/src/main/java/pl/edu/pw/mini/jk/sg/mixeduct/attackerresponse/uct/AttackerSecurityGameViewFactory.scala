package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.uct

import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import scala.language.existentials
import com.google.common.collect.ImmutableList
import org.apache.commons.collections4.Factory
import org.apache.commons.math3.distribution.EnumeratedDistribution
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame

case class AttackerSecurityGameViewFactory[AM, AS <: AdvanceableAttackerState[AM], DM <: DefenderMove] (
  factory: NormalizableFactory[AM, DM, _ <: DefenderAttackerGame[AM, DM, AS, _, _]],
  defenderStrategy: EnumeratedDistribution[ImmutableList[DM]]
) extends Factory[AttackerSecurityGameView[AM,AS,DM]] {
  override def create = new AttackerSecurityGameView[AM,AS,DM](factory.create, defenderStrategy.sample())
}
