package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join
import pl.edu.pw.mini.jk.sg.uct.{UCTDecisionStatistics, UctReadableStatistics}

object MaxPayoffJoin extends SimpleJoiner {
  override def joinStats(
    stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit
  ): UCTDecisionStatistics = new UCTDecisionStatistics(1, 1, stats.view.map(_.averagePayoff).max)
}
