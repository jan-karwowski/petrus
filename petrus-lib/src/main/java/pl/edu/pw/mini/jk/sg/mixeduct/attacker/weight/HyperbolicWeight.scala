package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.PayoffWeightFunction

object HyperbolicWeight extends PayoffWeightFunction {
  override def apply(payoff: Double, maxPayoff: Double): Double = 1/(1+maxPayoff-payoff)
}
