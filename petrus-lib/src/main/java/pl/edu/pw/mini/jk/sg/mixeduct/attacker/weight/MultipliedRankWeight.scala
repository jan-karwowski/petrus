package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight

object MultipliedRankWeight extends RankPayoffWeightFunction {
  override def apply(payoff: Double, maxPayoff: Double, minPayoff: Double, rank: Int): Double =
    1/Math.pow(2,rank) * (if(maxPayoff - minPayoff > 0) (payoff-minPayoff) else 1)
}
