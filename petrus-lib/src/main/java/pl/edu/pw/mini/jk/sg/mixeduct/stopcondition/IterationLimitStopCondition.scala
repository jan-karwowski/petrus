package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult



class IterationLimitStopCondition[DM <: DefenderMove, DS](iterations: Int) extends StopCondition[DM, DS] {
  var iterationCount=0

  def isStopCondition: Boolean = iterationCount > iterations
  def registerTrainingResult(result: IterationResult[DM, DS, _, _, _]): Unit =
    iterationCount = iterationCount + 1
}

