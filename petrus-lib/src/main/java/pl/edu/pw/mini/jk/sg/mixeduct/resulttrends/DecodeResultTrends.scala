package pl.edu.pw.mini.jk.sg.mixeduct.resulttrends

import argonaut.DecodeJson


object DecodeResultTrends {
  val decode : DecodeJson[ResultTrendsConfig] = DecodeJson.jdecode4L(
    (x2: Int, x3: Int, x4: Int, x5: Double) => new ResultTrendsConfig(x2, x3, x4, x5))("oscilateCheckIterations","oscilateCheckSmoothSteps","oscilateCheckFirstIteration", "oscilateCheckThreshold")
}
