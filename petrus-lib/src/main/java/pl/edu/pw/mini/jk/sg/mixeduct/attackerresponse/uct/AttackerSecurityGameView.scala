package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.uct

import java.util.List
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.game.common.{Accident, DefenderMove, SecurityGame}
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.uct.SinglePlayerGame
import pl.edu.pw.mini.jk.sg.uct.PayoffInfo
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame

class AttackerSecurityGameView[AM, AS <: AdvanceableAttackerState[AM], DM <: DefenderMove]
  (game : DefenderAttackerGame[AM, DM, AS, _, _] with SecurityGame, defenderStrategy : ImmutableList[DM])
    extends SinglePlayerGame[AM, AS] with SecurityGame{
  private var moveNumber = 0;

  override def getCurrentState: AS = game.getAttackerState

  override def possibleMoves: List[AM] = game.getAttackerState.getPossibleMoves

  override def makeMove(move: AM) = {
    game.playDefenderMove(defenderStrategy.get(moveNumber));
    game.playAttackerMove(move);
    moveNumber=moveNumber+1;
    val po = game.finishRound()
    PayoffInfo(po.attackerPayoff, po.catchAccidentsCount, po.attackAccidentsCount)
  }

  override def randomMove(rng: RandomGenerator) = game.randomAttackerMove(rng)

  override def getTimeStep: Int = game.getTimeStep
  override def getPreviousAccidents: List[Accident] = game.getPreviousAccidents
  override def accidentOccured : Boolean = game.accidentOccured
//  override def getTargetCount: Int = game.getTargetCount
}
