package pl.edu.pw.mini.jk.sg.mixeduct.transfer

import java.util.HashMap
import java.util.Iterator
import java.util.Map
import java.util.function.BiFunction
import java.util.stream.Stream

import org.apache.commons.collections4.queue.CircularFifoQueue
import org.apache.commons.math3.random.RandomGenerator
import com.google.common.collect.ImmutableList

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.mixeduct.util.TreeUtils
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import java.{util => ju}
import java.util.function.Consumer

import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.lib.Logging

final  class BestMoveFrequencyProbability[
  DM <: DefenderMove, DS <: AdvanceableDefenderState[DM]
](
  roundLimit: Int,
  historyLength: Int
) extends  AttackersInformationTransfer[DM,DS] {
  private val logger = org.log4s.getLogger
  private var lastTree: UctNode[DS, DM] = _
  private val bestHistory = new CircularFifoQueue[ImmutableList[DM]](historyLength)

  override def newAttackerTree(oldTree: UctNode[DS,DM], rng: RandomGenerator): UctNode[DS,DM] = {
    lastTree = oldTree
    val bestMove = TreeUtils.getBestPathInTreeExtendedJava(oldTree, roundLimit, rng)
    logger.debug(s"Best path found: ${bestMove.asScala.toList}")
    bestHistory.add(bestMove)
    oldTree.createEmptyTree
  }

  override def getDefenderMoveProbabilities(rng: RandomGenerator, tlc: Consumer[_ >: pl.edu.pw.mini.jk.sg.uct.tree.UctNode[_ <: DS, _ <: DM]]): ju.Map[ImmutableList[DM],java.lang.Double] = {
    tlc.accept(lastTree)
    val output = new java.util.HashMap[ImmutableList[DM], java.lang.Double]()

    bestHistory.asScala.foreach(m => output.put(m, output.getOrDefault(m, 0.0)+1))
    output
  }
}
