package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;

public class BestStrategyEverFactory implements StrategyChooserFactory {
    @Override
    public <DM extends DefenderMove, AM> StrategyChooser<DM, AM> create(GameMatrices<AM, DM, ?, ?, ?>  gameMatrices){
	return new BestStrategyEver<>();
    }
}

