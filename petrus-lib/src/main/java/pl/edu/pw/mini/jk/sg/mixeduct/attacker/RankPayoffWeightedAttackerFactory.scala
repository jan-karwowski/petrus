package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator

final case class RankPayoffWeightedAttackerFactory(
  historyLength: Int,
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
    rng: RandomGenerator,
    initialStrategy: ImmutableList[org.apache.commons.math3.util.Pair[ImmutableList[AM],java.lang.Double]]
  ): AttackerWithHistory[AM] = new RankPayoffWeightedAttacker[AM](initialStrategy, historyLength, rng)
}
