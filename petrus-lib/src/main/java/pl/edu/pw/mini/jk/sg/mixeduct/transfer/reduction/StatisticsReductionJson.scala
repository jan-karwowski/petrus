package pl.edu.pw.mini.jk.sg.mixeduct.transfer.reduction

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder

object StatisticsReductionJson {
  val decodeVisitsCounterDiscounting : DecodeJson[VisitCounterDiscounting] =
    DecodeJson.jdecode1L(VisitCounterDiscounting.apply)("alpha")

  val decodeVisitsCounterDiscountingFloor : DecodeJson[VisitCounterDiscountingFloor] =
    DecodeJson.jdecode1L(VisitCounterDiscountingFloor.apply)("alpha")

  val defaultDecode : DecodeJson[StatisticsReduction] = TraitJsonDecoder[StatisticsReduction](Map(
    "visitsCounterDiscounting" -> decodeVisitsCounterDiscounting,
    "visitsCounterDiscountingFloor" -> decodeVisitsCounterDiscountingFloor
  )).decoder
}
