package pl.edu.pw.mini.jk.sg.opt.util

class GamePayoff (defenderPayoff: Double, attackerPayoff: Double) {
  def getAttackerPayoff : Double = attackerPayoff
  def getDefenderPayoff : Double = defenderPayoff
}
