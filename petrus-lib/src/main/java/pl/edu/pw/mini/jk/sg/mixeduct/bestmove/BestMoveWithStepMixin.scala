package pl.edu.pw.mini.jk.sg.mixeduct.bestmove

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import _root_.pl.edu.pw.mini.jk.sg.game.common.Cloneable
import com.google.common.collect.ImmutableMap
import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker

trait BestMoveWithStepMixin[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
] {
  this: AbstractBestMoveOracle[DM, AM, DS, AS, G] =>

  protected def bestMoveWithStep(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      currPayoff: Double
  )(step: Double) = {
    logger.debug(s"Trying step ${step}, currP: ${currPayoff}")
    heuristicMoveOrder(attStr)
      .find(move => {
        val ds = bumpMove(move, step)
        val bestResponse = OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, ds)
        val payoff =
          gameMatrices.getExpectedPayoff(ds, AbstractBestImprove.strategyListToMap(bestResponse))
        payoff.defenderPayoff > currPayoff
      })
      .map(m => Map(m -> step))
  }

}
