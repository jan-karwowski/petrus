package pl.edu.pw.mini.jk.lib;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public final class CollectionHelpers {
	private CollectionHelpers() {}

	public static <K, L> ImmutableMap<K, L> pairsToMap(List<Pair<K, L>> list) {
		return ImmutableMap.copyOf(list.stream().collect(Collectors.toMap(Pair::getKey, Pair::getValue)));
	}

    public static <K, L> ImmutableList<Pair<K, L>> mapToPairs(Map<K, L> map) {
	return ImmutableList.copyOf(map.entrySet().stream().map(e -> new Pair<>(e.getKey(), e.getValue())).iterator());
    }

	public static <T, M> Map<Integer, T> sequenceToIntegerMap(final Map<ImmutableList<M>, T> map,
			List<ImmutableList<M>> seq) {
		return map.entrySet().stream().collect(Collectors.toMap(e -> seq.indexOf(e.getKey()), e -> e.getValue()));
	}
	

    public static <T> ImmutableList<T> collectionToIL(final Collection<T> col) {
	return ImmutableList.copyOf(col);
    }
    
}
