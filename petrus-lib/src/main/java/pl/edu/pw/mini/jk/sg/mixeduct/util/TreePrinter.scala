package pl.edu.pw.mini.jk.sg.mixeduct.util

import pl.edu.pw.mini.jk.sg.uct.tree.{ UctNode, UnmodifiableDecisionStatisticsView }
import scala.collection.JavaConverters._

class TreePrinter()

object TreePrinter {
  def stringify[S, M](tree: UctNode[S, M]) : String = {
    val stats = tree.getStatistics.asScala.map(stat => {
      val succ = stringify(tree.getSuccessor(stat.move).get)
      s"${stat.move}: ${toStr(stat.statistics)}, ${succ}"
    }).toList
    
    s"(${tree.getState}: ${stats})"
  }

  def toStr(stat: UnmodifiableDecisionStatisticsView) : String =
    s"${stat.visitsCount}, ${stat.realVisitsCount}, ${stat.averagePayoff}"
}
