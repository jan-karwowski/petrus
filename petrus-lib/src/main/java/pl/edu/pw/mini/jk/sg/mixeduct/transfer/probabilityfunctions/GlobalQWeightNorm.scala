package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

import scala.collection.JavaConverters._

object GlobalQWeightNorm extends NodeProbabilityWeightFunction {
  override type GlobalStat = Double
  override def calculateGlobalStat(stats: => Seq[UnmodifiableDecisionStatisticsView]): Double =
    stats.map(_.averagePayoff).min
  override def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _], stat: Double): Double = {
    val sum = node.getStatistics.asScala.view.map(_.statistics.averagePayoff - stat).sum
    if(sum == 0) 1/node.getStatistics.size()
    else (statistics.averagePayoff-stat)/sum
  }

  override val leafOnlyWeight = false
}
