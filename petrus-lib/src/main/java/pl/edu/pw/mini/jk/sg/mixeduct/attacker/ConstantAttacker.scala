package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import com.google.common.collect.ImmutableList

import scala.collection.JavaConverters._
import _root_.org.apache.commons.math3.util.{Pair => ACPair}
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.distribution.EnumeratedDistribution
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult

final class ConstantAttacker[M](
    initialStrategy: ImmutableList[ACPair[ImmutableList[M], java.lang.Double]],
    rng: RandomGenerator
) extends AttackerWithHistory[M] {

  override def getInstance(): AttackerStrategy[M] =
    new SimpleAttackerStrategy[M](
      new EnumeratedDistribution(
        rng,
        initialStrategy
      )
    )

  override def registerIterationResult[DM <: DefenderMove](
      iteration: Int,
      iterationResult: IterationResult[DM, M, _, _, _],
      trainingAttacker: AttackerStrategy[M]
  ): Unit = {}
}
