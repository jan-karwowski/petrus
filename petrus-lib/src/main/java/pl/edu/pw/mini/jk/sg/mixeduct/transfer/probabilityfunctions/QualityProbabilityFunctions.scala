package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions

import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView

import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

object QualityProbabilityFunctions {
  val expQuality: NodeProbabilityWeightFunction = SimpleNodeProbabilityWeightFunction((move, parent) => Math.exp(move.averagePayoff))

  def linearQuality(lowerBound: Double): NodeProbabilityWeightFunction =
    SimpleNodeProbabilityWeightFunction((move, parent) => {
      val min = parent.getStatistics.asScala.view.map(_.statistics.averagePayoff).min
      val max = parent.getStatistics.asScala.view.map(_.statistics.averagePayoff).max
      if(min!=max)lowerBound + (move.averagePayoff - min) / (max - min)
      else 1
    })

  def linearQualityNotNorm(lowerBound: Double): NodeProbabilityWeightFunction =
    SimpleNodeProbabilityWeightFunction((move, parent) => {
      val min = parent.getStatistics.asScala.view.map(_.statistics.averagePayoff).min
      val max = parent.getStatistics.asScala.view.map(_.statistics.averagePayoff).max
      if(min!=max)lowerBound + (move.averagePayoff - min)
      else 1
    })

  def sigmoidQuality : NodeProbabilityWeightFunction = SimpleNodeProbabilityWeightFunction((move, parent) => 0.5 * (Math.tanh(move.averagePayoff) + 1))
}
