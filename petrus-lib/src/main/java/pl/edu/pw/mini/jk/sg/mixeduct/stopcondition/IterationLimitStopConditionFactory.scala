package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove


case class IterationLimitStopConditionFactory(iterationLimit: Int) extends StopConditionFactory {
  def create[DM <: DefenderMove, AM] = new IterationLimitStopCondition(iterationLimit)
}

