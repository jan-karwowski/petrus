package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import org.apache.commons.collections4.queue.CircularFifoQueue
import org.apache.commons.math3.distribution.EnumeratedDistribution
import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult
import org.apache.commons.math3.util.Pair
import java.{lang => jl}
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger.AttackerHistoryChanger

final class DiscountingHistoryAttackerFactory[M](
    initialStrategy: ImmutableList[Pair[ImmutableList[M], jl.Double]],
    historyLength: Int,
    rng: RandomGenerator,
    ahc: AttackerHistoryChanger,
    discountingCoefficient: Double
) extends ProbabilisticStrategyAttackerWithHistoryFactory[M](
      initialStrategy,
      historyLength,
      rng,
      ahc
    ) {
  override def getInstance(): AttackerStrategy[M] =
    new DiscountingStrategyJoin(discountingCoefficient, ImmutableList.copyOf(history.iterator), rng)
}
