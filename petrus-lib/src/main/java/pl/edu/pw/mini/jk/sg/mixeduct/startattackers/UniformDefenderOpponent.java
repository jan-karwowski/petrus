package pl.edu.pw.mini.jk.sg.mixeduct.startattackers;

import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.ImmutableList;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker;

public class UniformDefenderOpponent implements AttackerProbabilityBuilder {

    @Override
    public <DM extends DefenderMove, AM> ImmutableList<Pair<ImmutableList<AM>, Double>> buildProbabilities(
            GameMatrices<AM, DM, ?, ?, ?> gameMatrices) {
        return ImmutableList.copyOf(OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices,
										     gameMatrices.getDefenderSequences().stream().collect(Collectors.toMap(i -> i, i -> 1.))));
    }

}
