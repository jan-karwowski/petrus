package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import org.apache.commons.math3.distribution.EnumeratedDistribution
import com.google.common.collect.ImmutableList
import java.{util => ju}

final class SimpleAttackerStrategy[M](
  distribution: EnumeratedDistribution[ImmutableList[M]]
) extends AttackerStrategy[M] {
  override def sample(): ImmutableList[M] = distribution.sample()
  override def getPmf(): ju.List[org.apache.commons.math3.util.Pair[ImmutableList[M],java.lang.Double]] = distribution.getPmf()
}
