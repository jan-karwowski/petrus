package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove



class CombinedMixedUctStopCondition[DM <: DefenderMove, DS](
  conditions: List[StopCondition[DM, DS]]) extends StopCondition[DM, DS]{

  def isStopCondition: Boolean = conditions.exists(_.isStopCondition)
  def registerTrainingResult(result: pl.edu.pw.mini.jk.sg.mixeduct.IterationResult[DM, DS, _, _, _]): Unit = conditions.foreach(_.registerTrainingResult(result))
}
