package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.RankPayoffWeightFunction
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping.WeightDamping

final case class PayoffRankWeightedAttackerFactory(
    historyLength: Int,
    rankPayoffWeightFunction: RankPayoffWeightFunction,
    weightDamping: WeightDamping
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[
        org.apache.commons.math3.util.Pair[ImmutableList[AM], java.lang.Double]
      ]
  ): AttackerWithHistory[AM] = new PayoffRankWeightedAttacker[AM](
    initialStrategy,
    historyLength,
    rankPayoffWeightFunction,
    weightDamping,
    rng
  )
}
