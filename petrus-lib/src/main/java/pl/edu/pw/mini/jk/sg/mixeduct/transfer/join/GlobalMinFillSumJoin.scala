package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{UCTDecisionStatistics, UctReadableStatistics}
import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions

object GlobalMinFillSumJoin extends TreeJoinerFunctions {
  override type GS = Double
  override type CS = Unit

  override def joinStats(
      stats: Iterable[UctReadableStatistics],
      treeCount: Int,
      localStats: Unit,
      globalStats: Double
  ): UCTDecisionStatistics = {
    val ct = stats.size
    new UCTDecisionStatistics(
      1,
      1,
      ((treeCount - stats.size) * globalStats + stats.view.map(_.averagePayoff).sum) / treeCount
    )
  }

  override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]): Double =
    stats.view.map(_.averagePayoff).min

  override def calculateContextStats(stats: => Iterable[UctReadableStatistics]): CS = ()
}
