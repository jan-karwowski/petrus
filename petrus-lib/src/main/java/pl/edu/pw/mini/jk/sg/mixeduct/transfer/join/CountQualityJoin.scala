package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.tree.{TreeJoinerFunctions }
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

object CountQualityJoin extends TreeJoinerFunctions {
  override type CS = Double
  override type GS = Unit

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, maxQ: Double, globalStats: Unit) : UCTDecisionStatistics = 
    new UCTDecisionStatistics(
      stats.map(_.visitsCount).sum/treeCount,
      stats.map(_.realVisitsCount).sum/treeCount,
      stats.map(_.averagePayoff).sum/treeCount + maxQ * stats.size / treeCount
    )
  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) : Double = stats.map(_.averagePayoff).max 
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 

}
