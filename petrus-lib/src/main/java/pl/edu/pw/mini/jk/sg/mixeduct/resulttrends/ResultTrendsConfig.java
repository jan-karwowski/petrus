package pl.edu.pw.mini.jk.sg.mixeduct.resulttrends;

public class ResultTrendsConfig {
    private final int oscilateCheckIterations;
    private final int oscilateCheckSmoothSteps;
    private final int oscilateCheckFirstIteration;
    private final double oscilateCheckThreshold;

    public ResultTrendsConfig(
            int oscilateCheckIterations,
            int oscilateCheckSmoothSteps,
            int oscilateCheckFirstIteration,
            double oscilateCheckThreshold) {
        super();
        this.oscilateCheckIterations = oscilateCheckIterations;
        this.oscilateCheckSmoothSteps = oscilateCheckSmoothSteps;
        this.oscilateCheckFirstIteration = oscilateCheckFirstIteration;
        this.oscilateCheckThreshold = oscilateCheckThreshold;
    }

    public ResultTrends createResultTrends() {
        return new ResultTrends(oscilateCheckIterations, oscilateCheckSmoothSteps,
                oscilateCheckFirstIteration, oscilateCheckThreshold);
    }

}
