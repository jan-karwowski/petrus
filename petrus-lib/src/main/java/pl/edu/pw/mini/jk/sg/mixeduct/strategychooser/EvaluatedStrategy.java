package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;

public class EvaluatedStrategy<DM extends DefenderMove> implements Comparable<EvaluatedStrategy<DM>> {
    private final ImmutableMap<ImmutableList<DM>, Double> defenderStrategy;
    private final double expectedPayoff;
    
    public EvaluatedStrategy(ImmutableMap<ImmutableList<DM>, Double> defenderStrategy, double expectedPayoff) {
        super();
        this.defenderStrategy = defenderStrategy;
        this.expectedPayoff = expectedPayoff;
    }

    @Override
    public int compareTo(EvaluatedStrategy<DM> o) {
        return Double.compare(expectedPayoff, o.expectedPayoff);
    }
    
    public ImmutableMap<ImmutableList<DM>, Double> getDefenderStrategy() {
        return defenderStrategy;
    }
    
    public double getExpectedPayoff() {
        return expectedPayoff;
    }
}
