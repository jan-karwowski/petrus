package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import org.apache.commons.math3.random.RandomGenerator
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomDataGenerator

trait LimitedRandomDirectionMixin[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
] {
  this: AbstractBestImprove[DM, AM, DS, AS, G] =>

  def limitedRandomDirection(
      step: Double,
      directionCount: Int
  )(implicit rng: RandomGenerator): Map[ImmutableList[DM], Double] = {
    val ct = 1+rng.nextInt(Math.min(gameMatrices.getDefenderSequences.size, directionCount))
    val sample = (new RandomDataGenerator(rng))
      .nextSample(
        gameMatrices.getDefenderSequences(), ct
      )
      .view
      .map(_.asInstanceOf[ImmutableList[DM]])
      .map(str => (str, rng.nextDouble()))
      .toMap

    if (sample.values.sum > 0)
      RandomDirectionMixin.normalize(sample, step)
    else
      limitedRandomDirection(step, directionCount)
  }
}
