package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoinerFunctions }


case object AveragePayoffJoin extends TreeJoinerFunctions {
  override type CS = Unit
  override type GS = Unit

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit) : UCTDecisionStatistics = 
    new UCTDecisionStatistics(
      stats.map(_.visitsCount).sum/treeCount,
      stats.map(_.realVisitsCount).sum/treeCount,
      stats.map(_.averagePayoff).sum/treeCount
    )

  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) = Unit
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 

}
