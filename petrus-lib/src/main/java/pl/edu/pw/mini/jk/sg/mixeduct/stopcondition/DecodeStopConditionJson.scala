package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder



object DecodeStopConditionJson {
  val decodeILSC : DecodeJson[IterationLimitStopConditionFactory] =
    DecodeJson.jdecode1L(IterationLimitStopConditionFactory.apply)("iterations")

  val decodeNISC : DecodeJson[NoImprovementStopConditionFactory] =
    DecodeJson.jdecode1L(NoImprovementStopConditionFactory.apply)("iterations")

  val decodeRegularStopContitions : DecodeJson[StopConditionFactory] =
    TraitJsonDecoder[StopConditionFactory](Map("iterationLimit" -> decodeILSC, "noImprovement" -> decodeNISC)).decoder
}
