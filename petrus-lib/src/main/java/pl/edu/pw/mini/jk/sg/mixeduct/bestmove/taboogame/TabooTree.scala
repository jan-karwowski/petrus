package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState

import scala.collection.JavaConverters._

final class TabooTree[M](
    val tabooMoves: Map[M, (Boolean, TabooTree[M])]
) {
  def isBlocked(move: M) = tabooMoves.get(move).map(_._1).getOrElse(false)

  def next(move: M) = tabooMoves.get(move).map(_._2)

  def forbidMoveSequence(seq: List[M], state: AdvanceableState[M]): (TabooTree[M], Boolean) =
    seq match {
      case m :: rest => {
        tabooMoves.get(m) match {
          case None => {
            val (child, allBlocked) =
              new TabooTree[M](Map()).forbidMoveSequence(rest, state.advance(m))
            val newMoves = tabooMoves + ((m, (allBlocked, child)))
            (
              new TabooTree[M](newMoves),
              state
                .getPossibleMoves()
                .asScala
                .forall(m => newMoves.get(m).map(_._1).getOrElse((false)))
            )
          }
          case Some((true, _)) =>
            (
              this,
              state
                .getPossibleMoves()
                .asScala
                .forall(m => tabooMoves.get(m).map(_._1).getOrElse(false))
            )
          case Some((false, tree)) => {
            val (child, allBlocked) =
              tree.forbidMoveSequence(rest, state.advance(m))
            val newMoves = tabooMoves + ((m, (allBlocked, child)))
            (
              new TabooTree[M](newMoves),
              state
                .getPossibleMoves()
                .asScala
                .forall(m => newMoves.get(m).map(_._1).getOrElse((false)))
            )
          }
        }
      }
      case Nil => (this, state.getPossibleMoves().isEmpty())
    }
}
