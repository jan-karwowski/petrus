package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

trait SimpleNodeProbabilityWeightFunction extends NodeProbabilityWeightFunction {
  def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _]): Double

  override type GlobalStat = Unit
  override def calculateGlobalStat(stats: => Seq[UnmodifiableDecisionStatisticsView]) : Unit = ()
  override def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _], globStat: Unit) = probabilityWeight(statistics, node)

  override val leafOnlyWeight: Boolean = false
}


object SimpleNodeProbabilityWeightFunction {
  def apply(fun : (UnmodifiableDecisionStatisticsView, UctNode[_,_]) => Double) =
    new SimpleNodeProbabilityWeightFunction {
      override def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _]): Double =
        fun(statistics, node)
    }
}
