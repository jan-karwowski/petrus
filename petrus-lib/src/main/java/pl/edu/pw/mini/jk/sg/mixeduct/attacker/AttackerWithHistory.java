package pl.edu.pw.mini.jk.sg.mixeduct.attacker;

import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult;
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;

public interface AttackerWithHistory<M> {
    public AttackerStrategy<M> getInstance();
    public<DM extends DefenderMove> void registerIterationResult(final int iteration, final IterationResult<DM, M, ?, ?, ?> iterationResult, final AttackerStrategy<M> trainingAttacker);
}
