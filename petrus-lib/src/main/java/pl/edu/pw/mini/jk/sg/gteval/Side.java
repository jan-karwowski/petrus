package pl.edu.pw.mini.jk.sg.gteval;

import java.util.function.Function;

import pl.edu.pw.mini.jk.sg.opt.util.GamePayoff;

public enum Side {
	DEFENDER(GamePayoff::getDefenderPayoff), ATTACKER(GamePayoff::getAttackerPayoff);
	
	private Side(final Function<GamePayoff, Double> payoffExtractor) {
		this.payoffExtractor = payoffExtractor;
	}
	private final Function<GamePayoff, Double> payoffExtractor;
	public Function<GamePayoff, Double> getPayoffExtractor() {
		return payoffExtractor;
	}
}
