package pl.edu.pw.mini.jk.sg.mixeduct.transfer.transformers
import pl.edu.pw.mini.jk.sg.uct.tree.PayoffTransformer
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

object IdentityTransformer extends PayoffTransformer {
  override type GlobalStat = Unit
  override type NodeStat = Unit

  override def calculateNodeStat(node: UctNode[_, _]): Unit = ()
  override def calculateGlobalStat(stat: => Iterable[UnmodifiableDecisionStatisticsView]): Unit = ()
  override def transformStatistics(
      stat: UnmodifiableDecisionStatisticsView,
      nodeStat: Unit,
      globalStat: Unit
  ): UCTDecisionStatistics =
    new UCTDecisionStatistics(stat.visitsCount, stat.realVisitsCount, stat.averagePayoff)
}
