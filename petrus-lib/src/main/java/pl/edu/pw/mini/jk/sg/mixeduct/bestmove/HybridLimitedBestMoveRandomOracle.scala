package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import com.google.common.collect.{ImmutableList, ImmutableMap}
import org.apache.commons.math3.random.RandomGenerator

final class HybridLimitBestMoveRandomOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
](
    gameMatrices: GameMatrices[AM, DM, AS, DS, G],
    stepEps: Double,
    randomSamples: Int,
    dimensionLimit: Int
) extends AbstractBestMoveOracle[DM, AM, DS, AS, G](gameMatrices)
    with ReducingBestMoveStepMixin[DM, AM, DS, AS, G]
    with BestMoveWithStepMixin[DM, AM, DS, AS, G]
    with LimitedRandomDirectionMixin[DM, AM, DS, AS, G]
    with IterativeRandomMixin[DM, AM, DS, AS, G] {
  def bestMoveProcedure(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      sortedMoves: Seq[ImmutableList[DM]],
      currPayoff: Double
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] = {
    val currPayoff = payoff
    reducingStepSearch(1, stepEps)(currPayoff, bestMoveWithStep(attStr, currPayoff)).orElse {
      logger.debug("Fallback to random search!!")
      reducingStepSearch(1, stepEps)(
        currPayoff,
        step =>
          findGoodIteration(
            currPayoff,
            randomSamples,
            (rng => limitedRandomDirection(step, dimensionLimit)(rng))
          )
      )
    }
  }
}
