package pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger

import argonaut.DecodeJson


case class OscilationExtendingHistoryChanger(oscilationIncreaseRatio: Double)  extends AttackerHistoryChanger {
  override def newHistoryLength(initialHist: Int, currHist: Int, maxHist: Int, iteration: Int, oscillating: Boolean) : Int =
    if(oscillating && currHist == maxHist)
      (maxHist * oscilationIncreaseRatio).toInt
    else
      maxHist
}

object OscilationExtendingHistoryChanger {
  def decoder : DecodeJson[OscilationExtendingHistoryChanger] = DecodeJson(c => for {
    oir <- (c --\ "increaseRatio").as[Double]
  } yield(OscilationExtendingHistoryChanger(oir)))
}
