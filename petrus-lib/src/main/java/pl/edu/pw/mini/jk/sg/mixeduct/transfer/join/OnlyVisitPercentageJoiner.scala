package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join
import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions
import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics


final case class OnlyVisitPercentageJoiner(portion: Double) extends TreeJoinerFunctions {
  override type GS = Unit
  override type CS = Unit
  override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]): Unit = ()
  override def calculateContextStats(stats: => Iterable[UctReadableStatistics]): Unit = ()

  override def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit): UCTDecisionStatistics = {
    val sum = stats.map(_.visitsCount).sum
    stats.toVector.sortBy(_.visitsCount).foldRight(UCTDecisionStatistics.zeroStats())(
      (stat, output) =>
      if(output.visitsCount >= sum * portion) output
      else {output.appendOther(stat); output}
    )
  }


}
