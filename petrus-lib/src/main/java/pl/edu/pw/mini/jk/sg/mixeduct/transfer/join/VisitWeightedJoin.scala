package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions


case object VisitWeightedJoin extends TreeJoinerFunctions {
  override type CS = Unit
  override type GS = Unit

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit) : UCTDecisionStatistics = {
    val s = UCTDecisionStatistics.zeroStats()
    stats.foreach(s.appendOther(_))
    s
  }
  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) = Unit
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 
}
