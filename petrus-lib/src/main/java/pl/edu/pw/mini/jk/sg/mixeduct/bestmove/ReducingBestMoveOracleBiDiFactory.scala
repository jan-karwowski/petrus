package pl.edu.pw.mini.jk.sg.mixeduct.bestmove

import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracleFactory
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.mixeduct.{DefenderOracle, TreeLogConfig}

final case class ReducingBestMoveOracleBiDiFactory(stepEps: Double) extends DefenderOracleFactory {
  def apply[
      DM <: DefenderMove,
      AM,
      DS <: AdvanceableDefenderState[DM],
      AS <: AdvanceableAttackerState[AM],
      G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
  ](
      gameMatrices: GameMatrices[AM, DM, AS, DS, G],
      treeLogConfig: TreeLogConfig
  ): DefenderOracle[DM, AM, DS, AS, G] =
    new ReducingBestMoveOracleBiDi[DM, AM, DS, AS, G](stepEps, gameMatrices)
}
