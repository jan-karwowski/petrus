package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser;


import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;

public interface StrategyChooserFactory {
    public <DM extends DefenderMove, AM> StrategyChooser<DM, AM> create(GameMatrices<AM, DM, ?, ?, ?>  gameMatrices);
}
