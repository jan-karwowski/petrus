package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.{ DefenderMove }


case class NoImprovementStopConditionFactory(iterations: Int) extends StopConditionFactory {
  def create[DM <: DefenderMove, AM] = new NoImprovementStopCondition(iterations)
}

