package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.uct

import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.util.Pair
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode



trait TreeToStrategy {
  def treeToStrategy[AM, AS <: AdvanceableAttackerState[AM]](
    uctTree: UctNode[AS, AM],
    gameMatrices: GameMatrices[AM, _, AS, _, _],
    rng: RandomGenerator) : ImmutableList[Pair[ImmutableList[AM], java.lang.Double]]
}

