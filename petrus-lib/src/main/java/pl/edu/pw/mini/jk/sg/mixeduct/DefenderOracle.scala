package pl.edu.pw.mini.jk.sg.mixeduct
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import java.{util => ju}
import com.google.common.collect.ImmutableList

trait DefenderOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
] {
  def calculateDefenderStrategy(iteration: Int, attackerProbabilities: AttackerStrategy[AM])(
      implicit rng: RandomGenerator
  ): ju.Map[ImmutableList[DM], java.lang.Double]
}
