package pl.edu.pw.mini.jk.sg.mixeduct.transfer

import java.util.Map
import org.apache.commons.math3.random.RandomGenerator
import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

trait AttackersInformationTransfer[DM, S <: AdvanceableState[DM]] {
    def newAttackerTree(oldTree: UctNode[S,DM], rng: RandomGenerator ) : UctNode[S, DM]
  def getDefenderMoveProbabilities(
    rng: RandomGenerator,
    tlc: java.util.function.Consumer[_ >: UctNode[_ <: S,_ <: DM]]) : Map[ImmutableList[DM], java.lang.Double] ;
}
