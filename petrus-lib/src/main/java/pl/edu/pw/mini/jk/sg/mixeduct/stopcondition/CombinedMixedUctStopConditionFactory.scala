package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove


case class CombinedMixedUctStopConditionFactory(scFactories: List[StopConditionFactory]) extends StopConditionFactory {
  def create[DM <: DefenderMove, AM] = new CombinedMixedUctStopCondition(scFactories.map(_.create[DM,AM]))
}

