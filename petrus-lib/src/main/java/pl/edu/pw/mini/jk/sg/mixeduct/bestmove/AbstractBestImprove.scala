package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracle
import scala.collection.mutable.{HashMap => MMap}
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import com.google.common.collect.ImmutableMap
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices


import org.log4s._
import java.{util => ju}
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker

abstract class AbstractBestImprove[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
](protected val gameMatrices: GameMatrices[AM, DM, AS, DS, G]) extends DefenderOracle[DM, AM, DS, AS, G] {
  protected val logger = getLogger("petrus.DefenderOracle")

  protected val strategy: MMap[ImmutableList[DM], Double] = MMap()
  private var stepSum = 0.0;

  protected def currStrategy =
    strategy.mapValues(v => new java.lang.Double(v.toDouble / stepSum.toDouble)).asJava

  def bestImprovementStep(
      attackerProbabilities: AttackerStrategy[AM]
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]]

  def calculateDefenderStrategy(iteration: Int, attackerProbabilities: AttackerStrategy[AM])(
      implicit rng: RandomGenerator
  ): ju.Map[ImmutableList[DM], java.lang.Double] = {
    val bestMove = bestImprovementStep(attackerProbabilities)
    bestMove.foreach {
      _.foreach {
        case (m, step) => {
          strategy.put(m, strategy.getOrElse(m, 0.0) + step)
          stepSum = stepSum + step
        }
      }
    }

    logger.debug(s"MoveFreq: ${stepSum} ${strategy}")

    currStrategy
  }

  protected def bumpMove(
      move: ImmutableList[DM],
      step: Double
  ): ImmutableMap[ImmutableList[DM], java.lang.Double] = {
    val sc = strategy.clone()
    sc.put(move, sc.getOrElse(move, 0.0) + step)
    ImmutableMap.copyOf(
      sc.mapValues(x => new java.lang.Double(x.toDouble / (stepSum + step).toDouble)).asJava
    )
  }

  def isImprovementDirection(
      currPayoff: Double,
      direction: Map[ImmutableList[DM], Double]
  ): Boolean = {
    val str = bumpDirection(direction)
    val currAtt = AbstractBestImprove.strategyListToMap(
      OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, str)
    )

    gameMatrices.getExpectedPayoff(str, currAtt).defenderPayoff > currPayoff
  }

  protected def payoff: Double =       if (currStrategy.isEmpty) Double.NegativeInfinity
      else {
        val currAtt = AbstractBestImprove.strategyListToMap(
          OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, currStrategy)
        )
        gameMatrices.getExpectedPayoff(ImmutableMap.copyOf(currStrategy), currAtt).defenderPayoff
      }


  protected def bumpDirection(
      dir: Map[ImmutableList[DM], Double]
  ): ImmutableMap[ImmutableList[DM], java.lang.Double] = {
    val ss = dir.values.sum
    val sc = strategy.clone()
    dir.foreach {
      case (move, step) =>
        sc.put(move, sc.getOrElse(move, 0.0) + step)
    }
    ImmutableMap.copyOf(
      sc.mapValues(x => new java.lang.Double(x.toDouble / (stepSum + ss).toDouble)).asJava
    )
  }

}

object AbstractBestImprove {

  def strategyListToMap[M](
      str: java.util.List[org.apache.commons.math3.util.Pair[ImmutableList[M], java.lang.Double]]
  ): ImmutableMap[ImmutableList[M], java.lang.Double] = {
    val builder = ImmutableMap.builder[ImmutableList[M], java.lang.Double]()
    str.asScala.foreach(p => builder.put(p.getFirst(), p.getSecond()))
    builder.build()
  }
}
