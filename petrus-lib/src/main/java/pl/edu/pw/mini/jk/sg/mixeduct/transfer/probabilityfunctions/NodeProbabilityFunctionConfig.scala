package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions

import argonaut.{ DecodeJson, DecodeResult }


object NodeProbabilityFunctionDecoder {
  implicit val decoder: DecodeJson[NodeProbabilityWeightFunction] =
    DecodeJson( name => name.as[String].flatMap{n => n match { 
      case "VISITS_COUNT" => DecodeResult.ok(VisitsCount)
      case "MODIFIED_NORMALIZED_VISITS_COUNT" => DecodeResult.ok(ModifiedNormalizedVisitsCount)
      case "NORMALIZED_VISITS_COUNT" => DecodeResult.ok(NormalizedVisitsCount)
      case "BRANCHED_NORMALIZED_VISITS_COUNT" => DecodeResult.ok(BranchedNormalizedVisitsCount)
      case "QUALITY_1" => DecodeResult.ok(QualityProbabilityFunctions.linearQuality(0))
      case "QUALITY_NOT_NORM" => DecodeResult.ok(QualityProbabilityFunctions.linearQualityNotNorm(0))
      case "EXP_Q" => DecodeResult.ok(QualityProbabilityFunctions.expQuality)
      case "SIGMOID_Q" => DecodeResult.ok(QualityProbabilityFunctions.sigmoidQuality)
      case "GLOBAL_MIN_Q" => DecodeResult.ok(GlobalQWeight)
      case "GLOBAL_MIN_NORMALIZED" => DecodeResult.ok(GlobalQWeightNorm)
      case "GLOBAL_MIN_Q_LEAF" =>  DecodeResult.ok(GlobalQLeafWeight)
      case _ => DecodeResult.fail("unknown node probability wieight function " + name, name.history)
    }})
}
