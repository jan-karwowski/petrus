package pl.edu.pw.mini.jk.sg.mixeduct.attacker

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult
import org.apache.commons.math3.random.RandomGenerator
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.util.{Pair => ACPair}
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.RankPayoffWeightFunction
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping.WeightDamping
import scala.collection.mutable.{HashMap => MMap}
import org.apache.commons.math3.distribution.EnumeratedDistribution
import scala.collection.JavaConverters._
import com.google.common.collect.ImmutableMap

final class PayoffRankWeightedAttacker[M](
    initialStrategy: ImmutableList[ACPair[ImmutableList[M], java.lang.Double]],
    historyLength: Int,
    rankPayoffWeightFunction: RankPayoffWeightFunction,
    weightDamping: WeightDamping,
    rng: RandomGenerator
) extends AttackerWithHistory[M] {
  private type LastSeenIteration = Int
  private type AccumulatedWeight = Double
  private var currIteration: Int = 0
  private val strategies: MMap[ImmutableList[M], (LastSeenIteration, AccumulatedWeight)] = MMap()
  private var str: java.util.List[ACPair[ImmutableList[M], java.lang.Double]] = initialStrategy

  override def getInstance(): AttackerStrategy[M] =
    new SimpleAttackerStrategy[M](
      new EnumeratedDistribution(
        rng,
        str
      )
    )

  override def registerIterationResult[DM <: DefenderMove](
      iteration: Int,
      iterationResult: IterationResult[DM, M, _, _, _],
      trainingAttacker: AttackerStrategy[M]
  ): Unit = {
    val maxAttPayoff = iterationResult.givenPayoff.attackerPayoff
    iterationResult.attackerStrategy.strategy.keySet().asScala.foreach { str =>
      strategies.put(str, (currIteration, strategies.getOrElse(str, (0,0.0))._2))
    }
    currIteration = currIteration + 1

    strategies
      .filter { case (_, (it, w)) => it <= currIteration - historyLength }
      .foreach { case (k, _) => strategies -= k }

    val strategyRanking = strategies.toSeq.map{case (str, (_, w)) =>
      (str, w, iterationResult.gameMatrices.getExpectedPayoff(iterationResult.defenderStrategy,
      ImmutableMap.of(str, 1.0)).attackerPayoff)}.sortBy(-_._3).toVector

    val minAttPayoff = strategyRanking.view.map(_._3).min
    str = strategyRanking.zipWithIndex.map{case ((str, oldw, payoff), rank) =>
      new ACPair[ImmutableList[M], java.lang.Double](
        str,
        oldw+rankPayoffWeightFunction.apply(payoff, maxAttPayoff, minAttPayoff, rank)
      )}.asJava

    val stat = weightDamping.calcGlobalStat(str.asScala.view.map(_.getValue().toDouble))
    str.asScala.foreach(pair => {
      val lastSeen = strategies(pair.getFirst())._1
      strategies.update(pair.getFirst(), (lastSeen, weightDamping.modifyWeight(stat)(pair.getSecond().toDouble)))
    })
  }
}
