package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight

object OnlyRankWeight extends RankPayoffWeightFunction {
  def apply(payoff: Double, maxPayoff: Double, minPayoff: Double, rank: Int): Double =  1/Math.pow(2,rank)
}
