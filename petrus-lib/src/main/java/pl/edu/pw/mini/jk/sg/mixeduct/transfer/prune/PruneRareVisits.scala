package pl.edu.pw.mini.jk.sg.mixeduct.transfer.prune

import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.tree.{ PruneTreeFunctions, UctNode }
import scala.collection.JavaConverters._


case class PruneRareVisits(dropLowerThan: Double)
    extends PruneTreeFunctions {
  require(dropLowerThan >= 0)
  require(dropLowerThan < 1)

  override type T = Int

  override def nodeStatistics(node: UctNode[_,_]) : Int = {
    if(node.getStatistics.size == 0)
      0
    else
      node.getStatistics.asScala.map(_.statistics.visitsCount).max
  }
  override def movePredicate(statistics: UctReadableStatistics, nodeStat: Int) = statistics.visitsCount.toDouble/nodeStat.toDouble >= dropLowerThan
}
