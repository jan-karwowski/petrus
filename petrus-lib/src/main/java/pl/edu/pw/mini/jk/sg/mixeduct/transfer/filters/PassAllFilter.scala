package pl.edu.pw.mini.jk.sg.mixeduct.transfer.filters

import java.util.Collection
import org.apache.commons.collections4.queue.CircularFifoQueue
import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

final class PassAllFilter[DM <: DefenderMove, DS <: AdvanceableState[DM]](treeCount: Int) extends BestPartFilter[DM, DS] {
  private val trees: CircularFifoQueue[UctNode[DS,DM]] = new CircularFifoQueue(treeCount);

  override def getFilteredCollecton() : Collection[UctNode[DS,DM]] = ImmutableList.copyOf(trees.iterator());

  override def registerTree(tree: UctNode[DS,DM]) : Collection[UctNode[DS,DM]] = {
    trees.add(tree);
    getFilteredCollecton();
  }
}
