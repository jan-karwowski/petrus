package pl.edu.pw.mini.jk.sg.mixeduct.attacker

import com.google.common.collect.ImmutableList
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.util.Pair

import collection.JavaConverters._


case class UniformStrategy[M](distribution: EnumeratedDistribution[ImmutableList[M]]) extends AttackerStrategy[M] {
  def getPmf(): java.util.List[Pair[ImmutableList[M],java.lang.Double]] = distribution.getPmf
  def sample(): ImmutableList[M] = distribution.sample()
}


object UniformStrategy {
  def fromPairs[M](pairs: Seq[(ImmutableList[M], Double)],
    rng: RandomGenerator) = UniformStrategy(new EnumeratedDistribution(rng, pairs.
      map{case (a,b) => new Pair[ImmutableList[M], java.lang.Double](a,b)}.toList.asJava ))

  def fromRepeats[M](strategies: Seq[ImmutableList[M]], rng: RandomGenerator) = {
    val pairs = strategies.groupBy(identity).mapValues(_.size.toDouble).toSeq
    fromPairs(pairs, rng)
  }
}
