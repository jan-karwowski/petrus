package pl.edu.pw.mini.jk.lib;

import java.util.Optional;
import java.util.function.Supplier;

public class Lazy<T> {
    private Optional<T> calculatedValue;
    private Supplier<T> supplier;

    public T get() {
	if(!calculatedValue.isPresent()) {
	    calculatedValue = Optional.of(supplier.get());
	    supplier = null; // hack, żeby zwolnić pamięć.
	}
	return calculatedValue.get();
    }

    public Lazy(Supplier<T> supplier) {
	this.supplier = supplier;
	this.calculatedValue = Optional.empty();
    }
}
