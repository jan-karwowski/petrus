package pl.edu.pw.mini.jk.sg.mixeduct.transfer

import scala.collection.JavaConverters._

import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.tree.{PruneTreeFunctions, UctNode}

case class BestPartPruneFunctions(portion: Double) extends PruneTreeFunctions {
  override type T = Double
  override def movePredicate(statistics: UctReadableStatistics, cutoff: Double) = statistics.averagePayoff >= cutoff
  override def nodeStatistics(node: UctNode[_, _]): Double = {
    val payoffs = node.getStatistics.asScala.map(_.statistics.averagePayoff).toVector.sorted.reverse
    payoffs.applyOrElse((payoffs.size*portion).ceil.toInt, (x: Any) => -1)
  }
}
