package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.{ TraitJsonDecoder }



object AttackerSolverDecode {
  def decoder(decoders: Map[String, DecodeJson[_ <: AttackerSolver]]) : DecodeJson[AttackerSolver] = TraitJsonDecoder[AttackerSolver](decoders).decoder

  implicit val decodeEA : DecodeJson[OptimalAttacker] = DecodeJson.UnitDecodeJson.map(x => new OptimalAttacker())
  implicit val defaultDecode : DecodeJson[AttackerSolver] = decoder(Map("exact" -> decodeEA))
}
