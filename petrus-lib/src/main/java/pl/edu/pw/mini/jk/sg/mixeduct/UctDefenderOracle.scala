package pl.edu.pw.mini.jk.sg.mixeduct
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.mixeduct.transfer.AttackersInformationTransfer
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import pl.edu.pw.mini.jk.sg.uct.UctPlayer
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.uct.tree.PruneTreeFunctions
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import pl.edu.pw.mini.jk.sg.uct.I2UctConfig
import _root_.java.{util => ju}
import com.google.common.collect.ImmutableList

final class UctDefenderOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
](
    config: UctDefenderOracleConfig,
    gameMatrices: GameMatrices[AM, DM, AS, DS, G],
    treeLogConfig: TreeLogConfig
) extends DefenderOracle[DM, AM, DS, AS, G] {
  private val attackerInformationTransfer: AttackersInformationTransfer[DM, DS] =
    config.informationTransfer.create(
      gameMatrices.getGameLength(),
      gameMatrices,
      config.i2UctConfig.uctTreeFactory.treeJoiner
    )
  private val interstepPruneFunction: Option[PruneTreeFunctions] =
    config.interStepPruneFunction
  import config.i2UctConfig

  private var uctTree: UctNode[DS, DM] =
    config.i2UctConfig.uctTreeFactory.create(gameMatrices.getDefenderInitialState())

  override def calculateDefenderStrategy(iteration: Int, attackerProbabilities: AttackerStrategy[AM])(
      implicit rng: RandomGenerator
  ): ju.Map[ImmutableList[DM], java.lang.Double] = {
    val uctPlayer: UctPlayer[DM, DS, DefenderSecurityGameView[DM, AM, DS, G]] =
      UctPlayer.fromConfig[DM, DS, DefenderSecurityGameView[DM, AM, DS, G]](
        DefenderSecurityGameView
          .transformFactory(gameMatrices.getNoaiFactory(), attackerProbabilities),
        gameMatrices.getGameLength(),
        i2UctConfig,
        rng
      )

    for (j <- Range(0, gameMatrices.getGameLength())) {
      uctPlayer.doLearningSequence(j, uctTree);
      interstepPruneFunction.foreach(pf => uctTree = uctTree.pruneTree(pf))
    }
    uctTree = attackerInformationTransfer.newAttackerTree(uctTree, rng)

    attackerInformationTransfer.getDefenderMoveProbabilities(rng, treeLogConfig.log(iteration))
  }
}
