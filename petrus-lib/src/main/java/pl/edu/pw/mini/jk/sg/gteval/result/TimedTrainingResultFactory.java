package pl.edu.pw.mini.jk.sg.gteval.result;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;


@FunctionalInterface
public interface TimedTrainingResultFactory<M extends DefenderMove,S> {
    public TimedTrainingResult<M,S> create(long timeMs);
}
