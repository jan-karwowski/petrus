package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger.AttackerHistoryChanger
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator

final case class DiscountingHistoryAttackerFactoryConfig(
    historyLength: Int,
    historyChanger: AttackerHistoryChanger,
    discountingFactor: Double
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[
        org.apache.commons.math3.util.Pair[ImmutableList[AM], java.lang.Double]
      ]
  ): AttackerWithHistory[AM] =
    new DiscountingHistoryAttackerFactory[AM](
      initialStrategy,
      historyLength,
      rng,
      historyChanger,
      discountingFactor
    )
}
