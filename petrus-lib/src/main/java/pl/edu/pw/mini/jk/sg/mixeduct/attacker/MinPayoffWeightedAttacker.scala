package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult
import scala.collection.mutable.{HashMap => MMap}
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.distribution.EnumeratedDistribution

import scala.collection.JavaConverters._
import _root_.org.apache.commons.math3.util.{Pair => ACPair}
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import com.google.common.collect.ImmutableMap
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove

final class MinPayoffWeightedAttacker[M](
  initialStrategy: ImmutableList[ACPair[ImmutableList[M], java.lang.Double]],
  historyLength: Int,
  payoffWeightFunction: PayoffWeightFunction,
  rng: RandomGenerator
) extends AttackerWithHistory[M] {
  private var currIteration: Int = 0
  private val strategies: MMap[ImmutableList[M], Int] = MMap()
  private var str: java.util.List[ACPair[ImmutableList[M], java.lang.Double]] = initialStrategy

  override def getInstance(): AttackerStrategy[M] =
    new SimpleAttackerStrategy[M](new EnumeratedDistribution(
      rng,
      str
    ))

  override def registerIterationResult[DM <: DefenderMove](
    iteration: Int, iterationResult: IterationResult[DM, M, _, _, _], trainingAttacker: AttackerStrategy[M]
  ) : Unit = {
    val maxAttPayoff = iterationResult.givenPayoff.attackerPayoff
    iterationResult.attackerStrategy.strategy.keySet().asScala.foreach{str =>
      strategies.put(str, currIteration)
    }
    currIteration = currIteration + 1

    strategies.filter{case (_, it) => it <= currIteration - historyLength}
      .foreach{case (k, _) => strategies-=k}

    str = strategies.keys.map(str => new ACPair[ImmutableList[M], java.lang.Double](
      str,
      payoffWeightFunction(iterationResult.gameMatrices.getExpectedPayoff(iterationResult.defenderStrategy,
        ImmutableMap.of(str, 1.0)).attackerPayoff, maxAttPayoff)
    )).toVector.asJava
  }
}
