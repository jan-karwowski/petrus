package pl.edu.pw.mini.jk.sg.mixeduct.resulttrends;

public class ResultTrends {
	private final Oscilation oscilationCheck;    
	private int currentIteration = 0;
    
	public ResultTrends(int oscilateCheckIterations, int oscilateCheckSmoothSteps,
			int oscilateCheckFirstIteration, double oscilateCheckThreshold) {
		super();

		if (oscilateCheckFirstIteration == -1) {
			oscilationCheck = new OscilationNocheck();
		} else {
			oscilationCheck = new OscilationCheck(oscilateCheckIterations, oscilateCheckSmoothSteps,
					oscilateCheckFirstIteration, oscilateCheckThreshold);
		}
	}

	public void registerResult(final double result) {
		oscilationCheck.registerResult(result);
	}

	public boolean isOscilating() {
		return oscilationCheck.isOscilating(currentIteration);
	}
}
