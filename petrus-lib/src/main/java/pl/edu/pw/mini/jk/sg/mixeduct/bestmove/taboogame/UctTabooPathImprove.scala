package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.AbstractBestImprove
import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import pl.edu.pw.mini.jk.sg.uct.I2UctConfig
import pl.edu.pw.mini.jk.sg.uct.UctPlayer
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.collections4.Factory
import pl.edu.pw.mini.jk.sg.uct.SinglePlayerGame
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.mixeduct.util.TreeUtils
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderSecurityGameView
import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker
import com.google.common.collect.ImmutableMap
import scala.annotation.tailrec

final class UctTabooPathImprove[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
](
    i2UctConfig: I2UctConfig[SecurityGame],
    stepEps: Double,
    gameMatrices: GameMatrices[AM, DM, AS, DS, G]
) extends AbstractBestImprove[DM, AM, DS, AS, G](gameMatrices) {
  private type MyGame = TabooGame[DM, DS, DefenderSecurityGameView[DM, AM, DS, G]]
  private type MyFactory = TabooGameFactory[DM, DS, DefenderSecurityGameView[DM, AM, DS, G]]

  private def getBestUctStrategy(
      gameFactory: Factory[MyGame]
  )(implicit rng: RandomGenerator): List[DM] = {
    val uctPlayer = UctPlayer.fromConfig[DM, DS, MyGame](
      gameFactory,
      gameMatrices.getGameLength(),
      i2UctConfig,
      rng
    )
    val tree = i2UctConfig.uctTreeFactory.create[DS, DM](gameMatrices.getDefenderInitialState())
    for (j <- Range(0, gameMatrices.getGameLength())) {
      uctPlayer.doLearningSequence(j, tree);
    }
    TreeUtils.getBestPathInTreeExtended(tree, gameMatrices.getGameLength, rng)
  }

  private def improvementTry(
      currPayoff: Double,
      factory: MyFactory,
      step: Double
  )(implicit rng: RandomGenerator): Either[MyFactory, List[DM]] = {
    val str = getBestUctStrategy(factory)
    val ds = bumpMove(ImmutableList.copyOf(str.asJava.iterator()), step)
    val bestResponse =
      OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, ds)
    val payoff =
      gameMatrices.getExpectedPayoff(ds, AbstractBestImprove.strategyListToMap(bestResponse))

    if (payoff.defenderPayoff > currPayoff) Right(str)
    else {
      factory.forbidSeq(str); Left(factory);
    }
  }

  def bestImprovementStep(
      attackerProbabilities: AttackerStrategy[AM]
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] = {
    val spgf = DefenderSecurityGameView.transformFactory[DM, AM, DS, G](
      gameMatrices.getNoaiFactory(),
      attackerProbabilities
    )
    val factory = new TabooGameFactory[DM, DS, DefenderSecurityGameView[DM, AM, DS, G]](spgf, None)
    val currPayoff = {
      val currAtt = AbstractBestImprove.strategyListToMap(
        OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, currStrategy)
      )
      gameMatrices.getExpectedPayoff(ImmutableMap.copyOf(currStrategy), currAtt).defenderPayoff
    }

    @tailrec
    def improvementLoop(fct: MyFactory, step: Double): Option[(List[DM], Double)] = {
      improvementTry(currPayoff, fct, step) match {
        case Right(str)                                      => Some((str, step))
        case Left(fct) if fct.create().possibleMoves.isEmpty => None
        case Left(fct)                                       => improvementLoop(fct, step)
      }
    }

    def stepReduction(step: Double): Option[(List[DM], Double)] =
      if (step <= stepEps) None
      else improvementLoop(factory, step).orElse(stepReduction(step / 2))

    stepReduction(1.0).map { case (a, b) => Map(ImmutableList.copyOf(a.asJava.iterator()) -> b) }
  }
}
