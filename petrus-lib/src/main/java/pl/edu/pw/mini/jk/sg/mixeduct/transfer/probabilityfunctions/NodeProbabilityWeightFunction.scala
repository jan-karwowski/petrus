package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView

trait NodeProbabilityWeightFunction {
  type GlobalStat

  def calculateGlobalStat(stats: => Seq[UnmodifiableDecisionStatisticsView]) : GlobalStat

  /**
    *  @param statistics given modve statistics
    *  @param node  a node from which the move is played (provides a way to browse siblings)
    *  @param stat a global stat calculated for the whole tree
    */
  def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_,_], stat: GlobalStat): Double

  val leafOnlyWeight: Boolean
}
