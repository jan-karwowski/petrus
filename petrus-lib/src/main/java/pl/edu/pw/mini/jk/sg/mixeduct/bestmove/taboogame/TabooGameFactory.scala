package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import org.apache.commons.collections4.Factory
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.SinglePlayerGame
import com.google.common.collect.ImmutableList

final class TabooGameFactory[M, S <: AdvanceableState[M], G <: SinglePlayerGame[M, S] with SecurityGame](
    gf: Factory[G],
    var taboo: Option[TabooTree[M]]
) extends Factory[TabooGame[M, S, G with SecurityGame]] {
  def create(): TabooGame[M, S, G with SecurityGame] = new TabooGame(gf.create(), taboo)

  def forbidSeq(moves: List[M]): Unit = {
    val originalGame = gf.create()
    taboo = taboo match {
      case Some(t) =>
        Some(t.forbidMoveSequence(moves, originalGame.getCurrentState())._1)
      case None =>
        Some(
          (new TabooTree(Map()))
            .forbidMoveSequence(moves, originalGame.getCurrentState())
            ._1
        )
    }
  }
}
