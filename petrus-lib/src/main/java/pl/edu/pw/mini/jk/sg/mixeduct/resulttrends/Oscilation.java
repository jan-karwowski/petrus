package pl.edu.pw.mini.jk.sg.mixeduct.resulttrends;

public interface Oscilation {

	void registerResult(double result);

	boolean isOscilating(int currentIteration);

}