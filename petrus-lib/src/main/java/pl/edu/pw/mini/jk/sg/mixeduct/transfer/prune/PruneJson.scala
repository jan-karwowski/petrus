package pl.edu.pw.mini.jk.sg.mixeduct.transfer.prune;

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import pl.edu.pw.mini.jk.sg.uct.tree.PruneTreeFunctions



object PruneJson {
  implicit val pruneRareVisitsDecode = DecodeJson.jdecode1L(PruneRareVisits.apply)("dropLowerThan")

  implicit val pruneKeepAll = DecodeJson.UnitDecodeJson.map(x => KeepAll)

  implicit val decoder : DecodeJson[PruneTreeFunctions] = TraitJsonDecoder[PruneTreeFunctions](Map(
    "rareVisit" -> pruneRareVisitsDecode,
    "keepAll" -> pruneKeepAll
  )).decoder
}
