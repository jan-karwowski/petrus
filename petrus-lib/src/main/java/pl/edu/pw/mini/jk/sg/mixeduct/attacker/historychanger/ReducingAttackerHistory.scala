package pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger

import argonaut.{ Argonaut, DecodeJson }


case class ReducingAttackerHistory(reductionCoefficient: Double) extends AttackerHistoryChanger {
  override def newHistoryLength(initialHist: Int, currHist: Int, maxHist: Int, iteration: Int, oscillating: Boolean) : Int =
    (initialHist * scala.math.pow(reductionCoefficient, iteration.toDouble)).toInt
}

object ReducingAttackerHistory {
  implicit def decoder : DecodeJson[ReducingAttackerHistory] = Argonaut.casecodec1(ReducingAttackerHistory.apply, ReducingAttackerHistory.unapply)("reductionCoefficient")
}
