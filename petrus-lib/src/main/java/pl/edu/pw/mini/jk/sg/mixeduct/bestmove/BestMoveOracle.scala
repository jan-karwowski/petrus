package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import java.{util => ju}
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracle
import com.google.common.collect.ImmutableList
import scala.collection.JavaConverters._
import com.google.common.collect.ImmutableMap
import org.apache.commons.math3.random.RandomGenerator

final class BestMoveOracle[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
](
    gameMatrices: GameMatrices[AM, DM, AS, DS, G]
) extends AbstractBestMoveOracle[DM, AM, DS, AS, G](gameMatrices) {
  protected def bestMoveProcedure(
      attStr: ImmutableMap[ImmutableList[AM], java.lang.Double],
      sortedMoves: Seq[ImmutableList[DM]],
      currPayoff: Double
  )(implicit rng: RandomGenerator): Option[Map[ImmutableList[DM], Double]] =
    bestMoveWithStep(attStr, sortedMoves, 1, currPayoff)
      .orElse(sortedMoves.headOption)
      .map(x => Map((x, 1.0)))
}
