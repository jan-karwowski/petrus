package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse;

import java.util.Map;

import org.apache.commons.math3.random.RandomGenerator;

import com.google.common.collect.ImmutableList;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState;
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;

public interface AttackerSolver {
    public <AM, AS extends AdvanceableAttackerState<AM>, DM extends DefenderMove, DS extends AdvanceableDefenderState<DM>>
	AttackerSolverResult<AM>
	 calculateAttackerStrategy(
            final Map<ImmutableList<DM>, Double> defenderMoveProbabilities,
            final GameMatrices<AM, DM, AS, DS,?> gameMatrices, RandomGenerator rng);
    
    public boolean isExact();
}
