package pl.edu.pw.mini.jk.sg.mixeduct.transfer.strategyfilter

import argonaut.{ Argonaut, CodecJson, DecodeJson }
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder


trait StrategyFilterConfig {
  def getStrategyFilter[M] : StrategyFilter[M]
}

object StrategyFilterConfig {
  def decode(decoders: Map[String, DecodeJson[_ <: StrategyFilterConfig]]) =
    TraitJsonDecoder[StrategyFilterConfig](decoders).decoder

  val defaultDecoder = decode(Map(
    "null" -> NullStrategyFilterConfig.decoder,
    "bestGap" -> BestGapStrategyFilterConfig.decoder
  ))
}

case object NullStrategyFilterConfig extends StrategyFilterConfig {
  override def getStrategyFilter[M] = new NullStrategyFilter()

  implicit val decoder : DecodeJson[StrategyFilterConfig] = DecodeJson.UnitDecodeJson.map(_ => NullStrategyFilterConfig)
}

case class BestGapStrategyFilterConfig(bestGap: Double) extends StrategyFilterConfig {
  override def getStrategyFilter[M] =  new BestGapStrategyFilter(bestGap)
}

object BestGapStrategyFilterConfig {
  implicit val decoder : CodecJson[BestGapStrategyFilterConfig] =
    Argonaut.casecodec1(BestGapStrategyFilterConfig.apply, BestGapStrategyFilterConfig.unapply)("bestGap")
}


