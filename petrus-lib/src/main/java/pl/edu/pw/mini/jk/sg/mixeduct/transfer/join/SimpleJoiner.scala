package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions
import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics

trait SimpleJoiner extends TreeJoinerFunctions {
  override type CS = Unit
  override type GS = Unit

  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) = Unit
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 
}
