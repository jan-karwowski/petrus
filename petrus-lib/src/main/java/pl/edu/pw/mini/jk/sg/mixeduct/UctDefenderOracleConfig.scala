package pl.edu.pw.mini.jk.sg.mixeduct
import pl.edu.pw.mini.jk.sg.uct.tree.PruneTreeFunctions
import pl.edu.pw.mini.jk.sg.uct.I2UctConfig
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.mixeduct.transfer.AttackersInformationTransferFactory
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.{
  AdvanceableAttackerState,
  AdvanceableDefenderState
}
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices

final case class UctDefenderOracleConfig(
    informationTransfer: AttackersInformationTransferFactory,
    i2UctConfig: I2UctConfig[SecurityGame],
    interStepPruneFunction: Option[PruneTreeFunctions]
) extends DefenderOracleFactory {
  def apply[DM <: DefenderMove, AM, DS <: AdvanceableDefenderState[DM], AS <: AdvanceableAttackerState[
    AM
  ], G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
    G
  ]](
      gameMatrices: GameMatrices[AM, DM, AS, DS, G],
      treeLogConfig: TreeLogConfig
  ): DefenderOracle[DM, AM, DS, AS, G] = new UctDefenderOracle(this, gameMatrices, treeLogConfig)
}
