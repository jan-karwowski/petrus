package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoinerFunctions }


case object MedianJoin extends TreeJoinerFunctions {
  override type CS = Unit
  override type GS = Unit

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit) : UCTDecisionStatistics = {
    val position = (treeCount / 2) - (treeCount - stats.size)
    implicit val comp : Ordering[UctReadableStatistics] = Ordering.by(_.averagePayoff)
    stats.toVector.sorted.toVector(position).toStats
  }
   
  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) = Unit
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) = Unit 

}
