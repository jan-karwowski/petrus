package pl.edu.pw.mini.jk.sg.mixeduct.transfer.filters

import java.util.function.Function
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

trait BestPartFilterFactory {
  def createInstance[DM <: DefenderMove, DS <: AdvanceableState[DM]](
    treeQualityFunction: UctNode[DS,DM] => Double
  ) : BestPartFilter[DM, DS]
}
