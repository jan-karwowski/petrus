package pl.edu.pw.mini.jk.sg.mixeduct.startattackers;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.ImmutableList;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;

public interface AttackerProbabilityBuilder {
    <DM extends DefenderMove, AM> ImmutableList<Pair<ImmutableList<AM>, Double>> buildProbabilities(GameMatrices<AM,DM, ?, ?, ?> gameMatrices);
}
