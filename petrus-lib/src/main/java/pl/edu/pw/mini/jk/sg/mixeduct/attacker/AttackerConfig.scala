package pl.edu.pw.mini.jk.sg.mixeduct.attacker

import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.util.Pair
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import argonaut._

import collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.uct.{DecodeSelectionFunctionJson, SelectionFunction}
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger.AttackerHistoryChanger
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger.AttackerHistoryDecode
import _root_.pl.edu.pw.mini.sg.json.TraitSubtypeCodec
import pl.edu.pw.mini.sg.json.FlatTraitJsonCodec

import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.{ExponentialWeight, HyperbolicWeight}
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.RankPayoffWeightFunction
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping.WeightDamping
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping.ForgetHistory
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.MultipliedRankWeight
import _root_.pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping.ExponentialDamping
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.OnlyRankWeight
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.PayoffExponentialWeight
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight.OnlyBestStrategy

trait AttackerConfig {
  def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[Pair[ImmutableList[AM], java.lang.Double]]
  ): AttackerWithHistory[AM]
}

case class ProbabilisticStrategyAttackerConfig(
    historyLength: Int,
    historyChanger: AttackerHistoryChanger
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[Pair[ImmutableList[AM], java.lang.Double]]
  ) =
    new ProbabilisticStrategyAttackerWithHistoryFactory[AM](
      initialStrategy,
      historyLength,
      rng,
      historyChanger
    )
}

case class QualityWeightedAttackerConfig(
    strategyPoolSize: Int,
    strategySampleSize: Int,
    selectionFunction: SelectionFunction
) extends AttackerConfig {
  override def getAttackerWithHistory[AM](
      rng: RandomGenerator,
      initialStrategy: ImmutableList[Pair[ImmutableList[AM], java.lang.Double]]
  ) =
    new QualityWeightedHistoryAttacker(
      strategyPoolSize,
      strategySampleSize,
      selectionFunction,
      initialStrategy.asScala.map(p => (p.getFirst, p.getSecond.toDouble)).toList,
      rng
    )
}

object AttackerConfig {
  def probabilisticASDecoder(
      implicit ahcDecode: DecodeJson[AttackerHistoryChanger]
  ): DecodeJson[ProbabilisticStrategyAttackerConfig] =
    DecodeJson.jdecode2L(ProbabilisticStrategyAttackerConfig.apply)(
      "historyLength",
      "historyChanger"
    )

  def qualityWADecoder(
      implicit sfDecode: DecodeJson[SelectionFunction]
  ): DecodeJson[QualityWeightedAttackerConfig] =
    DecodeJson.jdecode3L(QualityWeightedAttackerConfig.apply)(
      "strategyPoolSize",
      "strategySampleSize",
      "selectionFunction"
    )

  def discountingDecoder(
      implicit ahcDecode: DecodeJson[AttackerHistoryChanger]
  ): DecodeJson[DiscountingHistoryAttackerFactoryConfig] =
    DecodeJson.jdecode3L(DiscountingHistoryAttackerFactoryConfig.apply)(
      "historyLength",
      "historyChanger",
      "discountingFactor"
    )

  val trendDecoder : CodecJson[TrendWeightedAttackerConfig] =
    CodecJson.casecodec4(TrendWeightedAttackerConfig.apply, TrendWeightedAttackerConfig.unapply)(
      "baseWeight", "damping", "cutoff", "weightBias"
    )

  implicit val payoffWeightFunctionCodec = new FlatTraitJsonCodec[PayoffWeightFunction](List(
    TraitSubtypeCodec.singletonCodec(ExponentialWeight, "exponential"),
    TraitSubtypeCodec.singletonCodec(HyperbolicWeight, "hyperbolic")
  ))

  val payoffWeightCodec : CodecJson[MinPayoffWeightedAttackerFactory] = CodecJson.casecodec2(
    MinPayoffWeightedAttackerFactory.apply, MinPayoffWeightedAttackerFactory.unapply
  )(
    "historyIterations", "weightFunction"
  )

  val payoffRankCodec : CodecJson[RankPayoffWeightedAttackerFactory] = CodecJson.casecodec1(
    RankPayoffWeightedAttackerFactory.apply, RankPayoffWeightedAttackerFactory.unapply
  )(
    "historyIterations"
  )

  implicit val payoffRankWeightCodec = new FlatTraitJsonCodec[RankPayoffWeightFunction](List(
    TraitSubtypeCodec.singletonCodec(MultipliedRankWeight, "multipliedRankWeight"),
    TraitSubtypeCodec.singletonCodec(OnlyRankWeight, "onlyRankWeight"),
    TraitSubtypeCodec.singletonCodec(PayoffExponentialWeight, "payoffExponentialWeight"),
    TraitSubtypeCodec.singletonCodec(OnlyBestStrategy, "onlyBestStrategy")
  ))

  val exponentialDamping = CodecJson.casecodec1(ExponentialDamping.apply, ExponentialDamping.unapply)("alpha")
  implicit val weightDamping = new FlatTraitJsonCodec[WeightDamping](List(
    TraitSubtypeCodec.singletonCodec(ForgetHistory, "forgetHistory"),
    TraitSubtypeCodec(exponentialDamping, exponentialDamping, "exponentialDamping")
  ))

  val bothPayoffRankCodec: CodecJson[PayoffRankWeightedAttackerFactory] = CodecJson.casecodec3(
    PayoffRankWeightedAttackerFactory.apply, PayoffRankWeightedAttackerFactory.unapply
  )(
    "historyLength", "rankPayoffWeightFunction", "weightDamping"
  )


  val constantDecoder : DecodeJson[ConstantAttackerFactory.type] = DecodeJson.UnitDecodeJson.map(_ => ConstantAttackerFactory)

  def acDecoder(
      implicit ascDecode: DecodeJson[AttackerHistoryChanger],
      sfDecode: DecodeJson[SelectionFunction]
  ): DecodeJson[AttackerConfig] =
    TraitJsonDecoder[AttackerConfig](
      Map(
        "simpleHistoryAttacker" -> probabilisticASDecoder,
        "discountingHistoryAttacker" -> discountingDecoder,
        "qualitySelectionHistoryAttacker" -> qualityWADecoder,
        "trendWeightedHistoryAttacker" -> trendDecoder,
        "payoffWeightedHistoryAttacker" -> payoffWeightCodec,
        "rankWeightedHistoryAttacker" -> payoffRankCodec,
        "constantAttacker" -> constantDecoder,
        "rankPayoffWeightedAttacker" -> bothPayoffRankCodec
      )
    ).decoder


  val defaultDecoder =
    acDecoder(AttackerHistoryDecode.defaultDecoder, DecodeSelectionFunctionJson.defaultDecode)
}
