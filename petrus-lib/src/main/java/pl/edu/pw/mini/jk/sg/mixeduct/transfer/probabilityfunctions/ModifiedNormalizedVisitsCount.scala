package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions

import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

object ModifiedNormalizedVisitsCount extends SimpleNodeProbabilityWeightFunction {
  override def probabilityWeight(move: UnmodifiableDecisionStatisticsView, node: UctNode[_,_]) = 
    move.visitsCount.toDouble / node.getVisitsCount.toDouble
}
