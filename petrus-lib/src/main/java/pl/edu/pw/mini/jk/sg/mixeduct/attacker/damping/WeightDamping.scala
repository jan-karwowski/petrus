package pl.edu.pw.mini.jk.sg.mixeduct.attacker.damping

trait WeightDamping {
  type GlobalStat
  def calcGlobalStat(allWeights: Seq[Double]) : GlobalStat
  def modifyWeight(stat: GlobalStat)(weight: Double) : Double
}
