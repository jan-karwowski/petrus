package pl.edu.pw.mini.jk.sg.mixeduct.transfer.filters

import java.util.Collection

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

trait BestPartFilter[DM <: DefenderMove, DS <: AdvanceableState[DM]] {
  def registerTree(tree: UctNode[DS,DM]) : Collection[UctNode[DS,DM]]
  def getFilteredCollecton(): Collection[UctNode[DS,DM]]
}
