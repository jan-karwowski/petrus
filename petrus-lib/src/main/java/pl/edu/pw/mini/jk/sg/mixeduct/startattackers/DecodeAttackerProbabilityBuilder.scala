package pl.edu.pw.mini.jk.sg.mixeduct.startattackers

import argonaut.{ DecodeJson, DecodeResult }


object DecodeAttackerProbabilityBuilder {
  val decoder : DecodeJson[AttackerProbabilityBuilder] = DecodeJson(c => DecodeJson.StringDecodeJson.decode(c).flatMap(s =>
    s match {
      case "uniformMoves" => DecodeResult.ok(new  UniformMoves())
      case "uniformDefenderOpponent" => DecodeResult.ok(new UniformDefenderOpponent())
      case _ => DecodeResult.fail("Unknown probability builder "+c, c.history)
    }))
}
