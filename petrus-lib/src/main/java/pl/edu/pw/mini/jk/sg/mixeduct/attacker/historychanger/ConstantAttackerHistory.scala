package pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger

import argonaut.DecodeJson


case object ConstantAttackerHistory extends AttackerHistoryChanger {
  override def newHistoryLength(initialHist: Int, currHist: Int, maxHist: Int, iteration: Int, oscillating: Boolean) : Int = maxHist

  def instance = ConstantAttackerHistory
  implicit val decoder : DecodeJson[AttackerHistoryChanger] = DecodeJson.UnitDecodeJson.map(x => ConstantAttackerHistory)
}


