package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse

import com.google.common.collect.{ ImmutableList, ImmutableMap }
import java.util.List
import org.apache.commons.math3.util.Pair
import pl.edu.pw.mini.jk.lib.CollectionHelpers


case class AttackerSolverResult[AM](strategy: ImmutableMap[ImmutableList[AM],java.lang.Double], exact: Boolean) {
  def strategyAsPairs : ImmutableList[Pair[ImmutableList[AM], java.lang.Double]] = CollectionHelpers.mapToPairs(strategy)
}

object AttackerSolverResult {
  def fromPairs[AM](strategy: List[Pair[ImmutableList[AM], java.lang.Double]], exact: Boolean) : AttackerSolverResult[AM] = AttackerSolverResult(
    CollectionHelpers.pairsToMap(strategy),
    exact
  )
}
