package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracleFactory
import pl.edu.pw.mini.jk.sg.uct.I2UctConfig
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.{
  AdvanceableAttackerState,
  AdvanceableDefenderState
}
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.mixeduct.{DefenderOracle, TreeLogConfig}

final case class UctTabooPathImproveFactory(
    i2UctConfig: I2UctConfig[SecurityGame],
    stepEps: Double
) extends DefenderOracleFactory {
  def apply[DM <: DefenderMove, AM, DS <: AdvanceableDefenderState[DM], AS <: AdvanceableAttackerState[
    AM
  ], G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
    G
  ]](
      gameMatrices: GameMatrices[AM, DM, AS, DS, G],
      treeLogConfig: TreeLogConfig
  ): DefenderOracle[DM, AM, DS, AS, G] =
    new UctTabooPathImprove[DM, AM, DS, AS, G](i2UctConfig, stepEps, gameMatrices)
}
