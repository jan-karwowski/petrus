package pl.edu.pw.mini.jk.sg.mixeduct.transfer.filters

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

class WeightedTree[DM <: DefenderMove, DS](val tree: UctNode[DS, DM], val weight: Double)
