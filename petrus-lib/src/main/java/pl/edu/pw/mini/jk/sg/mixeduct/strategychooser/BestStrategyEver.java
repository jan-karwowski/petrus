package pl.edu.pw.mini.jk.sg.mixeduct.strategychooser;

import java.util.Optional;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult;

public class BestStrategyEver<DM extends DefenderMove, AM> implements StrategyChooser<DM, AM> {
    private Optional<IterationResult<DM, AM, ?, ?, ?>> bestStrategy = Optional.empty();

    public BestStrategyEver() {
    }

    @Override
    public void registerStrategy(IterationResult<DM, AM, ?, ?, ?> iterationResult) {
	final double newScore = iterationResult.givenPayoff().defenderPayoff();
        if (!bestStrategy.isPresent() || (newScore > bestStrategy.get().optimalAttackerPayoff().defenderPayoff() && iterationResult.optimalAttackerPayoff().defenderPayoff() > bestStrategy.get().optimalAttackerPayoff().defenderPayoff())) {
            bestStrategy = Optional.of(iterationResult);
        } else {
        }
    }
    
    @Override
    public IterationResult<DM, AM, ?, ?, ?> getBestIterationResult() {
	if (!bestStrategy.isPresent())
            throw new IllegalStateException("No strategy was registered");

        return bestStrategy.get();
    }
}
