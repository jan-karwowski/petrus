package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions

import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView;
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

class VisitsCount

object VisitsCount extends SimpleNodeProbabilityWeightFunction {
  lazy val instance = this;
  override def probabilityWeight(move: UnmodifiableDecisionStatisticsView, node: UctNode[_, _]) = move.realVisitsCount.toDouble;
}
