package pl.edu.pw.mini.jk.sg.mixeduct.attacker
import pl.edu.pw.mini.jk.sg.mixeduct.IterationResult

import scala.collection.mutable.{HashMap => MMap}
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer
import org.apache.commons.math3.distribution.EnumeratedDistribution
import org.apache.commons.math3.util.Pair
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove

final class TrendWeightedHistoryAttacker[M](
    val baseWeight: Double,
    val damping: Double,
    val cutoff: Double,
    val weightBias: Double,
    val strategy: MMap[ImmutableList[M], Double],
    val rng: RandomGenerator
) extends AttackerWithHistory[M] {
  var prevDefenderPayoff: Option[Double] = None

  override def getInstance(): AttackerStrategy[M] = {
    new SimpleAttackerStrategy(new EnumeratedDistribution(rng, strategy.toSeq.map {
      case (str, prob) => new Pair(str, new java.lang.Double(prob))
    }.asJava))
  }
  override def registerIterationResult[DM <: DefenderMove](
      iteration: Int,
      iterationResult: IterationResult[DM, M, _, _, _],
      trainingAttacker: AttackerStrategy[M]
  ): Unit = {
    val currDefenderPayoff = iterationResult.optimalAttackerPayoff.defenderPayoff
    val weight = baseWeight * (prevDefenderPayoff match {
      case Some(prev) => prev - currDefenderPayoff
      case None       => 1.0
    }) + weightBias
    prevDefenderPayoff = Some(currDefenderPayoff)

    val toDelete = ArrayBuffer[ImmutableList[M]]()
    iterationResult.optimalAttacker.asScala.foreach {
      case (str, prob) =>
        strategy.get(str) match {
          case None if weight > 0                            => strategy += ((str, prob * weight))
          case Some(oldProb) if oldProb + prob * weight <= 0 => toDelete += str
          case Some(oldProb)                                 => strategy.update(str, oldProb + prob * weight)
          case _                                             => ()
        }
    }

    if (toDelete.size == strategy.size) {
      strategy.keys.foreach(str => strategy.update(str, baseWeight))
    } else {
      toDelete.foreach(str => strategy -= str)
      strategy.foreach { case (st, pr) => strategy.update(st, pr * damping) }
      val max = strategy.values.max
      if (max > cutoff) {
        strategy.foreach { case (st, pr) => if (pr < cutoff) strategy -= st }
      }
    }
  }
}
