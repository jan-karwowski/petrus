package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.SinglePlayerGame
import pl.edu.pw.mini.jk.sg.uct.PayoffInfo
import java.{util => ju}
import scala.collection.JavaConverters._
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.game.common.Accident
import com.google.common.collect.ImmutableList

final class TabooGame[M, S <: AdvanceableState[M], G <: SinglePlayerGame[M, S] with SecurityGame](
    originalGame: G,
    var taboo: Option[TabooTree[M]]
) extends SinglePlayerGame[M, S]
    with SecurityGame {
  def getCurrentState(): S = originalGame.getCurrentState
  def makeMove(move: M): PayoffInfo = {
    taboo = taboo.flatMap(_.next(move))
    originalGame.makeMove(move)
  }
  def getTimeStep(): Int = originalGame.getTimeStep()
  def possibleMoves(): ju.List[M] =
    originalGame
      .possibleMoves()
      .asScala
      .filter(m => !taboo.map(_.isBlocked(m)).getOrElse(false))
      .asJava

  def randomMove(rng: RandomGenerator): M = {
    val pm = possibleMoves()
    pm.get(rng.nextInt(pm.size()))
  }

  def accidentOccured(): Boolean = originalGame.accidentOccured()
  def getPreviousAccidents(): ju.List[Accident] = originalGame.getPreviousAccidents()

  def forbidSeq(moves: ImmutableList[M]): Unit = {
    taboo = taboo match {
      case Some(t) =>
        Some(t.forbidMoveSequence(moves.asScala.toList, originalGame.getCurrentState())._1)
      case None =>
        Some(
          (new TabooTree(Map()))
            .forbidMoveSequence(moves.asScala.toList, originalGame.getCurrentState())
            ._1
        )
    }
  }
}
