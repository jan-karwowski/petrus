package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoinerFunctions  }

case object MeanCountJoin extends TreeJoinerFunctions {
  private val quantizationLevel = 100000

  override type CS = Double
  override type GS = Unit

  override def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localMean: Double, x : Unit) : UCTDecisionStatistics = {
    val q = stats.count(_.averagePayoff >= localMean).toDouble/treeCount.toDouble
    new UCTDecisionStatistics((q * quantizationLevel).toInt, (q * quantizationLevel).toInt, q)
}
  override def calculateContextStats(stats: => Iterable[UctReadableStatistics]) : Double =  stats.map(_.averagePayoff).sum/stats.size  
  override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) : Unit = Unit

}
