package pl.edu.pw.mini.jk.sg.mixeduct.attacker;


import java.util.List;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.ImmutableList;

public interface AttackerStrategy<AM> {
    ImmutableList<AM> sample();
    List<Pair<ImmutableList<AM>, Double>> getPmf();
}
