package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import _root_.pl.edu.pw.mini.jk.sg.game.common.Cloneable
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import com.google.common.collect.ImmutableList

trait ReducingBestMoveStepMixin[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
] {
  this: AbstractBestMoveOracle[DM, AM, DS, AS, G] =>
  def reducingStepSearch(initialStep: Double, stepEps: Double)(
      currPayoff: Double,
      stepSearchAction: (Double) => Option[Map[ImmutableList[DM], Double]]
  ): Option[Map[ImmutableList[DM], Double]] =
    Stream
      .iterate(initialStep)(_ / 2.0)
      .takeWhile(_ >= stepEps)
      .flatMap(step => stepSearchAction(step))
      .find(isImprovementDirection(currPayoff, _))
}
