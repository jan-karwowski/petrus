package pl.edu.pw.mini.jk.sg.mixeduct.transfer.reduction

import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

trait StatisticsReduction {
  def apply(stats: UCTDecisionStatistics) : UCTDecisionStatistics
  def apply() : UCTDecisionStatistics => UCTDecisionStatistics = (x => apply(x))
}
