package pl.edu.pw.mini.jk.sg.mixeduct.transfer.prune

import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.tree.{ PruneTreeFunctions, UctNode }

object KeepAll extends PruneTreeFunctions {
  override type T = Unit

  override def movePredicate(statistics: UctReadableStatistics, nodeStat: Unit): Boolean = true
  override def nodeStatistics(node: UctNode[_, _]): Unit = Unit
}
