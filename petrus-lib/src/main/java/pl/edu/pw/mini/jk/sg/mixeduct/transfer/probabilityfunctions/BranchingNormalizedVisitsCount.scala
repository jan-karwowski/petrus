package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions

import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

class BranchedNormalizedVisitsCount 

object BranchedNormalizedVisitsCount extends SimpleNodeProbabilityWeightFunction {
  override def probabilityWeight(move: UnmodifiableDecisionStatisticsView, node: UctNode[_,_]) = 
    move.realVisitsCount.toDouble / node.getRealVisitsCount.toDouble * node.moveCount
  lazy val instance = this;
}
