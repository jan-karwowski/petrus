package pl.edu.pw.mini.jk.sg.mixeduct.transfer.transformers
import argonaut.DecodeJson
import pl.edu.pw.mini.jk.sg.uct.tree.PayoffTransformer
import pl.edu.pw.mini.sg.json.FlatTraitJsonCodec
import pl.edu.pw.mini.sg.json.TraitSubtypeCodec

object PayoffTransformerJson {
  val decode : DecodeJson[PayoffTransformer] = new FlatTraitJsonCodec[PayoffTransformer](List(
    TraitSubtypeCodec.singletonCodec(IdentityTransformer, "identity"),
    TraitSubtypeCodec.singletonCodec(NormalizeTransformer, "normalize")
  ))
}
