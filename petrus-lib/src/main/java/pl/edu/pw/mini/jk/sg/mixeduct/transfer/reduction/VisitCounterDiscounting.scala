package pl.edu.pw.mini.jk.sg.mixeduct.transfer.reduction

import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

final case class VisitCounterDiscounting(coeff: Double) extends StatisticsReduction {
  override def apply(stats: UCTDecisionStatistics): UCTDecisionStatistics =
    new UCTDecisionStatistics((stats.visitsCount*coeff).ceil.toInt, stats.realVisitsCount, stats.averagePayoff)
}
