package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight

object PayoffExponentialWeight extends RankPayoffWeightFunction {
  def apply(payoff: Double, maxPayoff: Double, minPayoff: Double, rank: Int): Double =
    Math.exp(payoff - maxPayoff)
}
