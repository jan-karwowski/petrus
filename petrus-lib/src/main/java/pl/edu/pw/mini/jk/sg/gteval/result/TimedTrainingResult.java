package pl.edu.pw.mini.jk.sg.gteval.result;

import argonaut.Json;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices;
import squants.time.Time;



public class TimedTrainingResult<DM extends DefenderMove, AM> extends TrainingResult<DM,AM> {
    private final long timeMs;

    public TimedTrainingResult(final ImmutableMap<ImmutableList<DM>, Double> defenderStrategy, final ImmutableMap<ImmutableList<AM>, Double> attackerStrategy,
			       final GameMatrices<AM,DM,?,?,?> gameMatrices, long timeMs) {
	super(defenderStrategy, attackerStrategy, gameMatrices);
	this.timeMs = timeMs;
    }

    @Override
    public Json getTrainingResultAsJson(final String methodName, final Json methodConfig, final Time preprocessingTime) {
	return getGameMatrices().getNoaiFactory().defenderResultAsJson(getDefenderStrategy(), timeMs/1000.0, methodName, methodConfig, preprocessingTime);
    }

    
    @Override
    public String methodSpecificStatistics() {
	return "Time: "+timeMs+"\n";
    }
}

