package pl.edu.pw.mini.jk.sg.mixeduct.attacker.weight

object OnlyBestStrategy extends RankPayoffWeightFunction {
  override def apply(payoff: Double, maxPayoff: Double, minPayoff: Double, rank: Int): Double =
    if(rank == 0) 1
    else 0
}
