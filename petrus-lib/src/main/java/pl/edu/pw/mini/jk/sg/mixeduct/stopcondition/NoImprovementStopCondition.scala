package pl.edu.pw.mini.jk.sg.mixeduct.stopcondition

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove

class NoImprovementStopCondition[DM <: DefenderMove,DS](noImprovementIterations: Int) extends StopCondition[DM, DS] {
  var lastImproved : Option[(Int, Double)] = None
  var currentIteration = 0

  def isStopCondition: Boolean = lastImproved.map(_._1+ noImprovementIterations < currentIteration).getOrElse(false)

  def registerTrainingResult(result: pl.edu.pw.mini.jk.sg.mixeduct.IterationResult[DM, DS, _, _, _]): Unit = {
    currentIteration = currentIteration + 1
    lastImproved match {
      case None => lastImproved = Some((currentIteration, result.givenPayoff.defenderPayoff))
      case Some((li, lv)) => if(result.givenPayoff.defenderPayoff > lv)
        lastImproved = Some((currentIteration, result.givenPayoff.defenderPayoff))
    }
  }
}
