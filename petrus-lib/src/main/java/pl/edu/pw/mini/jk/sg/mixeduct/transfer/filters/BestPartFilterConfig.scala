package pl.edu.pw.mini.jk.sg.mixeduct.transfer.filters

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder


object BestPartFilterFactoryDecode {
  def decode(decoders: Map[String, DecodeJson[_ <: BestPartFilterFactory]]) : DecodeJson[BestPartFilterFactory] = TraitJsonDecoder[BestPartFilterFactory](decoders).decoder

  val decodePassAll : DecodeJson[PassAllFilterFactory] = DecodeJson.jdecode1L((x: Int) => new PassAllFilterFactory(x))("treeCount")

  val decodeBestTree : DecodeJson[BestTreeFilterFactory] = DecodeJson.jdecode1L((x: Int) => new BestTreeFilterFactory(x))("bestTreeCount")

  val defaultDecode = decode(Map("passAll" -> decodePassAll, "bestTree" -> decodeBestTree))
}
