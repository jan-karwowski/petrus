package pl.edu.pw.mini.jk.sg.mixeduct
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import _root_.pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import _root_.pl.edu.pw.mini.jk.sg.game.common.Cloneable
import _root_.argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.BestMoveOracleFactory
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.ReducingBestMoveOracleFactory
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.ReducingBestMoveOracleBiDiFactory
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame.UctTabooPathImproveFactory
import pl.edu.pw.mini.jk.sg.uct.I2UctConfig
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.RandomDirectionBestMoveFactory
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.HybridBestMoveRandomOracleFactory
import pl.edu.pw.mini.jk.sg.mixeduct.bestmove.HybridLimitedBestMoveRandomOracleFactory

trait DefenderOracleFactory {
  def apply[
      DM <: DefenderMove,
      AM,
      DS <: AdvanceableDefenderState[DM],
      AS <: AdvanceableAttackerState[AM],
      G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with Cloneable[G]
  ](
      gameMatrices: GameMatrices[AM, DM, AS, DS, G],
      treeLogConfig: TreeLogConfig
  ): DefenderOracle[DM, AM, DS, AS, G]
}


object DefenderOracleFactory {
  val uctOracleDecode = {
    import pl.edu.pw.mini.jk.sg.mixeduct.transfer.prune.PruneJson._
    import pl.edu.pw.mini.jk.sg.mixeduct.transfer.AttackersInformationTransferFactory.defaultDecoder
    implicit val i2d =  pl.edu.pw.mini.jk.sg.uct.I2UctConfig.defaultDecode
    DecodeJson.jdecode3L(UctDefenderOracleConfig.apply)(
      "informationTransfer", "i2Uct", "pruneFunction"
    )
  }

  val reducingBestMoveDecode = DecodeJson.jdecode1L(ReducingBestMoveOracleFactory.apply)("stepEps")
  val reducingBestMoveBiDiDecode = DecodeJson.jdecode1L(ReducingBestMoveOracleBiDiFactory.apply)("stepEps")

  private implicit val i2uct = I2UctConfig.defaultDecode
  val uctBestMoveDecode = DecodeJson.jdecode2L(UctTabooPathImproveFactory.apply)("i2uct", "stepEps")
  val randomDir = DecodeJson.jdecode2L(RandomDirectionBestMoveFactory.apply)("samples", "stepEps")
  val hybridRandomDir = DecodeJson.jdecode2L(HybridBestMoveRandomOracleFactory.apply)("stepEps", "samples")
  val lHybridRandomDir = DecodeJson.jdecode3L(HybridLimitedBestMoveRandomOracleFactory)("stepEps", "samples", "dimensions")

  val defaultDecoder : DecodeJson[DefenderOracleFactory] = new TraitJsonDecoder[DefenderOracleFactory](
    Map(
      "uct" -> uctOracleDecode,
      "exhaustiveIncrement" -> DecodeJson.UnitDecodeJson.map(_ => BestMoveOracleFactory),
      "reducingExhaustiveIncrement" -> reducingBestMoveDecode,
      "reducingExhaustiveIncrementBiDi" -> reducingBestMoveBiDiDecode,
      "uctTabooIncrement" -> uctBestMoveDecode,
      "randomDirection" -> randomDir,
      "hybridRandomDirection" -> hybridRandomDir,
      "limitedHybridRandomDirection" -> lHybridRandomDir
    )
  ).decoder
}
