package pl.edu.pw.mini.jk.sg.opt.util;

public class ColumnValue {
    private final String name;
    private final double value;

    public ColumnValue(String name, double value) {
        super();
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

}
