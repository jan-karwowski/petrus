package pl.edu.pw.mini.jk.sg.mixeduct.transfer.probabilityfunctions
import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode

object GlobalQLeafWeight extends NodeProbabilityWeightFunction {
  override type GlobalStat = (Double, Double)
  override def calculateGlobalStat(stats: => Seq[UnmodifiableDecisionStatisticsView]): (Double, Double) =
    (stats.map(_.averagePayoff).min, stats.map(_.averagePayoff).max)
  override def probabilityWeight(statistics: UnmodifiableDecisionStatisticsView, node: UctNode[_, _], stat: (Double, Double)): Double =
    if(stat._1 == stat._2) 1
    else statistics.averagePayoff-stat._1

  override val leafOnlyWeight = true
}
