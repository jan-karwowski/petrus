package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join

import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions
import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

final case class OnlyMostVisitedJoiner(portion: Double)  extends TreeJoinerFunctions {
  override type GS = Unit
  override type CS = Unit

  override def calculateContextStats(stats: => Iterable[UctReadableStatistics]): Unit = ()

  override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]): Unit = ()

  override def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit): UCTDecisionStatistics = {
    val threshold = {
      val ss = stats.map(_.visitsCount).toVector.sorted
      ss(ss.length*(1-portion).toInt)
    }
    val s = UCTDecisionStatistics.zeroStats()
    stats.filter(_.visitsCount >= threshold).foreach(s.appendOther)
    s
  }
}
