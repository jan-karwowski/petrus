package pl.edu.pw.mini.jk.sg.mixeduct.bestmove
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.DefenderAttackerGame
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.mixeduct.attacker.AttackerStrategy
import pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.OptimalAttacker
import com.google.common.collect.ImmutableMap
import pl.edu.pw.mini.jk.sg.mixeduct.DefenderOracle
import scala.collection.mutable.{HashMap => MMap}
import org.log4s._
import scala.collection.JavaConverters._
import org.apache.commons.math3.random.RandomDataGenerator

final class RandomDirectionBestMove[
    DM <: DefenderMove,
    AM,
    DS <: AdvanceableDefenderState[DM],
    AS <: AdvanceableAttackerState[AM],
    G <: DefenderAttackerGame[AM, DM, AS, DS, _] with SecurityGame with pl.edu.pw.mini.jk.sg.game.common.Cloneable[
      G
    ]
](
    gameMatrices: GameMatrices[AM, DM, AS, DS, G],
    directionsToTry: Int,
    stepEps: Double
) extends AbstractBestImprove[DM, AM, DS, AS, G](gameMatrices) with RandomDirectionMixin[DM, AM, DS, AS, G] {


  def bestImprovementStep(attackerProbabilities: AttackerStrategy[AM])(
      implicit rng: RandomGenerator
  ): Option[Map[ImmutableList[DM], Double]] = {
    val bestResponse =
      if (currStrategy.isEmpty) attackerProbabilities.getPmf()
      else OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, currStrategy)
    val attStr = AbstractBestImprove.strategyListToMap(bestResponse)
    val currPayoff =
      if (currStrategy.isEmpty) Double.NegativeInfinity
      else {
        val currAtt = AbstractBestImprove.strategyListToMap(
          OptimalAttacker.solveAttackerOptimalStrategyPlus(gameMatrices, currStrategy)
        )
        gameMatrices.getExpectedPayoff(ImmutableMap.copyOf(currStrategy), currAtt).defenderPayoff
      }

    def localQuality(dir: Map[ImmutableList[DM], Double]) : Double = {
      val str = bumpDirection(dir)
      gameMatrices.getExpectedPayoff(str, attStr).defenderPayoff
    }


    Stream
      .iterate(1.0)(_ / 2.0)
      .takeWhile(_ >= stepEps)
      .flatMap(step => Stream.tabulate(directionsToTry)(_ => randomDirection(step)).sortBy(-localQuality(_)))
      .find { dir =>
        isImprovementDirection(currPayoff, dir)
      }
  }
}
