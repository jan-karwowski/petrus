package pl.edu.pw.mini.jk.sg.mixeduct.attackerresponse.uct

import pl.edu.pw.mini.jk.sg.mixeduct.util.TreeUtils
import scala.collection.JavaConverters._
import com.google.common.collect.ImmutableList
import org.apache.commons.math3.random.RandomGenerator
import org.apache.commons.math3.util.Pair
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.gteval.GameMatrices
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode


object BestPathStrategy extends TreeToStrategy {
  override def treeToStrategy[AM, AS <: AdvanceableAttackerState[AM]](uctTree: UctNode[AS,AM], gameMatrices: GameMatrices[AM, _, AS, _, _], rng: RandomGenerator) : ImmutableList[Pair[ImmutableList[AM],java.lang.Double]] = 
    ImmutableList.of(new Pair(
     ImmutableList.copyOf(TreeUtils.getBestPathInTreeExtended(uctTree, gameMatrices.getGameLength, rng).asJava.asInstanceOf[java.lang.Iterable[AM]]), 1)
   );
}
