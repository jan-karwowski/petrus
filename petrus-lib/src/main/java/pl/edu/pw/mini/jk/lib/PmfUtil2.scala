package pl.edu.pw.mini.jk.lib
import java.{util => ju, lang => jl}
import com.google.common.collect.ImmutableList

import scala.collection.JavaConverters._
import com.google.common.collect.ImmutableMap

object PmfUtil2 {
  def normalizePmf[DM](distribution: ju.Map[ImmutableList[DM], jl.Double]) : ImmutableMap[ImmutableList[DM], jl.Double] = {
    val psum = distribution.asScala.map(_._2.toDouble).sum
    require(psum > 0, s"Non-positive psum: $psum")
    ImmutableMap.copyOf(distribution.asScala.map{case (k, p) => (k, new jl.Double(p/psum))}.toMap.asJava)
  }
}
