package pl.edu.pw.mini.jk.lib

import java.util.Optional

import scala.language.implicitConversions

object JavaInterop {
  implicit def toJavaOptional[T](x: Option[T]) : Optional[T] = x match {
    case None => Optional.empty[T]
    case Some(x) => Optional.of(x)
  }
}
