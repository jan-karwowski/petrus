package pl.edu.pw.mini.jk.sg.mixeduct
import better.files.File
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import java.util.function.Consumer

sealed trait TreeLogConfig {
  def checkLog(iteration: Int) : Boolean
  def logFile(iteration: Int) : File

  def log(iteration: Int, tree: UctNode[_,_]) : Unit =
    if(checkLog(iteration)) 
      TreeLogHelper.log(iteration, tree)(this)
    else
      ()

  def log(iteration: Int) : Consumer[UctNode[_,_]] =
    t => log(iteration, t)
}

final case class TreeLogConfigEnable(
  logEveryN: Int,
  logFileDir: File
) extends TreeLogConfig {
  def this(logEveryN: Int, logFileDir: String) = this(logEveryN, File(logFileDir))

  override def checkLog(iteration: Int): Boolean = iteration > 1 && (iteration % logEveryN == 0)
  override def logFile(iteration: Int): File = logFileDir / s"it${iteration}.tree"
}

object TreeLogConfigDisable extends TreeLogConfig {
  override def checkLog(iteration: Int) : Boolean = false
  override def logFile(iteration: Int): File = ???
}
