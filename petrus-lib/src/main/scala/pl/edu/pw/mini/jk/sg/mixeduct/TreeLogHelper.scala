package pl.edu.pw.mini.jk.sg.mixeduct
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import pl.edu.pw.mini.jk.sg.uct.tree.export.ExportUctTree

object TreeLogHelper {
  def log[S, M](iteration: Int, tree: UctNode[S, M])(implicit tlc: TreeLogConfig) : Unit = {
    val logFile = tlc.logFile(iteration)
    logFile.parent.createDirectories()
    logFile.writeBytes(
      ExportUctTree.codec.encode(tree.exportTree.bimap(_.toString, _.toString)).require.bytes.toIterable.iterator
    )
  }
}
