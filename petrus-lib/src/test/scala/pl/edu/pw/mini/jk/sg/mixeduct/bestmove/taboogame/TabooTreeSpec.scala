package pl.edu.pw.mini.jk.sg.mixeduct.bestmove.taboogame
import org.scalatest.WordSpec
import org.scalatest.Matchers
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import com.google.common.collect.ImmutableList
import scala.collection.JavaConverters._

final class TabooTreeSpec extends WordSpec with Matchers {
  final case class State(succ: Map[Int, State]) extends AdvanceableState[Int] {
    def getTimeStep(): Int = ???
    def advance(move: Int): State = succ.get(move).get
    def getPossibleMoves(): ImmutableList[Int] = ImmutableList.copyOf(succ.keys.asJava)
  }

  "game with two levels" when {
    val g = State(Map(
      1 -> State(Map(1 -> State(Map()), 2 -> State(Map()))),
      2 -> State(Map(1 -> State(Map())))
    ))

    "blocked sequence 2,1" should {
      val taboo = (new TabooTree(Map())).forbidMoveSequence(List(2,1), g)._1
      "not block move 1" in {
        taboo.isBlocked(1) should be(false)
      }

      "block move 2" in {
        taboo.isBlocked(2) should be(true)
      }
    }

    "blocked sequence 1,2" should {
      val taboo = (new TabooTree(Map())).forbidMoveSequence(List(1,2), g)._1

      "not block move 1" in {
        taboo.isBlocked(1) should be(false)
      }

      "not block move 2" in {
        taboo.isBlocked(2) should be(false)
      }

      "not block move 1,1" in {
        taboo.next(1).map(_.isBlocked(1)) should be(Some(false))
      }

      "block move 1,2" in {
        taboo.next(1).map(_.isBlocked(2)) should be(Some(true))
      }
    }

    "blocked sequence 1,2 and 1,1" should {
       val taboo = (new TabooTree(Map())).forbidMoveSequence(List(1,2), g)._1.forbidMoveSequence(List(1,1), g)._1

      "block move 1" in {
        taboo.isBlocked(1) should be(true)
      }

      "not block move 2" in {
        taboo.isBlocked(2) should be(false)
      }

      "block move 1,1" in {
        taboo.next(1).map(_.isBlocked(1)) should be(Some(true))
      }

      "block move 1,2" in {
        taboo.next(1).map(_.isBlocked(2)) should be(Some(true))
      }
    }
  }
}

