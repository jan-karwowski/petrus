package pl.edu.pw.mini.jk.sg.mixeduct.transfer.join;

import argonaut.Json
import org.scalatest.WordSpec
import pl.edu.pw.mini.jk.sg.uct.tree.TreeJoinerFunctions

class ReadTreeJoinMethodSpec extends WordSpec {
  "ReadTreeJoin" should {
    import ReadTreeJoinMethod.decodeFunctions

    "read visit weighted join" in {
      assert(Json.jString("VISIT_WEIGHTED_JOIN").as[TreeJoinerFunctions].value.get == VisitWeightedJoin)
    }

    "not read XYZZY" in {
       assert(Json.jString("XYZZY").as[TreeJoinerFunctions].isError)
    }

    "not read {}" in {
      assert(Json.jEmptyObject.as[TreeJoinerFunctions].isError)
    }
  }
}
