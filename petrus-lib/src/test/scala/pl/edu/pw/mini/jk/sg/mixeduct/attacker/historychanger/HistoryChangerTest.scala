package pl.edu.pw.mini.jk.sg.mixeduct.attacker.historychanger

import org.scalatest._
import argonaut._

class HistoryChangerDecoderSpec extends WordSpec {
  val reducingAttackerHistory = """{"reductionCoefficient": 0.9}"""
  val oscilationExtendingHistory = Json(("increaseRatio", Json.jNumber(1.5)))

  "ConstantHistoryChanger" should {
    implicit val decoder = ConstantAttackerHistory.decoder
    "parse empty json" in {
      assert(Parse.decode("""{}""") == (Right(ConstantAttackerHistory)))
    }

    "fail to parse json with field" in {
      assert(Parse.decode("""{"val":"v"}""").isLeft)
    }
  }

  "ReducingHistoryChanger" should {
    implicit val decoder = ReducingAttackerHistory.decoder

    "parse json with coefficient" in {
      assert(Parse.decode(reducingAttackerHistory) == (Right(ReducingAttackerHistory(0.9))))
    }

    "fail to parse json with additional field" in {
      assert(Parse.decode("""{"reductionCoeffcient": 0.9, "a":"b"}""").isLeft)
    }

    "fail to parse empty json" in {
      assert(Parse.decode("{}").isLeft)
    }
  }

  "OscilationExtendingChanger" should {
    implicit val decoder = OscilationExtendingHistoryChanger.decoder

    "parse json with increaseRatio" in {
      assert(oscilationExtendingHistory.as(decoder).toEither == Right(OscilationExtendingHistoryChanger(1.5)))
    }

    "not parse empty json" in {
      assert(Json.jObjectFields().as(decoder).isError)
    }
  }

  "AttackerHistoryChanger" should {
    implicit val decoder = new AttackerHistoryDecode(Map("constant" -> ConstantAttackerHistory.decoder,
      "reducing" -> ReducingAttackerHistory.decoder,
      "extending" -> OscilationExtendingHistoryChanger.decoder)).decoder

    "parse constant" in {
      assert(Json(("name", Json.jString("constant")), ("config", Json.jEmptyObject)).as(decoder).toEither == Right(ConstantAttackerHistory))
    }

    "parse oscilation extending" in {
      assert(Json(("name", Json.jString("extending")), ("config", oscilationExtendingHistory)).as(decoder).toEither == Right(OscilationExtendingHistoryChanger(1.5)))
    }
  }
}
