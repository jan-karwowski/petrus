package pl.edu.pw.mini.jk.sg.mixeduct.util
import org.scalatest.WordSpec
import org.scalatest.Matchers
import pl.edu.pw.mini.jk.sg.uct.tree.normal.TreeNode
import pl.edu.pw.mini.jk.sg.uct.tree.UctNodeStatisticsContainer
import scala.collection.mutable
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics
import org.scalamock.scalatest.MockFactory
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import scala.collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.uct.tree.UctMoveStatistics
import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import com.google.common.collect.ImmutableList

final class TreeUtilsSpec extends WordSpec with Matchers with MockFactory {
  final class State(id: Int) extends AdvanceableState[Int] {
    override def advance(move: Int): AdvanceableState[Int] = new AdvanceableState[Int] {
      override def advance(move: Int): AdvanceableState[Int] = ???
      override def getTimeStep(): Int = 1
      override def getPossibleMoves(): ImmutableList[Int] = ImmutableList.of()
    }
    override def getTimeStep(): Int = 0
    override def getPossibleMoves(): ImmutableList[Int] = ImmutableList.of(1,2,3)
  }

  "getBestPathInTree" when {
    "run with 1-level depp tree" should {

      val tree = mock[UctNode[State, Int]]
      
      (tree.getBestMove _) expects(*) onCall(_ => 3)
      (tree.getSuccessor _) expects(3) onCall(_ => None)
      (tree.getState _) expects() onCall(_ => new State(0))

      "return the best move without calling rng" in {
        val rng = mock[RandomGenerator]
        TreeUtils.getBestPathInTreeExtended(tree, 1, rng) should equal (List(3))
      }
    }
  }
}
