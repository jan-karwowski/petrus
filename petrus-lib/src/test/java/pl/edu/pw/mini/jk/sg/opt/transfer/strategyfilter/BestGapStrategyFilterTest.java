package pl.edu.pw.mini.jk.sg.opt.transfer.strategyfilter;



import java.util.Map;

import org.junit.Test;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import junit.framework.TestCase;
import pl.edu.pw.mini.jk.sg.mixeduct.transfer.strategyfilter.BestGapStrategyFilter;

public class BestGapStrategyFilterTest {

    @Test
    public void filterTest() {
        Map<ImmutableList<Integer>, Double>  strategy = ImmutableMap.<ImmutableList<Integer>, Double>builder()
                .put(ImmutableList.of(1, 2, 3, 4, 5), 0.2)
                .put(ImmutableList.of(1, 2, 3, 4, 4), 0.1)
                .put(ImmutableList.of(1, 2, 3, 4, 3), 0.01)
                .put(ImmutableList.of(1, 2, 3, 4, 2), 0.001)
                .put(ImmutableList.of(1, 2, 3, 4, 1), 0.0001)
                .build();
        
        final BestGapStrategyFilter<Integer> bestGapStrategyFilter = new BestGapStrategyFilter<>(0.01);
        
        final Map<ImmutableList<Integer>, Double> filteredStrategy = bestGapStrategyFilter.filterStrategy(strategy);
        
        TestCase.assertEquals(ImmutableMap.<ImmutableList<Integer>, Double>builder()
                .put(ImmutableList.of(1, 2, 3, 4, 5), 0.2)
                .put(ImmutableList.of(1, 2, 3, 4, 4), 0.1)
                .put(ImmutableList.of(1, 2, 3, 4, 3), 0.01)
                .build(), filteredStrategy);
    }

}
