package pl.edu.pw.mini.sg.bosansky

import argonaut.Parse
import org.scalactic.TolerantNumerics
import org.scalatest.{Matchers, WordSpec}
import pl.edu.pw.mini.sg.solver.gurobisolver.{GurobiConfig, GurobiSolver}
import pl.edu.pw.mini.sg.game.graph.sud.{DeterministicSingleUnitMoveGraphGame, SingleUnitAttackerState, SingleUnitDefenderState}
import pl.edu.pw.mini.sg.game.graph.{DeterministicGraphGameConfig, DeterministicGraphGameConfigJson}

import scala.io.Source

class PayoffSpec extends WordSpec with Matchers{
  implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.003)
  private val config = GurobiConfig().copy(threads = 1)


  "graph game 3a" should {
    val solver = new DeterministicGameSolver(config.solverFromConfig,
      new DeterministicGameUtilsProvider[Int, Int, SingleUnitDefenderState, SingleUnitAttackerState, DeterministicSingleUnitMoveGraphGame])

    import DeterministicGraphGameConfigJson.deterministicGraphGameConfigCodec
    val fileSource = Source.fromInputStream(getClass.getResourceAsStream("game3a-5.ggame"))
    val gameConfig = Parse.decode[DeterministicGraphGameConfig](fileSource.mkString) match {
      case Right(c) => c
    }
    fileSource.close()
    val game = DeterministicSingleUnitMoveGraphGame(gameConfig)
    "calculate optimal defender strategy" should {
      "defender payoff should be -.15" in {
        val (defenderStrategy, payoffs, preprocessTime) = solver.findStrategy(game)

        payoffs.defender should equal(-.15)
      }

    }
  }

  "graph game sb-96" should {
    val solver = new DeterministicGameSolver(config.solverFromConfig,
      new DeterministicGameUtilsProvider[Int, Int, SingleUnitDefenderState, SingleUnitAttackerState, DeterministicSingleUnitMoveGraphGame])

    import DeterministicGraphGameConfigJson.deterministicGraphGameConfigCodec
    val fileSource = Source.fromInputStream(getClass.getResourceAsStream("smallbuilding-96-5.ggame"))
    val gameConfig = Parse.decode[DeterministicGraphGameConfig](fileSource.mkString) match {
      case Right(c) => c
    }
    fileSource.close()
    val game = DeterministicSingleUnitMoveGraphGame(gameConfig)
    "calculate optimal defender strategy" should {
      "defender payoff should be 0.006" in {
        val (defenderStrategy, payoffs, preprocessTime) = solver.findStrategy(game)

        payoffs.defender should equal(0.006)
      }

    }
  }


}
