package pl.edu.pw.mini.sg.bosansky

import argonaut.Json
import argonaut.Argonaut._
import pl.edu.pw.mini.sg.results.MethodInfo
import pl.edu.pw.mini.sg.solver.SolverInfo


case class BosanskyMethodInfo(override val configFileName: String,
                              override val gitHash: String,
                              override val version: String,
                              solverInfo: SolverInfo) extends MethodInfo {
  override val methodName: String = "bosansky"

  override val additionalInformation: Json = {
    import SolverInfo.solverInfoCodec
    ("solverInfo" := solverInfoCodec.encode(solverInfo)) ->: jEmptyObject
  }

}
