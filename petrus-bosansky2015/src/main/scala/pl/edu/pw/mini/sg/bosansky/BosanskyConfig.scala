package pl.edu.pw.mini.sg.bosansky

import argonaut.CodecJson
import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.{ PayoffChanger, PayoffChangerJson }
import pl.edu.pw.mini.sg.solver.SolverConfig

final case class BosanskyConfig(M: Double, solverConfig: SolverConfig,
  timeLimitSeconds: Option[Double], payoffChanger: PayoffChanger,
  anchoringAlpha: Option[Double]
)


object BosanskyConfigJson {
  import SolverConfig.solverConfigCodec
  import PayoffChangerJson.payoffChangerCodec
  val bosanskyConfigCodec: CodecJson[BosanskyConfig] = CodecJson.casecodec5(BosanskyConfig.apply, BosanskyConfig.unapply)(
    "M", "solver", "timeLimitSeconds", "payoffChanger", "anchoringAlpha"
  )
}
