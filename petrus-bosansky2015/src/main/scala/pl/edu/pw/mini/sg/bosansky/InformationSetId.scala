package pl.edu.pw.mini.sg.bosansky

import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState

/**
  * Represents the id of a player's information set.
  * @param precedingMoves   all moves (extended with states in which they were done) which lead to the information set
  * @param reachedState     the state that is reached after executing all the moves
  * @tparam M               extended move type
  * @tparam S               state type
  */
case class InformationSetId[M, S <: PlayerVisibleState[M]](precedingMoves: List[ExtendedMove[M, S]], reachedState: S) {

}
