package pl.edu.pw.mini.sg.bosansky

import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState

/**
  * Represents a move extended with the state in which the move was done. This is used in order to have unique moves
  * (the same move in a different information set has a different name)
  * @param move             the player's move
  * @param precedingState   state in which the player has done the move
  * @tparam M               move type
  * @tparam S               state type
  */
case class ExtendedMove[M, S <: PlayerVisibleState[M]](move: M, precedingState: S) {
  override def toString: String = s"M($move,$precedingState)"
}
