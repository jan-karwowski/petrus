import java.io.File
import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}
import sbtcrossproject.CrossProject

lazy val publishOptions = Seq(  publishTo := {
    val mymvnrepo = "https://sg.mini.pw.edu.pl/mavenrepo/"
    if (version.value.trim.endsWith("SNAPSHOT"))
      Some("snapshots" at mymvnrepo + "snapshot")
    else
      Some("publishTo" at mymvnrepo + "release")
  } 
)
lazy val commonSettings = Seq(
  organization := "pl.edu.pw.mini.jk",
  version := "2.8.46",
  scalaVersion := "2.12.10",
  resolvers += "sgreleases" at "https://sg.mini.pw.edu.pl/mavenrepo/release",
  //resolvers += "JK Maven2 Repository" at "http://pages.mini.pw.edu.pl/~karwowskij/repo/"
  //scalacOptions := Seq("-opt:l:classpath","-unchecked", "-feature", "-deprecation")
  scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-Xdisable-assertions", "-Ywarn-unused-import"),
  javacOptions ++= Seq("-parameters", "-Xlint:unchecked"),
  packArchive := Seq(),
  credentials += Credentials(Path.userHome / ".sbt" / ".credentials"),
  libraryDependencies ++= Seq(
    "org.log4s" %%% "log4s" % "1.6.1",
    "org.scalatest" %%% "scalatest" % "3.0.1" % Test,
    "org.scalamock" %%% "scalamock-scalatest-support" % "3.5.0" % Test,
    "org.scalacheck" %%% "scalacheck" % "1.14.0" % Test
  ),
  testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-verbosity", "2"),
  testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oS")
) ++ publishOptions


val scodec = "org.scodec" %% "scodec-core" % "1.10.3"
val betterFiles = "com.github.pathikrit" %% "better-files" % "3.4.0"
val caffeine = "com.github.ben-manes.caffeine" % "caffeine" % "2.6.0"
val groovy = "org.codehaus.groovy" % "groovy-all" % "2.4.0"
val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
val scalacsv = "com.github.tototoshi" %% "scala-csv" % "1.3.5"
val commonsMath = "org.apache.commons" % "commons-math3" % "3.2"
val commonsIo = "commons-io" % "commons-io" % "2.5"
val guava = "com.google.guava" % "guava" % "17.0"
val protonpack = "com.codepoetics" % "protonpack" % "1.8"
val commonsCollections = "org.apache.commons" % "commons-collections4" % "4.0"
val jacksonAnnotations = "com.fasterxml.jackson.core" % "jackson-annotations" % "2.2.3"
val jacksonDatabind = "com.fasterxml.jackson.core" % "jackson-databind" % "2.2.3"
val opencsv = "net.sf.opencsv" % "opencsv" % "2.3"
val jtsCore = "com.vividsolutions" % "jts-core" % "1.14.0"
val canes = "pl.edu.pw.mini.jk" %% "canes-venatici" % "1.4.2"
val squantsDep = libraryDependencies += "org.typelevel"  %%% "squants"  % "1.3.0"
val argonautDep = libraryDependencies += "io.argonaut" %%% "argonaut" % "6.2"
val scalazDep = libraryDependencies += "org.scalaz" %%% "scalaz-core" % "7.2.8"
val scip = "de.zib" % "scip" % "5.0.2"

val javaTestDeps = Seq(
  "junit" % "junit" % "4.11" % Test,
  "org.hamcrest" % "hamcrest-core" % "1.3" % Test,
  "com.novocode" % "junit-interface" % "0.11" % Test,
  "org.mockito" % "mockito-core" % "2.7.14" % Test
)


lazy val execSettings = Seq(
  libraryDependencies += betterFiles,
  libraryDependencies += "org.rogach" %% "scallop" % "3.1.0",
  libraryDependencies += groovy,
  libraryDependencies += logback,
  publishArtifact := false
)

lazy val javaExecSettings = Seq(
  libraryDependencies += groovy,
  libraryDependencies += logback,
  libraryDependencies += "com.lexicalscope.jewelcli" % "jewelcli" % "0.8.9",
  publishArtifact := false
)

def setBuildInfo(packageName: String) = {
  val gitCommitString = SettingKey[String]("gitCommit")
  Seq(
    gitCommitString := git.gitHeadCommit.value.getOrElse("Not Set"),
    buildInfoKeys := Seq[BuildInfoKey](version, name, "gitCommit" -> git.gitHeadCommit.value.getOrElse("Not Set")),
    buildInfoPackage := packageName
  )
}

scalaVersion := "2.12.10"
scalaVersion in ThisBuild := "2.12.10"
fork := true

lazy val gurobiLib = sys.env.get("GUROBI_HOME").map(x => "gurobi" %% "gurobi" % "8.0.1" artifacts(Artifact("gurobi",
  (file(x) /"lib"/"gurobi.jar").toURL)))


lazy val rootJava = Project(id = "petrus", base = file(".")
).settings(commonSettings: _*)
.enablePlugins(PackPlugin).settings(
  packMain := Map(
    "petrus-gteval" -> "pl.edu.pw.mini.jk.sg.gteval.App",
    "petrus-dumpmatrix" -> "pl.edu.pw.mini.sg.dumpmatrix.DumpMatrix",
    "petrus-graphviz" -> "pl.edu.pw.mini.sg.graphviz.App",
    "petrus-graphviz2" -> "pl.edu.pw.mini.sg.graphviz.App2",
    "petrus-gtopt" -> "pl.edu.pw.mini.jk.sg.gtopt.App",
    "petrus-gamegen" -> "pl.edu.pw.mini.sg.gamegen.App",
    "petrus-chkconfig" ->  "pl.edu.pw.mini.jk.sg.chkconfig.ChkConfig",
    "petrus-se" -> "pl.edu.pw.mini.sg.se.SEFinder",
    "petrus-matrixgen" -> "pl.edu.pw.mini.sg.matrix.Generator",
//  "petrus-column-lp" -> "pl.edu.pw.mini.sg.colgen.ColGen", // nie zadziała bez włączonego colgen do dependsOn
    "petrus-matrix-calc" -> "pl.edu.pw.mini.sg.matrix.calc.Calc",
    "petrus-mtg-compactlp" -> "pl.edu.pw.mini.sg.compactlp.CompactLP",
    "petrus-efgen" -> "pl.edu.pw.mini.sg.efgame.gen.EfGen",
    "petrus-gamestat" -> "pl.edu.pw.mini.sg.stat.GameStat",
    "petrus-flipit-gen" -> "pl.edu.pw.mini.sg.game.flipit.generator.Generator",
    "petrus-flipit-test" -> "pl.edu.pw.mini.sg.game.flipit.test.Test",
    "petrus-graph-gen" -> "pl.edu.pw.mini.sg.gamegen.BuildingLikeGraphGeneratorDiversePenalty",
    "petrus-old-checker" -> "pl.edu.pw.mini.sg.checker.Checker",
    "petrus-strategy-checker" -> "pl.edu.pw.mini.sg.strategychecker.Checker"
  ),
  packJvmOpts := Map(
    "petrus-se" -> Seq("-Djava.library.path=$GUROBI_HOME/lib"),
    "petrus-mtg-compactlp" -> Seq("-Djava.library.path=$GUROBI_HOME/lib")
  ),
  packArchive := Seq(packArchiveTgz.value, packArchiveTxz.value),
  packResourceDir ++= Map(baseDirectory.value / "games" -> "games", baseDirectory.value / "configs" -> "configs", baseDirectory.value / "solver" -> "solver")
).aggregate(
  lib, gamegen, chkconfig, graphviz, gteval, gtopt, profile, mtg, gameJVM, libjsonJVM, mtgIT, i2uct, matrixGame, dumpmatrix, mcts, mixed, gameUtil, petrusSe, graphGameJVM, commonJVM, matrixGameGenerator, gamePluginLibrary, matrixCalc,
  searchGame, solver, bosansky, efGame, efGen, gameStat, graphLibJVM, flipitGame, flipitGen, gameJava,
  cacheProxyJVM, rngProxyJVM, flipitTest, newGraphGen, oldchecker, strategychecker, deterministicGames, newMatrixGame, appLib
).dependsOn (
  chkconfig, graphviz, gteval, gtopt, dumpmatrix, gamegen, petrusSe, matrixGameGenerator, matrixCalc, newGraphGen
//    , colgen
  , compactlp, efGen, gameStat, flipitGen, flipitTest, oldchecker, strategychecker
).settings(publishArtifact := false)

lazy val rootJS = CrossProject(id = "petrus", base = file("."))(JSPlatform).aggregate(
  game, libjson, rngProxy, cacheProxy, common, graphLib, graphGame
).settings(publishArtifact := false).settings(publishOptions: _*).js

lazy val game = CrossProject("petrus-game", file("petrus-game"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*).dependsOn(libjson, rngProxy).settings(
  squantsDep,
  scalazDep
)
lazy val gameJVM = game.jvm
lazy val gameJS = game.js
lazy val gameJava = Project("petrus-game-java", file("petrus-game-java")).settings(commonSettings: _*).dependsOn(game.jvm,libjson.jvm).settings(libraryDependencies ++= Seq(guava,commonsMath,commonsIo,jacksonAnnotations,protonpack, commonsCollections%Test)++javaTestDeps)
lazy val gameUtil = Project("petrus-game-util", file("petrus-game-util")).settings(commonSettings: _*).dependsOn(gameJVM, commonJVM)
lazy val mcts = Project("petrus-mcts", file("petrus-mcts")).settings(commonSettings: _*).dependsOn(gameJVM)
lazy val mixed = Project("petrus-mixed", file("petrus-mixed")).settings(commonSettings: _*).dependsOn(gameJVM % "test->test;compile->compile",mcts,gameUtil).settings(libraryDependencies += commonsMath)
lazy val i2uct = Project("petrus-i2uct", file("petrus-i2uct")).settings(commonSettings: _*).dependsOn(gameJava).settings(libraryDependencies ++= Seq(scodec, betterFiles, commonsCollections))
lazy val libjson = CrossProject("petrus-lib-json", file("petrus-lib-json"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*).settings(argonautDep)
lazy val libjsonJVM=libjson.jvm
lazy val libjsonJS=libjson.js
lazy val lib = Project("petrus-lib", file("petrus-lib")).settings(commonSettings: _*).dependsOn(i2uct).settings(libraryDependencies++=javaTestDeps)
lazy val gamegen = Project("petrus-gamegen", file("petrus-gamegen")).settings(commonSettings: _*).dependsOn(gameJava).settings(publishArtifact := false, libraryDependencies ++= Seq(jacksonAnnotations,jacksonDatabind)++javaTestDeps).settings(javaExecSettings: _*)
lazy val chkconfig = Project("petrus-chkconfig", file("petrus-chkconfig")).settings(commonSettings: _*).dependsOn(lib,gameJava,strategyMethods,deterministicGames).settings(publishArtifact := false).settings(execSettings: _*)
lazy val graphviz = Project("petrus-graphviz", file("petrus-graphviz")).settings(commonSettings: _*).dependsOn(gameJava,graphGameJVM).settings(execSettings: _*).settings(libraryDependencies ++= Seq(opencsv)++javaTestDeps)
lazy val gteval = Project("petrus-gteval", file("petrus-gteval")).settings(commonSettings: _*).dependsOn(lib, gamePluginLibrary, gameJava).enablePlugins(BuildInfoPlugin).settings(javaExecSettings: _*).settings(libraryDependencies++=javaTestDeps).settings(setBuildInfo("petrus.gteval"))
//lazy val colgen = Project("petrus-column-lp", file("petrus-column-lp")).settings(commonSettings: _*).dependsOn(lib).dependsOn(game).dependsOn(mtg, matrixGame).enablePlugins(BuildInfoPlugin)
lazy val dumpmatrix = Project("petrus-dumpmatrix", file("petrus-dumpmatrix")).settings(commonSettings: _*).dependsOn(lib, mtg, matrixGame, gameJava).enablePlugins(BuildInfoPlugin).settings(execSettings: _*).settings(libraryDependencies += scalacsv).settings(setBuildInfo("petrus.dumpmatrix"))
lazy val gtopt = Project("petrus-gtopt", file("petrus-gtopt")).settings(commonSettings: _*).dependsOn(lib).dependsOn( gameJava, gamePluginLibrary).settings(javaExecSettings: _*).settings(libraryDependencies++=javaTestDeps)
lazy val profile = Project("petrus-profile", file("petrus-profile")).settings(commonSettings: _*).dependsOn(gameJava).settings(publishArtifact := false, libraryDependencies += commonsCollections)
lazy val mtg = Project("petrus-mtg", file("petrus-mtg")).settings(commonSettings: _*).dependsOn(gameJava).settings(libraryDependencies ++= Seq(canes, jtsCore), squantsDep)
lazy val mtgIT = Project("petrus-mtg-integration-tests", file("petrus-mtg-integration-tests")).settings(commonSettings: _*).dependsOn(mtg, lib, gteval).settings(publishArtifact := false)
lazy val matrixGame = Project("petrus-matrix-game", file("petrus-matrix-game")).settings(commonSettings: _*).dependsOn(gameJava)
lazy val newMatrixGame = Project("petrus-new-matrix-game", file("petrus-new-matrix-game")).settings(commonSettings: _*).dependsOn(gameJVM, matrixGame)
lazy val matrixGameGenerator = Project("petrus-matrix-game-generator", file("petrus-matrix-game-generator")).settings(commonSettings: _*).dependsOn(matrixGame).settings(publishArtifact := false).settings(execSettings: _*)
lazy val graphGame = CrossProject("petrus-graph-game", file("petrus-graph-game"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*).dependsOn(game, common, graphLib, cacheProxy)
lazy val graphGameJVM = graphGame.jvm
lazy val graphGameJS = graphGame.js
lazy val petrusSe = Project("petrus-se", file("petrus-se")).settings(commonSettings: _*).dependsOn(gameJVM,deterministicGames,strategyMethods, commonJVM, appLib).enablePlugins(BuildInfoPlugin).settings(execSettings: _*).settings(setBuildInfo("petrus.se"))
lazy val common = CrossProject("petrus-common", file("petrus-common"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*).dependsOn(libjson)
lazy val commonJVM = common.jvm
lazy val commonJS = common.js
lazy val gamePluginLibrary = Project("petrus-game-plugin-library", file("petrus-game-plugin-library")).settings(commonSettings: _*).dependsOn(gameJava, mtg, matrixGame)
lazy val matrixCalc = Project("petrus-matrix-game-calc", file("petrus-matrix-game-calc")).settings(commonSettings: _*).dependsOn(newMatrixGame).settings(execSettings: _*)
lazy val bosansky = Project("petrus-bosansky2015", file("petrus-bosansky2015")).settings(commonSettings: _*).dependsOn(gameJVM, solver, mixed, commonJVM, graphGameJVM).enablePlugins(BuildInfoPlugin).settings(setBuildInfo("petrus.bosansky2015"))
lazy val correlatedStrategies = Project("petrus-correlated-strategies", file("petrus-correlated-strategies")).settings(commonSettings: _*).dependsOn(gameJVM, solver, bosansky, mixed, commonJVM).enablePlugins(BuildInfoPlugin)
lazy val compactlp = Project("petrus-mtg-compactlp", file("petrus-mtg-compactlp")).settings(commonSettings: _*).dependsOn(mtg, solver, commonJVM,appLib).enablePlugins(BuildInfoPlugin).settings(setBuildInfo("petrus.mtgcompactlp")).settings(execSettings: _*)
lazy val solver = {
  gurobiLib match {
    case None => Project("petrus-solver", file("petrus-solver")).settings(commonSettings: _*).dependsOn(libjsonJVM).settings(
      unmanagedSourceDirectories in Compile += baseDirectory.value / "src" / "main" / "without-gurobi",
      libraryDependencies += scip, squantsDep
    )
    case Some(libG) => {
      Project("petrus-solver", file("petrus-solver")).settings(commonSettings: _*).dependsOn(libjsonJVM).settings(
        unmanagedSourceDirectories in Compile += baseDirectory.value / "src" / "main" / "with-gurobi"
        , libraryDependencies+=libG, libraryDependencies += scip, squantsDep)
    }
  }
}
lazy val searchGame = Project("petrus-search-game", file("petrus-search-game")).settings(commonSettings: _*).dependsOn(gameJVM, commonJVM, graphLibJVM).settings(libraryDependencies += caffeine)
lazy val strategyMethods = Project("petrus-strategy-methods", file("petrus-strategy-methods")).settings(commonSettings: _*).dependsOn(mixed, bosansky, correlatedStrategies)
lazy val deterministicGames = Project("petrus-deterministic-game-plugins", file("petrus-deterministic-game-plugins")).settings(commonSettings: _*).dependsOn(gameJVM, graphGameJVM, newMatrixGame, searchGame, efGame,flipitGame)
lazy val efGame = Project("petrus-ef-game", file("petrus-ef-game")).settings(commonSettings: _*).dependsOn(gameJVM, commonJVM, gameUtil).settings(libraryDependencies += commonsMath)
lazy val efGen = Project("petrus-ef-gen", file("petrus-ef-gen")).settings(commonSettings: _*).dependsOn(efGame).settings(execSettings: _*).settings(libraryDependencies += commonsMath)
lazy val gameStat = Project("petrus-gamestat", file("petrus-gamestat")).settings(commonSettings: _*).dependsOn(deterministicGames).settings(publishArtifact := false).settings(execSettings: _*)
lazy val flipitGame = Project("petrus-flipit", file("petrus-flipit")).settings(commonSettings: _*).dependsOn(gameJVM, gameUtil, graphLibJVM, commonJVM)
lazy val graphLib = CrossProject("petrus-graph-lib", file("petrus-graph-lib"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*).dependsOn(libjson)
lazy val graphLibJVM = graphLib.jvm
lazy val graphLibJS = graphLib.js
lazy val flipitGen = Project("petrus-flipit-gen", file("petrus-flipit-gen")).settings(commonSettings: _*).dependsOn(flipitGame).settings(publishArtifact := false, libraryDependencies += commonsMath).settings(execSettings: _*)
lazy val cacheProxy = CrossProject("petrus-cache-proxy", file("petrus-cache-proxy"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Full).settings(commonSettings: _*).jvmSettings(
  libraryDependencies += caffeine
)
lazy val cacheProxyJVM = cacheProxy.jvm
lazy val cacheProxyJS = cacheProxy.js
lazy val rngProxy = CrossProject("petrus-rng-proxy", file("petrus-rng-proxy"))(JVMPlatform, JSPlatform).withoutSuffixFor(JVMPlatform).crossType(CrossType.Pure).settings(commonSettings: _*)
lazy val rngProxyJVM = rngProxy.jvm
lazy val rngProxyJS = rngProxy.js
lazy val appLib = Project("petrus-app-lib", file("petrus-app-lib")).settings(commonSettings: _*).settings(libraryDependencies += betterFiles)
lazy val flipitTest = Project("petrus-flipit-test", file("petrus-flipit-test")).settings(commonSettings: _*).dependsOn(flipitGame, mixed).settings(execSettings: _*)
lazy val newGraphGen = Project("petrus-graph-gen", file("petrus-graph-gen")).settings(commonSettings: _*).dependsOn(gamegen, graphGameJVM).settings(execSettings: _*)
lazy val oldchecker = Project("petrus-old-checker", file("petrus-old-checker")).settings(commonSettings: _*).dependsOn(gtopt,graphGameJVM,mixed).settings(execSettings: _*).settings(setBuildInfo("petrus.se"))
lazy val strategychecker = Project("petrus-strategy-checker", file("petrus-strategy-checker")).settings(commonSettings: _*).dependsOn(gameJVM, commonJVM, mixed, graphGameJVM).settings(execSettings: _*)
