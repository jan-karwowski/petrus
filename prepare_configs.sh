#!/bin/bash

SUFFIX=$1
INFILE=base_${SUFFIX}
GAMES="$(seq 0 9)"


for game in $GAMES; do
    sed "s/\"%GAMENUM%\"/${game}/" < $INFILE > game${game}_$SUFFIX
done

