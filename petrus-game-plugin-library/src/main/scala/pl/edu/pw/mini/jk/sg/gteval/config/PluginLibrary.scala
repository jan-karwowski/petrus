package pl.edu.pw.mini.jk.sg.gteval.config

import java.io.File

import pl.edu.pw.mini.jk.sg.game.{GamePlugin, GameVariantRunner}
import pl.edu.pw.mini.sg.movingtargets.MovingTargetsGamePlugin
import pl.edu.pw.mini.sg.matrix.MatrixGamePlugin

case class PluginLibrary(plugins: Seq[GamePlugin]) {
  def getGameVariantRunner(file: File) : GameVariantRunner =
    plugins.find(_.matchFile(file)).map(_.fileToGameVariantRunner(file)) match {
      case Some(r) => r
      case None => throw new RuntimeException(s"No plugin supporting file: $file")
    }

  def getGameVariantRunner(file: File, transformer: File) : GameVariantRunner =
    plugins.find(_.matchFile(file)).map(_.fileToGameVariantRunner(file, transformer)) match {
      case Some(r) => r
      case None => throw new RuntimeException(s"No plugin supporting file: $file")
    }

  def isFileSupported(file: File) : Boolean = plugins.exists(_.matchFile(file))
}



object PluginLibrary {
  val defaultLibrary = PluginLibrary(List(
    MovingTargetsGamePlugin, MatrixGamePlugin
  ))
}
