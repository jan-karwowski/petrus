package pl.edu.pw.mini.sg.matrix

import argonaut.CodecJson


final case class GeneratorConfig(
  correlation: Double,
  defenderMoves: Int,
  attackerMoves: Int
)

object GeneratorConfig {
  implicit val generatorCodec : CodecJson[GeneratorConfig] =
    CodecJson.casecodec3(GeneratorConfig.apply, GeneratorConfig.unapply
    )(
      "correlation", "defenderMoves", "attackerMoves"
    )
}
