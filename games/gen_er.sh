#!/bin/bash

GENERATOR=/home/jan/research/security_games/petrus/petrus-gamegen/target/petrus-gamegen-1.0-SNAPSHOT-jar-with-dependencies.jar

VERTEX_COUNT=$1
TARGET_COUNT=$2
EDGE_PROB=$3
MIN_INDEX=$4
MAX_INDEX=$5

if [ $# -ne 5 ] ; then
    echo Bad invocation!
    exit 1
fi

for i in $(seq $MIN_INDEX $MAX_INDEX) ; do
    java -jar $GENERATOR --game-type ER \
	 --target-count $TARGET_COUNT \
	 --vertex-count $VERTEX_COUNT \
	 --edge-probability $EDGE_PROB --defender-count 1 \
	 --output-file er-$VERTEX_COUNT-$TARGET_COUNT-$(bc -q <<< "$EDGE_PROB*100" | sed 's/.0$//')-$i.json
done

