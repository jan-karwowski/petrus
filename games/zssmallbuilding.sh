#!/bin/bash

for i in $(seq 1 200); do 
    java -jar ../bin/petrus-gamegen-assembly-1.1-SNAPSHOT.jar \
	 --output-file zssmallbuilding3-${i}.json \
	 --output-files-prefix zssmallbuilding3-${i} \
	 --method-config configs/zssmallbuilding3.json \
	 --game-type BUILDINGLIKE
done


for i in $(seq 1 200); do 
    java -jar ../bin/petrus-gamegen-assembly-1.1-SNAPSHOT.jar \
	 --output-file zssmallbuilding4-${i}.json \
	 --output-files-prefix zssmallbuilding4-${i} \
	 --method-config configs/zssmallbuilding4.json \
	 --game-type BUILDINGLIKE
done

