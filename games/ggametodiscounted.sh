#!/bin/sh

# parametry kara obroncy, kara akatujacego, pliki .ggame

if [ $# -lt 3 ] ; then
    echo Arugments: defender_penalty attacker_penalty input...
    exit 1
fi

dfp=$1
afp=$2

shift 2

while [ $# -gt 0 ]; do
    jq '{"graphConfig": ., "attackerRoundLoss": 0.'$afp', "defenderRoundLoss": 0.'$dfp', "id": "'$(uuidgen)'"}' < "$1" > "$(basename $1 .ggame)-$dfp-$afp.dggame"
    shift
done
