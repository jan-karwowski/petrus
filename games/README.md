# Pliki z grami

## Założenia co do gier

- Gry na grafie skierowanym
- Mamy obrońcę, który dysponuje określoną liczbą jednostek (zazwyczaj
  1 w bieżących przykładach), wszystkie jednostki startują z
  wierzchołka 0
- Format gry zakłada 1 lub więcej rodzajów atakującego, ale wszystkie
  implementacje w programie obsługują tylko jeden rodzaj. Rodzaje
  atakujących różnią się wartością wypłat (dla obu graczy) za każdy
  incydent. 
- Atakujący ma określoną liczbę jednostek
- Każdy z graczy przesuwa się do sąsiedniego wierzchołka lub pozostaje
  w bieżącym
- Atakujący, gdy wejdzie do celu dostaje wypłatę i jego jednostka
  znika z planszy. 

## Format

Json o następującej strukturze

- staticGameConfiguration
    - graph -- definicja struktury grafu
        - edges -- tablica krawędzi
        - vertexCount -- liczba wierzchołków
    - defenderCount -- liczba jednostek obrońcy
    - defenseProbabilityFunc -- jakie jest prawdopodobieństwo, że gdy
      atakujący i obrońca będą w jednym wierzchołku, to atakujący
      zostanie złapany. W tej chwili jedyna wartość to "DISCRETE" --
      zawsze zostanie złapany.
	- targets -- które z wierzchołków grafu to cele KOLEJNOŚĆ MA
      ZNACZENIE (tablica wypłat)
	- spawns -- punkty startowe atakującego (w tej chwili musi być
      dokładnie jeden)
	- attackerCount -- liczba **rodzajów** atakującego (nie to samo co
      attackerCount w obiekcie zewnętrznym. Niezbyt szczęśliwy dobór
      nazw. 
	- attackerRewards -- wypłaty dla atakującego za zdobycie celu --
      tablica tablic. Zewnętrzna tablica ma tyle elementów ile jest
      celów, wartości na i-tej pozycji dotyczą i-tego celi z tablicy
      targets. Każdy element to wewnętrzna tablica o długości równej
      liczbie rodzajów atakujących. Każdy element tej tablicy to
      wartość wypłaty dla atakującego danego rodzaju.
	- defenderPenalties -- Struktura taka sama jak attackerRewadrs,
      tym razem elementy to kary dla obrońcy za dojście danego
      typu atakującego do celu.
	- vertexAttackerPenalties -- Tablica tablic. Tablica zewnętrzna o
      długości równej liczbie **wierzchołków** w grafie, wewnętrzne
      tablice jak wyżej. Kary dla atakującego za bycie złapanym w
      wierzchołku.
	- vertexDefenderRewards
	- stateHistoryHorizon -- nieużywane
	- historyTimeHorizon -- nieużywane

- attackerCount -- liczba jednostek atakującego (nie mylić z wartością
  wewnątrz staticGameConfiguration!!!

## Gry

- game[1236]* -- "podstawowy" zestaw gier -- ręcznie wyrzeźbione gry
  do wstępnych testów Mixed-UCT -- sprawdzają różne rzeczy związane z
  jej działaniem
- smallbuilding* -- aktualnie wałkowany zestaw testowy -> patrz opis
  generatora w petrus-gamegen
- inne gry o prefiksach takich jak nazwy generatorów -- zestawy
  wygenerowane danym generatorem.

