#!/bin/sh

GENERATOR=/home/jan/research/security_games/petrus/petrus-gamegen/target/petrus-gamegen-1.0-SNAPSHOT-jar-with-dependencies.jar

TARGETS=$1
DEFENDERS=$2

COUNT=$3

echo > ../schedule/ssmd-$TARGETS-$DEFENDERS

for c in $(seq $COUNT); do
    java -jar $GENERATOR --defender-count $DEFENDERS --target-count $TARGETS --game-type SSMD --output-file ssmd-$TARGETS-$DEFENDERS-$c.json
    echo ssmd-$TARGETS-$DEFENDERS-$c >> ../schedule/ssmd-$TARGETS-$DEFENDERS 
done

