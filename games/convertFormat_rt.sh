#!/bin/sh

jq -c '{
"staticGameConfiguration": {
"graph": {
"edges": .edges,
"vertexCount": .vertexCount
},
"defenderCount": 1,
"historyInState": true,
"defenseProbabilityFunc": .defenseProbabilityFunc,
"targets":.targets,
"spawns":.spawns,
"defenderPenalties":.defenderPenalties,
"attackerRewards":.attackerRewards,
"vertexDefenderRewards":.vertexDefenderRewards,
"vertexAttackerPenalties":.vertexAttackerPenalties,
"attackerCount":.attackerCount,
"stateHistoryHorizon": .stateHistoryHorizon,
"historyTimeHorizon": .historyTimeHorizon
}, 
"attackerCount": 1
}' < "$1" > "$2"
