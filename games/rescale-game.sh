#!/bin/sh

set -e

while [ $# -gt 0 ] ; do
    cp $1 $1.old
    case $1 in
	*.ggame) jq '.vertexDefenderRewards |= map(.*2-1)'  $1.old | jq '.vertexAttackerPenalties |= map(.*2-1)' | jq '.targetDefenderPenalties |= map(.*2-1)' | jq  '.targetAttackerRewards |= map(.*2-1)' > $1
		  ;;
	*.dgame) jq '.graphConfig.vertexDefenderRewards |= map(.*2-1)'  $1.old | jq '.graphConfig.vertexAttackerPenalties |= map(.*2-1)' | jq '.graphConfig.targetDefenderPenalties |= map(.*2-1)' | jq  '.graphConfig.targetAttackerRewards |= map(.*2-1)' > $1
    esac
    rm $1.old
    shift
done
    
