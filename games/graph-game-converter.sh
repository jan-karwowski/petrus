#!/bin/bash

INPUT=$1
ROUNDS=$2
OUTPUT=$(basename $INPUT .json)-$ROUNDS.ggame

# TODO: przeliczenia wypłat

jq '{
 "graphConfig": .staticGameConfiguration.graph,
 "rounds": '$ROUNDS',
 "id": "'$(uuidgen)'",
 "defenderUnitCount": .staticGameConfiguration.defenderCount,
 "targets": .staticGameConfiguration.targets,
 "spawn": .staticGameConfiguration.spawns[0],
 "vertexDefenderRewards": .staticGameConfiguration.vertexDefenderRewards|map(.5+.[0]/60),
 "vertexAttackerPenalties": .staticGameConfiguration.vertexAttackerPenalties|map(.5-.[0]/60),
 "targetDefenderPenalties": .staticGameConfiguration.defenderPenalties|map(.5-.[0]/60),
 "targetAttackerRewards": .staticGameConfiguration.attackerRewards|map(.5+.[0]/60)
}' < $INPUT > $OUTPUT
