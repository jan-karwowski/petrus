#!/bin/bash

GENERATOR=/home/jan/research/security_games/petrus/petrus-gamegen/target/petrus-gamegen-1.0-SNAPSHOT-jar-with-dependencies.jar

VERTEX_COUNT=10
TARGET_COUNT=3
EDGE_PROB=.25
MIN_INDEX=$1
MAX_INDEX=$2

if [ $# -ne 2 ] ; then
    echo Bad invocation!
    exit 1
fi

for i in $(seq $MIN_INDEX $MAX_INDEX) ; do
    java -jar $GENERATOR --game-type ER \
	 --target-count $TARGET_COUNT \
	 --vertex-count $VERTEX_COUNT \
	 --edge-probability $EDGE_PROB --defender-count 1 \
	 --method-config /dev/null \
	 --output-file gameer${TARGET_COUNT}t${VERTEX_COUNT}v2p${i}.json
done

