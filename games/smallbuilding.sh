#!/bin/bash

for i in 102 103 104 105; do 
    java -jar ../bin/petrus-gamegen-assembly-1.1-SNAPSHOT.jar \
	 --output-file smallbuilding-${i}.json \
	 --output-files-prefix smallbuilding-${i} \
	 --method-config configs/smallbuilding.json \
	 --game-type BUILDINGLIKE
done

