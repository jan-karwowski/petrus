#!/bin/bash

for i in $(seq 100); do 
    java -jar ../bin/petrus-gamegen-assembly-1.1-SNAPSHOT.jar \
	 --output-file zrsmallbuilding-${i}.json \
	 --output-files-prefix zrsmallbuilding-${i} \
	 --method-config configs/zrsmallbuilding.json \
	 --game-type BUILDINGLIKE
done

