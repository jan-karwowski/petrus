#!/bin/bash

for i in $(seq 50); do 
    java -jar ../petrus-gamegen/target/petrus-gamegen-1.0-SNAPSHOT-jar-with-dependencies.jar \
	 --output-file mediumbuilding-${i}.json \
	 --output-files-prefix mediumbuilding-${i} \
	 --method-config configs/mediumbuilding.json \
	 --game-type BUILDINGLIKE
done

