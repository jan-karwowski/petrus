#!/bin/bash

GAME=$1
OUTDIR=compactlp-out

mkdir -p $OUTDIR

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GUROBI_HOME/lib:$SCIP_HOME

JAVA_OPTS="-Djava.library.path=$LD_LIBRARY_PATH" ./target/pack/bin/petrus-mtg-compactlp -i $GAME -e -s $2 -o $OUTDIR/$(basename $GAME .sgmt).s$2.compactlp.trmt --solverOutput $OUTDIR/$(basename $GAME .sgmt).s$2.compactlp.solver.trmt