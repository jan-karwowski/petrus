#!/bin/bash

ex="4 5 7 8 10 12 15 18 20 25"
len="2 3 5 10 20 40"

template_path=/home/jan/research/security_games/petrus/template.json

for e in $ex; do
    for l in $len; do
	sed -e 's/%LEN%/'$l'/' -e 's/%EX%/'$e'/' < $template_path > ex${e}-len${l}.json
	echo ex${e}-len${l}  >> logfile
    done
done
