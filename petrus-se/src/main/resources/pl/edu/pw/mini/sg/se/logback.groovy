import static ch.qos.logback.classic.Level.*

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.FileAppender
import ch.qos.logback.core.ConsoleAppender



appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%d %logger{20} %msg%n"
  }
}

root(INFO, ["CONSOLE"])

logger("pl.edu.pw.mini.*", INFO, ["CONSOLE"])
