package pl.edu.pw.mini.sg.se

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.apache.commons.math3.random.{RandomGenerator => CommonsRNG}

final class RNGImpl(
  rng: CommonsRNG
) extends RandomGenerator {
  override def nextInt(upperBoundExcl: Int) : Int = rng.nextInt(upperBoundExcl)
  override def nextDouble() : Double = rng.nextDouble()
  override def nextBoolean(): Boolean = rng.nextBoolean()
}
