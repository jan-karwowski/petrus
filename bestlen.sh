#!/bin/bash

EXS="4 5 7 8 10 12 15"
LENS="2 3 5 10 20 40"
GAMES="5-bA 5-l2A 5-l3A 5-l4A 5-l5A 5-e2A 5-e3A 5-e4A 5-e5A 5-bB 5-l2B 5-l3B 5-l4B 5-l5B 5-e2B 5-e3B 5-e4B 5-e5B"

for g in $GAMES; do
    echo -n ,$g
done

echo

for ex in $EXS;  do
    echo -n $ex
    for game in $GAMES; do
	echo -n ,$((for a in $LENS; do echo -n ${a},; grep uctattacks game${game}_ex${ex}-len${a}.agg.csv|cut -d, -f 4; done)|LC_ALL=C sort -k 2 -t, -n|head -n 1|cut -f 1 -d , )
    done
    echo
done



