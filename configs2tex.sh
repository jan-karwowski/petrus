#!/bin/bash 

CONFIG_PATH=/home/jan/research/security_games/petrus_conf

CONFIG_TRANSFORMER_PATH=/home/jan/research/security_games/petrus/conf_json2tex.sh

CONFIGS="ex12_len2 ex12_len4 ex12_len4_long ex15_len2 ex7_len2 ex18_len3 ex20_len2"

for config in $CONFIGS ; do
    $CONFIG_TRANSFORMER_PATH < $CONFIG_PATH/base_${config}.json > ${config}.tex
done

