#!/bin/bash

test=$1

grep Payoff ${test}.result.*| cut -f2- -d, > ${test}.Uct


R -q --vanilla <<EOF

uct <- read.csv("${test}.Uct", header=F)

make.row <- function(name, data) {
   data.frame(row.names=name, min=round(min(data),1), mean=round(mean(data),1), median=round(median(data),1), max=round(max(data),1), sd=round(sd(data),1))
}

output <- rbind(make.row("uctpayoff",uct[,1]), make.row("uctdefenses",uct[,2]), make.row("uctattacks",uct[,3]))

write.csv(output, "../out_agg/${test}.agg.csv")

EOF

