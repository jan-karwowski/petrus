package pl.edu.pw.mini.jk.sg.chkconfig

import java.io.File

import org.rogach.scallop.{ScallopConf, ValueConverter, listArgConverter}


final class CmdOptions(args: Seq[String]) extends ScallopConf(args) {
  implicit val fileListConverter: ValueConverter[List[File]] = listArgConverter(new File(_))

  val legacyGameConfigs = opt[List[File]](descr = "legacy game configs", name="game-configs")
  val mixeductConfigs = opt[List[File]](descr = "mixeduct configs", name="mixeduct-configs")
  val deterministicGameConfigs = opt[List[File]](descr = "deterministic game configs", name="deterministic-games")
  val uasConfigs = opt[List[File]](descr = "uct attacker sampler configs", name="uas-configs")
  val verbose = opt[Boolean](name="verbose", short ='v', default = Some(false))

  verify()
}
