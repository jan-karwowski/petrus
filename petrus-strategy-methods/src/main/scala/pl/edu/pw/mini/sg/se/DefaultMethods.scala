package pl.edu.pw.mini.sg.se

import scalaz.NonEmptyList



object DefaultMethods {
  val list: NonEmptyList[StrategyMethodParser[StrategyMethod]] =
    NonEmptyList(UctAttackerSamplerMethod,
      UniformStrategyMethodParser,
      NestedMcsAttackerSamplerMethod,
      BosanskyMethodParser,
      CorrelatedStrategiesMethodParser)
}
