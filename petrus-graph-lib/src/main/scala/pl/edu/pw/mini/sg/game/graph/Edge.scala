package pl.edu.pw.mini.sg.game.graph


final case class Edge(
  from: Int,
  to: Int
)

