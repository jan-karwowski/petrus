package pl.edu.pw.mini.sg.game.graph

final case class GraphConfig(
  vertexCount: Int,
  edges: Vector[Edge]
)
