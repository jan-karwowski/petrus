package pl.edu.pw.mini.sg.game.graph

import argonaut.CodecJson


object GraphConfigJson {
  implicit val edgeCodec : CodecJson[Edge] = CodecJson.casecodec2(Edge.apply, Edge.unapply)("from", "to")
  implicit val graphConfigCodec: CodecJson[GraphConfig] =
    CodecJson.casecodec2(GraphConfig.apply, GraphConfig.unapply)(
    "vertexCount", "edges"
  )
}
