#!/bin/bash -x

set -e

GAME=$1

DS=$2
AS=$3
RL=$4
CONF=$5
MDS_CONF=$6

OUTDIR=/home/jan/research/security_games/gtopt-stat


RSCRIPT_PATH=/home/jan/research/security_games/petrus/gtopt-results/aggregate.R

TEMPDIR=$(mktemp -d)

echo udp > $TEMPDIR/uct_defender_payoff
echo uap > $TEMPDIR/uct_attacker_payoff
echo gdp > $TEMPDIR/gt_defender_payoff
echo gap > $TEMPDIR/gt_attacker_payoff
    
for output_file in eval-${GAME}-${DS}-${AS}-${RL}-${CONF}-${MDS_CONF}.*.out ; do
    grep -A 1 "Defender strategy" $output_file |sed -n 2p | cut -f3 -d\   >> $TEMPDIR/uct_defender_payoff
    grep -A 1 "Defender strategy" $output_file |sed -n 5p | cut -f3 -d\   >> $TEMPDIR/gt_defender_payoff
    
    grep -A 1 "Attacker strategy" $output_file |sed -n 2p | cut -f3 -d\   >> $TEMPDIR/uct_attacker_payoff
    grep -A 1 "Attacker strategy" $output_file |sed -n 5p | cut -f3 -d\   >> $TEMPDIR/gt_attacker_payoff
done

paste -d,  $TEMPDIR/{uct_defender_payoff,uct_attacker_payoff,gt_defender_payoff,gt_attacker_payoff} \
      > $OUTDIR/${GAME}-${DS}-${AS}-${RL}-${CONF}-${MDS_CONF}.results.csv


Rscript $RSCRIPT_PATH $OUTDIR/${GAME}-${DS}-${AS}-${RL}-${CONF}-${MDS_CONF}.results.csv \
	      $OUTDIR/${GAME}-${DS}-${AS}-${RL}-${CONF}-${MDS_CONF}.aggregate.csv
    

rm -rf $TEMPDIR
