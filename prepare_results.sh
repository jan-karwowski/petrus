#!/bin/bash

for results in full slice bp; do
    csvtool namedcol game,config,uctDefenderPayoff.mean,uctDefenderPayoff.sd,uctAttackerPayoff.mean,uctAttackerPayoff.sd,uctTimeMs.mean,gtoptDefenderPayoff.mean,gtoptAttackerPayoff.mean,gtoptTime.mean  $results.stat.csv  > ../opis/$results.results.csv
    (
	cd ../opis
	cat results.table.header.tex > $results.results.tex
	csv2latex --nohead --nohline $results.results.csv | tail -n +3 >>  $results.results.tex
    )
done
