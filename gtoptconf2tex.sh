#!/bin/bash

CONF=$1
CNAME=$(basename $CONF .json)

TYPE=$(jq -r .treeTransferType $CONF)

case $TYPE in
    BEST_MOVE)

jq -r '"\\begin{table}
\\begin{tabular}{l|r}
Parameter&Value \\\\
\\hline
Iterations & "+(.updateIterations|tostring)+"\\\\
UCT node evaluations & "+(.oneAttackerEvaluations|tostring)+"\\\\
Transfer method & "+.treeTransferType+"\\\\
\\end{tabular}
\\caption{Config set '$CNAME'}
\\label{conf:'$CNAME'}
\\end{table}
"' < $CONF | sed 's/_/\\_/g'


    ;;
    FULL_TREE|BEST_PART)


jq -r '"\\begin{table}
\\begin{tabular}{l|r}
Parameter&Value \\\\
\\hline
Iterations & "+(.updateIterations|tostring)+"\\\\
UCT node evaluations & "+(.oneAttackerEvaluations|tostring)+"\\\\
Transfer method & "+.treeTransferType+"\\\\
Tree to probability method & "+.nodeProbabilityFunction+"\\\\
\\end{tabular}
\\caption{Config set '$CNAME'}
\\label{conf:'$CNAME'}
\\end{table}
"' < $CONF | sed 's/_/\\_/g'


	
    ;;
esac


