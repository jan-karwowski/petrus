package pl.edu.pw.mini.sg.game.deterministic

import pl.edu.pw.mini.game.flipit.deterministic.allpoints.AllPointsFlipItGamePlugin
import pl.edu.pw.mini.game.flipit.deterministic.noinfo.NoInfoFlipItGamePlugin
import pl.edu.pw.mini.game.search.SearchGamePlugin
import pl.edu.pw.mini.sg.game.deterministic.plugin.GamePlugin
import pl.edu.pw.mini.sg.game.graph.GraphGamePlugin
import pl.edu.pw.mini.sg.game.graph.discounting.DiscountingGraphGamePlugin
import pl.edu.pw.mini.sg.matrix.deterministic.MatrixGamePlugin
import pl.edu.pw.mini.sg.efgame.EfGamePlugin
import pl.edu.pw.mini.sg.efgame.seeded.EfSeededGamePlugin

object Plugins {
  val defaultPlugins : List[GamePlugin] = List(
    GraphGamePlugin, MatrixGamePlugin, SearchGamePlugin, EfGamePlugin, EfSeededGamePlugin,
    DiscountingGraphGamePlugin, AllPointsFlipItGamePlugin, NoInfoFlipItGamePlugin
  )
}

