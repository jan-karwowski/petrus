package pl.edu.pw.mini.jk.sg.gteval

import org.scalatest.{ Matchers, WordSpec }
import squants.time.Seconds




class MilpStrategySpec extends WordSpec with Matchers{
  "gurobi stat reader" should {
    "return time of 1576.53s" in {
      val contents = scala.io.Source.fromURL(getClass.getResource("test.gurobi.stat")).mkString

      MilpStrategy.parseGurobiStatTime(contents) should equal(Some(Seconds(1576.53)))
    }
  }

  "common prefix length" should {
    "be 0 for abc and xxxxxxx" in {
      MilpStrategy.commonPrefixLength("abc", "xxxxxx") should equal(0)
    }

    "be 3 for abc and abcxxxx" in {
      MilpStrategy.commonPrefixLength("abc", "abcxxxx") should equal(3)
    }

    "be 1 for abcaaaaa and aabc" in {
      MilpStrategy.commonPrefixLength("abcaaaa", "aabc") should equal(1)
    }
  }
}
