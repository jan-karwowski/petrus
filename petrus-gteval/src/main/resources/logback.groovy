import static ch.qos.logback.classic.Level.*

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.FileAppender
import ch.qos.logback.core.ConsoleAppender



root(INFO)


appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%msg%n"
  }
}
//logger("stackelberg.optimal.response", DEBUG, ["CONSOLE"])
def drFile = System.getProperty("petrus.defenderResultLog")
if(drFile != null) {
 appender("DR_FILE", FileAppender) {
  file = drFile
  append = false
  encoder(PatternLayoutEncoder) {
    pattern = "%msg%n"
   }
 }
 logger("petrus.DefenderResult", DEBUG, ["DR_FILE"])
}

def asFile = System.getProperty("petrus.attackerStrategyLog")

if(asFile != null) {
 appender("AS_FILE", FileAppender) {
  file = asFile
  append = false
  encoder(PatternLayoutEncoder) {
    pattern = "%msg%n"
   }
 }
 logger("petrus.AttackerStrategy", DEBUG, ["AS_FILE"])
}

def aasFile = System.getProperty("petrus.averagedAttackerStrategyLog")

if(aasFile != null) {
 appender("AAS_FILE", FileAppender) {
  file = aasFile
  append = false
  encoder(PatternLayoutEncoder) {
    pattern = "%msg%n"
   }
 }
 logger("petrus.AveragedAttackerStrategy", DEBUG, ["AAS_FILE"])
}

def ahFile = System.getProperty("petrus.attackerHistoryLog")

if(ahFile != null) {
 appender("AH_FILE", FileAppender) {
  file = ahFile
  append = false
  encoder(PatternLayoutEncoder) {
	pattern = "%msg%n"
   }
 }
 logger("petrus.AttackerHistory", DEBUG, ["AH_FILE"])
}


def dsFile = System.getProperty("petrus.defenderStrategyLog")

if(dsFile != null) {
 appender("DS_FILE", FileAppender) {
  file = dsFile
  append = false
  encoder(PatternLayoutEncoder) {
    pattern = "%msg%n"
   }
 }
 logger("petrus.DefenderStrategy", DEBUG, ["DS_FILE"])
 logger("pl.edu.pw.mini.jk.sg.mixeduct.transfer", DEBUG, ["DS_FILE"])
 logger("petrus.DefenderOracle", DEBUG, ["DS_FILE"])
}

