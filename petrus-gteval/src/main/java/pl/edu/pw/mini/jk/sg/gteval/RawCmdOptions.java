package pl.edu.pw.mini.jk.sg.gteval;

import java.io.File;

import com.lexicalscope.jewel.cli.Option;



public interface RawCmdOptions {
    @Option(longName = "help", helpRequest = true)
    public boolean isHelp();
    
    @Option(longName={"game-path"})
    public File getSgcPath();
    public boolean isSgcPath();
    
    @Option(longName="round-limit")
    public int getRoundLimit();
    public boolean isRoundLimit();

    
    @Option(longName={"moving-targets-game", "game-config"})
    public File getGameConfig();
    public boolean isGameConfig();

    @Option(longName="game-transformer")
    public File getGameTransformer();
    public boolean isGameTransformer();

    @Option(longName="json-strategy-output")
    public File getJsonStrategyOutput();
    public boolean isJsonStrategyOutput();
    
    @Option(longName="stackelberg-solution")
    public File getStackelbergSolutionPath();
    
    @Option(longName="mixed-strategy-config")
    public File getMixedStrategyConfig();

    @Option(longName="treeLogFreq")
    public int getTreeLogFreq();
    public boolean isTreeLogFreq();

    @Option(longName="treeLogDir")
    public String getTreeLogDir();
    public boolean isTreeLogDir();
}
