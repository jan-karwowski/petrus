package pl.edu.pw.mini.jk.sg.gteval;

import argonaut.Json
import petrus.gteval.BuildInfo



class AppHelper;

object AppHelper {
  def addCommitHash(json: Json) : Json = Json.jObjectFields(("githash", Json.jString(BuildInfo.gitCommit)), ("config", json))
}
