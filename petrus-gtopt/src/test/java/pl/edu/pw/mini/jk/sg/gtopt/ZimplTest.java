package pl.edu.pw.mini.jk.sg.gtopt;


import static junit.framework.TestCase.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class ZimplTest {

	@Test
	public void testParamMatrix() {
		final double[][] matrix = new double[][] { { 1, 3, 5 }, { 2, 4, 6 }};
		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		final PrintStream stream = new PrintStream(bos);
		Zimpl.printParamMatrix(stream, "ar", "lx", "ly", matrix);
		stream.flush();
		final String real = new String(bos.toByteArray());
		final String expected = "param ar[lx*ly] := | 0, 1, 2 |\n"
				+ "|0|1.0, 3.0, 5.0|\n"
				+ "|1|2.0, 4.0, 6.0|\n"
				+ ";\n";
		assertEquals(expected, real);
	}

}
