package pl.edu.pw.mini.jk.sg.gtopt;

import com.lexicalscope.jewel.cli.CliFactory
import java.io.File
import org.scalatest.{WordSpec, Matchers}



class CmdlineOptionsSpec extends WordSpec with Matchers {

  def parseArgs(args: Seq[String]) = CliFactory.createCli(classOf[CmdlineOptions]).parseArguments(args : _*)

  "cmdline options --game-config abc --output-file xyzzy" should {
    val opts = Array("--game-config", "abc", "--output-file", "xyzzy")
    val parsed = parseArgs(opts)

    "return abc path for game config" in {
      parsed.getGameConfig() should equal(new File("abc"))
    }

    "return xyzzy for output file" in {
      parsed.getOutputFile() should equal(new File("xyzzy"))
    }

    "return false for zimpl" in {
      parsed.isZimplOutput() should be(false)
    }

    "nod advertise round limit" in {
      parsed.isRoundLimit() should be(false)
    }
  }

  "cmdline options --game-config abc --zimpl --output-file xyzzy" should {
    val opts = Array("--game-config", "abc", "--zimpl", "--output-file", "xyzzy")
    val parsed = parseArgs(opts)

    "return abc path for game config" in {
      parsed.getGameConfig() should equal(new File("abc"))
    }

    "return xyzzy for output file" in {
      parsed.getOutputFile() should equal(new File("xyzzy"))
    }

    "return true for zimpl" in {
      parsed.isZimplOutput() should be(true)
    }
  }

  "cmdline options --game-config abc --zimpl --output-file xyzzy --round-limit 5" should {
    val opts = Array("--game-config", "abc", "--zimpl", "--output-file", "xyzzy", "--round-limit", "5")
    val parsed = parseArgs(opts)

    "return abc path for game config" in {
      parsed.getGameConfig() should equal(new File("abc"))
    }

    "return xyzzy for output file" in {
      parsed.getOutputFile() should equal(new File("xyzzy"))
    }

    "return true for zimpl" in {
      parsed.isZimplOutput() should be(true)
    }

    "advertise round limit" in {
      parsed.isRoundLimit() should be(true)
    }

    "have round limit 5" in {
      parsed.getRoundLimit should be(5)
    }
  }


  "cmdline options --game-config abc --output-file xyzzy --move-codes asdf" should {
    val opts = Array("--game-config", "abc", "--output-file", "xyzzy", "--move-codes", "asdf")
    val parsed = parseArgs(opts)

    "return abc path for game config" in {
      parsed.getGameConfig() should equal(new File("abc"))
    }

    "return xyzzy for output file" in {
      parsed.getOutputFile() should equal(new File("xyzzy"))
    }

    "return false for zimpl" in {
      parsed.isZimplOutput() should be(false)
    }

    "nod advertise round limit" in {
      parsed.isRoundLimit() should be(false)
    }

    "advertise move codes" in {
      parsed.isMoveCodesPath() should be(true)
    }

    "move codes should be asdf" in {
      parsed.getMoveCodesPath() should be(new File("asdf"))
    }
  } 
}
