package pl.edu.pw.mini.jk.sg.gtopt;

public enum ZimplVarType {
	BINARY("binary"), REAL("real"), INTEGER("integer");
	
	private final String codeWord;

	private ZimplVarType(String codeWord) {
		this.codeWord = codeWord;
	}
	
	public String getCodeWord() {
		return codeWord;
	}
	
}
