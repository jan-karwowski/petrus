package pl.edu.pw.mini.jk.sg.gtopt;

import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Zimpl {
	public static void printParamMatrix(PrintStream outStream, String arrayName, String xName, String yName,
			double[][] matrix) {
		outStream.format("param %s[%s*%s] := ", arrayName, xName, yName);

		// coumn headers
		outStream.print("| ");
		outStream
				.print(IntStream.range(0, matrix[0].length).mapToObj(Integer::toString).collect(Collectors.joining(", ")));
		outStream.print(" |\n");

		for (int x = 0; x < matrix.length; x++) {
			outStream.format("|%d|", x);
			for (int y = 0; y < matrix[0].length; y++) {
				if (y > 0) {
					outStream.print(", ");
				}
				outStream.print(matrix[x][y]);
			}
			outStream.print("|\n");
		}
		outStream.println(";");
	}

	public static String intSet(String setName, int min, int max) {
		return String.format("set %s := {%d .. %d};", setName, min, max);
	}

	public static String variable(String varName, List<String> indices, ZimplVarType varType,
			Optional<Double> lowerBound, Optional<Double> upperBound) {
		StringBuilder builder = new StringBuilder();

		builder.append("var ").append(varName);
		if (indices.size() > 0) {
			builder.append("[").append(indices.stream().collect(Collectors.joining("*"))).append("]");
		}

		builder.append(" ").append(varType.getCodeWord());
		if (varType != ZimplVarType.BINARY) {
			if (lowerBound.isPresent()) {
				builder.append(" >= ").append(lowerBound.get());
			} else {
				builder.append(" >= -infinity ");
			}

			if (upperBound.isPresent()) {
				builder.append(" <= ").append(upperBound.get());
			} else {
				builder.append(" <= infinity");
			}
		}

		return builder.append(";").toString();
	}
}
