package pl.edu.pw.mini.jk.sg.gtopt;

import com.lexicalscope.jewel.cli.Option;
import java.io.File;

public interface CmdlineOptions {
    @Option(longName = "game-config")
    File getGameConfig();

    @Option(longName = "round-limit")
    int getRoundLimit();
    boolean isRoundLimit();

    @Option(longName = "zimpl")
    boolean isZimplOutput();

    @Option(longName = "move-codes")
    File getMoveCodesPath();
    boolean isMoveCodesPath();

    @Option(longName = "output-file")
    File getOutputFile();

    @Option(longName="game-transformer")
    public File getGameTransformer();
    public boolean isGameTransformer();
}
