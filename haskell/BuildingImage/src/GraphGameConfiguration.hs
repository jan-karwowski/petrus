{-# LANGUAGE DeriveGeneric #-}

module GraphGameConfiguration(GraphGameConfiguration, parseGGCFile)
where

import GHC.Generics
import Data.Aeson
import qualified Data.ByteString.Lazy as BS

import PetrusFiles(GameGraph, GameDescription(..))

data GraphGameConfiguration = GraphGameConfiguration {
  vertexDefenderRewards :: [Double],
  targetDefenderPenalties :: [Double],
  targets :: [Int],
  spawn :: Int,
  defenderUnitCount :: Int,
  graphConfig :: GameGraph,
  vertexAttackerPenalties :: [Double],
  rounds :: Int,
  id :: String,
  targetAttackerRewards :: [Double]
  } deriving (Show, Generic)


instance GameDescription GraphGameConfiguration where
  targetIds = targets
  spawnIds = (:[]) . spawn
  gameGraph = graphConfig
  defenderPenalty g idx = targetDefenderPenalties g !! idx
  attackerReward g idx = targetAttackerRewards g !! idx


instance FromJSON GraphGameConfiguration

parseGGCFile :: BS.ByteString -> Either String GraphGameConfiguration
parseGGCFile = eitherDecode
