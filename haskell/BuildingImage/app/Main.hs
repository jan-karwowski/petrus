module Main
  where

import GraphGameConfiguration(parseGGCFile)
import PetrusFiles
import PictureGenerator
import System.Environment
import qualified Data.ByteString.Lazy as BS
import Data.List(isSuffixOf)

main :: IO ()

main = do
  [gameFile, dataFile] <- getArgs
  dataFileContents <- readFile dataFile
  let genOutput conf = putStrLn $ drawBuilding conf (parseNodeDataFile dataFileContents)
  gameFileContents <- BS.readFile gameFile
  if ".ggame" `isSuffixOf` gameFile
    then  either putStrLn genOutput (parseGGCFile gameFileContents) 
    else  either putStrLn genOutput (parseGameFile gameFileContents) 



