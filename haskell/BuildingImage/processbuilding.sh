#!/bin/sh

GAMES_PATH=/home/jan/research/security_games/petrus/games/
BI_PATH=/home/jan/research/security_games/petrus/haskell/BuildingImage/.stack-work/dist/x86_64-linux/Cabal-2.4.0.1/build/BuildingImage-exe/BuildingImage-exe

GAME=$1

if [ -e ${GAMES_PATH}/${GAME}.json ]; then 
    $BI_PATH ${GAMES_PATH}/${GAME}.json ${GAMES_PATH}/${GAME}.nodepos.csv > $GAME.tex
elif [ -e ${GAMES_PATH}/${GAME}.ggame ]; then
    $BI_PATH ${GAMES_PATH}/${GAME}.ggame ${GAMES_PATH}/${GAME}.nodepos.csv > $GAME.tex
else
    echo "Game file not found!!" >&2
    exit 1
fi
