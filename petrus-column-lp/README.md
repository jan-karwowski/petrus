To enable integration with the Gurobi solver, one must run the program with environmental variables:

JAVA_OPTS=${GUROBI_HOME}/lib
LD_LIBRARY_PATH=${GUROBI_HOME}/lib

