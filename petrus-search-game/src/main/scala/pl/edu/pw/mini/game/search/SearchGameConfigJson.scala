package pl.edu.pw.mini.game.search

import argonaut.CodecJson
import pl.edu.pw.mini.sg.game.graph.{Edge, GraphConfig, GraphConfigJson}

object SearchGameConfigJson {
    import GraphConfigJson._

  implicit val searchGameConfigJson: CodecJson[SearchGameConfig] = CodecJson.casecodec10(SearchGameConfig.apply, SearchGameConfig.unapply)(
    "graphConfig",
    "rounds",
    "defenderUnitCount",
    "attackerStartingPosition",
    "defenderStartingPositions",
    "targets",
    "defenderVertices",
    "trackVertices",
    "coveringUpTracksPossible",
    "id"
  )
}
