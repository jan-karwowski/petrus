package pl.edu.pw.mini.game.search

import argonaut.DecodeJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.{GameFactory, GamePlugin}


object SearchGamePlugin extends GamePlugin {
  override type GameConfig = SearchGameConfig

  override val fileExtension: String = "sgame"
  override val configJsonDecode: DecodeJson[SearchGameConfig] = SearchGameConfigJson.searchGameConfigJson
  override val singleCoordFactory: GameFactory[SearchGameConfig] = SearchGameFactory
}
