package pl.edu.pw.mini.game.search

/**
  * Class representing one attacker move
  * @param newPosition    new position of the attacker
  */
final case class SearchGameAttackerMove(newPosition: Int) {
  override def toString: String = s"SGAM($newPosition)"
}