package pl.edu.pw.mini.game.search

import argonaut.CodecJson

final case class SearchGameTarget(position: Int, payoff: Double) {

}

object SearchGameTarget {
  implicit val searchGameTargetJson: CodecJson[SearchGameTarget] = CodecJson.casecodec2(SearchGameTarget.apply, SearchGameTarget.unapply)(
    "position", "payoff"
  )
}
