package pl.edu.pw.mini.game.search

import argonaut.{CodecJson, EncodeJson}
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson

object SearchGameJson extends DeterministicGameJson[List[Int], SearchGameAttackerMove, SearchGameDefenderState , SearchGameAttackerState] {
  override implicit val defenderMoveEncode: EncodeJson[List[Int]] = EncodeJson.ListEncodeJson[Int].contramap(_.toList)
  override implicit val attackerMoveEncode: EncodeJson[SearchGameAttackerMove] = CodecJson.casecodec1(SearchGameAttackerMove.apply, SearchGameAttackerMove.unapply)(
    "newPosition"
  )

  override implicit val defenderStateEncode: EncodeJson[SearchGameDefenderState] = EncodeJson.jencode3L((sgds: SearchGameDefenderState) =>
    (sgds.unitPositions, sgds.timeStep, sgds.tracks))(
    "positions","timeStep", "tracks"
  )(EncodeJson.ListEncodeJson[Int].contramap(_.toList), CodecJson.derived[Int], EncodeJson.ListEncodeJson[Int].contramap(_.toList))

  override implicit val attackerStateEncode: EncodeJson[SearchGameAttackerState] = EncodeJson.jencode2L((sgas: SearchGameAttackerState) =>
    (sgas.attackerPosition, sgas.timeStep))(
    "attackerPosition", "timeStep"
  )
}
