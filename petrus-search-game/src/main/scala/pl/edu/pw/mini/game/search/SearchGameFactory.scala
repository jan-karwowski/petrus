package pl.edu.pw.mini.game.search

import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactory

object SearchGameFactory extends GameFactory[SearchGameConfig]{
  override type DefenderMove = List[Int]
  override type AttackerMove = SearchGameAttackerMove
  override type DefenderState = SearchGameDefenderState
  override type AttackerState = SearchGameAttackerState
  override type Game = SearchGame

  override def create(gameConfig: SearchGameConfig): SearchGame = SearchGame(gameConfig)

  override def maxDefenderPayoff(gameConfig: pl.edu.pw.mini.game.search.SearchGameConfig): Double = 1
  override def minDefenderPayoff(gameConfig: pl.edu.pw.mini.game.search.SearchGameConfig): Double = -1
  override def maxAttackerPayoff(gameConfig: SearchGameConfig): Double = gameConfig.targets.map(_.payoff).max
  override def minAttackerPayoff(gameConfig: SearchGameConfig): Double = -1

  override val json: SearchGameJson.type = SearchGameJson
}
