package pl.edu.pw.mini.game.search

import java.util.UUID

import org.scalatest.{Matchers, WordSpec}
import pl.edu.pw.mini.sg.game.graph.{Edge, GraphConfig}

class SearchGameDefenderStateSpec extends WordSpec with Matchers {
  val config = SearchGameConfig(
    GraphConfig(7, List(Edge(5, 1), Edge(5, 2), Edge(6, 1),Edge(6, 2), Edge(6, 3), Edge(1, 1), Edge(2, 2), Edge(3, 3),Edge(0, 1), Edge(0, 2), Edge(0, 3), Edge(1, 2), Edge(2, 1), Edge(2, 3), Edge(1, 4), Edge(2, 4), Edge(3, 4)).toVector),
    5, 2, 0, Vector(5, 6), List(SearchGameTarget(4, 1.0)), List(List(1, 2, 5), List(1, 2, 3, 6)), List(1,2,3), coveringUpTracksPossible = true,
    UUID.fromString("658f740c-8c22-473f-8d6f-5cbea8c10f3d"))

  "The SearchGameDefenderState class" should {
    val finishedState = config.defenderInitialState.copy(timeStep = 5)
    val state = SearchGameDefenderState(config, List(2, 3), 1, Set())
    "define a possibleMoves method" which {
      "returns empty list if game has finished" in {
        val moves = finishedState.possibleMoves
        moves should be (empty)
      }

      "returns combinations of all possible vertices for defenders at the beginning of a game" in {
        val moves = config.defenderInitialState.possibleMoves
        moves should contain theSameElementsAs List(
          List(1, 1), List(1, 2), List(1, 3), List(2, 1), List(2, 2), List(2, 3)
        )
      }

      "returns combinations of reachable vertices for the given defender positions" in {
        val moves = state.possibleMoves
        moves should contain theSameElementsAs List(
          List(1, 3), List(2, 3)
        )
      }
    }

    "define an equals method" which {
      "returns true for states with the same positions and timesteps" in {
        val result = state.equals(SearchGameDefenderState(config, List(2, 3), 1, Set()))
        result should be (true)
      }

      "returns false for comparision between different types" in {
        val result = state.equals(4)
        result should be (false)
      }

      "returns false for states with different positions" in {
        val result = state.equals(SearchGameDefenderState(config, List(1, 3), 1, Set()))
        result should be (false)
      }

      "returns false for states with different timesteps" in {
        val result = state.equals(SearchGameDefenderState(config, List(2, 3), 4, Set()))
        result should be (false)
      }

      "returns false for states with different tracks" in {
        val result = state.equals(SearchGameDefenderState(config, List(2, 3), 1, Set(1, 2)))
        result should be (false)
      }
    }

  }
}
