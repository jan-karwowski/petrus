package pl.edu.pw.mini.game.search

import java.util.UUID

import argonaut.Parse
import org.scalatest.{Matchers, WordSpec}
import SearchGameConfigJson.searchGameConfigJson
import pl.edu.pw.mini.sg.game.graph.{Edge, GraphConfig}

class SearchGameConfigJsonSpec extends WordSpec with Matchers{

  private val gameJson =
    """
      |{
      |    "graphConfig": {
      |        "edges": [
      |            {
      |                "from": 0,
      |                "to": 1
      |            },
      |            {
      |                "from": 1,
      |                "to": 2
      |            }
      |        ],
      |        "vertexCount": 3
      |    },
      |    "rounds": 5,
      |    "defenderUnitCount": 1,
      |    "defenderStartingPositions": [1],
      |    "attackerStartingPosition": 0,
      |    "targets": [
      |        {
      |           "position": 2,
      |           "payoff": 1.5
      |        }
      |    ],
      |    "defenderVertices": [
      |        [1, 2]
      |    ],
      |    "trackVertices": [
      |        1,2
      |    ],
      |    "coveringUpTracksPossible": true,
      |    "id": "658f740c-8c22-473f-8d6f-5cbea8c10f3d"
      |}
    """.stripMargin


  "SearchGameConfigJson" should {
    "be able to turn json into config" in {
      val config = Parse.decode[SearchGameConfig](gameJson) match {
        case Right(c) => c
      }
      config should equal (SearchGameConfig(
        GraphConfig(3, List(Edge(0, 1), Edge(1, 2)).toVector),
        5, 1, 0, Vector(1), List(SearchGameTarget(2, 1.5)), List(List(1, 2)), List(1, 2), coveringUpTracksPossible = true,
        UUID.fromString("658f740c-8c22-473f-8d6f-5cbea8c10f3d")))
    }
  }
}
