#!/bin/bash

DIRECTORY=$1

for G in ${DIRECTORY}*.sgmt; do
    ./run_mtg_compact_lp.sh $G $2
done