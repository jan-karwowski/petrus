#!/bin/bash

CONF=ex20-len10
IT=5000
RP=40
RO=10
AC=1
OAM=.4


outdir=gtopt10beta1

#for g in 3 3a 3b 3c 3d; do
for g in 3 3b; do

    for repeat in $(seq 8); do
	    java -jar petrus-gteval/target/petrus-gteval-1.0-SNAPSHOT-jar-with-dependencies.jar --game-path ../games/game${g}.json --defender-start 0 --attacker-start 8 --round-limit 5 --algorithm-config ../conf/${CONF}.json --stackelberg-solution ../gtopt/game${g}-0-8-5.sol --update-iterations $IT --one-attacker-evaluations $RP --one-attacker-roots $RO --save-tree-path ../${outdir}/eval-game${g}-0-8-5-${CONF}-${IT}-${RP}-${RO}.${AC}.${OAM}.${repeat}.tree --damping $AC --other-attacker-moves-probability $OAM > ../${outdir}/eval-game${g}-0-8-5-${CONF}-${IT}-${RP}-${RO}.${AC}.${OAM}.${repeat}.out
	done
    done
