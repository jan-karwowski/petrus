#!/bin/sh

env LD_LIBRARY_PATH="$GUROBI_HOME"/lib sbt -Djava.library-path="$GUROBI_HOME"/lib -J-XX:MaxMetaspaceSize=512m
