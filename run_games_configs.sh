#!/bin/bash


#for config in ex12_len4 ex15_len2 ex7_len2 ex18_len3 ex20_len2 ex4_len3; do
#for config in ex10_len4 ex15_len40 ex7_len40 ex20_len40 ex12_len40; do
#for config in $(cat confs); do
for config in ex7-len10 RANDOM; do
    #for a in 0 1 2 3 6 7; do
    #    for game in 5-b 5-l2 5-l3 5-l4 5-l5 5-e2 5-e3 5-e4 5-e5; do
    for game in 4; do
	for type in A B ; do
	    for attacker in ema1 cea1; do
		./run_tests.sh ${config} game${game} $type $attacker
	    done
	done
    done
done

