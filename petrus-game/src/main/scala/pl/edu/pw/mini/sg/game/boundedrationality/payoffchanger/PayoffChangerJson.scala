package pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger

import argonaut.{ CodecJson, DecodeJson, DecodeResult, EncodeJson }
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}

object PayoffChangerJson {

  implicit val prospectTheoryCodec: CodecJson[ProspectTheoryPayoff] = CodecJson.casecodec3(
    ProspectTheoryPayoff.apply, ProspectTheoryPayoff.unapply
  )("alpha", "beta", "omega")

  implicit val neutralPayoffChangerEncode : EncodeJson[NeutralPayoffChanger.type ] = EncodeJson.UnitEncodeJson.contramap { x => Unit }
  implicit val neutralPayoffChangerDecode : DecodeJson[NeutralPayoffChanger.type] = DecodeJson(x => DecodeResult.ok(NeutralPayoffChanger))

  implicit val payoffChangerCodec: FlatTraitJsonCodec[PayoffChanger] =
    new FlatTraitJsonCodec[PayoffChanger](List(
      new TraitSubtypeCodec(prospectTheoryCodec, prospectTheoryCodec, "prospectTheoryPayoff"),
      new TraitSubtypeCodec(neutralPayoffChangerEncode, neutralPayoffChangerDecode, "neutralPayoffChanger")
    ))
}
