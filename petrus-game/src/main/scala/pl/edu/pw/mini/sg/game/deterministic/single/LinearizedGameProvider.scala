package pl.edu.pw.mini.sg.game.deterministic.single

import pl.edu.pw.mini.sg.game.deterministic.{ AdvanceablePlayerVisibleState, PureStrategy }

import scala.language.higherKinds

trait LinearizedGame[M,S <: AdvanceablePlayerVisibleState[M,S], G <: LinearizedGame[M, S, G]]
    extends SinglePlayerGame[M, S, G]{
  self : G =>
  def pureStrategy : PureStrategy[M, S]
}

trait LinearizedGameProvider {
  type Game[M,S <: AdvanceablePlayerVisibleState[M, S]] <: LinearizedGame[M,S, Game[M,S]]
  def create[M,S <: AdvanceablePlayerVisibleState[M,S]](state: S) : Game[M, S]
  def pureStrategyToLinearizedGameMoveSequence[M, S <: AdvanceablePlayerVisibleState[M, S]](
    root: S,
    pureStrategy : PureStrategy[M, S]
  ) : List[M]
}
