package pl.edu.pw.mini.sg.game.deterministic.single;


// We assume that no two different moves lead to the same immediate state 
final case class HistoricalDecisions[M, S](
  currentState: S,
  move: M,
  nextStates: List[HistoricalDecisions[M, S]]
)
