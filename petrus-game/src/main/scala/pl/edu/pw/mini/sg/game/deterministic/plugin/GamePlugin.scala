package pl.edu.pw.mini.sg.game.deterministic.plugin

import java.io.File

import scala.io.Source
import scala.util.{Failure, Success, Try}

import argonaut.{DecodeJson, Parse}



trait GamePlugin {
  type GameConfig <: config.GameConfig

  val fileExtension: String
  val configJsonDecode: DecodeJson[GameConfig]
  val singleCoordFactory : GameFactory[GameConfig]

  def readConfig(file: File) : Try[GameConfig] =
    Try(Source.fromFile(file).mkString).flatMap(str => 
      Parse.decodeWith[Try[GameConfig], GameConfig](
        str,
        x => Success(x),
        err => Failure[GameConfig](new RuntimeException(s"Json syntax error $err")),
        (msg, hist) => Failure[GameConfig](new RuntimeException(s"Json structure error $msg in $hist"))
      )(configJsonDecode)
    )
}
