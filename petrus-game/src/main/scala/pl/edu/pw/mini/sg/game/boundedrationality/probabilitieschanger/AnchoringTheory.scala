package pl.edu.pw.mini.sg.game.boundedrationality

import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.ProbabilitiesChangerRegular

final case class AnchoringTheory(alpha: Double) extends ProbabilitiesChangerRegular {

  override def changeProbability[DM](
      prob: Seq[(DM, Double)],
      allMoves: => Seq[DM]
  ): Seq[(DM, Double)] = {
    val len = prob.length.toDouble
    prob.map { case (m, p) => (m, alpha / len + (1 - alpha) * p) }
  }
}
