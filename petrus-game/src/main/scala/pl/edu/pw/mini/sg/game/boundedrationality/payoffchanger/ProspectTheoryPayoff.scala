package pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger

import pl.edu.pw.mini.sg.game.Payoff

final case class ProspectTheoryPayoff(alpha: Double , beta: Double , omega: Double) extends PayoffChanger {
  override def changeExpectedPayoff(payoff : Payoff) : Payoff = {
    new Payoff(valueFunc(payoff.defender), valueFunc(payoff.attacker))
  }

  def valueFunc(x:Double):Double = {
    if (x >= 0) {
      math.pow(x,alpha)
    }
    else {
      -omega*math.pow(-x, beta)
    }
  }
}
