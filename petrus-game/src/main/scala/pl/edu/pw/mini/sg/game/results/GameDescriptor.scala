package pl.edu.pw.mini.sg.game.results

import java.util.UUID

import argonaut.CodecJson

final case class GameDescriptor(
  gameType: String,
  id: UUID
)

object GameDescriptor {
  implicit val gameDescriptorCodec : CodecJson[GameDescriptor] = CodecJson.casecodec2(GameDescriptor.apply, GameDescriptor.unapply)("type", "id")

}
