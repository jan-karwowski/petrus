package pl.edu.pw.mini.sg.game.deterministic.single;

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.{ BehavioralStrategy, DeterministicGame, PlayerVisibleState }



final case class AttackerGameView[
  DM, AM, 
  DS <: PlayerVisibleState[DM],
  AS <: PlayerVisibleState[AM],
  G <: DeterministicGame[DM, AM, DS,AS, G]
](
  gameState: G,
  defenderStrategy: BehavioralStrategy[DM, DS],
  rng: RandomGenerator
) extends SinglePlayerGame[AM, AS, AttackerGameView[DM,AM,DS,AS,G]] with GameWithPayoff {

  def makeMove(move: AM) : AttackerGameView[DM, AM, DS, AS, G] = {
    val dmove = defenderStrategy.randomMove(rng)
    val adv = gameState.makeMove(dmove, move)
    copy[DM, AM, DS, AS, G](gameState=adv, defenderStrategy = defenderStrategy.nextState(dmove, adv.defenderState))
    }

  def playerState : AS = gameState.attackerState
  def possibleMoves: List[AM] = gameState.attackerState.possibleMoves

  override def attackerPayoff: Double = gameState.attackerPayoff
  override def defenderPayoff: Double = gameState.defenderPayoff
}
