package pl.edu.pw.mini.sg.game.deterministic.single;

trait GameWithPayoff {
  def defenderPayoff: Double
  def attackerPayoff: Double
}
