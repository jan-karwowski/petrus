package pl.edu.pw.mini.sg.game.deterministic;

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.single.SinglePlayerGame

import scala.language.higherKinds


// Potrzebuję:
// - umieć przechować dane pośrednie
// - znać pełny stan gry (a+d)
// - a może single player game? -- w aktualnym pomyśle wystarczy
trait TreeExpander[-M, -S, -G] {
  type SubtreeData[MO,ST]
  def zeroSubtreeData[MM <: M, SS <: S]: SubtreeData[MM,SS]
  def expand[MM <: M, SS <: S, GG <: SinglePlayerGame[MM,SS,GG] with G](
    state: GG, subtreeData: SubtreeData[MM,SS]
  )(implicit rng: RandomGenerator) : (MM, SubtreeData[MM, SS])
}
