package pl.edu.pw.mini.sg.game.deterministic.json

import argonaut.{EncodeJson, DecodeJson}


trait DeterministicGameJson[DM, AM, DS, AS] {
  implicit val defenderMoveEncode: EncodeJson[DM]
  implicit val attackerMoveEncode: EncodeJson[AM]
  implicit val defenderStateEncode: EncodeJson[DS]
  implicit val attackerStateEncode: EncodeJson[AS]
}

trait DeterministicGameJsonDecoders[DM, AM, DS, AS] {
  implicit val defenderMoveDecode: DecodeJson[DM]
  implicit val attackerMoveDecode: DecodeJson[AM]
  implicit val defenderStateDecode: DecodeJson[DS]
  implicit val attackerStateDecode: DecodeJson[AS]
}
