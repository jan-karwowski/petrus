package pl.edu.pw.mini.sg.game.deterministic.plugin

import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson
import pl.edu.pw.mini.sg.game.deterministic.{ AdvanceablePlayerVisibleState, DeterministicGame, PlayerVisibleState }



trait GameFactory[GC] {
  type DefenderMove
  type AttackerMove
  type DefenderState <: PlayerVisibleState[DefenderMove]
  type AttackerState <: AdvanceablePlayerVisibleState[AttackerMove, AttackerState]
  type Game <: DeterministicGame[DefenderMove, AttackerMove, DefenderState, AttackerState, Game]

  def create(gameConfig: GC) : Game
  def minDefenderPayoff(gameConfig: GC): Double
  def maxDefenderPayoff(gameConfig: GC): Double
  def minAttackerPayoff(gameConfig: GC): Double
  def maxAttackerPayoff(gameConfig: GC): Double
  val json: DeterministicGameJson[DefenderMove, AttackerMove, DefenderState, AttackerState]
}
