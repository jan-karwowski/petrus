package pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger


sealed trait ProbabilitiesChanger

trait ProbabilitiesChangerRegular extends ProbabilitiesChanger {
  def changeProbability[DM](prob: Seq[(DM,Double)], allMoves: => Seq[DM]): Seq[(DM, Double)]
  /**
    @param prob probability of last move
    @param parentBranchingCoeff  1/(nuber of branches in parent)
    */
}

trait ProbabilitiesChangerLeaf extends ProbabilitiesChanger {
  def leafAdjust(prob: Double, parentBranchingCoeff: Double) : Double
}
