package pl.edu.pw.mini.sg.game.deterministic.single;

import pl.edu.pw.mini.sg.game.deterministic.{DeterministicGame, PlayerVisibleState, PureStrategy}
import org.log4s._

final case class DefenderGameView[
  DM,
  AM, 
  DS <: PlayerVisibleState[DM],
  AS <: PlayerVisibleState[AM],
  G <: DeterministicGame[DM, AM, DS,AS, G]
](gameState: G, attackerStrategy: PureStrategy[AM, AS]) extends SinglePlayerGame[DM, DS,
DefenderGameView[DM,AM,DS,AS,G]] with GameWithPayoff {
  private [this] val log = getLogger

  def makeMove(move: DM) : DefenderGameView[DM, AM, DS, AS, G] = {
    if(attackerStrategy.isInstanceOf[DummyPureStrategy[AM, AS]]) {
      log.error(s"Bad strategy: defenderState: ${gameState.defenderState}, attackerState: ${gameState.attackerState}, strategyState: ${attackerStrategy.state}")
    }
    val gs = gameState.makeMove(move, attackerStrategy.move)
    copy(gameState = gs,
      attackerStrategy = attackerStrategy.nextState(gs.attackerState)
    )
  }

  def playerState : DS = gameState.defenderState
  def possibleMoves: List[DM] = gameState.defenderState.possibleMoves

  override def defenderPayoff = gameState.defenderPayoff
  override def attackerPayoff = gameState.attackerPayoff
}
