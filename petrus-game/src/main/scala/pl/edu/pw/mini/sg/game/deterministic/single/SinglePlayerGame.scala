package pl.edu.pw.mini.sg.game.deterministic.single;


trait SinglePlayerGame[M, S, G <: SinglePlayerGame[M, S, G]] {
  self: G => 
  def playerState: S
  def makeMove(move: M): G
  def possibleMoves : List[M]
}
