package pl.edu.pw.mini.sg.game.deterministic.plugin

final case class GameParams(
  maxDefenderPayoff: Double,
  minDefenderPayoff: Double
)
