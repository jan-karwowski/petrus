package pl.edu.pw.mini.sg.game.boundedrationality

import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.{NeutralPayoffChanger, PayoffChanger}
import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.{NeutralProbabilityChanger, ProbabilitiesChanger}

final case class BoundedRationality (
  payoffChanger: PayoffChanger,
  probabilitiesChanger: ProbabilitiesChanger,
)


object BoundedRationality {
  val fullyRational : BoundedRationality = BoundedRationality(
    NeutralPayoffChanger,
    NeutralProbabilityChanger,
  )
}
