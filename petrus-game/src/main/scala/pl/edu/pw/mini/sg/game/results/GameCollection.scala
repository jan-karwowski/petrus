package pl.edu.pw.mini.sg.game.results

import java.util.UUID
import pl.edu.pw.mini.sg.game.deterministic.plugin.config.GameConfig
import java.{util => ju}

trait GameCollection[GC <: GameConfig] {

  /**
    @returns Some(game config) with given id or None if there is no such config in the collection
    */
  def getById(uuid: UUID): Option[GC]
}

object GameCollection {
  def fromMap[GC <: GameConfig](map: Map[UUID, GC]): GameCollection[GC] =
    new GameCollection[GC] {
      def getById(uuid: ju.UUID): Option[GC] = map.get(uuid)
    }
}
