package pl.edu.pw.mini.sg.game.boundedrationality

import argonaut.CodecJson
import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.{PayoffChanger, PayoffChangerJson}
import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.{ProbabilitiesChanger, ProbabilitiesChangerJson}
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}

object BoundedRationalityJson {

  implicit def boundedRationalityCodec(
  implicit
    payoffChangerTC: FlatTraitJsonCodec[PayoffChanger],
    probabilitiesChangerTC: FlatTraitJsonCodec[ProbabilitiesChanger],
                                     ) : CodecJson[BoundedRationality] =
    CodecJson.casecodec2(BoundedRationality.apply, BoundedRationality.unapply)(
      "payoffChanger", "probabilitiesChanger"
    )

}
