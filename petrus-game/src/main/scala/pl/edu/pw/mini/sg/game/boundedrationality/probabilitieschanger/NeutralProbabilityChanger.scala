package pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger

case object NeutralProbabilityChanger extends ProbabilitiesChangerRegular {

  override def changeProbability[DM](prob: Seq[(DM,Double)], allMoves: => Seq[DM]): Seq[(DM, Double)] = {
    prob
  }

}
