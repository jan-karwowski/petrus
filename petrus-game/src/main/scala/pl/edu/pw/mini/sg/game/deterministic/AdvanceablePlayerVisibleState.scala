package pl.edu.pw.mini.sg.game.deterministic

import pl.edu.pw.mini.sg.rng.RandomGenerator


trait AdvanceablePlayerVisibleState[M, S <: AdvanceablePlayerVisibleState[M, S]] extends PlayerVisibleState[M] {
  self: S => 
  def possibleContinuations(move: M) : List[S]
  def sampleContinuations(move: M)(implicit rng: RandomGenerator) : S
}
