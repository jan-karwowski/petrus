package pl.edu.pw.mini.sg.game.deterministic;

trait PlayerVisibleState[M] extends Immutable {
  def possibleMoves: List[M]
}
