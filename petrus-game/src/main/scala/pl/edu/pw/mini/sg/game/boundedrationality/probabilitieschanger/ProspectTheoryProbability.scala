package pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger

final case class ProspectTheoryProbability(gamma: Double) extends ProbabilitiesChangerRegular {

  override def changeProbability[DM](prob: Seq[(DM,Double)], allMoves: => Seq[DM]): Seq[(DM, Double)] = {
    val x = prob.map{case (dm, prob) => (dm, weightingFunc(prob))}
    val sum = prob.view.map(_._2).sum
    x.map{case (dm, prob) => (dm, prob/sum)}
  }

  def weightingFunc(x: Double): Double = {
    val v =math.pow(x, gamma) / math.pow(math.pow(x, gamma) + math.pow(1 - x, gamma), 1 / gamma)
    require(v >= 0, s"v: $v, x: $x")
    v
  }

}
