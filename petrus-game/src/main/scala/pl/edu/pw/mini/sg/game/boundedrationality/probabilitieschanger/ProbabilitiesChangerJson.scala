package pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger

import argonaut.{ CodecJson, DecodeJson, DecodeResult, EncodeJson }
import pl.edu.pw.mini.sg.game.boundedrationality.AnchoringTheory
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}

object ProbabilitiesChangerJson {

  implicit val anchoringTheoryCodec : CodecJson[AnchoringTheory] = CodecJson.casecodec1(AnchoringTheory.apply, AnchoringTheory.unapply)("alpha")

  implicit val prospectTheoryProbabilityCodec : CodecJson[ProspectTheoryProbability] = CodecJson.casecodec1(ProspectTheoryProbability.apply, ProspectTheoryProbability.unapply)("gamma")

  implicit val neutralProbabilityChangerEncode : EncodeJson[NeutralProbabilityChanger.type ] = EncodeJson.UnitEncodeJson.contramap { x => Unit }
  implicit val neutralProbabilityChangerDecode : DecodeJson[NeutralProbabilityChanger.type] = DecodeJson(x => DecodeResult.ok(NeutralProbabilityChanger))

  implicit val simplifiedAnchoringTheoryCodec : CodecJson[SimplifiedAnchoringTheory] = CodecJson.casecodec1(SimplifiedAnchoringTheory.apply, SimplifiedAnchoringTheory.unapply)("alpha")

  implicit val probabilitiesChangerCodec: FlatTraitJsonCodec[ProbabilitiesChanger] =
    new FlatTraitJsonCodec[ProbabilitiesChanger](List(
      new TraitSubtypeCodec(anchoringTheoryCodec, anchoringTheoryCodec, "anchoringTheory"),
      new TraitSubtypeCodec(simplifiedAnchoringTheoryCodec, simplifiedAnchoringTheoryCodec, "simplifiedAnchoringTheory"),
      new TraitSubtypeCodec(prospectTheoryProbabilityCodec, prospectTheoryProbabilityCodec, "prospectTheoryProbability"),
      new TraitSubtypeCodec(neutralProbabilityChangerEncode, neutralProbabilityChangerDecode, "neutralProbabilityChanger")
    ))
}
