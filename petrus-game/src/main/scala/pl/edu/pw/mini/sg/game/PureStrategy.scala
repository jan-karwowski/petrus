package pl.edu.pw.mini.sg.game.deterministic;


trait PureStrategy[M, S] {
  def state : S
  def move: M

  def nextState(resultingState: S) : PureStrategy[M, S]
  def shortString: String 
}
