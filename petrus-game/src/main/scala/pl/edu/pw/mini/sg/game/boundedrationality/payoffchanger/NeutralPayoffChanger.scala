package pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger

import pl.edu.pw.mini.sg.game.Payoff

final object NeutralPayoffChanger extends PayoffChanger {

  override def changeExpectedPayoff(payoff: Payoff): Payoff = {
    payoff
  }
}
