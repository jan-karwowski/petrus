package pl.edu.pw.mini.sg.game.deterministic.plugin.config

import java.util.UUID

trait GameConfig {
  val id: UUID
}
