package pl.edu.pw.mini.sg.game.results


import pl.edu.pw.mini.sg.game.deterministic.{BehavioralStrategy, PlayerVisibleState}

final case class OutOfTreeNode[M, S <: PlayerVisibleState[M]](override val state: S) extends BehavioralStrategy[M, S] {
  override def nextState(move: M,reachedState: S): BehavioralStrategy[M,S] = new OutOfTreeNode[M, S](reachedState)
  override def probabilities: List[(M, Double)] = {
    val l = state.possibleMoves.length
    state.possibleMoves.map(m => (m, 1.0/l.toDouble))
  }

  override def successorsInTree: Iterable[(M, S)] = Seq()
}


final case class ImmutableBehavioralStrategy[DM, DS <: PlayerVisibleState[DM]](
  override val state: DS,
  override val probabilities: Vector[(DM, Double)],
  successors: Map[(DM, DS), BehavioralStrategy[DM, DS]]
) extends BehavioralStrategy[DM, DS] {
  override def nextState(move: DM,reachedState: DS): BehavioralStrategy[DM,DS] = {
    successors.get((move, reachedState)).getOrElse(new OutOfTreeNode[DM, DS](reachedState))
  }


  def successorsInTree: Iterable[(DM, DS)] = successors.keys
}
