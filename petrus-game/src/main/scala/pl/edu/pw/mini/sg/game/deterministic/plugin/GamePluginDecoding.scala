package pl.edu.pw.mini.sg.game.deterministic.plugin

trait GamePluginDecoding extends GamePlugin {
  override val singleCoordFactory : GameFactoryDecoding[GameConfig]
}
