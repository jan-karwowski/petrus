package pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger

import pl.edu.pw.mini.sg.game.Payoff

trait PayoffChanger {

  def changeExpectedPayoff(payoff: Payoff): Payoff = {
    payoff
  }

  final def changeAttackerPayoff(defender: Double, attacker: Double) : Double =
    changeExpectedPayoff(new Payoff(defender, attacker)).attacker

  final def apply(payoff: Payoff) : Payoff = changeExpectedPayoff(payoff)
}
