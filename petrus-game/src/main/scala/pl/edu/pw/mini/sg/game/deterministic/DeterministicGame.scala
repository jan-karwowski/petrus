package pl.edu.pw.mini.sg.game.deterministic;


trait DeterministicGame[DM, AM, DS <: PlayerVisibleState[DM], AS <: PlayerVisibleState[AM], G <: DeterministicGame[DM, AM, DS, AS, G]] extends Immutable {
  self : G =>

  final type AttackerState = AS
  final type DefenderState = DS
  def makeMove(defenderMove: DM, attackerMove: AM) : G
  def defenderState : DS
  def attackerState : AS
  def isLeaf: Boolean = {
    require(defenderState.possibleMoves.isEmpty == attackerState.possibleMoves.isEmpty)
    return defenderState.possibleMoves.isEmpty
  }

  def defenderPayoff: Double
  def attackerPayoff: Double
}
