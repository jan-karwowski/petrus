package pl.edu.pw.mini.sg.game.deterministic.plugin

import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJsonDecoders

trait GameFactoryDecoding[GC] extends GameFactory[GC] {
  def jsonDecoders(gameConfig: GC) : DeterministicGameJsonDecoders[DefenderMove, AttackerMove, DefenderState, AttackerState]
}
