package pl.edu.pw.mini.sg.game

import argonaut.CodecJson


final class Payoff(val defender: Double, val attacker: Double) {
  def +(payoff2: Payoff) : Payoff = new Payoff(defender+payoff2.defender, attacker+payoff2.attacker)
  def *(coeff: Double) : Payoff = new Payoff(defender * coeff, attacker * coeff)

  override def toString: String = s"P(d: ${defender}, a: ${attacker})"

  override def hashCode = (defender, attacker).##
  override def equals(other: Any) : Boolean = other match {
    case p : Payoff => p.defender == defender && p.attacker == attacker
    case _ => false
  }
}

object Payoff {
  implicit val payoffCodec : CodecJson[Payoff] = CodecJson.casecodec2[Double, Double, Payoff]((d, a) => new Payoff(d, a), p => Some((p.defender, p.attacker)))("defender", "attacker")


  val zero: Payoff = new Payoff(0,0)
}
