package pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger


final case class SimplifiedAnchoringTheory(alpha: Double) extends ProbabilitiesChangerLeaf {
  require(alpha >=0)

  override def leafAdjust(prob: Double, parentBranchingCoeff: Double) : Double =
    alpha*parentBranchingCoeff+prob
}
