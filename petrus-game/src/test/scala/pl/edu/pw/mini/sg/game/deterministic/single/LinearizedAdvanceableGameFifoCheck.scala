package pl.edu.pw.mini.sg.game.deterministic.single


object LinearizedAdvanceableGameFifoCheck extends
    LinearizedGameCheck("Linearized Advanceable Game Fifo", LinearizedAdvanceableGameFifo)
