
add.score <- function(results.file) {
    results <- read.csv(results.file, header=TRUE)
    randres <- results$randomDefenderPayoff.mean
    optres <- results$gtoptDefenderPayoff.mean

    calculate.score <- function (score) {
        round((sroce - randres)/(optres-randres),2)
    }

    results <- rbind(results, round(do.call(data.frame, Map(function(col) mean(col, na.rm=TRUE), results)),2))
    
    write.csv(results, results.file, row.names=FALSE)
}


args <- commandArgs(trailingOnly = TRUE)

add.score(args[1])
