# File formats used by the program

The program uses several kinds of files. All files are based on json
format. Here we provide a description of file formats used. There are
the following types of files:

 - files describing games, located in games directory in repository
   root. Each type of game has its own file extension. There are the
   following game types:
     - [a game on a graph](graph-game.md), extension .ggame
	 - [a matrix (normal form) game](matrix-game.md) (.nfgame)
	 - moving targets game (.sgmt). It is defined in another
	 project -- securitygamesmovingtargets (TODO project link)
 - configuration files to main SE approximation method in directory
   mixed-config:
       - [with uct based attacker sampling](uctattackersampler.md)
	   - [with nested-mcs based sampling](nestedmcs.md)
	   - yielding uniform random strategy (for evaluation of other
         methods) -- the only possible value of configuration file in
         in uniform.json there are no configurable parameters in this
         method. 
 - configuration for implementations of other methods (by other people
   unless stated otherwise)
     - TODO

<!-- Local Variables: -->
<!-- ispell-local-dictionary: "en_US" -->
<!-- End: -->
