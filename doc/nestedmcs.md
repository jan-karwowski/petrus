# Nested monte carlo search attacker strategy sampler

The format is very similar to UCT/MCTS attacker sampler. There are
three fields in the object:

| field name        | value                       |
| -------           | ------                      |
| nestedMcsConfig   | config of nested mcs search |
| infeasiblePenalty | see MCTS sampler            |
| mixedConfig       | see MCTS sampler            |

## Nested MCS config

The object contains only single field

| field name | value                                                    |
| -------    | ------                                                   |
| depth      | depth of nested mcs search                               |
| samples    | number of MC samples at level of depth to evaluate state |


