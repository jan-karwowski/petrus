# Game on a graph

## Rules

(for names in double quotes refer to file format)

The game takes place on a directed graph.

1. Defender units are placed initially in vertex 0
2. Attacker unit is placed in vertex "spawn"
3. In each turn players make their decisions simultaneously .The
   decision is to move units to adjacent vertices or stay in a
   current one. Note that edges are directed and players can move only
   from edge begin to end, not otherwise. To simulate non-directed
   graph one can make a graph with pairs of edges in opposite
   directions. 
4. The game ends: 
    - after "rounds" rounds is played -- both players receive payoff
      of 0.5
    - after defender and attacker meet in one vertex. The attacker is
      caught and receives given penalty. The defender receives a
      reward. (Both payoffs are in accordance to game config file)
    - after the defender reaches the target vertex and there is no
      defender in that vertex. Than the defender receives according
      penalty and the attacker reward. 

## File format

Json fields:

 - graphConfig -- definition of a directed graph the game takes place
   on. Vertices are numbered from 0 to vertexCount - 1
      - edges array of objects:
	        - from -- an integer -- number of the vertex where the
              edge starts
     	    - to -- an integer
      - vertexCount -- number of vertices. Must be compatible with
        edges array (large enough to fit all edges)
 - rounds -- integer -- number of rounds -- length of the game. 
 - defenderUnitCount -- number of defender units. One or more.
 - spawn -- a vertex number where the attacker starts
 - targets -- an array of vertex numbers where the targets are
 - vertexDefenderRewards -- payoff for defender for catching the
   attacker in given target. Length must be equal to vertexCount
 - vertexAttackerPenalties -- payoff for attacker when it's caught by
   the defender. Must be of length vertexCount.
 - targetAttackerRewards -- payoff which the attacker receives when it
   reaches the target successfully. Must be of the same length as
   targets array.
 - targetDefenderPenalties -- payoff which the defender receives when
   attacker reaches the target successfully. Must be of the same
   length as targets array.
 - id -- an UUID that uniquely identifies particular game
 - things that are not specified in any field: 
    - defender's start is always in vertex 0
	- number of attacker's units is always 1
	- when there is no security accident during the game both parties
      receive 0.5 payoff. Usually penalties in game are smaller than
      0.5 and rewards are greater than 0.5. All payoffs should be from
      range [0,1]

<!-- Local Variables: -->
<!-- ispell-local-dictionary: "en_US" -->
<!-- End: -->
