# UCT/MCTS Attacker Sampler configuration file

A file format is json-based. Root object contains the following
fields: 

| field name             | field value                                                                          |
| ---------------------- | --------------------------------------------------------------------                 |
| mixedMethod            | a configuration for finding defender strategy for given attacker                     |
| infeasiblePenalty      | penalty propagated in MCTS if there is no feasible defender strategy                 |
| mcts                   | [MCTS configuration](#mcts-configuration)                                            |
| infeasibleProgragation | [should infeasible strategy responses be added to the tree](#infeasible-propagation) |



## MCTS Configuration

An object with MCTS configuration contains the following fields:

| field name             | field value                                                                                         |
| ---------------------- | --------------------------------------------------------------------                                |
| selection              | [selection function](#available-selection-functions) (confidence bound, but see parameter statCalc) |
| simulations            | number of visits of root state until algorithm is stopped                                           |
| statCalc               | [How Q estimate is calculated](#available-stat-calculators)                                         |

	
	
### Available selection functions

#### UCT

UCB1 formula. Object contains fields:

| field name | field value                    |
| ------     | -----                          |
| type       | "uct"                          |
| c          | value of C coefficient in UCB1 |

Note: Q value for UCB1 comes from `statCalc`.

### Available stat calculators

#### Mean

Q estimate is an arithmetic mean of sample results

| field name | field value |
| ------     | -----       |
| type       | "mean"      |

#### Max

Q estimate is a maximum of sample results

| field name | field value |
| ------     | -----       |
| type       | "max"       |


## Infeasible propagation

When MCTS attacker's strategy sampler reaches a strategy for which do
not exists defender's strategy that forces such optimal attacker's
response (i.e. attacker's strategy is dominated) a penalty is
propagated. The mixed strategy method generates some (infeasible)
strategy tough. The payoff of such defender against its optimal
attacker (different than currently sampled) maybe be propagated
through the MCTS tree as well, depending on this setting. 
Possible values are:

 - `"noPropagation"` -- only sampled sequence result is propagated
 - `"justPropagation"` -- the result of the defender against its best
   response is propagated as well. Without any changes against
   defender's strategy
 - `"recalcPropagation"` -- the result is first improved against its
   best response and then propagated.

Note that the two latter options increase simulations counter in root
by 2 (and therefore cause algorithm to stop sooner). 

## Defender's mixed strategy finder

The details of the method are mainly described in published or
submitted papers. The object contains the following fields:


| field name                   | field value                                                                                                                                |
| ------------------           | -------------------------------------------------                                                                                          |
| adjuster                     | in-node strategy adjuster type                                                                                                             |
| expander                     | behavioral strategy tree expansion method                                                                                                  |
| refinementIterations         | maximum number of positive pass iterations                                                                                                 |
| fasibilityIterations         | maximum number of feasibility pass iterations after single positive pass. If exceeded current attacker's strategy is considered dominated. |
| poorImprovementIterationStop | number of positive pass iterations without improvement until algorithm is stopped                                                          |
| poorImprovementEps           | Minimal payoff gain that is considered improvement                                                                                         |
| subtreeChooser               | How to choose subtrees of behavioral strategy to improve                                                                                   |
| improveDecision              | When should be parent node updated after child changes                                                                                     |
### Available adjusters

#### Momentum

| field name    | field value  |
| ------------- | ------------ |
| type          | "momentum"   |


#### Momentum direction limit

| field name    | field value              |
| ------------- | ------------             |
| type          | "momentumDirectionLimit" |


#### Momentum update sign limit
| field name    | field value         |
| ------------- | ------------        |
| type          | "momentumUpdateSignLimit" |


#### Squared normalized momentum
| field name    | field value                                                       |
| ------------- | ------------                                                      |
| type          | "squaredNormalizerMomentum"                                       |
| exponent      | normalizer exponent value (in early version fixed at 2, thus name |


#### inhibiting

| field name    | field value  |
| ------------- | ------------ |
| type          | "inhibiting" |
| rate          |              |


#### linear inhibiting

| field name    | field value        |
| ------------- | ------------       |
| type          | "linearInhibiting" |
| rate          |                    |


#### global damping

| field name    | field value  |
| ------------- | ------------ |
| type          | "globalDamping" |


#### global sign change

| field name    | field value  |
| ------------- | ------------ |
| type          | "globalSignChange" |


#### coord sign change

| field name    | field value  |
| ------------- | ------------ |
| type          | "coordSignChange" |

#### bisect

| field name    | field value                                       |
| ------------- | ------------                                      |
| type          | "bisect"                                          |
| fallback      | fallback adjuster config (cannot be bisect again) |

#### bisect rotate

| field name    | field value                                       |
| ------------- | ------------                                      |
| type          | "bisectRotate"                                    |
| fallback      | fallback adjuster config (cannot be bisect again) |
| edgeDist      |                                                   |


#### push to edge

| field name    | field value                                       |
| ------------- | ------------                                      |
| type          | "pushToEdge"                                      |
| inner         | fallback adjuster config (cannot be bisect again) |


#### iterative push to edge

| field name    | field value                                       |
| ------------- | ------------                                      |
| type          | "iterativePushToEdge"                             |
| inner         | fallback adjuster config (cannot be bisect again) |


#### decreasing rate

| field name    | field value                                      |
| ------------- | ------------                                     |
| type          | "decreasingRate"                                 |
| rate          | rate decreasing factor (should be less than one) |


### Available tree expansions

#### Random move expander

| field name                | value                                                    |
| ----------                | -------------------------------------------------------- |
| type                      | "random"                                                 |
| subtreeExpandedMultiplier | double                                                   |
| probabilityBoost          | double                                                   |
| positiveExpander          | expander in positivePass                                 |
| negativeExpander          | expander in feasibility pass                             |
| initialExpander           | expander for building initial strategy from empty tree   |

All expander fields have the same type, described below.

### Inner expanders for tree exapnsions

#### MCTS based expander

| field name         | value                                                                |
| ----------         | --------------------------------------------------------             |
| type               | "mcts"                                                               |
| mctsParams         | mcts parameters, same format as in attacker sampler's mcts parameter |
| evaluationFunction | which game result is propagated in mcts tree                         |

##### Possible evaluationFunctions

 - `"positiveAttacker"` -- attacker's result
 - `"negativeAttacker"` -- negative attacker's result
 - `"positiveDefender"` -- defender's result
 - `"negativeDefender"` -- negative defender's result

### Available subtree choosers

Decides if strategy adjustment will descend to given subtree. Based on
assessment values. 

#### Value subtree chooser

| field name       | value                                                    |
| ----------       | -------------------------------------------------------- |
| type             | "value"                                                  |
| probabilityBoost | double                                                   |


### Available improvement decisions


#### Always improve
The strategy is improved unconditionally. Object fields:

| field name    | field value  |
| ------------- | ------------ |
| type          | "always"     |

#### Proportinal improve
The strategy is improved proportionally to current assesment value. 
Fields: 

| field name | field value                                      |
|------------|--------------------------------------------------|
| type       | "proportional"                                   |
| lowerLimit | assessment value below which no update occurs    |
| upperLimit | assessment value over which update occurs always |

<!-- Local Variables: -->
<!-- ispell-local-dictionary: "en_US" -->
<!-- End: -->

