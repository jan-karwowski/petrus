# Matrix game 

Fields in config file: 

 - id -- unique id of the game
 - defenderMatrix -- matrix of payoffs that defender receives
 - attackerMatrix -- matrix of payoffs that attacker receives

Both matrices must be of the same size defender is row payer (first
matrix index), attacker is a column player (second, inner,  matrix index)
