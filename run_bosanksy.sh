#!/bin/bash

GAME=$1
filename=$(basename $GAME)
filename=${filename%.*}
OUTDIR=bosansky2015-out

mkdir -p $OUTDIR

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GUROBI_HOME/lib:$SCIP_HOME

JAVA_OPTS="-Djava.library.path=$LD_LIBRARY_PATH" ./target/pack/bin/petrus-se -g $GAME -o $OUTDIR/${filename}.trmt -m $2