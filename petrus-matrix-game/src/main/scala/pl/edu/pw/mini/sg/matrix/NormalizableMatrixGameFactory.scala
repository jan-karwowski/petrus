package pl.edu.pw.mini.sg.matrix

import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import pl.edu.pw.mini.jk.sg.game.immutable.mutable.MutableWrapper
import squants.time.Time


case class NormalizableMatrixGameFactory(gameConfig: MatrixGameConfig)
    extends NormalizableFactory[CoordMove, CoordMove, MutableWrapper[CoordMove, CoordMove, State, State, MatrixGame]] {
  val game = MatrixGame.fromConfig(gameConfig)

  override def create: MutableWrapper[CoordMove, CoordMove, State, State, MatrixGame] = new MutableWrapper[CoordMove, CoordMove, State, State, MatrixGame](game)
  override def denormalize = this
  override def normalize = this

  override def defenderResultAsJson(strategy: java.util.Map[_ <: java.util.List[pl.edu.pw.mini.sg.matrix.CoordMove], java.lang.Double],trainingTimeS: Double,methodName: String,methodConfig: argonaut.Json, preprocessingTime: Time): argonaut.Json = ???
  override def defenderStrategyToJson(strategy: java.util.Map[_ <: java.util.List[pl.edu.pw.mini.sg.matrix.CoordMove], java.lang.Double]): argonaut.Json = ???
}
