package pl.edu.pw.mini.sg.matrix

import pl.edu.pw.mini.jk.sg.game.SimpleGamePlugin


case object MatrixGamePlugin extends SimpleGamePlugin[MatrixGameConfig](
  _.endsWith(".nfgame"),
  SimpleGamePlugin.fileParserFromJson(MatrixGameConfigJson.matrixGameConfigDecodeJson),
  MatrixVariantRunner.apply _
)
