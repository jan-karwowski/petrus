package pl.edu.pw.mini.sg.matrix

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove

final case class CoordMove(
  chosen: Int) extends DefenderMove
