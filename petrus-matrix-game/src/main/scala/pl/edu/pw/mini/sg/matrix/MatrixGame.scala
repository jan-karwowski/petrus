package pl.edu.pw.mini.sg.matrix

import pl.edu.pw.mini.jk.sg.game.immutable.{ ImmutableGame, Payoffs }

/**
 * Macierzy wypłat jako wektor wektorów,
 * wsp. dużego wektora -> ruchy obrońcy
 * wsp. małego wektora -> ruchy atakującego
 */
final case class MatrixGame(
  defenderPayoffs: Vector[Vector[Double]],
  attackerPayoffs: Vector[Vector[Double]],
  defenderState: State,
  attackerState: State,
  override val attackerPayoff: Double,
  override val defenderPayoff: Double) extends ImmutableGame[CoordMove, CoordMove, State, State, MatrixGame] {
  override def attackerVisibleState: State = attackerState
  override def defenderVisibleState: State = defenderState
  override def makeMoves(defenderMove: CoordMove, attackerMove: CoordMove): (Payoffs, MatrixGame) = {
    val attackerPayoff = attackerPayoffs(defenderMove.chosen)(attackerMove.chosen)
    val defenderPayoff = defenderPayoffs(defenderMove.chosen)(attackerMove.chosen)
    (Payoffs(defenderPayoff, attackerPayoff, 0, 0),
      copy(
        attackerPayoff = attackerPayoff, defenderPayoff = defenderPayoff,
        defenderState = defenderState.advance(defenderMove),
        attackerState = attackerState.advance(attackerMove)))
  }
  override def timeStep: Int = defenderState.timeStep
}

object MatrixGame {
  def fromConfig(config: MatrixGameConfig): MatrixGame =
    MatrixGame(config.defenderMatrix, config.attackerMatrix,
      State.fromDimension(config.defenderMatrix.length),
      State.fromDimension(config.defenderMatrix(0).length),
      0, 0)
}
