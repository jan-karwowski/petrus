package pl.edu.pw.mini.sg.matrix

import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.game.common.advanceable.{ AdvanceableAttackerState, AdvanceableDefenderState }
import pl.edu.pw.mini.jk.sg.game.immutable.AdvanceableState
import scala.collection.JavaConverters._

case class State(possibleMoves: ImmutableList[CoordMove], timeStep: Int) extends AdvanceableState[CoordMove, State] with AdvanceableDefenderState[CoordMove] with AdvanceableAttackerState[CoordMove] {
  override def advance(move: CoordMove): State = {
    if(!possibleMoves.contains(move)) throw new IllegalArgumentException(s"Move $move not allowed in state")
    else copy(possibleMoves = ImmutableList.of(), timeStep = timeStep+1)
  }
  override def getPossibleMoves: ImmutableList[CoordMove] = possibleMoves
  override def getTimeStep: Int = timeStep
}

object State {
  def fromDimension(dim: Int): State = State(
    ImmutableList.copyOf(Range(0, dim).view.map(CoordMove.apply).iterator.asJava),
    0
  )
}
