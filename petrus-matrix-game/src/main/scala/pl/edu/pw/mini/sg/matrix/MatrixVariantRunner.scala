package pl.edu.pw.mini.sg.matrix

import pl.edu.pw.mini.jk.sg.game.immutable.mutable.MutableWrapper
import pl.edu.pw.mini.jk.sg.game.{ GameVariantRunner, NormalizableFactoryAction , GameVariantRunnerResult}
import squants.time.Milliseconds


case class MatrixVariantRunner(gameConfig: MatrixGameConfig)
    extends GameVariantRunner {
  val gameFactory = NormalizableMatrixGameFactory(gameConfig)

  override def runWithCompactGameFactory[R](action: NormalizableFactoryAction[R]): GameVariantRunnerResult[R] = 
    GameVariantRunnerResult(action.runWithNormalizableFactory[CoordMove, CoordMove, State, State,
     MutableWrapper[CoordMove, CoordMove, State, State, MatrixGame]](
      gameFactory ,1), Milliseconds(0));

  override def runWithCoordGameFactory[R](action: pl.edu.pw.mini.jk.sg.game.NormalizableFactoryAction[R]): GameVariantRunnerResult[R] = runWithCompactGameFactory(action);
  override def runWithMultiMovePerTimestepFactory[R](action: pl.edu.pw.mini.jk.sg.game.NormalizableFactoryAction[R]): GameVariantRunnerResult[R] = runWithCompactGameFactory(action);
  override def runWithSingleMovePerTimestepFactory[R](action: pl.edu.pw.mini.jk.sg.game.NormalizableFactoryAction[R]): GameVariantRunnerResult[R] = runWithCompactGameFactory(action);
}
