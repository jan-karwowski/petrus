package pl.edu.pw.mini.sg.matrix

import argonaut.Parse
import org.scalatest.{ Matchers, WordSpec }
import java.util.UUID


class MatrixGameConfigJsonSpec extends WordSpec with Matchers {
  val id = UUID.fromString("5a8f1dc8-0cbb-48af-9286-078ad823c8fa")

  val goodInput = Parse.parse(s"""{
"id": "${id.toString}",
"defenderMatrix": [[1,2],[3,4],[5,6]],
"attackerMatrix": [[0,2],[4,6],[8,10]]
}""").right.get

  val badShape = Parse.parse(s"""{
"id": "${id.toString}",
"defenderMatrix": [[1,2],[3],[5,6]],
"attackerMatrix": [[0,2],[4,6],[8,10]]
}""").right.get

  val badShape2 = Parse.parse(s"""{
"id": "${id.toString}",
"defenderMatrix": [[1,2],[3,4],[5,6]],
"attackerMatrix": [[0,2],[4],[8,10]]
}""").right.get

  val notMatchingSizes = Parse.parse(s"""{
"id": "${id.toString}",
"defenderMatrix": [[1,2],[3,4],[5,6]],
"attackerMatrix": [[0,2,1],[4,6,1],[8,10,1]]
  }""").right.get

  import MatrixGameConfigJson.matrixGameConfigDecodeJson

  "MatrixGameConfig decoder" should {
    "decode proper input" in {
      goodInput.as[MatrixGameConfig].toEither should equal(
        Right(MatrixGameConfig(id, Vector(Vector(0.1,0.2), Vector(0.3,0.4), Vector(0.5,0.6)),
          Vector(Vector(0,0.2), Vector(0.4,0.6), Vector(0.8,1.0))))
      )
    }
  }
}
