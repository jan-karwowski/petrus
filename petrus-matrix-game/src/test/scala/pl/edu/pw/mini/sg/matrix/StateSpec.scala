package pl.edu.pw.mini.sg.matrix

import org.scalatest.{ Matchers, WordSpec }

import scala.collection.JavaConverters._

class StateSpec extends WordSpec with Matchers {
  "state with dimension 3" when {
    val state = State.fromDimension(3)
    "initially" should {
      "allow three moves" in {
        state.getPossibleMoves.asScala should contain only(CoordMove(0), CoordMove(1), CoordMove(2))
      }

      "have timestep 0" in {
        state.timeStep should equal(0)
      }

      "thorw illegal argument exception with CoordMove(3)" in {
        an[IllegalArgumentException] shouldBe thrownBy { state.advance(CoordMove(3)) }
      }
    }

    "after playing move" should {
      val adv = state.advance(CoordMove(0))
      "not allow any move" in {
        adv.getPossibleMoves.asScala shouldBe empty
      }

      "have timestep 1" in {
        adv.timeStep should equal(1)
      }

      "throw illegal argument exception with CoordMove(0)" in {
        an[IllegalArgumentException] shouldBe thrownBy { adv.advance(CoordMove(0)) }
      }
    }
  }
}
