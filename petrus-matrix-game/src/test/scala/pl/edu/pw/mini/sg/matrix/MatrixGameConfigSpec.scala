package pl.edu.pw.mini.sg.matrix

import org.scalatest.{Matchers, WordSpec}



class MatrixGameConfigSpec extends WordSpec with Matchers {
  val goodShape = Vector(Vector(1,2), Vector(1,2), Vector(1,2))
  val badShape = Vector(Vector(1), Vector(1,2), Vector(1,2))
  val ev = Vector()

  "getSecondDimension" when {
    "cheking bad shaped matrix" should {
      "return error" in {
        MatrixGameConfig.getSecondDimension(badShape).isLeft should equal(true)
      }
    }

    "checking good shaped matrix" should {
      "return 2" in {
        MatrixGameConfig.getSecondDimension(goodShape) should equal(Right(2))
      }
    }

    "checking empty vector" should {
      "return error" in {
        MatrixGameConfig.getSecondDimension(ev).isLeft should be(true)
      }
    }
  }

  "getMatrixDimensions" when {
    "checkingBadShapedMatrix" should {
      "return error" in {
        MatrixGameConfig.getMatrixDimensions(badShape).isLeft should be(true)
      }
    }

    "checking good shaped matrix" should {
      "return (3,2)" in {
        MatrixGameConfig.getMatrixDimensions(goodShape) should equal(Right((3,2)))
      }
    }

    "checking empty vector" should {
      "return error" in {
        MatrixGameConfig.getMatrixDimensions(ev).isLeft should equal(true)
      }
    }
  }
}
