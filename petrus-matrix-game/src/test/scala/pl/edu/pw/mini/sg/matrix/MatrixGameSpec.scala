package pl.edu.pw.mini.sg.matrix

import java.util.UUID
import org.scalatest.{ Matchers, WordSpec }
import pl.edu.pw.mini.jk.sg.game.immutable.Payoffs
import scala.collection.JavaConverters._

class MatrixGameSpec extends WordSpec with Matchers {
  val id = UUID.fromString("5a8f1dc8-0cbb-48af-9286-078ad823c8fa")

  "game with matrix ((1,2),(2,3),(4,5)) and am=dm*2" when {
    val matrix = Vector(Vector(1.0,2.0), Vector(2.0,3.0), Vector(4.0,5.0))
    val config = MatrixGameConfig(id, matrix, matrix.map(_.map(_*2)))
    val game = MatrixGame.fromConfig(config)

    "initial state" should {
      "have timestep 0" in {
        game.timeStep should equal(0)
      }

      "allow 3 defender moves" in {
        game.defenderVisibleState.possibleMoves.asScala should contain only(CoordMove(0), CoordMove(1), CoordMove(2))
      }

      "allow 2 attacker moves" in {
        game.attackerVisibleState.possibleMoves.asScala should contain only(CoordMove(0), CoordMove(1))
      }
    }

    "after playing (1,1)" should {
      val adv = game.makeMoves(CoordMove(1), CoordMove(1))
      val adg = adv._2

      "return  (3,6) payoff" in {
        adv._1 should equal(Payoffs(3, 6, 0, 0))
      }

      "have timestep 1" in {
        adg.timeStep should equal(1)
      }

      "not allow any moves" in {
        adg.defenderVisibleState.getPossibleMoves.asScala shouldBe empty
        adg.attackerVisibleState.getPossibleMoves.asScala shouldBe empty
      }

      "have defender reward 3" in {
        adg.defenderPayoff should equal(3)
      }

      "have attacker reward 6" in {
        adg.attackerPayoff should equal(6)
      }
    }
  }
}
