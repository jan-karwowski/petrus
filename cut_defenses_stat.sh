#!/bin/bash


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $DIR/el_config.sh

for game in $games; do
    for config in $configs; do
	echo -n \\gameinfo\{${game}\}\&\\config\{$(echo ${config}|sed 's/_/-/g')\}\&
	grep '"uctdefenses"' game${game}_${config}.agg.csv | cut -d, -f2- --output-delimiter=\&
	echo \\\\
    done
    echo \\hline
done

