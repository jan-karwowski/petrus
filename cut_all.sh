#!/bin/bash

SUFFIX=$1

../petrus/cut_attacks_stat.sh > ../opis/results-attack$SUFFIX.tex
../petrus/cut_defenses_stat.sh > ../opis/results-defenses$SUFFIX.tex
../petrus/cut_payoffs_stat.sh > ../opis/results-payoffs$SUFFIX.tex
../petrus/r_cut_attacks_stat.sh > ../opis/results-rattacks$SUFFIX.tex
../petrus/r_cut_defenses_stat.sh > ../opis/results-rdefenses$SUFFIX.tex
../petrus/r_cut_payoff_stat.sh > ../opis/results-rpayoff$SUFFIX.tex
