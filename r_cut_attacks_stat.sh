#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $DIR/el_config.sh

for game in $games; do
	echo -n \\gameinfo\{${game}\}\&
	grep '"randomattacks"' game${game}_ex12_len4.agg.csv | cut -d, -f2- --output-delimiter=\&
	echo \\\\
done

