package pl.edu.pw.mini.jk.sg.uct.statsupdater.simple

import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdaterFactory


object SimpleStatsUpdaterFactory extends StatsUpdaterFactory[Any, Any] {
  def create[S, M]() = new SimpleStatsUpdater[S, M]()
}
