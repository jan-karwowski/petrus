package pl.edu.pw.mini.jk.sg.uct.statsupdater

trait StatsUpdaterFactory[-S, -M] {
  def create[SS <: S, MM <: M]() : StatsUpdater[SS, MM]
}
