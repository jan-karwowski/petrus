package pl.edu.pw.mini.jk.sg.uct.statsupdater.tslimit

import argonaut.{ CodecJson }


object TimestepLimitUpdaterFactoryJson {
  implicit val tslUpdaterFactoryDecode : CodecJson[TimestepLimitUpdaterFactory] =
    CodecJson.casecodec1(TimestepLimitUpdaterFactory.apply, TimestepLimitUpdaterFactory.unapply)("limit")
}
