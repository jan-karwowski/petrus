package pl.edu.pw.mini.jk.sg.uct.tree

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }


trait TreeJoinerFunctions {
  type CS
  type GS

  def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: CS, globalStats: GS) : UCTDecisionStatistics
  def calculateContextStats(stats: => Iterable[UctReadableStatistics]) : CS
  def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) : GS 
}


object TreeJoinerFunctions {
  implicit class SimpleJoiner(fun: (Iterable[UctReadableStatistics], Int) => UCTDecisionStatistics) extends TreeJoinerFunctions {
    override type CS = Unit
    override type GS = Unit

    override def joinStats(stats: Iterable[UctReadableStatistics], treeCount: Int, localStats: Unit, globalStats: Unit) : UCTDecisionStatistics = fun(stats, treeCount)
    override def calculateContextStats(stats: => Iterable[UctReadableStatistics]) : Unit = Unit
    override def calculateGlobalStats(stats: => Iterable[UctReadableStatistics]) : Unit = Unit
  }
}
