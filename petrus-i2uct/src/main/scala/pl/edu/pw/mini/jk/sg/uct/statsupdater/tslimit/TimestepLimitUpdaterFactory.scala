package pl.edu.pw.mini.jk.sg.uct.statsupdater.tslimit

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdaterFactory



case class TimestepLimitUpdaterFactory(limit: Int)
    extends StatsUpdaterFactory[AdvanceableState[_], Any] {
  def create[S <: AdvanceableState[_], M]() = new TimestepLimitUpdater[S,M](limit)
}
