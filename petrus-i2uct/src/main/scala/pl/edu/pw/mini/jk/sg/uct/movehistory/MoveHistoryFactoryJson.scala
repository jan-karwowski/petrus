package pl.edu.pw.mini.jk.sg.uct.movehistory;

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import pl.edu.pw.mini.jk.sg.uct.movehistory.fixed.FixedMoveHistoryFactory
import pl.edu.pw.mini.jk.sg.uct.movehistory.tslimit.TimestepLimitHistoryFactory


object MoveHistoryFactoryJson {
  val fmhDecode = DecodeJson.UnitDecodeJson.map(x => FullMoveHistoryFactory)
  val emhDecode = DecodeJson.UnitDecodeJson.map(x => EmptyMoveHistoryFactory)
  val fixedmhDecode = DecodeJson.IntDecodeJson.map(new FixedMoveHistoryFactory(_))

  val tslimitDecode = DecodeJson.IntDecodeJson.map(new TimestepLimitHistoryFactory(_))

  def decode(decoders: Map[String, DecodeJson[_ <: MoveHistoryFactory]]) :
      DecodeJson[MoveHistoryFactory] = TraitJsonDecoder[MoveHistoryFactory](decoders).decoder

  val defaultDecode = decode(Map("full" -> fmhDecode,
    "empty" -> emhDecode, "fixed" -> fixedmhDecode,
      "tslimit" -> tslimitDecode
  ))
}
