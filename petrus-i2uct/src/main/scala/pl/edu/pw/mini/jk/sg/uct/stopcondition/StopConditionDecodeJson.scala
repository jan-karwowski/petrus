package pl.edu.pw.mini.jk.sg.uct.stopcondition

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame



object StopConditionDecodeJson {
  implicit val fasDecoder: DecodeJson[FirstAccidentStopCondition] = DecodeJson.UnitDecodeJson.map(x => new FirstAccidentStopCondition())

  implicit val decoder : DecodeJson[GameStopCondition[SecurityGame]] =
    // IMPORTANT: Currently GameMatrices assumes that firstAccident is set
    // If another stop condition will be given, GM will still use first accident!!
    TraitJsonDecoder[GameStopCondition[SecurityGame]](Map("firstAccident" -> fasDecoder)).decoder
}
