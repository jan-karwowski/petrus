package pl.edu.pw.mini.jk.sg.uct.statsupdater.fixedlength

import argonaut.DecodeJson



object FixedLengthUpdaterFactoryJson {
  val decode = DecodeJson.jdecode1L(FixedLengthUpdaterFactory.apply)("length")
}
