package pl.edu.pw.mini.jk.sg.uct.statsupdater.fixedlength

import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdaterFactory



case class FixedLengthUpdaterFactory(length: Int)
    extends StatsUpdaterFactory[Any, Any] {
  override def create[SS, MM]() = new FixedLengthUpdater[SS,MM](length)
}
