package pl.edu.pw.mini.jk.sg.uct.tree

import pl.edu.pw.mini.jk.sg.uct.{ UCTDecisionStatistics, UctReadableStatistics }



final class UnmodifiableDecisionStatisticsView(mutableStats: UCTDecisionStatistics) extends UctReadableStatistics {
  final def visitsCount: Int = mutableStats.getVisitsCount
  final def realVisitsCount: Int = mutableStats.getRealVisitsCount
  final def averagePayoff: Double = mutableStats.getAveragePayoff
}
