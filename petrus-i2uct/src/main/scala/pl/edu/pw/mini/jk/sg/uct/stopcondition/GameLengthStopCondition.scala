package pl.edu.pw.mini.jk.sg.uct.stopcondition;

import java.util.function.IntToDoubleFunction;
import pl.edu.pw.mini.jk.sg.game.common.SecurityGame;

final class GameLengthStopCondition(targetValue: Double, lengthTransform: IntToDoubleFunction)
  extends GameStopCondition[SecurityGame] {

  def this(targetValue: Double, lengthTransform: Int => Double) = this(
    targetValue,
    new IntToDoubleFunction {
      override def applyAsDouble(v: Int) = lengthTransform(v)
    }
  )

  override def isGameStopCondition(game: SecurityGame): Boolean = {
    val length = lengthTransform.applyAsDouble(game.getTimeStep);
    length >= targetValue;
  }
}
