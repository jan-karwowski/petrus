package pl.edu.pw.mini.jk.sg.uct.tree.movehistory

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.movehistory.MoveHistoryFactory
import pl.edu.pw.mini.jk.sg.uct.tree.{UctNode, UctTreeFactory}



case class LimitedHistoryTreeFactory(mhbf: MoveHistoryFactory)
    extends UctTreeFactory {
  override def create[S <: AdvanceableState[M], M](initialState: S): UctNode[S,M] =
    LimitedHistoryTree.emptyTree[S, M](mhbf, initialState).getRoot
  override def treeJoiner: LimitedHistoryTreeJoiner.type = LimitedHistoryTreeJoiner
}
