package pl.edu.pw.mini.jk.sg.uct.tree

import collection.JavaConverters._
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState

trait TreeJoiner {
  def joinTrees[S <: AdvanceableState[M], M](functions: TreeJoinerFunctions)(trees: Iterable[UctNode[S,M]]) : UctNode[S,M]
  def joinTrees[S <: AdvanceableState[M], M](functions: TreeJoinerFunctions, trees: java.lang.Iterable[UctNode[S,M]]) : UctNode[S, M] =
    joinTrees(functions)(trees.asScala)
}
