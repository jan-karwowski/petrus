package pl.edu.pw.mini.jk.sg.uct.statsupdater.fixedlength

import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdater
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import scala.collection.mutable.Queue


final class FixedLengthUpdater[S, M](length: Int) extends StatsUpdater[S, M] {
  private var currentLength: Int = 0
  private val nodesToUpdate = Queue[Option[(UctNode[S, M], M)]]()

  private def increaseEnsureLength() = {
    if(currentLength == length)
      nodesToUpdate.dequeue()
    else
      currentLength+=1
  }

  override def registerDecision(node: UctNode[S, M], move: M) : Unit = {
    node.registerVisit(move)
    nodesToUpdate.enqueue(Some((node, move)))
    increaseEnsureLength()
  }
  override def registerMcMove(state: S, move: M): Unit  = {
    nodesToUpdate.enqueue(None)
    increaseEnsureLength()
  }
  override def updatePayoffs(payoff: Double, state: S) : Unit = nodesToUpdate.foreach {
    case Some((node, move)) => node.registerPayoff(move, payoff)
    case _ => Unit
  }
}

