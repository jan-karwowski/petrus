package pl.edu.pw.mini.jk.sg.uct.tree;

import scala.collection.JavaConverters._

import org.apache.commons.math3.random.RandomGenerator
import pl.edu.pw.mini.jk.sg.uct.{SelectionFunction, UCTDecisionStatistics}


abstract class UctNodeStandardStats[S, M](protected val statisticsContainer : UctNodeStatisticsContainer[M]) extends UctNode[S,M] {
  override def moveCount = statisticsContainer.moveCount
  override def getStatistics: java.util.Collection[UctMoveStatistics[M]] = statisticsContainer.getStatistics
  override def getBestMove(rng: RandomGenerator) : M = statisticsContainer.getBestMove(rng)
  override def getVisitsCount : Int = statisticsContainer.getVisitsCount
  override def selectMove(sf: SelectionFunction) : Option[M] = statisticsContainer.selectMove(sf)
  override def registerVisit(move: M): Unit = statisticsContainer.registerVisit(move)
  override def getRealVisitsCount : Int = statisticsContainer.getRealVisitsCount
  override def registerPayoff(move: M, payoff: Double): Unit = statisticsContainer.registerPayoff(move, payoff)
  override def selectOrRandomUnvisidedMove(fun: SelectionFunction, rng: RandomGenerator) : M = statisticsContainer.selectOrRandomUnvisitedMove(fun, rng)

}


object UctNodeStandardStats {
  def mapFromMoveList[M](possibleMoves: java.util.List[M]) : Map[M, UCTDecisionStatistics] = mapFromMoveList(possibleMoves.asScala)

  def mapFromMoveList[M](possibleMoves: Seq[M]): Map[M, UCTDecisionStatistics] =  {
        val builder = Map.newBuilder[M, UCTDecisionStatistics]
        builder.sizeHint(possibleMoves.size)
        builder++=(possibleMoves.map((_, new UCTDecisionStatistics(0,0,0))))
        builder.result()
      }
}
