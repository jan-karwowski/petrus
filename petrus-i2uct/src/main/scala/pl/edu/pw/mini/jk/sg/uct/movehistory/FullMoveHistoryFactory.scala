package pl.edu.pw.mini.jk.sg.uct.movehistory


object FullMoveHistoryFactory extends MoveHistoryFactory {

  override def create[S]() : MoveHistory[S] = new FullMoveHistory(0, List())
}
