package pl.edu.pw.mini.jk.sg.uct.stopcondition

trait GameStopCondition[-G] {
  def isGameStopCondition(game: G) : Boolean
}
