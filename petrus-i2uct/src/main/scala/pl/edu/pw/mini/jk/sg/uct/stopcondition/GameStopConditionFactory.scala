package pl.edu.pw.mini.jk.sg.uct.stopcondition

import pl.edu.pw.mini.jk.sg.uct.SinglePlayerGame;

trait GameStopConditionFactory[-SG <: SinglePlayerGame[_,_]] {
     def createGameStopCondition : GameStopCondition[SG]
}
