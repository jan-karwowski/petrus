package pl.edu.pw.mini.jk.sg.uct.movehistory.fixed

import pl.edu.pw.mini.jk.sg.uct.movehistory.MoveHistory
import scala.collection.immutable.Queue



final case class FixedMoveHistory(queue: Queue[Any], maxLength: Int)
    extends MoveHistory[Any] {
  override def appendMove(state: Any, move: Any) : FixedMoveHistory = {
    val nq = queue.enqueue(move)
    if(nq.length > maxLength)
      copy(queue = nq.dequeue._2)
    else
      copy(nq)
  }
}
