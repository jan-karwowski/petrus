package pl.edu.pw.mini.jk.sg.uct.statsupdater.perpayoff

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdaterFactory


case class PerPayoffUpdaterFactory(tsLimit: Int)
    extends StatsUpdaterFactory[AdvanceableState[_], Any] {
  def create[SS <: AdvanceableState[_], MM]() =  new PerPayoffUpdater(tsLimit)
}
