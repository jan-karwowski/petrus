package pl.edu.pw.mini.jk.sg.uct.tree.movehistory

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.sg.uct.movehistory.{ MoveHistoryFactoryJson, MoveHistoryFactory }



object LimitedHistoryTreeFactoryJson {
  implicit def factoryDecode(implicit mhbf: DecodeJson[MoveHistoryFactory]) : DecodeJson[LimitedHistoryTreeFactory] = DecodeJson.jdecode1L(LimitedHistoryTreeFactory.apply)("moveHistory")

  implicit val decode : DecodeJson[LimitedHistoryTreeFactory] = factoryDecode(MoveHistoryFactoryJson.defaultDecode)
}

