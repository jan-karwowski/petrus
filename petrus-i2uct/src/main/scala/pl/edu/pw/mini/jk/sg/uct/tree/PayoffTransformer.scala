package pl.edu.pw.mini.jk.sg.uct.tree
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

trait PayoffTransformer {
  type GlobalStat
  type NodeStat

  def calculateNodeStat(node: UctNode[_, _]) : NodeStat
  def calculateGlobalStat(stat: => Iterable[UnmodifiableDecisionStatisticsView]): GlobalStat
  def transformStatistics(stat: UnmodifiableDecisionStatisticsView, nodeStat: NodeStat, globalStat: GlobalStat) : UCTDecisionStatistics
}
