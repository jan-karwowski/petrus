package pl.edu.pw.mini.jk.sg.uct.statsupdater

import pl.edu.pw.mini.jk.sg.uct.tree.UctNode


// Mógłby być kowariantny. Ale nie wiem, czy warto. 
trait StatsUpdater[S, M] {
  def registerDecision(node: UctNode[S, M], move: M) : Unit
  def registerMcMove(state: S, move: M): Unit 
  def updatePayoffs(payoff: Double, state: S) : Unit 
}
