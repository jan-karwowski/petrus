package pl.edu.pw.mini.jk.sg.uct.tree

import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics



trait PruneTreeFunctions {
  type T

  def nodeStatistics(node: UctNode[_,_]) : T
  /** True -- zostawić ruch */
  def movePredicate(statistics: UctReadableStatistics, nodeStat: T) : Boolean
}

object PruneTreeFunctions {
  class SimplePruneTreeFunctions(fun: UctReadableStatistics => Boolean) extends PruneTreeFunctions {
    override type T = Unit
    override def nodeStatistics(node: UctNode[_,_]) = Unit
    override def movePredicate(statistics: UctReadableStatistics, nodeStat: Unit) = fun(statistics)
  }

  import language.implicitConversions
  implicit def simplePrunePredicate(fun: UctReadableStatistics => Boolean) : PruneTreeFunctions = new SimplePruneTreeFunctions(fun)

}

class PureTreeFunctionsJava

object PruneTreeFunctionsJava {
  def fromJavaFunction(function: java.util.function.Predicate[UctReadableStatistics]) : PruneTreeFunctions =
    PruneTreeFunctions.simplePrunePredicate(function.test(_))
}

