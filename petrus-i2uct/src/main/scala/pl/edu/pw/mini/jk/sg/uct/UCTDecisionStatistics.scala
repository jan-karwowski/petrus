package pl.edu.pw.mini.jk.sg.uct;

import java.io.Serializable

import pl.edu.pw.mini.jk.sg.uct.tree.UnmodifiableDecisionStatisticsView

final class UCTDecisionStatistics(var visitsCount: Int, var realVisitsCount: Int, averagePayoffCons: Double) extends UctReadableStatistics with Serializable {
  private var totalPayoff : Double = averagePayoffCons*visitsCount
  def this(stats :  UCTDecisionStatistics) = this(visitsCount = stats.visitsCount, realVisitsCount = stats.realVisitsCount, averagePayoffCons = stats.getAveragePayoff)

  def appendOther(other: UctReadableStatistics) : Unit = {
    val totalVisits = visitsCount + other.visitsCount;
    visitsCount = totalVisits;
    realVisitsCount += other.realVisitsCount;
    totalPayoff +=other.averagePayoff*other.visitsCount;
  }


  def averagePayoff = getAveragePayoff
  def getAveragePayoff : Double = if(visitsCount > 0) totalPayoff/visitsCount else 0;


  def increaseVisitCounter() : Unit = {
    visitsCount+=1;
    realVisitsCount+=1;
  }

  def registerPayoff(payoff: Double) : Unit = {
    require(!payoff.isInfinity)
    totalPayoff+=payoff;
  }
  def typedClone = new UCTDecisionStatistics(visitsCount, realVisitsCount, getAveragePayoff)
  
  override def toString =  "UCTStat[visitsCount=" + visitsCount + ", averagePayoff=" + getAveragePayoff + "]";

  def unmodifiableView = new UnmodifiableDecisionStatisticsView(this)

  //Compatibility memebers
  def getVisitsCount = visitsCount
  def getRealVisitsCount = realVisitsCount

}

object UCTDecisionStatistics {
  def zeroStats() : UCTDecisionStatistics = new UCTDecisionStatistics(0, 0, 0);
}
