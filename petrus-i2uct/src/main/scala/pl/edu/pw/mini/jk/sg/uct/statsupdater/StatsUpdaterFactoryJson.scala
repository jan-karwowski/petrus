package pl.edu.pw.mini.jk.sg.uct.statsupdater

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.statsupdater.perpayoff.PerPayoffUpdaterFactoryJson
import pl.edu.pw.mini.jk.sg.uct.statsupdater.simple.SimpleStatsUpdaterFactory
import pl.edu.pw.mini.jk.sg.uct.statsupdater.fixedlength.FixedLengthUpdaterFactoryJson
import pl.edu.pw.mini.jk.sg.uct.statsupdater.tslimit.TimestepLimitUpdaterFactoryJson


object StatsUpdaterFactoryJson {
  implicit val simpleDecode : DecodeJson[SimpleStatsUpdaterFactory.type] = DecodeJson.UnitDecodeJson.map(x => SimpleStatsUpdaterFactory)

  implicit val decode: DecodeJson[StatsUpdaterFactory[AdvanceableState[_], Any]]= TraitJsonDecoder[StatsUpdaterFactory[AdvanceableState[_], Any]](Map(
    "simple" -> simpleDecode,
    "fixed" -> FixedLengthUpdaterFactoryJson.decode,
    "perPayoff" -> PerPayoffUpdaterFactoryJson.decode,
    "tsLimit" -> TimestepLimitUpdaterFactoryJson.tslUpdaterFactoryDecode
  )).decoder
}
