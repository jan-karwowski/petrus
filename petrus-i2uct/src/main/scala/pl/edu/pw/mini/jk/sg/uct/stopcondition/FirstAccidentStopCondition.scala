package pl.edu.pw.mini.jk.sg.uct.stopcondition;

import pl.edu.pw.mini.jk.sg.game.common.SecurityGame;

final class FirstAccidentStopCondition extends GameStopCondition[SecurityGame] {
  override def isGameStopCondition(game: SecurityGame) : Boolean = game.accidentOccured
}
