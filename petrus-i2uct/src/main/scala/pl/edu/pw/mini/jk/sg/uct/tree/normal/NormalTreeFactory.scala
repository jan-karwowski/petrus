package pl.edu.pw.mini.jk.sg.uct.tree.normal

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.tree.{ TreeJoiner, UctNode, UctTreeFactory }

case object NormalTreeFactory extends UctTreeFactory {
  override def create[S <: AdvanceableState[M], M](initialState: S): UctNode[S,M] =
    TreeNode.fromInitialState(initialState)
  override def treeJoiner: TreeJoiner = NormalTreeJoiner
}
