package pl.edu.pw.mini.jk.sg.uct;

import argonaut.DecodeJson
import pl.edu.pw.mini.jk.lib.TraitJsonDecoder

object DecodeSelectionFunctionJson {
  val decodeUcb1 : DecodeJson[Ucb1Selection] = DecodeJson.jdecode1L((x: Double) => new Ucb1Selection(x))("C")

  def decode(decoders: Map[String, DecodeJson[_ <: SelectionFunction]]): DecodeJson[SelectionFunction] = TraitJsonDecoder[SelectionFunction](decoders).decoder

  val defaultDecode = decode(Map("ucb1" -> decodeUcb1))
}
