package pl.edu.pw.mini.jk.sg.uct.movehistory.tslimit

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.movehistory.MoveHistoryFactory


case class TimestepLimitHistoryFactory(limit: Int) extends MoveHistoryFactory {
  override def create[S <: AdvanceableState[_]](): TimestepLimitHistory = TimestepLimitHistory(limit)
}
