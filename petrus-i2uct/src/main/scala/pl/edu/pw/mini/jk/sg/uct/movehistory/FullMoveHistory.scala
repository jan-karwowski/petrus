package pl.edu.pw.mini.jk.sg.uct.movehistory

case class FullMoveHistory[S](length: Int, previousMoves: List[Any]) extends MoveHistory[S] {
  override def appendMove(state: S, move: Any) : MoveHistory[S] = new FullMoveHistory[S](length+1, move :: previousMoves)
  override def equals(other: Any) : Boolean = other match {
    case other: FullMoveHistory[S] => this.length == other.length && this.previousMoves.equals(other.previousMoves)
    case _ => false
  }
  
  override lazy val hashCode = previousMoves.hashCode()
}
