package pl.edu.pw.mini.jk.sg.uct.movehistory;

object EmptyMoveHistoryFactory extends MoveHistoryFactory {
	override def create[S]() : EmptyMoveHistory.type = EmptyMoveHistory
}
