package pl.edu.pw.mini.jk.sg.uct.movehistory


// Czy chcę tu uwaględnić stany?
// Wtedy mógłbym mieć warunek na przykład na timeStep!!
// Ale czy potrafię to zrobić? Będę miał ruch z poprzednim time stepem.
// TO OK -- bo to znaczy, że ruch odbywa się przed skokiem. DOBRZE.
trait MoveHistory[-S] {
  /**Rejestruje ruch wraz ze stanem z którego wykonywany jest ruch w historii.
    
    @param state Stan z którego wykonywany jest ruch
    @param move Ruch
    */   
  def appendMove(state: S, move: Any) : MoveHistory[S]
  override def equals(other: Any) : Boolean
  override def hashCode : Int
}
