package pl.edu.pw.mini.jk.sg.uct.statsupdater.simple

import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdater
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import scala.collection.mutable.ArrayBuffer


final class SimpleStatsUpdater[S, M] extends StatsUpdater[S, M] {
  val desicions = ArrayBuffer[(UctNode[S, M], M)]()

  def registerDecision(node: UctNode[S, M], move: M) : Unit = {
    desicions.append((node, move))
    node.registerVisit(move)
  }
  def registerMcMove(state: S, move: M): Unit = Unit
  def updatePayoffs(payoff: Double, state: S) : Unit = desicions.foreach{case (node, move) => node.registerPayoff(move, payoff)}

}
