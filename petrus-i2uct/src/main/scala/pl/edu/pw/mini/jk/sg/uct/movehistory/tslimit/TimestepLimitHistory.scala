package pl.edu.pw.mini.jk.sg.uct.movehistory.tslimit

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.movehistory.MoveHistory
import pl.edu.pw.mini.jk.sg.uct.utils.{ EmptyStepLimitQueue, StepLimitQueue }

final case class TimestepLimitHistory(queue: StepLimitQueue[Any])
    extends MoveHistory[AdvanceableState[_]] {
  override def appendMove(state: AdvanceableState[_], move: Any) : TimestepLimitHistory = 
    copy(queue.append(state.getTimeStep, move))
}

object TimestepLimitHistory {
  def apply(limit: Int) : TimestepLimitHistory  = TimestepLimitHistory(EmptyStepLimitQueue(limit))
}
