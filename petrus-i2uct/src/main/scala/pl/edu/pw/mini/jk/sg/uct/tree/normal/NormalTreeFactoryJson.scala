package pl.edu.pw.mini.jk.sg.uct.tree.normal

import argonaut.DecodeJson


object NormalTreeFactoryJson {
  implicit val normalTreeFactoryDecode : DecodeJson[NormalTreeFactory.type] =
    DecodeJson.UnitDecodeJson.map(_ => NormalTreeFactory)
}
