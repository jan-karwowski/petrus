package pl.edu.pw.mini.jk.sg.uct.movehistory;

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState

trait MoveHistoryFactory {
  def create[S <: AdvanceableState[_]]() : MoveHistory[S]
}
