package pl.edu.pw.mini.jk.sg.uct.tree.export
import pl.edu.pw.mini.jk.sg.uct.UCTDecisionStatistics

final case class ExportStatistics(
  visitsCount: Int, realVisitsCount: Int, averagePayoff: Double
)

object ExportStatistics {
  def fromUDS(uds: UCTDecisionStatistics) = ExportStatistics(uds.visitsCount,
    uds.realVisitsCount, uds.averagePayoff)
}
