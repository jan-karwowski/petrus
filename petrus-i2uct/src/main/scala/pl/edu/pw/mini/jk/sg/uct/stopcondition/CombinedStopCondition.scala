package pl.edu.pw.mini.jk.sg.uct.stopcondition;

import pl.edu.pw.mini.jk.sg.game.common.SecurityGame


final case class CombinedStopCondition[SG <: SecurityGame](conditions : Seq[GameStopCondition[SG]] with Immutable) extends GameStopCondition[SG] {
  override def isGameStopCondition(game : SG) : Boolean = conditions.exists((sc : GameStopCondition[SG]) => sc.isGameStopCondition(game))
}
