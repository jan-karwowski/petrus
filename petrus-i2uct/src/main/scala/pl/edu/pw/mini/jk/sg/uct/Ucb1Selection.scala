package pl.edu.pw.mini.jk.sg.uct


final case class Ucb1Selection(c : Double) extends SelectionFunction {
  override def apply(stats: UctReadableStatistics, visits: Int) : Double =
    stats.averagePayoff +
  c * Math.sqrt(Math.log(visits.toDouble) / stats.visitsCount);
}
