package pl.edu.pw.mini.jk.sg.uct

import java.util.List
import org.apache.commons.math3.random.RandomGenerator


trait SinglePlayerGame[M, S] {
    def getCurrentState() : S
    def possibleMoves() : List[M]
    def randomMove(rng: RandomGenerator) : M
    def makeMove(move: M) : PayoffInfo;
    def getTimeStep() : Int
}
