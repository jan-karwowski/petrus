package pl.edu.pw.mini.jk.sg.uct.movehistory.fixed

import pl.edu.pw.mini.jk.sg.uct.movehistory.{MoveHistoryFactory}
import scala.collection.immutable.Queue



case class FixedMoveHistoryFactory(maxLength: Int) extends MoveHistoryFactory {
  override def create[T]() : FixedMoveHistory = new FixedMoveHistory(Queue(), maxLength)
}
