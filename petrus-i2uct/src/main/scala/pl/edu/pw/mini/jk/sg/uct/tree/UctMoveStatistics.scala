package pl.edu.pw.mini.jk.sg.uct.tree


final class UctMoveStatistics[M](val move: M, val statistics: UnmodifiableDecisionStatisticsView)

object UctMoveStatistics {
  def apply[M](move: M, statistics: UnmodifiableDecisionStatisticsView) = new UctMoveStatistics(move, statistics)
}
