package pl.edu.pw.mini.jk.sg.uct.movehistory

object EmptyMoveHistory extends MoveHistory[Any] {
 override def appendMove(state: Any, move: Any): MoveHistory[Any] = this
 override def hashCode: Int = getClass().hashCode()
 override def equals(other: Any) : Boolean = this.getClass().equals(other.getClass())

}
