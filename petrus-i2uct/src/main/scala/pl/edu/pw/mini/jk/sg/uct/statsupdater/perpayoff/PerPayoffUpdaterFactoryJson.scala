package pl.edu.pw.mini.jk.sg.uct.statsupdater.perpayoff

import argonaut.DecodeJson


object PerPayoffUpdaterFactoryJson {
  implicit val decode : DecodeJson[PerPayoffUpdaterFactory] = DecodeJson.jdecode1L(PerPayoffUpdaterFactory.apply)("tsLimit")
}
