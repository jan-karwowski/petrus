package pl.edu.pw.mini.jk.sg.uct.tree

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState

trait UctTreeFactory {
  def create[S <: AdvanceableState[M], M](initialState: S) : UctNode[S,M]
  def treeJoiner : TreeJoiner
}
