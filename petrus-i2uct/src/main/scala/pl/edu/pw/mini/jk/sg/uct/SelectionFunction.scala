package pl.edu.pw.mini.jk.sg.uct

trait SelectionFunction {
    def apply(statistics: UctReadableStatistics, sumOfVisits: Int) : Double
}
