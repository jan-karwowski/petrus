package pl.edu.pw.mini.jk.sg.uct.statsupdater.tslimit

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.statsupdater.StatsUpdater
import pl.edu.pw.mini.jk.sg.uct.tree.UctNode
import pl.edu.pw.mini.jk.sg.uct.utils.{ EmptyStepLimitQueue, StepLimitQueue }



final class TimestepLimitUpdater[S<: AdvanceableState[_], M] (tsLimit: Int)
    extends StatsUpdater[S, M] {
  var stateQueue : StepLimitQueue[Option[(UctNode[S, M], M)]] = EmptyStepLimitQueue(tsLimit)

  override def registerDecision(node: UctNode[S,M],move: M): Unit = {
    stateQueue = stateQueue.append(node.getState.getTimeStep, Some((node, move)))
    node.registerVisit(move)
  }
  override def registerMcMove(state: S, move: M): Unit = {
    stateQueue = stateQueue.append(state.getTimeStep, None)
  }
  override def updatePayoffs(payoff: Double,state: S): Unit = {
    if(payoff != 0) {
      stateQueue.foreach {
        case Some((un, m)) => { un.registerPayoff(m, payoff) }
        case None => Unit
      }
    }
  }
}
