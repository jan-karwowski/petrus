package pl.edu.pw.mini.jk.sg.uct;

trait UctReadableStatistics {
  def averagePayoff: Double
  def visitsCount: Int
  def realVisitsCount: Int

 final def toStats = new UCTDecisionStatistics(visitsCount, realVisitsCount, visitsCount*averagePayoff)
}
