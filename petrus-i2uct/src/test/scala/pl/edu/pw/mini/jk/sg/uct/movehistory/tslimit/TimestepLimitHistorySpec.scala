package pl.edu.pw.mini.jk.sg.uct.movehistory.tslimit

import org.scalatest._
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState

class TimestepLimitHistorySpec extends WordSpec with Matchers {

  def fakeState(timeStep: Int) = {
    new AdvanceableState[Nothing] {
      override def advance(move : Nothing) = ???
      override def getPossibleMoves() = ???
      override def getTimeStep() = timeStep
    }
  }

  "TimestepLimitHistory" should {
    "not equal for different numbers of moves within single timestep" in {
      val s = fakeState(1)

      val hist1 = TimestepLimitHistory(1)
      val hist2 = TimestepLimitHistory(1).appendMove(s,1)
      val hist3 = hist1.appendMove(s, 2)
      val hist4 = hist2.appendMove(s, 1).appendMove(s, 1)
        .appendMove(s, 1).appendMove(s, 1).appendMove(s, 1)
      val hist5 = hist4.appendMove(s, 1)

      hist1 should not equal(hist2)
      hist1 should not equal(hist3)
      hist1 should not equal(hist4)
      hist1 should not equal(hist5)
      hist2 should not equal(hist3)
      hist2 should not equal(hist4)
      hist2 should not equal(hist5)
      hist3 should not equal(hist4)
      hist3 should not equal(hist5)
      hist4 should not equal(hist5)
    }

    "not equal for same moves in different time step" in {
      val s = fakeState(1)
      val t = fakeState(2)

      val hist1 = TimestepLimitHistory(1)
      hist1.appendMove(s, 1) should not equal(hist1.appendMove(t, 1))
    }

    "equal for same moves in same timestep" in {
      val s = fakeState(1)
      val t = fakeState(1)

      val hist1 = TimestepLimitHistory(1)
      hist1.appendMove(s, 1) should equal(hist1.appendMove(t, 1))
    }

    "not equal for different moves in previous timestep" in {
      val s = fakeState(1)
      val t = fakeState(2)

      val hist1 = TimestepLimitHistory(1).appendMove(s, 1).appendMove(s, 2).appendMove(t, 1)
      val hist2 = TimestepLimitHistory(1).appendMove(s, 2).appendMove(s, 1).appendMove(t, 1)

      hist1 should not equal(hist2)
    }

    "equal for same moves in previous timestep" in {
      val s = fakeState(1)
      val t = fakeState(2)

      val hist1 = TimestepLimitHistory(1).appendMove(s, 1).appendMove(s, 2).appendMove(t, 1)
      val hist2 = TimestepLimitHistory(1).appendMove(s, 1).appendMove(s, 2).appendMove(t, 1)

      hist1 should equal(hist2)
    }

    "equal after dropping moves from old timestep" in {
      val s = fakeState(1)
      val t = fakeState(2)

      val hist1 = TimestepLimitHistory(1).appendMove(s, 1).appendMove(s, 2).appendMove(t, 1).appendMove(fakeState(3), 5)
      val hist2 = TimestepLimitHistory(1).appendMove(s, 2).appendMove(s, 1).appendMove(t, 1).appendMove(fakeState(3), 5)

      hist1 should equal(hist2)

    }

    "work with 0 timestep" in {
      val s = fakeState(1)
      val t = fakeState(2)

      val hist1 = TimestepLimitHistory(0).appendMove(s, 1).appendMove(s, 2).appendMove(t, 1)
      val hist2 = TimestepLimitHistory(0).appendMove(s, 9).appendMove(s, 2).appendMove(t, 1)

      hist1 should equal(hist2)
    }
  }
}
