package pl.edu.pw.mini.jk.sg.uct.tree.export
import org.scalatest.WordSpec
import org.scalatest.Matchers

import scodec.bits._
import scodec.Attempt

final class ExportUctTreeCodecSpec extends WordSpec with Matchers {
  "uct tree codec" when {
    val tree = ExportUctTree[String, String]("aa", Map(
      "x" -> (ExportStatistics(3, 4, 4.5), ExportUctTree[String, String]("bb", Map())),
      "y" -> (ExportStatistics(3, 4, 4.5), ExportUctTree[String, String]("cc", Map(
        "z" -> (ExportStatistics(3, 4, 4.5), ExportUctTree[String, String]("dd", Map())),
        "w" -> (ExportStatistics(3, 4, 4.5), ExportUctTree[String, String]("ee", Map()))
      )))
    ))
    "encoding a tree" should {
      "succeed with non empty output" in {
        val enc : Attempt[BitVector] = ExportUctTree.codec.encode(tree)
        enc.isSuccessful should be(true)
        enc.require.length should be (1072)
      }
    }
    "encoding and decoding sequence" should {
      "yield identity result" in {
        val enc : Attempt[BitVector] = ExportUctTree.codec.encode(tree)
        enc.isSuccessful should be(true)
        val dec = ExportUctTree.codec.decodeValue(enc.require)
        dec.isSuccessful should be (true)
        dec.require should equal(tree)
      }
    }
  }
}
