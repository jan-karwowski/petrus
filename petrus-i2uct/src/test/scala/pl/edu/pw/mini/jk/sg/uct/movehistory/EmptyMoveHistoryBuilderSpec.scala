package pl.edu.pw.mini.jk.sg.uct.movehistory

import org.scalatest.Matchers
import org.scalatest.WordSpec



class EmptyMoveHistoryBuilderSpec extends WordSpec with Matchers {
  "empty move history builder" should {
    val emhb : MoveHistory[Int] = EmptyMoveHistory

    "should return equal values always" in {
      val a = emhb
      val c = emhb.appendMove(5,2)

      a should equal(c)
    }
  }
}
