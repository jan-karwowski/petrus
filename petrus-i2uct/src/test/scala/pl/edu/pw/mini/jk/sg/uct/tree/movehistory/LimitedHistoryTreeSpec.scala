package pl.edu.pw.mini.jk.sg.uct.tree.movehistory

import com.google.common.collect.ImmutableList
import org.scalatest.WordSpec
import org.scalatest.Matchers
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableState
import pl.edu.pw.mini.jk.sg.uct.UctReadableStatistics
import pl.edu.pw.mini.jk.sg.uct.movehistory.EmptyMoveHistoryFactory
import pl.edu.pw.mini.jk.sg.uct.tree.{ PruneTreeFunctions, UctNode }

object KeepAll extends PruneTreeFunctions {
  override type T = Unit

  def movePredicate(stat: UctReadableStatistics, x: Unit) = true
  override def nodeStatistics(x: UctNode[_,_]) = Unit
}

class LimitedHistoryTreeSpec extends WordSpec with Matchers {


  private final case class IntAdvanceableState(v: Int, timestep: Int) extends AdvanceableState[Int] {
    def advance(dif: Int) = copy(dif, timestep+1)
    def getPossibleMoves() = ImmutableList.of(v-1, v, v+1)
    def getTimeStep() = timestep
  }

  "Limited history tree" should {
    val tree = LimitedHistoryTree.emptyTree[IntAdvanceableState, Int](EmptyMoveHistoryFactory, IntAdvanceableState(2, 1))

    val root = tree.getRoot

    val s2 = IntAdvanceableState(3, 2)
    root.addSuccessor(3, s2, s2.getPossibleMoves)
    root.registerVisit(3)
    root.registerPayoff(3, 2.0)

    "root have 1 vc" in {
      root.getVisitsCount should equal(1)
    }

    "root have 1 stat size" in {
      root.getStatistics.size should equal (1)
    }

    "tree pruned with true predicate return the same effect" in {
      root.pruneTree(KeepAll).getVisitsCount should equal (1)
    }
  }
}
