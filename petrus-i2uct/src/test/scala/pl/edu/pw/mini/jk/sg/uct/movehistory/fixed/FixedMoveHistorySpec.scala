package pl.edu.pw.mini.jk.sg.uct.movehistory.fixed

import org.scalatest._

class FixedMoveHistorySpec extends WordSpec with Matchers {
  val factory = new FixedMoveHistoryFactory(3)
  "FixedMoveHistory" should {
    "give true when comparing two empty histories" in {
      val h1 = factory.create()
      val h2 = factory.create()

      h1 should equal(h2)
    }

    "give false when comparing non-empty and empty history" in {
      val h1 = factory.create[Int]()
      val h2 = factory.create[Int]().appendMove(6,2)

      h1 should not equal(h2)
    }

    "give true when comparing two histories with one element" in {
      val h1 = factory.create[Int]().appendMove(6,2)
      val h2 = factory.create[Int]().appendMove(6,2)

      h1 should equal(h2)
    }

    "give true when comparing two histories after overflow" in {
      val h1 = factory.create[Int]().appendMove(6,2).appendMove(7,3).appendMove(8,1)
      val h2 = factory.create[Int]().appendMove(1,15).appendMove(1,8).appendMove(1,2).appendMove(1,3).appendMove(1,1)

      h1 should equal(h2)
    }
  }
}
