#!/bin/bash

GAME=$1
CONFIG=$2
REPEAT=$3

mkdir -p seout$REPEAT

../../target/pack/bin/petrus-se -g ../../games/$GAME.ggame -o seout$REPEAT/$GAME.result -m ../../mixed-configs/$CONFIG.json  2> seout$REPEAT/$GAME.err > seout$REPEAT/$GAME.out


