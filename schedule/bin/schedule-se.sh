#!/bin/bash

for repeat in 1 2 3 4 5 6 7 8 9 10; do
for a in game{1,2,3,3a,3b,3c,3d,6,6a,6b,6c,6d}-5 $(seq 1 100| sed 's/^/smallbuilding-/' | sed 's/$/-5/'); do 
    tsp ./run-se.sh $a linear $repeat
done
done
