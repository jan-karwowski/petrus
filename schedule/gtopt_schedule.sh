#!/bin/bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $MY_DIR/paths.inc

if [ $# -ne 1 ] ; then
    echo Wymagany plik z eksperymentem
    exit 1
fi

EXFILE="$1"
EXPERIMENT=$(basename "$EXFILE" .exp)

. $EXFILE

mkdir -p $EXPERIMENT_OUT_PATH/$EXPERIMENT

rm -f $EXPERIMENT_OUT_PATH/$EXPERIMENT/$EXPERIMENT

for c in $MDS_CONF; do
    for gset in $GAME_SETS; do
	xargs -n 2 echo < $gset | sed 's/$/ '$c'/' >> $EXPERIMENT_OUT_PATH/$EXPERIMENT/$EXPERIMENT
    done
	
    for repeat in $(seq $FROMID $TOID); do
	    for gset in $GAME_SETS; do
		xargs -n 2 tsp ./gtopt_single_run.sh $EXPERIMENT $c $repeat $LOG "$SWITCHES" < $gset
	    done
	done
    done
	
