#!/bin/bash
set -e

EXPERIMENT=$(basename $1 .exp)

. $1


rm -fr $EXPERIMENT
mkdir $EXPERIMENT

mkdir ${EXPERIMENT}/games
mkdir ${EXPERIMENT}/configs

PETRUS_VERSION="$(grep 'version :=' ../build.sbt|cut -d\" -f 2)"


for a in $MDS_CONF; do
    ../target/pack/bin/petrus-chkconfig -m ../configs/$a.json
    cp ../configs/$a.json $EXPERIMENT/configs/
done

for gs in ${GAME_SETS}; do
    cp $gs $EXPERIMENT/
    for game in $(cut -d\  -f1 $gs); do
	case $game in
	    *.nfgame) cp ../games/$game $EXPERIMENT/games/ ;;
	    *) cp ../games/$game.json $EXPERIMENT/games/ ;;
	esac
    done
done

sed 's/%EXPERIMENT%/'$EXPERIMENT'/g' < gtopt_schedule2.sh > $EXPERIMENT/gtopt_schedule.sh
sed 's/%PETRUS_VERSION%/'$PETRUS_VERSION'/g' < gtopt_single_run2.sh > $EXPERIMENT/gtopt_single_run.sh

chmod u+x $EXPERIMENT/gtopt_schedule.sh $EXPERIMENT/gtopt_single_run.sh

cp $1 $EXPERIMENT/

tar cJf ${EXPERIMENT}.tar.xz ${EXPERIMENT}
rm -r ${EXPERIMENT}
