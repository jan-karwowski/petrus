#!/bin/bash

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

EXPERIMENT=$1
MDS=$2
repeat=$3
log=$4
switches=$5

g=$6
rl=$7

outdir=$EXPERIMENT


. $MY_DIR/paths.inc
    
PROGRAM_PATH=${PETRUS_BIN_PATH}/petrus-gteval
PROP_PARAMS=""
PARAMS=""

if [ -z "$TRANS" -o $TRANS == NONE ]; then
    TRANSOPT=""
    TRANSSUFFIX=""
else
    TRANSOPT="--game-transformer ${MIXED_STRATEGY_CONFIGS_PATH}/${TRANS}.json"
    TRANSSUFFIX="tr$TRANS"
fi

OUTFILE_PREFIX=${EXPERIMENT_OUT_PATH}/${outdir}/eval-$(echo $g | sed 's/\//-/g')${TRANSSUFFIX}-$rl-${MDS}.${repeat}


if [ $log = TREE ] ; then
    PROP_PARAMS="-Dpetrus.defenderResultLog=$OUTFILE_PREFIX.dr.log -Dpetrus.attackerStrategyLog=$OUTFILE_PREFIX.as.log -Dpetrus.defenderStrategyLog=$OUTFILE_PREFIX.ds.log -Dpetrus.averagedAttackerStrategyLog=$OUTFILE_PREFIX.aas.log -Dpetrus.attackerHistoryLog=$OUTFILE_PREFIX.ah.log"
    PARAMS="$PARAMS --treeLogDir $OUTFILE_PREFIX.tree.log --treeLogFreq ${TREE_LOG_FREQ:-500}"
elif [ $log = AS ] ; then
    PROP_PARAMS="-Dpetrus.defenderResultLog=$OUTFILE_PREFIX.dr.log -Dpetrus.attackerStrategyLog=$OUTFILE_PREFIX.as.log -Dpetrus.defenderStrategyLog=$OUTFILE_PREFIX.ds.log -Dpetrus.averagedAttackerStrategyLog=$OUTFILE_PREFIX.aas.log -Dpetrus.attackerHistoryLog=$OUTFILE_PREFIX.ah.log"
elif [ $log = DR ] ; then
    PROP_PARAMS="-Dpetrus.defenderResultLog=$OUTFILE_PREFIX.dr.log"
fi


mkdir -p ${EXPERIMENT_OUT_PATH}/${outdir}

if [ "$MDS" = "milp2" ] ; then
    SOL_FILE=${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.gurobi.base.sol.gz
    MDS="milp"
elif [ "$MDS" = "milp3" ] ; then
    SOL_FILE=${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.sol
    MDS="milp"
elif [ -f ${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.gurobi.sol ]; then 
    SOL_FILE=${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.gurobi.sol
elif [ -f ${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.gurobi.sol.gz ]; then 
    SOL_FILE=${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.gurobi.sol.gz
else
    SOL_FILE=${SOLVER_RESULTS_PATH}/${g}${TRANSSUFFIX}-$rl.sol
fi


if echo $switches | grep -q t; then
    echo Config logger properly
    exit 1
fi

if echo $switches | grep -q j; then
    PARAMS="$PARAMS --json-strategy-output $OUTFILE_PREFIX.trmt" 
fi
    
case $g in
    *.sgmt) GAMEOPTION="--moving-targets-game $MTG_TEST_PATH/${g}" ;;
    *.nfgame) GAMEOPTION="--game-config ${GRAPH_GAME_PATH}/${g}" ;;
    mtg*) echo "old mtg game support was removed";  exit 1;;
    *) GAMEOPTION="--game-path ${GRAPH_GAME_PATH}/${g}.json  --round-limit $rl";;
esac

if test -f $OUTFILE_PREFIX.out && grep -q "Defender strategy:" $OUTFILE_PREFIX.out ; then
    exit 0
else
    exec env JAVA_OPTS="$PROP_PARAMS -Xmx2g -XX:+HeapDumpOnOutOfMemoryError" ${PROGRAM_PATH}  \
	 $TRANSOPT $GAMEOPTION \
	 --stackelberg-solution $SOL_FILE \
	 --mixed-strategy-config ${MIXED_STRATEGY_CONFIGS_PATH}/${MDS}.json \
	 $PARAMS \
	 >  $OUTFILE_PREFIX.out \
	 2> $OUTFILE_PREFIX.err
fi


# -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=4000,suspend=y \
