#!/bin/bash

EXPERIMENT=%EXPERIMENT%
EXFILE=${EXPERIMENT}.exp

. $EXFILE

export TREE_LOG_FREQ

OUT_PATH=out/
mkdir -p ${OUT_PATH}


rm -f $${OUT_PATH}/$EXPERIMENT

for c in $MDS_CONF; do
    for gset in $GAME_SETS; do
	xargs -n 2 echo < $gset | sed 's/$/ '$c'/' >> ${OUT_PATH}/$EXPERIMENT
    done
	
    for repeat in $(seq $FROMID $TOID); do
	    for gset in $GAME_SETS; do
		xargs -n 2 tsp ./gtopt_single_run.sh $EXPERIMENT $c $repeat $LOG "$SWITCHES" < $gset
	    done
	done
    done
	
