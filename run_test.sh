#!/bin/bash

JAR_PATH=/home/jan/research/security_games/petrus/petrus-batch/target/petrus-batch-1.0-SNAPSHOT-jar-with-dependencies.jar
OUT_DIR=/home/jan/research/security_games/petrus_out/
TEST_CONFIG_DIR=/home/jan/research/security_games/conf/
GAMES_DIR=/home/jan/research/security_games/games/
ATTACKER_CONF_DIR=/home/jan/research/security_games/petrus/attacker_configs/

CONFIG_NAME=$1
GAME_NAME=$2
GAME_TYPE=$3
TEST_ID=$4
ATTACKER_CONFIG=$5

TEST_NAME=${GAME_NAME}${GAME_TYPE}_${CONFIG_NAME}_${ATTACKER_CONFIG}

java -jar $JAR_PATH $TEST_CONFIG_DIR/$CONFIG_NAME.json $GAMES_DIR/$GAME_NAME.json $GAME_TYPE  ${ATTACKER_CONF_DIR}/${ATTACKER_CONFIG}.json  $OUT_DIR/${TEST_NAME}.${TEST_ID}.stats.csv > $OUT_DIR/${TEST_NAME}.result.${TEST_ID}



