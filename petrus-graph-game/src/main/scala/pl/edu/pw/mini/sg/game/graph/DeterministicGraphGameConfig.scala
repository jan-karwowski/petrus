package pl.edu.pw.mini.sg.game.graph

import pl.edu.pw.mini.sg.cache.{CacheProxy, CacheProxyImpl}
import java.util.UUID
import pl.edu.pw.mini.sg.game.deterministic.plugin.config.GameConfig
import pl.edu.pw.mini.sg.game.graph.sud.{ SingleUnitAttackerState, SingleUnitDefenderState, FastArrayWrapper }
import pl.edu.pw.mini.sg.util.CachedHashPair
import scala.runtime.ScalaRunTime



final case class DeterministicGraphGameConfig (
  graphConfig: GraphConfig,
  targets: List[Int],
  spawn: Int,
  vertexDefenderRewards: Vector[Double],
  vertexAttackerPenalties: Vector[Double],
  targetDefenderPenalties: Vector[Double],
  targetAttackerRewards: Vector[Double],
  defenderUnitCount: Int,
  rounds: Int,
  id: UUID
) extends GameConfig {
  val possibleMovesFromVertex: Vector[List[Int]] = Range(0, graphConfig.vertexCount)
    .map(v => v :: graphConfig.edges.filter(_.from == v).map(_.to).toList).toVector

  override val hashCode : Int = ScalaRunTime._hashCode(this)

  val initialDefenderState  = SingleUnitDefenderState(this, FastArrayWrapper(Vector.fill(defenderUnitCount)(0)), 0, 0)

  val initialAttackerState = new SingleUnitAttackerState(this, spawn, 0, 0)

  val defenderCache : CacheProxy[CachedHashPair[Int, SingleUnitDefenderState], SingleUnitDefenderState] =
    CacheProxyImpl.createCache()

  val attackerCache : CacheProxy[CachedHashPair[Int, SingleUnitAttackerState], SingleUnitAttackerState] =
    CacheProxyImpl.createCache()

}
