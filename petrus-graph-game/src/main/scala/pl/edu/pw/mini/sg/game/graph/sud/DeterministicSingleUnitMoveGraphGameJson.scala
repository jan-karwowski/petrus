package pl.edu.pw.mini.sg.game.graph.sud

import argonaut.{ CodecJson, DecodeJson, EncodeJson }
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson


object DeterministicSingleUnitMoveGraphGameJson
    extends DeterministicGameJson[
  Int, Int, SingleUnitDefenderState, SingleUnitAttackerState
] {
  val POSITION_KEY = "position"
  val CURRENT_UNIT_KEY = "currentUnit"
  val TIME_STEP_KEY = "timeStep"
  val POSITIONS_KEY = "positions"

  val attackerMoveEncode: EncodeJson[Int] = CodecJson.derived[Int]
  val attackerStateEncode: EncodeJson[SingleUnitAttackerState] =
    EncodeJson.jencode3L((suas: SingleUnitAttackerState) =>
      (suas.attackerPosition, suas.complementaryDefender, suas.timeStep))(
      POSITION_KEY, CURRENT_UNIT_KEY, TIME_STEP_KEY
    )

  val defenderMoveEncode: EncodeJson[Int] = CodecJson.derived[Int]
  val defenderStateEncode: EncodeJson[SingleUnitDefenderState] =
    EncodeJson.jencode3L((suds: SingleUnitDefenderState) =>
      (suds.unitPositions, suds.currentUnit, suds.timeStep))(
      POSITIONS_KEY, CURRENT_UNIT_KEY, TIME_STEP_KEY
    )

}
