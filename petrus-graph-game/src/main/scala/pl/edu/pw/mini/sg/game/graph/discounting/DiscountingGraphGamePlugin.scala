package pl.edu.pw.mini.sg.game.graph.discounting

import argonaut.DecodeJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.GamePlugin
import json.DiscountingGraphGameConfigJson

object DiscountingGraphGamePlugin extends GamePlugin {
  override type GameConfig = DiscountingGraphGameConfig
  val configJsonDecode: DecodeJson[DiscountingGraphGameConfig] = DiscountingGraphGameConfigJson.discountingGraphGameConfigCodec
  val fileExtension: String = "dggame"
  val singleCoordFactory: DiscountingGraphGameFactory.type = DiscountingGraphGameFactory
}
