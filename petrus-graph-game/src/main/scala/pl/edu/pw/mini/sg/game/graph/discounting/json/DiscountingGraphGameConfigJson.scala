package pl.edu.pw.mini.sg.game.graph.discounting.json

import argonaut.CodecJson
import pl.edu.pw.mini.sg.game.graph.DeterministicGraphGameConfigJson

import pl.edu.pw.mini.sg.game.graph.discounting.DiscountingGraphGameConfig

object DiscountingGraphGameConfigJson {
  import DeterministicGraphGameConfigJson.deterministicGraphGameConfigCodec

  implicit val discountingGraphGameConfigCodec: CodecJson[DiscountingGraphGameConfig] =
    CodecJson.casecodec4(DiscountingGraphGameConfig.apply, DiscountingGraphGameConfig.unapply)(
      "graphConfig", "attackerRoundLoss", "defenderRoundLoss", "id"
    )
}
