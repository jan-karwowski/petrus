package pl.edu.pw.mini.sg.game.graph.sud

import argonaut.{ CodecJson, DecodeJson, EncodeJson }
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJsonDecoders
import pl.edu.pw.mini.sg.game.graph.DeterministicGraphGameConfig


final class DeterministicSingleUnitMoveGraphGameJsonDecoders(gameConfig: DeterministicGraphGameConfig)
    extends DeterministicGameJsonDecoders[
  Int, Int, SingleUnitDefenderState, SingleUnitAttackerState
] {
  import DeterministicSingleUnitMoveGraphGameJson._

  val attackerMoveDecode: DecodeJson[Int] = DecodeJson.IntDecodeJson
  val attackerStateDecode: DecodeJson[SingleUnitAttackerState] =
    DecodeJson.jdecode3L((attackerPosition: Int, complementaryDefender: Int, timeStep: Int) =>
      new SingleUnitAttackerState(gameConfig, attackerPosition, timeStep, complementaryDefender)
    )(
      POSITION_KEY, CURRENT_UNIT_KEY, TIME_STEP_KEY
    )

  val defenderMoveDecode: DecodeJson[Int] = DecodeJson.IntDecodeJson
  val defenderStateDecode: DecodeJson[SingleUnitDefenderState] =
    DecodeJson.jdecode3L((unitPositions: List[Int], currentUnit: Int, timeStep: Int) =>
      new SingleUnitDefenderState(gameConfig, new FastArrayWrapper(unitPositions.toArray), currentUnit, timeStep)
    )(
      POSITIONS_KEY, CURRENT_UNIT_KEY, TIME_STEP_KEY
    )

}
