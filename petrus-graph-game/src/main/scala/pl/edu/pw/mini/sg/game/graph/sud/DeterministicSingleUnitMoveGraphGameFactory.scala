package pl.edu.pw.mini.sg.game.graph.sud

import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJsonDecoders
import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactoryDecoding
import pl.edu.pw.mini.sg.game.graph.DeterministicGraphGameConfig


final object DeterministicSingleUnitMoveGraphGameFactory
    extends GameFactoryDecoding[DeterministicGraphGameConfig] {
  override type DefenderMove = Int
  override type AttackerMove = Int
  override type DefenderState = SingleUnitDefenderState
  override type AttackerState = SingleUnitAttackerState
  override type Game = DeterministicSingleUnitMoveGraphGame

  override def create(gameConfig: DeterministicGraphGameConfig) : DeterministicSingleUnitMoveGraphGame = DeterministicSingleUnitMoveGraphGame(gameConfig)

  override val json: DeterministicSingleUnitMoveGraphGameJson.type = DeterministicSingleUnitMoveGraphGameJson
  override def jsonDecoders(gameConfig: DeterministicGraphGameConfig) : DeterministicGameJsonDecoders[DefenderMove, AttackerMove, DefenderState, AttackerState] = new DeterministicSingleUnitMoveGraphGameJsonDecoders(gameConfig)

  def maxDefenderPayoff(gameConfig: DeterministicGraphGameConfig): Double =
    (gameConfig.targetDefenderPenalties++gameConfig.vertexDefenderRewards).max
  def minDefenderPayoff(gameConfig: DeterministicGraphGameConfig): Double =
    (gameConfig.targetDefenderPenalties++gameConfig.vertexDefenderRewards).min

  def maxAttackerPayoff(gameConfig: DeterministicGraphGameConfig): Double =
    (gameConfig.vertexAttackerPenalties++gameConfig.targetAttackerRewards).max
  def minAttackerPayoff(gameConfig: DeterministicGraphGameConfig): Double =
    (gameConfig.vertexAttackerPenalties++gameConfig.targetAttackerRewards).min
}
