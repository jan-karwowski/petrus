package pl.edu.pw.mini.sg.game.graph.discounting

import argonaut.EncodeJson
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson



object DiscountingGraphGameJson extends DeterministicGameJson[
  Int, Int,
  DiscountingGraphGameDefenderState,
  DiscountingGraphGameAttackerState
]{
  val attackerMoveEncode: EncodeJson[Int] = EncodeJson.IntEncodeJson
  val attackerStateEncode: EncodeJson[DiscountingGraphGameAttackerState] =
    EncodeJson.jencode3L((s : DiscountingGraphGameAttackerState) => (s.vertex, s.defenderUnit, s.timeStep))(
      "vertex", "defUnit", "timeStep")
  val defenderMoveEncode: EncodeJson[Int] = EncodeJson.IntEncodeJson
  val defenderStateEncode: EncodeJson[DiscountingGraphGameDefenderState] =
    EncodeJson.jencode3L((s : DiscountingGraphGameDefenderState) => (s.positions, s.defenderUnit, s.timeStep))(
      "positions", "defUnit", "timeStep")
}
