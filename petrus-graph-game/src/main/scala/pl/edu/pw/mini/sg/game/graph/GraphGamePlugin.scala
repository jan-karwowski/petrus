package pl.edu.pw.mini.sg.game.graph

import argonaut.DecodeJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.{GameFactoryDecoding, GamePluginDecoding}
import pl.edu.pw.mini.sg.game.graph.sud.DeterministicSingleUnitMoveGraphGameFactory


final object GraphGamePlugin extends GamePluginDecoding {
  override type GameConfig = DeterministicGraphGameConfig

  override val fileExtension = "ggame"
  override val configJsonDecode: DecodeJson[DeterministicGraphGameConfig] = DeterministicGraphGameConfigJson.deterministicGraphGameConfigCodec
  override val singleCoordFactory : DeterministicSingleUnitMoveGraphGameFactory.type = DeterministicSingleUnitMoveGraphGameFactory
}
