package pl.edu.pw.mini.sg.game.graph.discounting

import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import scala.util.hashing.MurmurHash3


final class DiscountingGraphGameDefenderState(
  val positions: Vector[Int],
  val defenderUnit: Int,
  val timeStep: Int,
  val gameConfig: DiscountingGraphGameConfig
) extends PlayerVisibleState[Int] {

  override def equals(other: Any) : Boolean = other match {
    case dgds: DiscountingGraphGameDefenderState => positions.equals(dgds.positions) && defenderUnit == dgds.defenderUnit && timeStep == dgds.timeStep
    case _ => false
  }

  override val hashCode : Int = {
    import MurmurHash3.{mix, mixLast, finalizeHash}
    finalizeHash(mixLast(mix(mix(MurmurHash3.arraySeed, positions.##), defenderUnit), timeStep), 3)
  }
  override def possibleMoves: List[Int] =
    if(timeStep >= gameConfig.graphConfig.rounds) Nil
    else gameConfig.graphConfig.possibleMovesFromVertex(positions(defenderUnit))

  def continuation(move: Int) : DiscountingGraphGameDefenderState = new DiscountingGraphGameDefenderState(
    positions.updated(defenderUnit, move),
    (defenderUnit+1)%gameConfig.graphConfig.defenderUnitCount,
    timeStep+(defenderUnit+1)/gameConfig.graphConfig.defenderUnitCount,
    gameConfig
  )

  def makeFinal : DiscountingGraphGameDefenderState = new DiscountingGraphGameDefenderState(
    positions, 0, gameConfig.graphConfig.rounds, gameConfig)
}
