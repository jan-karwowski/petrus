package pl.edu.pw.mini.sg.game.graph

import argonaut.CodecJson


object DeterministicGraphGameConfigJson {
  import GraphConfigJson._
  implicit val deterministicGraphGameConfigCodec : CodecJson[DeterministicGraphGameConfig] =
    CodecJson.casecodec10(DeterministicGraphGameConfig.apply, DeterministicGraphGameConfig.unapply)(
      "graphConfig", "targets", "spawn",
      "vertexDefenderRewards",
      "vertexAttackerPenalties",
      "targetDefenderPenalties",
      "targetAttackerRewards",
      "defenderUnitCount",
      "rounds",
      "id"
    )
}
