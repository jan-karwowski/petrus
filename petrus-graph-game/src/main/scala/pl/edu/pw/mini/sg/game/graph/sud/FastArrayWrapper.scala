package pl.edu.pw.mini.sg.game.graph.sud

import argonaut.EncodeJson
import java.util.Arrays



final class FastArrayWrapper(
  private val array: Array[Int]
) {
  def apply(idx: Int) : Int = array(idx)
  def updated(idx: Int, value: Int) : FastArrayWrapper = new FastArrayWrapper(array.updated(idx, value))
  def contains(value: Int) : Boolean = array.contains(value)

  override def hashCode = Arrays.hashCode(array)
  override def equals(other: Any) = other match {
    case faw: FastArrayWrapper => Arrays.equals(array, faw.array)
    case _ => false
  }
  override def toString = Arrays.toString(array)
}


object FastArrayWrapper {
  def apply(vec: Vector[Int]) : FastArrayWrapper = new FastArrayWrapper(vec.toArray)

  implicit val encodeJson : EncodeJson[FastArrayWrapper] =
    EncodeJson.ListEncodeJson[Int].contramap(_.array.toList)
}


