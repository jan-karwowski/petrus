package pl.edu.pw.mini.sg.game.graph.discounting

import java.util.UUID
import pl.edu.pw.mini.sg.game.deterministic.plugin.config.GameConfig
import pl.edu.pw.mini.sg.game.graph.DeterministicGraphGameConfig


final case class DiscountingGraphGameConfig(
  graphConfig: DeterministicGraphGameConfig,
  attackerRoundLoss: Double,
  defenderRoundLoss: Double,
  id: UUID
) extends GameConfig
