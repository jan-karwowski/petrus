package pl.edu.pw.mini.sg.game.graph.discounting

import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactory



object DiscountingGraphGameFactory extends GameFactory[DiscountingGraphGameConfig] {
  override type AttackerState = DiscountingGraphGameAttackerState
  override type DefenderState = DiscountingGraphGameDefenderState
  override type AttackerMove = Int
  override type DefenderMove = Int
  override type Game = DiscountingGraphGame

  override def create(gameConfig: DiscountingGraphGameConfig) : DiscountingGraphGame =
    new DiscountingGraphGame(
      new DiscountingGraphGameDefenderState(Vector.fill(gameConfig.graphConfig.defenderUnitCount)(0), 0, 0, gameConfig),
      new DiscountingGraphGameAttackerState(gameConfig.graphConfig.spawn, 0, 0, gameConfig),
      0.0, 0.0
    )
  override val json : DeterministicGameJson[Int, Int, DefenderState, AttackerState] = DiscountingGraphGameJson

  def maxDefenderPayoff(gameConfig: DiscountingGraphGameConfig): Double =
    (gameConfig.graphConfig.targetDefenderPenalties++gameConfig.graphConfig.vertexDefenderRewards).max
  def minDefenderPayoff(gameConfig: DiscountingGraphGameConfig): Double =
    (gameConfig.graphConfig.targetDefenderPenalties++gameConfig.graphConfig.vertexDefenderRewards).min -
  gameConfig.defenderRoundLoss*gameConfig.graphConfig.rounds

  def maxAttackerPayoff(gameConfig: DiscountingGraphGameConfig): Double =
    (gameConfig.graphConfig.vertexAttackerPenalties++gameConfig.graphConfig.targetAttackerRewards).max
  def minAttackerPayoff(gameConfig: DiscountingGraphGameConfig): Double =
    (gameConfig.graphConfig.vertexAttackerPenalties++gameConfig.graphConfig.targetAttackerRewards).min -
      gameConfig.attackerRoundLoss*gameConfig.graphConfig.rounds
}
