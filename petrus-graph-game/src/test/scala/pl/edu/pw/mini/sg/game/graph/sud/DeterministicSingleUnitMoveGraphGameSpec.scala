package pl.edu.pw.mini.sg.game.graph.sud

import argonaut.Parse
import org.scalactic.TolerantNumerics
import org.scalatest.{ Matchers, WordSpec }
import pl.edu.pw.mini.sg.game.graph.{ DeterministicGraphGameConfig, DeterministicGraphGameConfigJson }
import scala.io.Source



class DeterministicSingleUnitMoveGraphGameSpec extends WordSpec with Matchers {
  implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.01)

  val config = """
{
  "graphConfig": {
    "edges": [
      {
        "from": 0,
        "to": 6
      },
      {
        "from": 6,
        "to": 0
      },
      {
        "from": 1,
        "to": 6
      },
      {
        "from": 6,
        "to": 1
      },
      {
        "from": 2,
        "to": 6
      },
      {
        "from": 6,
        "to": 2
      },
      {
        "from": 3,
        "to": 6
      },
      {
        "from": 6,
        "to": 3
      },
      {
        "from": 4,
        "to": 6
      },
      {
        "from": 6,
        "to": 4
      },
      {
        "from": 5,
        "to": 6
      },
      {
        "from": 6,
        "to": 5
      }
    ],
    "vertexCount": 7
  },
  "rounds": 5,
  "defenderUnitCount": 1,
  "targets": [
    0,
    1,
    2,
    3,
    4
  ],
  "spawn": 5,
  "vertexDefenderRewards": [
    0.5166666666666667,
    0.5333333333333333,
    0.5333333333333333,
    0.5333333333333333,
    0.5333333333333333,
    0.5166666666666667,
    0.5166666666666667
  ],
  "vertexAttackerPenalties": [
    0.48333333333333334,
    0.48333333333333334,
    0.48333333333333334,
    0.48333333333333334,
    0.48333333333333334,
    0.48333333333333334,
    0.48333333333333334
  ],
  "targetDefenderPenalties": [
    0.3666666666666667,
    0.33333333333333337,
    0.3,
    0.25,
    0.16666666666666669
  ],
  "targetAttackerRewards": [
    0.5333333333333333,
    0.55,
    0.5666666666666667,
    0.5833333333333334,
    0.6666666666666666
  ],
    "id": "e3d889e4-d849-11e8-8bbb-4be7bee7078f"
}
"""

  "game1" when {
    import DeterministicGraphGameConfigJson.deterministicGraphGameConfigCodec
    val gameConfig: DeterministicGraphGameConfig = Parse.decode[DeterministicGraphGameConfig](
      config
    ) match {
      case Right(v) => v
    }

    val game = DeterministicSingleUnitMoveGraphGame(gameConfig)

    "playing sequence 6,6,6,6,5; 5,5,5,5,5" should {
      val endgame = game.makeMove(6, 5).makeMove(6, 5).makeMove(6,5)
        .makeMove(6, 5).makeMove(5, 5)

      "not allow any more moves" in {
        endgame.defenderState.possibleMoves shouldBe empty
        endgame.attackerState.possibleMoves shouldBe empty
      }

      "give .5166666 defender reward" in {
        endgame.defenderPayoff should equal(0.5166666)
      }

      "give .483333333 attacker reward" in {
        endgame.attackerPayoff should equal(0.4833333)
      }
    }

    "playing sequence 5;5" should {
      val endgame = game.makeMove(5, 5)

      "not allow any more moves" in {
        endgame.defenderState.possibleMoves shouldBe empty
        endgame.attackerState.possibleMoves shouldBe empty
      }

      "give .5166666 defender reward" in {
        endgame.defenderPayoff should equal(0.5166666)
      }

      "give .483333333 attacker reward" in {
        endgame.attackerPayoff should equal(0.4833333)
      }
    }
  }
}
