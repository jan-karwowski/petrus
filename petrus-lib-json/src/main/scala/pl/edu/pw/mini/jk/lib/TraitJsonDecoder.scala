package pl.edu.pw.mini.jk.lib

import argonaut.{ DecodeJson, DecodeResult }


case class TraitJsonDecoder[+T](decodes: Map[String, DecodeJson[_ <: T]]) {
  def decoder[TT >: T] : DecodeJson[TT] = DecodeJson(c => for {
    name <- (c --\"name").as[String]
    changer <- if(decodes.contains(name)) (c --\"config").as(decodes(name)) else DecodeResult.fail("Unknown name "+name, c.history)
  } yield(changer))
}

