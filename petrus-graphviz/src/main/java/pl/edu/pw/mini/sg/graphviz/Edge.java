package pl.edu.pw.mini.sg.graphviz;

public class Edge {
    private final Vertex v1;
    private final Vertex v2;

    public Edge(Vertex v1, Vertex v2) {
        super();
        this.v1 = v1;
        this.v2 = v2;
    }

    public String getCode() {
        return v1.getId() + "->" + v2.getId()+";";
    }
}
