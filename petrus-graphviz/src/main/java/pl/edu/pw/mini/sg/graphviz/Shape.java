package pl.edu.pw.mini.sg.graphviz;

public enum Shape {
    TRIANGLE("triangle"), CIRCLE("circle"), DIAMOND("diamond"),
    RECTANGLE("rectangle"),
    ELLIPSE("ellipse");
    
    private Shape(String code) {
        this.code = code;
    }
    private final String code;
    public String getCode(){
        return code;
    }
}
