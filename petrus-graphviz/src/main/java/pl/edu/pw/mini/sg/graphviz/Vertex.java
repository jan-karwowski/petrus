package pl.edu.pw.mini.sg.graphviz;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

public class Vertex {
    private final String id;
    private final String label;
    private final Shape shape;
    private final String color;
    private final Optional<String> fillcolor;
    private final ImmutableMap<String, String> optionalParameters;

    public Vertex(String id, String label, Shape shape, String color, Optional<String> fillcolor) {
        super();
        this.id = id;
        this.label = label;
        this.shape = shape;
        this.color = color;
        this.fillcolor = fillcolor;
        this.optionalParameters = ImmutableMap.of();
    }

    public Vertex(String id, String label, Shape shape, String color, Optional<String> fillcolor,
            Map<String, String> parameters) {
        super();
        this.id = id;
        this.label = label;
        this.shape = shape;
        this.color = color;
        this.fillcolor = fillcolor;
        this.optionalParameters = ImmutableMap.copyOf(parameters);
    }

    public String getCode() {
        final String optionals;

        if (optionalParameters.size() > 0) {
            optionals = "," + optionalParameters.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue())
                    .collect(Collectors.joining(","));
        } else {
            optionals = "";
        }

        return String.format("%s [shape=%s, color=%s, margin=0.01, label=\"%s\" %s %s];", id, shape.getCode(), color,
                label, fillcolor.isPresent() ? ",style=\"filled\", fillcolor=" + fillcolor.get() : "", optionals);
    }

    public String getId() {
        return id;
    }
}
