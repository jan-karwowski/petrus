package pl.edu.pw.mini.sg.graphviz;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

public class Graph {
    private final ImmutableList<Vertex> vertices;
    private final ImmutableList<Edge> edges;

    public Graph(ImmutableList<Vertex> vertices, ImmutableList<Edge> edges) {
        super();
        this.vertices = vertices;
        this.edges = edges;
    }

    public String getCode() {
        return Stream.of(Stream.of("digraph G {nodesep=0.2;ranksep=0.3;\n"), vertices.stream().map(Vertex::getCode), edges.stream().map(Edge::getCode),
                Stream.of("}")).flatMap(x -> x).collect(Collectors.joining("\n"));
    }
}
