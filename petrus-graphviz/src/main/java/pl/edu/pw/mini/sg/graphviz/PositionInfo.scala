package pl.edu.pw.mini.sg.graphviz;


case class PositionInfo(id: Int, x: Int, y: Int, floor: Int, nodeType: String)
