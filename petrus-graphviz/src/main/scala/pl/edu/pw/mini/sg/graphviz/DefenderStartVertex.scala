package pl.edu.pw.mini.sg.graphviz

class DefenderStartVertex(id: String, defenderReward: Double, attackerPenalty: Double, position: Option[(Double, Double)])
    extends RegularVertex(id, defenderReward, attackerPenalty, position) {
  override def fill: Option[String] = Some(""""#BBBBFF"""")
  override def color: String = "blue"
}
