package pl.edu.pw.mini.sg.graphviz


class TargetVertex(
  id: String, defenderReward: Double, attackerPenalty: Double,
  defenderPenalty: Double, attackerReward: Double,
  override val position: Option[(Double, Double)]
) extends AbstractVertex(id) {
  final val CP = 1.0

  override def color: String = "green"
  override def fill: Option[String] = Some("\"#CCFFCC\"")
  override def shape: Shape = Shape.RECTANGLE

  override def label: String = f"""<<TABLE border="0" cellspacing="0" cellpadding="1" cellborder="1"><TR><TD cellpadding="$CP"><B>$id</B></TD><TD cellpadding="$CP">I</TD><TD cellpadding="$CP">S</TD></TR><TR><TD cellpadding="$CP">D</TD><TD cellpadding="$CP">$defenderReward%.2f</TD><TD cellpadding="$CP">$defenderPenalty%.2f</TD></TR><TR><TD cellpadding="$CP">A</TD><TD cellpadding="$CP">$attackerPenalty%.2f</TD><TD cellpadding="$CP">$attackerReward%.2f</TD></TR></TABLE>>"""
  override def additionalParams: List[(String, String)] = List(("margin", "0"))
}
