package pl.edu.pw.mini.sg.graphviz

class StartVertex(id: String, defenderReward: Double, attackerPenalty: Double, pos: Option[(Double, Double)])
    extends RegularVertex(id, defenderReward, attackerPenalty, pos) {
  override def shape: Shape = Shape.TRIANGLE
  override def color: String = "red"
  override def fill = Some("\"#FFBBBB\"")

  override def additionalParams: List[(String, String)] = List(("fixedsize", "true"), ("width", "1.2"),
    ("height", "1.0"))
}
