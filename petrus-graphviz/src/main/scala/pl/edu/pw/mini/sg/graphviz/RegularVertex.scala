package pl.edu.pw.mini.sg.graphviz

class RegularVertex(id: String, defenderReward: Double, attackerPenalty: Double, override val position: Option[(Double, Double)])
    extends AbstractVertex(id) {
  final val CP : Double = 1;

  override def fill: Option[String] = None
  override def color: String = "black"
  override def shape: Shape = Shape.ELLIPSE
  override def label: String = f"""<<TABLE border="0" cellspacing="0" cellborder="1"><TR><TD  cellpadding="$CP" rowspan="2"><B>$id</B></TD><TD cellpadding="$CP">D</TD><TD cellpadding="$CP">$defenderReward%.2f</TD></TR><TR><TD cellpadding="$CP">A</TD><TD cellpadding="$CP">$attackerPenalty%.2f</TD></TR></TABLE>>"""
  override def additionalParams: List[(String, String)] = List(("margin", "0"))
}
