package pl.edu.pw.mini.sg.graphviz

abstract class AbstractVertex(
  id: String
) {
  def label: String
  def shape: Shape
  def position: Option[(Double, Double)] = None
  def color:String
  def fill: Option[String]
  def additionalParams: List[(String, String)] = Nil

  private def optionsToString: String = (additionalParams++
    fill.toList.flatMap(x => List(("fillcolor", x), ("style", "filled")))++
    position.map{case (x,y) => ("pos", s""""${x},${y}"""")}).map{case (k,v) => s", $k=$v"}.fold("")(_+_)
  def draw: String = {
    s"""$id [shape=${shape.getCode}, color=${color}, label=$label $optionsToString];"""
  }
}
