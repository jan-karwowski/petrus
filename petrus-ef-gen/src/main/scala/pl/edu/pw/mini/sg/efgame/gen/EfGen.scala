package pl.edu.pw.mini.sg.efgame.gen

import argonaut.Parse
import better.files.File
import java.util.UUID
import org.apache.commons.math3.random.MersenneTwister
import org.rogach.scallop.ScallopConf
import java.io.{File => JFile}
import better.files.FileOps
import argonaut.JsonIdentity._
import pl.edu.pw.mini.sg.efgame.EfGameConfigJson
import pl.edu.pw.mini.sg.efgame.seeded.{SeededEfConfig, SeededEfConfigJson}

object EfGen {
  private final class Options(args: Seq[String]) extends ScallopConf(args) {
    
    val config = opt[JFile](name = "config", short = 'c', required = true).map(_.toScala)
    val output = opt[List[String]](name = "output", short = 'o').map(_.map(s => File.apply(s)))
    val seededOut = opt[List[String]](name = "soutput", short = 's').map(_.map(s => File.apply(s)))

    verify()
  }

  def main(args: Array[String]) : Unit = {
    import GeneratorConfigJson._
    val options = new Options(args)

    val config = Parse.parse(options.config().contentAsString)
      .fold(err => throw new RuntimeException(s"Json parse error: $err"), identity).as[GeneratorConfig]
      .fold((a,b) => throw new RuntimeException(s"Json structure error ($a, $b)"), identity)

    implicit val rng = new MersenneTwister()

    options.output.foreach(_.foreach{file => 
      val game = Generator.randomGame(config)

      import EfGameConfigJson.efGameConfigCodec
      file.write(game.asJson.toString())
    })

    options.seededOut.foreach(_.foreach(file => {
      val sc = SeededEfConfig(config, rng.nextInt, UUID.randomUUID())
      import SeededEfConfigJson.seededEfConfigCodec
      file.write(sc.asJson.toString())
    }))
  }
}
