package pl.edu.pw.mini.sg.util


object VectorUtils {
  implicit final class UpdateHelpers[T](vec: Vector[T]) {
    final def updatedWith(idx: Int, fun: T => T) : Vector[T] = vec.updated(idx, fun(vec(idx)))
  }
}
