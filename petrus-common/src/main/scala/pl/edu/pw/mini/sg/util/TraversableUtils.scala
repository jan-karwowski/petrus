package pl.edu.pw.mini.sg.util

object TraversableUtils {

  /**
    * Zawiera metode sluzaca do wygenerowania iloczynu kartezjanskiego dwoch list. Zwraca liste par.
    */
  implicit class Crossable[X](xs: Traversable[X]) {
    def cross[Y](ys: Traversable[Y]): Traversable[(X, Y)] = for {x <- xs; y <- ys} yield (x, y)
  }

  /**
    * Generowanie wszystkich kombinacji elementow sekwencji z danej sekwencji
    * (z kazdej wewnetrznej sekwencji wybierany jest jeden element)
    * https://stackoverflow.com/questions/23425930/generating-all-possible-combinations-from-a-listlistint-in-scala
    *
    * @param xss Sekwencja sekwencji dla ktorych generujemy kombinacje
    * @tparam A Typ elementow wewnetrznych sekwencji
    * @return Sekwencja sekwencji z wszystkimi kombinacjami wejscia
    */
  def combs[A](xss: Seq[Seq[A]]): Seq[Seq[A]] = xss match {
    case Nil => List(Nil)
    case xs +: rss => for (x <- xs; cs <- combs(rss)) yield x +: cs
  }


}
