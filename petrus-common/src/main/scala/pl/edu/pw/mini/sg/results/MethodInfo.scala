package pl.edu.pw.mini.sg.results

import argonaut.{EncodeJson, Json}
import argonaut.Argonaut._


object MethodInfo {
  implicit val encodeJson : EncodeJson[MethodInfo] =
    EncodeJson(
      (i: MethodInfo) => {
        ("config" := i.configFileName) ->: ("gitHash" := i.gitHash) ->: ("version" := i.version) ->: i.additionalInformation
      }
    )
}

trait MethodInfo {
  val configFileName: String
  val gitHash: String
  val version: String
  val methodName: String
  val additionalInformation: Json = {
    jEmptyObject
  }
}
