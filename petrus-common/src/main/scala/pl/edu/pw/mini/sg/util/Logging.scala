package pl.edu.pw.mini.sg.util

import org.log4s._


trait Logging {
  protected[this] val log = getLogger
}
