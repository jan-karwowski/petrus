package pl.edu.pw.mini.sg.results

case class NewMixedMethodInfo(override val configFileName: String,
                              override val gitHash: String,
                              override val version: String) extends MethodInfo {
  override val methodName: String = "mixed"
}
