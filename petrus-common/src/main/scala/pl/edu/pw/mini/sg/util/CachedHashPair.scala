package pl.edu.pw.mini.sg.util

import scala.Product2
import scala.runtime.ScalaRunTime
import scala.util.hashing.MurmurHash3



final class CachedHashPair[T,R](
  val first: T,
  val second: R
) {
  override def toString: String = s"($first, $second)";
//  override val hashCode: Int = (first, second).hashCode
  override val hashCode = MurmurHash3.finalizeHash(MurmurHash3.mix(
    MurmurHash3.mix(0xcafebabe,first.hashCode), second.hashCode
  ), 2)
  override def equals(other: Any): Boolean = other match {
    case p : CachedHashPair[T, R] => p.first.equals(first) && p.second.equals(second)
    case _ => false
  }
}

object CachedHashPair {
  def apply[T,R](first: T, second: R) = new CachedHashPair(first, second)
}
