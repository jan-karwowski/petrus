package pl.edu.pw.mini.sg.util

import org.scalatest.{ Matchers, WordSpec }
import scala.collection.mutable.WrappedArray



class StreamUtilsSpec extends WordSpec with Matchers {
  "myStreamLength" should {
    "not exceed memory when calculating large stream length" in {
      def veryLongStream = Stream.tabulate(100000) { x => Array.fill(100000)(x) }

      StreamUtils.myStreamLength(veryLongStream) should equal(100000)
    }
  }

  "streamZip" should {
    "not exceed memory when calculating large stream length" in {
      def veryLongStream = Stream.tabulate(100000) { x => Array.fill(100000)(x) }

      StreamUtils.myStreamLength(StreamUtils.streamZip(veryLongStream, veryLongStream)) should equal(100000)
    }
  }

  "groupped stream" should {
     "not exceed memory when calculating large stream length" in {
       def veryLongStream = Stream.tabulate(100000) { x => Array.fill(100000)(x) }

       StreamUtils.myStreamLength(StreamUtils.streamGrouped(veryLongStream,10)) should equal(10000)
    }
  }

  "stream distinct" should {
     "not exceed memory when calculating large stream distinct" in {
       def veryLongStream = Stream.tabulate(100000) { x => WrappedArray.make(Array.fill(100000)(12)) }

       StreamUtils.streamDistinct(veryLongStream) should equal(Set(WrappedArray.make(Array.fill(100000)(12))))
    }
  }

  "stream maxBy" should {
     "not exceed memory when calculating large stream maxBy" in {
       def veryLongStream = Stream.tabulate(100000) { x => WrappedArray.make[Int](Array.fill(100000)(12)) }

       StreamUtils.streamMaxBy[WrappedArray[Int], Int](veryLongStream, _.apply(0)) should equal(WrappedArray.make(Array.fill(100000)(12)))
    }
  }
}
