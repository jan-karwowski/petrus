package pl.edu.pw.mini.sg.game.results

import better.files.File
import better.files.StringSplitter

object CpuInfo {
  def apply() : String = {
    procCpuInfo orElse windowsEnv getOrElse "UNKNOWN"
  }

  private def procCpuInfo() : Option[String] = {
    val cpuinfo = File("/proc/cpuinfo")

    if(cpuinfo.exists)
      (
        for {
        file <- cpuinfo.scanner(StringSplitter.Default)
        } yield file.lines.find(_.startsWith("model name")).map(_.split(":", 2)(1))
      ).get
    else
      None
  }

  //TODO
  private def windowsEnv() : Option[String] = None
}
