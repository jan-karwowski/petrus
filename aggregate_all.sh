#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $DIR/el_config.sh


for game in $games; do
    for config in $configs; do
	for attacker in $attakcers; do
	    $DIR/aggregation.sh game${game}_${config}_${attacker}
	done
    done
done

