package pl.edu.pw.mini.game.flipit.deterministic.allpoints

import pl.edu.pw.mini.game.flipit.deterministic.{ Flip, FlipItConfig, FlipItMove, NoOp }


private [allpoints] final case class Cache(
  config: FlipItConfig
) {
  val neighbours : Vector[Set[Int]] = Vector.tabulate(config.graph.vertexCount)(
    v => config.graph.edges.filter(_.from == v).map(_.to).toSet
  )

  val coneighbours : Vector[Set[Int]] = Vector.tabulate(config.graph.vertexCount)(
    v => config.graph.edges.filter(_.to == v).map(_.from).toSet
  )

  val flipMoves : Vector[Flip] = Vector.tabulate(config.graph.vertexCount)(Flip.apply)
  val allMoves : List[FlipItMove] = NoOp :: List.tabulate(config.graph.vertexCount)(flipMoves)
}
