package pl.edu.pw.mini.game.flipit.deterministic.allpoints

import pl.edu.pw.mini.game.flipit.deterministic.{ FlipItConfig, FlipItMove }
import pl.edu.pw.mini.game.flipit.deterministic.noinfo.Defender
import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactory


object AllPointsFlipItGameFactory extends
    GameFactory[FlipItConfig] {
  override type DefenderMove = FlipItMove
  override type AttackerMove = FlipItMove
  override type DefenderState = AllPointsFlipItDefenderState
  override type AttackerState = AllPointsFlipItAttackerState
  override type Game = AllPointsFlipItGame

  override def create(gameConfig: FlipItConfig) : AllPointsFlipItGame = {
    val cache = Cache(gameConfig)
    val initialDS = new AllPointsFlipItDefenderStateInner(Range(0, gameConfig.graph.vertexCount).toSet,
      1, 0.0, false, cache)
    val initialAS = new AllPointsFlipItAttackerStateInner(false, 0.0, Set(), 1, cache)
    new AllPointsFlipItGame(Vector.fill(gameConfig.graph.vertexCount)(Defender), initialDS, initialAS,
      0.0, 0.0, cache)
  }
  override val json = AllPointsFlipItGameJson

  def maxDefenderPayoff(gameConfig: FlipItConfig): Double = gameConfig.defenderRewards.sum*gameConfig.rounds
  def minDefenderPayoff(gameConfig: FlipItConfig): Double = -gameConfig.defenderCosts.max*gameConfig.rounds
  def maxAttackerPayoff(gameConfig: FlipItConfig): Double = gameConfig.attackerRewards.sum*gameConfig.rounds
  def minAttackerPayoff(gameConfig: FlipItConfig): Double = -gameConfig.attackerCosts.max*gameConfig.rounds
}
