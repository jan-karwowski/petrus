package pl.edu.pw.mini.game.flipit.deterministic.noinfo

import argonaut.{EncodeJson, Json, JsonIdentity}
import pl.edu.pw.mini.game.flipit.deterministic.{Flip, FlipItMove}
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson

object NoInfoFlipItGameJson extends DeterministicGameJson [
  FlipItMove, FlipItMove, NoInfoFlipItState, NoInfoFlipItAttackerState
]{
  implicit val stateEncode : EncodeJson[NoInfoFlipItState] = EncodeJson{
    case inner : NoInfoFlipItStateInner => Json.jString("inner")
    case terminal : NoInfoFlipitStateTerminal.type => Json.jString("terminal")
  }

  implicit val asEncode : EncodeJson[NoInfoFlipItAttackerState] = EncodeJson{
    case terminal : NoInfoFlipItAttackerStateTerminal.type => Json.jString("terminal")
    case inner : NoInfoFlipItAttackerStateInner => JsonIdentity
        .ToJsonIdentity(inner.moves.innerList.map(_.vertex)).asJson
  }

  override val attackerMoveEncode: EncodeJson[FlipItMove] = FlipItMove.moveEncode
  override val attackerStateEncode: EncodeJson[NoInfoFlipItAttackerState] = asEncode
  override val defenderMoveEncode: EncodeJson[FlipItMove] = FlipItMove.moveEncode
  override val defenderStateEncode: EncodeJson[NoInfoFlipItState] = stateEncode
}
