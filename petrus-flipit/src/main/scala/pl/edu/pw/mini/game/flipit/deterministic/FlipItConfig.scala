package pl.edu.pw.mini.game.flipit.deterministic

import argonaut.CodecJson
import java.util.UUID
import pl.edu.pw.mini.sg.game.deterministic.plugin.config.GameConfig
import pl.edu.pw.mini.sg.game.graph.{ GraphConfig, GraphConfigJson }
import scala.runtime.ScalaRunTime


final case class FlipItConfig(
  id: UUID,
  graph: GraphConfig,
  rounds: Int,
  entryNodes: Set[Int],
  defenderRewards: Vector[Double],
  defenderCosts: Vector[Double],
  attackerRewards: Vector[Double],
  attackerCosts: Vector[Double]
) extends GameConfig {
  require(defenderRewards.length == graph.vertexCount)
  require(defenderCosts.length == graph.vertexCount)
  require(attackerRewards.length == graph.vertexCount)
  require(attackerCosts.length == graph.vertexCount)
  require(entryNodes.forall(x => x >= 0 && x < graph.vertexCount))

  override val hashCode = ScalaRunTime._hashCode(this)
}


object FlipItConfig {
  import GraphConfigJson.graphConfigCodec
  implicit val jsonCodec: CodecJson[FlipItConfig] = CodecJson.casecodec8(FlipItConfig.apply, FlipItConfig.unapply)(
    "id", "graph", "rounds", "entryNodes", "defenderRewards", "defenderCosts", "attackerRewards", "attackerCosts"
  )
}
