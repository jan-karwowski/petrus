package pl.edu.pw.mini.game.flipit.deterministic.noinfo

import pl.edu.pw.mini.game.flipit.deterministic.FlipItConfig
import pl.edu.pw.mini.sg.game.deterministic.plugin.GamePlugin


object NoInfoFlipItGamePlugin extends GamePlugin {
  override type GameConfig = FlipItConfig

  override val fileExtension: String = "niflip"
  override val configJsonDecode = FlipItConfig.jsonCodec
  override val singleCoordFactory = NoInfoFlipItGameFactory

}
