package pl.edu.pw.mini.game.flipit.deterministic.allpoints

import argonaut.DecodeJson
import pl.edu.pw.mini.game.flipit.deterministic.FlipItConfig
import pl.edu.pw.mini.sg.game.deterministic.plugin.GamePlugin


object AllPointsFlipItGamePlugin extends GamePlugin {
  override type GameConfig = FlipItConfig

  override val fileExtension: String = "apflip"
  override val configJsonDecode: DecodeJson[GameConfig] = FlipItConfig.jsonCodec
  override val singleCoordFactory = AllPointsFlipItGameFactory
}
