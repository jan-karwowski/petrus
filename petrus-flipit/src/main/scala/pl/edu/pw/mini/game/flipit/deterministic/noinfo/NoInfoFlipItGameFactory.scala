package pl.edu.pw.mini.game.flipit.deterministic.noinfo

import pl.edu.pw.mini.game.flipit.deterministic.FlipItConfig
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson
import pl.edu.pw.mini.sg.game.deterministic.plugin.GameFactory
import pl.edu.pw.mini.game.flipit.deterministic.{FlipItMove, Flip}

object NoInfoFlipItGameFactory extends GameFactory[FlipItConfig]{
  override type DefenderMove = FlipItMove
  override type AttackerMove = FlipItMove
  override type DefenderState = NoInfoFlipItState
  override type AttackerState = NoInfoFlipItAttackerState
  override type Game = NoInfoFlipItGame

  override def create(gameConfig: FlipItConfig) : NoInfoFlipItGame = {
    val cache = new NoInfoFlipItCache(gameConfig)
    new NoInfoFlipItGame(gameConfig, Vector.fill(gameConfig.graph.vertexCount)(Defender),
      0, 0, cache.initialState, new NoInfoFlipItAttackerStateInner(cache,
        SortedList[Flip, Flip.ord.type](gameConfig.entryNodes.map(cache.moves).toList)(Flip.ord)
        , 1))
  }

  override val json: DeterministicGameJson[DefenderMove, AttackerMove, DefenderState, AttackerState] = NoInfoFlipItGameJson

  def maxDefenderPayoff(gameConfig: FlipItConfig): Double = gameConfig.defenderRewards.sum*gameConfig.rounds
  def minDefenderPayoff(gameConfig: FlipItConfig): Double = -gameConfig.defenderCosts.max*gameConfig.rounds
  def maxAttackerPayoff(gameConfig: FlipItConfig): Double = gameConfig.attackerRewards.sum*gameConfig.rounds
  def minAttackerPayoff(gameConfig: FlipItConfig): Double = -gameConfig.attackerCosts.max*gameConfig.rounds

}
