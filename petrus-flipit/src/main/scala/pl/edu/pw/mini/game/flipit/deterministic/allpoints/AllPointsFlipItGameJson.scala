package pl.edu.pw.mini.game.flipit.deterministic.allpoints

import argonaut.{ EncodeJson, Json, JsonIdentity }
import pl.edu.pw.mini.game.flipit.deterministic.FlipItMove
import pl.edu.pw.mini.sg.game.deterministic.json.DeterministicGameJson


object AllPointsFlipItGameJson extends
    DeterministicGameJson[FlipItMove, FlipItMove, AllPointsFlipItDefenderState, AllPointsFlipItAttackerState] {
  override val defenderMoveEncode: EncodeJson[FlipItMove] = FlipItMove.moveEncode
  override val attackerMoveEncode: EncodeJson[FlipItMove] = FlipItMove.moveEncode
  override val defenderStateEncode: EncodeJson[AllPointsFlipItDefenderState] =
    EncodeJson{
      case state: AllPointsFlipItDefenderStateInner =>
        Json.obj(
          "round" -> Json.jNumber(state.round),
          "previousPayoff" -> Json.jNumber(state.payoff),
          "previousSuccess" -> Json.jBool(state.previousMoveSuccess),
          "possiblyOwned" -> JsonIdentity.ToJsonIdentity(state.possiblyOwnedVertices).asJson
        )
      case AllPointsFlipItDefenderStateTerminal => Json.jString("terminal")
    }
  override val attackerStateEncode: EncodeJson[AllPointsFlipItAttackerState] =
    EncodeJson{
      case state: AllPointsFlipItAttackerStateInner =>
        Json.obj(
          "round" -> Json.jNumber(state.round),
          "previousPayoff" -> Json.jNumber(state.lastRoundReward),
          "previousSuccess" -> Json.jBool(state.previousMoveSucceded),
          "possiblyOwned" -> JsonIdentity.ToJsonIdentity(state.possiblyOwnedVertices).asJson
        )
      case AllPointsFlipItAttackerStateTerminal => Json.jString("terminal")
    }
}
