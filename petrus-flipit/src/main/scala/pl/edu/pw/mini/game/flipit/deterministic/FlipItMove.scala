package pl.edu.pw.mini.game.flipit.deterministic

import argonaut.{ EncodeJson, Json }


sealed trait FlipItMove {
  def flippedVertex : Option[Int]
}

object NoOp extends FlipItMove{
  override val flippedVertex = None
  override def toString = "NoOp"
}

final class Flip(val vertex: Int) extends FlipItMove {
  override val hashCode = vertex.##
  override def equals(other: Any) = other match {
    case m : Flip => m.vertex == vertex
    case _ => false
  }

  override def flippedVertex = Some(vertex)
  override def toString = s"Flip(${vertex})"
}

object Flip {
  def apply(vertex: Int) = new Flip(vertex)

  implicit val ord : Ordering[Flip] = Ordering.fromLessThan[Flip]((f1, f2) => f1.vertex < f2.vertex)
}

object FlipItMove {
  implicit val moveEncode : EncodeJson[FlipItMove] = EncodeJson{
    case f : Flip => Json.jNumber(f.vertex)
    case n : NoOp.type => Json.jString("noop")
  }
}
