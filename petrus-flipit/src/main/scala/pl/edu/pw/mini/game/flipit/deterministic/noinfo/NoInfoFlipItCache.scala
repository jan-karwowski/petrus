package pl.edu.pw.mini.game.flipit.deterministic.noinfo

import pl.edu.pw.mini.game.flipit.deterministic.{FlipItConfig, Flip, FlipItMove, NoOp}


final class NoInfoFlipItCache(
  val flipitConfig: FlipItConfig
) {
  override val hashCode = flipitConfig.##
  override def equals(other: Any) = other match {
    case fc: NoInfoFlipItCache => fc.flipitConfig == flipitConfig
    case _ => false
  }

  val moves : Vector[Flip] = Vector.tabulate(flipitConfig.graph.vertexCount)(Flip.apply)
  val possibleMoves : List[FlipItMove] = NoOp :: moves.toList
  val initialState: NoInfoFlipItState = Stream
    .iterate[NoInfoFlipItState](
      NoInfoFlipitStateTerminal, flipitConfig.rounds+1
    )(
      prev => new NoInfoFlipItStateInner(this, prev)
    ).last

  val neighbours : Vector[SortedList[Flip, Flip.ord.type]] = Vector.tabulate(flipitConfig.graph.vertexCount)(
    v => SortedList[Flip, Flip.ord.type](
      flipitConfig.graph.edges.filter(_.from == v).map(e => moves(e.to)).toList
    )(Flip.ord)
  )
}
