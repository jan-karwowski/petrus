#!/bin/bash
set -e

JAR=/home/jan/research/security_games/petrus/petrus-gtopt/target/petrus-gtopt-1.0-SNAPSHOT-jar-with-dependencies.jar

export GUROBI_HOME=/home/jan/Pobrane/gurobi651/linux64/
export GRB_LICENSE_FILE=/home/jan/Pobrane/gurobi651/gurobi.lic
GUROBI_PATH=/home/jan/Pobrane/gurobi651/linux64/bin/gurobi.sh
ZIMPL_PATH=/home/jan/Pobrane/scipoptsuite-3.1.1/zimpl-3.3.2/bin/zimpl

GAME=$1
MOVES=$2

PROBPREFIX=$(basename $GAME .json)-$MOVES

java -jar $JAR $GAME $MOVES ${PROBPREFIX}.codes   ${PROBPREFIX}.mps.gz
#$ZIMPL_PATH -t lp ${PROBPREFIX}.zpl


#$GUROBI_PATH > ${PROBPREFIX}.gurobi.stat  <<EOF
#from gurobipy import *
#setParam("Threads", 1)
#setParam("FeasibilityTol", 1e-9)
#setParam("IntFeasTol", 1e-9)
#m = read("${PROBPREFIX}.mps.gz")
#m.optimize()
#m.write("${PROBPREFIX}.gurobi.sol")
#EOF



#rm ${PROBPREFIX}.mps.gz
#rm ${PROBPREFIX}.lp

