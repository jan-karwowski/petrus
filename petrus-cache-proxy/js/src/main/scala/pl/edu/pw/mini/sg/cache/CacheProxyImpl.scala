package pl.edu.pw.mini.sg.cache


//Dummy implementation
final class CacheProxyImpl[K, V](
) extends CacheProxy[K, V] {
  override def get(key: K, calculate: K => V) = calculate(key)
}

object CacheProxyImpl {
  def createCache[K <: AnyRef, V <: AnyRef]() : CacheProxyImpl[K, V] = new CacheProxyImpl[K, V]()
}
