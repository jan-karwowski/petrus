package pl.edu.pw.mini.sg.cache

import com.github.benmanes.caffeine.cache.{ Cache, Caffeine }


class CacheProxyImpl[K, V](
  cache: Cache[K, V]
) extends CacheProxy[K, V] {
  override def get(key: K, calculate: K => V) = cache.get(key, (k => calculate(k)))
}

object CacheProxyImpl {
  def createCache[K <: AnyRef, V <: AnyRef]() : CacheProxyImpl[K, V] = new CacheProxyImpl[K, V](Caffeine.newBuilder().build[K,V]())
}
