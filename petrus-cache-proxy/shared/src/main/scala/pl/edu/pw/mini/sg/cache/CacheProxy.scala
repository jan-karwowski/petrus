package pl.edu.pw.mini.sg.cache

trait CacheProxy[K,V] {
  def get(key: K, calculate: K => V): V
}
