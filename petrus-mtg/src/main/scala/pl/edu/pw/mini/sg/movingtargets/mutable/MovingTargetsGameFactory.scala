package pl.edu.pw.mini.sg.movingtargets.mutable

import canes.game.{ AllUnitsPositions, PureDefenderStrategy }
import canes.game.attacker.AttackStart
import pl.edu.pw.mini.sg.movingtargets.helpers.FactoryWithJsonOutput
import pl.edu.pw.mini.sg.movingtargets.{ AttackDecision, MovingTargetsDefenderMove }
import scala.collection.JavaConverters._

import canes.game.attacker.{Strategy => AStrategy}
import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import pl.edu.pw.mini.sg.movingtargets.{AttackerDecision, MovingTargetsDefenderMove, MovingTargetsGameConfig, MovingTargetsImmutableGame}
import pl.edu.pw.mini.sg.movingtargets.helpers.FactoryWithJsonOutput

case class MovingTargetsGameFactory(gameConfig: MovingTargetsGameConfig)  extends NormalizableFactory[AttackerDecision, MovingTargetsDefenderMove, MovingTargetsGame] with FactoryWithJsonOutput[MovingTargetsDefenderMove] {

  lazy val game = MovingTargetsImmutableGame.fromConfig(gameConfig)

  override def create: pl.edu.pw.mini.sg.movingtargets.mutable.MovingTargetsGame = new MovingTargetsGame(game, game, Nil)
  override def denormalize: this.type = this
  override def normalize: this.type = this

  override def convertPureStrategy(seq: java.util.List[MovingTargetsDefenderMove]): PureDefenderStrategy = {
    PureDefenderStrategy(
    seq.asScala.map { case MovingTargetsDefenderMove(positions) =>  AllUnitsPositions(positions.map(gameConfig.toDoublePositions).toList) }.toVector)
  }

  def attackerStrategyConvert(strategy: java.util.Map[_ <: java.util.List[AttackerDecision], java.lang.Double]) : AStrategy = {
    strategy.keySet.asScala.head.asScala.zipWithIndex.find{ case (as : AttackDecision, _) => true case _ => false } match {
      case Some((AttackDecision(target), time)) => AStrategy(Some(AttackStart(target, time)))
      case _ => AStrategy(None)
    }
  }
}
