package pl.edu.pw.mini.sg.movingtargets.coord

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove


sealed trait CoordMove extends DefenderMove
final case class XMove(destination: Int) extends CoordMove
final case class YMove(destination: Int) extends CoordMove

