package pl.edu.pw.mini.sg.movingtargets.aggregate

import argonaut.CodecJson


final case class AggregateTransformerConfiguration(
  aggregation: Int
)

object AggregateTransformerConfiguration {
  implicit val atcCodec : CodecJson[AggregateTransformerConfiguration] =
    CodecJson.casecodec1(AggregateTransformerConfiguration.apply, AggregateTransformerConfiguration.unapply)("aggregation")
}
