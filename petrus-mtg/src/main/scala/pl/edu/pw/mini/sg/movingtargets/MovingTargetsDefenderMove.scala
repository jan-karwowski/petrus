package pl.edu.pw.mini.sg.movingtargets

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import scala.runtime.ScalaRunTime

case class MovingTargetsDefenderMove(newPositions: Vector[DefenderPosition]) extends DefenderMove  {
  override lazy val hashCode = ScalaRunTime._hashCode(this)
}

