package pl.edu.pw.mini.sg.movingtargets


sealed trait AttackerDecision
case object WaitDecision extends AttackerDecision
case class AttackDecision(targetId: Int) extends AttackerDecision

