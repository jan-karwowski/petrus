package pl.edu.pw.mini.sg.movingtargets.coverage

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import scala.collection.immutable.SortedSet

final case class CoverageDefenderMove(coveredTargetIds: SortedSet[Int]) extends DefenderMove {
  override val hashCode = coveredTargetIds.hashCode()
}
