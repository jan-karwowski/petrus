package pl.edu.pw.mini.sg.movingtargets

import argonaut.{ Parse }
import java.io.File
import pl.edu.pw.mini.jk.sg.game.{ ConfigParseException, GameVariantRunner, SimpleGamePlugin }
import aggregate.AggregateTransformerConfiguration
import pl.edu.pw.mini.sg.movingtargets.aggregate.{ AggregateGameVariantRunner, AggregatedNormalizableFactory }

final case object MovingTargetsGamePlugin extends SimpleGamePlugin[MovingTargetsGameConfig](
  name => name.endsWith(".sgmt"),
  SimpleGamePlugin.fileParserFromJson(MovingTargetsGameConfigReader.gameDecode),
  MovingTargetsVariantRunner.apply
) {
  override def fileToGameVariantRunner(file: File, transformer: File): GameVariantRunner = {
    val config = fileParser(file) match {
      case Left(error) => throw new ConfigParseException(error)
      case Right(config) => config
    }

    val transformerObj = Parse.decode[AggregateTransformerConfiguration](scala.io.Source.fromFile(transformer).mkString) match {
      case Right(x) => x
      case Left(err) => throw new ConfigParseException(s"Transformer cannot be parsed: ${err}")
    }

    new AggregateGameVariantRunner(config, transformerObj.aggregation)
  }
}
