package pl.edu.pw.mini.sg.movingtargets

import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState
import pl.edu.pw.mini.jk.sg.game.immutable.AdvanceableState

import scala.collection.JavaConverters._


case class MovingTargetsAttackerState private (timeStep: Int, hasMoved: Boolean, moves: ImmutableList[AttackerDecision]) extends AdvanceableAttackerState[AttackerDecision] with AdvanceableState[AttackerDecision, MovingTargetsAttackerState] {
  override def advance(move: AttackerDecision) : MovingTargetsAttackerState = {
    if(hasMoved && move != WaitDecision) throw new IllegalStateException("Attacker already attacked")
    move match {
      case WaitDecision => copy(timeStep = timeStep + 1)
      case AttackDecision(_) => MovingTargetsAttackerState(timeStep+1, true, moves)
    }
  }

  override def getPossibleMoves(): ImmutableList[AttackerDecision] =
    if(hasMoved) ImmutableList.of[AttackerDecision](WaitDecision)
    else moves
  
  override def getTimeStep = timeStep
}

object MovingTargetsAttackerState {
  def createState(hasMoved: Boolean, targets: List[Int]) = new MovingTargetsAttackerState(0, hasMoved, ImmutableList.copyOf((WaitDecision :: targets.map(AttackDecision.apply)).asJava.asInstanceOf[java.lang.Iterable[AttackerDecision]]))
}
