package pl.edu.pw.mini.sg.movingtargets

object GeometryHelpers {
  def distanceSq(a: (Double, Double), b: (Double, Double)): Double = (a,b) match {
    case ((x1, y1), (x2, y2)) => (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)
  }
  def distance(a: (Double, Double), b: (Double, Double)) = Math.sqrt(distanceSq(a,b))

 
}
