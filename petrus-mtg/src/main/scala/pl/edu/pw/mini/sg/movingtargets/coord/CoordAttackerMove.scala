package pl.edu.pw.mini.sg.movingtargets.coord

import com.google.common.collect.ImmutableList


sealed trait CoordAttackerMove
case object DummyMove extends CoordAttackerMove {
  val asList : ImmutableList[CoordAttackerMove] = ImmutableList.of(DummyMove)
}
case object WaitDecision extends CoordAttackerMove {
  val asList : ImmutableList[CoordAttackerMove] = ImmutableList.of(WaitDecision)
}
case class AttackDecision(targetId: Int) extends CoordAttackerMove
