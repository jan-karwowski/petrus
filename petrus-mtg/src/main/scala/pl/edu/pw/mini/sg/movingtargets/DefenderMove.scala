package pl.edu.pw.mini.sg.movingtargets

import scala.collection.mutable.HashMap



final case class DefenderPosition private(x: Int, y: Int) {
  final def asTuple=(x,y)
  final def asPositionTuple(implicit gameConfig: MovingTargetsGameConfig) : (Double, Double) = (x*gameConfig.spaceDiscretization+gameConfig.origin.x,y*gameConfig.spaceDiscretization+gameConfig.origin.y)

  final def translate(dx: Int, dy: Int) : DefenderPosition = DefenderPosition(x+dx, y+dy)
}

object DefenderPosition {
  private val instances = HashMap[(Int, Int), DefenderPosition]()

  def cached(x: Int, y: Int) : DefenderPosition = instances.get((x,y)) match {
    case None => {
      val dp = new DefenderPosition(x, y)
      instances.put((x,y), dp)
      dp
    }
    case Some(x) => x
  }
}
