package pl.edu.pw.mini.sg.movingtargets.coord

import canes.game.{ AllUnitsPositions, PureDefenderStrategy }
import scala.collection.JavaConverters._

import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import pl.edu.pw.mini.jk.sg.game.immutable.mutable.MutableWrapper
import pl.edu.pw.mini.sg.movingtargets.{MovingTargetsGameConfig, DefenderPosition}
import pl.edu.pw.mini.sg.movingtargets.helpers.FactoryWithJsonOutput
import pl.edu.pw.mini.sg.movingtargets.multi.MovingTargetsMultiMutableGameFactory

class CoordNormalizableFactory(override val gameConfig: MovingTargetsGameConfig) extends NormalizableFactory[CoordAttackerMove, CoordMove, MutableWrapper[CoordAttackerMove, CoordMove, CoordAttackerState, CoordDefenderState, CoordImmutableGame]] with FactoryWithJsonOutput[CoordMove] {
  val is = CoordImmutableGame.initialState(gameConfig)
  override def create = new MutableWrapper[CoordAttackerMove, CoordMove, CoordAttackerState, CoordDefenderState, CoordImmutableGame](is)
  override def denormalize = this
  override def normalize = this

  override def convertPureStrategy(seq: java.util.List[CoordMove]): PureDefenderStrategy = PureDefenderStrategy(MovingTargetsMultiMutableGameFactory.evenLengthSequences(gameConfig.defenderCount * 2)(seq.asScala).map(
    m => pairsToPositions(m)
  ).toVector)


  private final def pairsToPositions(pairs: Seq[CoordMove]) =
    AllUnitsPositions(MovingTargetsMultiMutableGameFactory.evenLengthSequences(2)(pairs).map {
      case Vector(XMove(x), YMove(y)) => gameConfig.toDoublePositions(DefenderPosition(x, y))
    }.toList)
}
