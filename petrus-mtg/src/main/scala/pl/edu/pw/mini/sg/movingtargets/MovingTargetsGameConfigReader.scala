package pl.edu.pw.mini.sg.movingtargets

import argonaut.{ DecodeJson, Json, Parse }
import java.io.File
import java.net.URL

import canes.game.{MovingTargetsGameConfigReader => MTR}

object MovingTargetsGameConfigReader {
  implicit val gameDecode : DecodeJson[MovingTargetsGameConfig] = MTR.gameDecode.map(MovingTargetsGameConfig.inferSpaceDiscretization)


  def readGameConfigFromJson(json: Json) = json.as[MovingTargetsGameConfig].toEither.left.map(_.toString)
  def readGameConfigFromFile(file: File) = Parse.parse(scala.io.Source.fromFile(file).mkString).right.flatMap(readGameConfigFromJson _)
  def readGameConfigFromFileWithException(file: File) = {
    readGameConfigFromFile(file) match {
      case Right(x) => x
      case Left(e) => throw new RuntimeException(e.toString)
    }
  }
  
  def readGameConfigFromUrl(url: URL) = Parse.parse(scala.io.Source.fromURL(url).mkString).right.flatMap(readGameConfigFromJson)
}
