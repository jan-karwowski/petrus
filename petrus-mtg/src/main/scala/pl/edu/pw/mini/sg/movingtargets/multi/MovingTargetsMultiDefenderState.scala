package pl.edu.pw.mini.sg.movingtargets.multi

import com.google.common.collect.ImmutableList
import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState
import pl.edu.pw.mini.sg.movingtargets.{ DefenderPosition, MovingTargetsGameConfig }
import collection.JavaConverters._

final case class MovingTargetsDefenderStateMulti(
  timeStep: Int, 
  plannedMoves: Vector[DefenderPosition],
  currentPositions: Vector[DefenderPosition],
  gameConfig: MovingTargetsGameConfig
) extends AdvanceableDefenderState[MovingTargetsDefenderMoveMulti] {
  override def advance(move: MovingTargetsDefenderMoveMulti): MovingTargetsDefenderStateMulti = {
    if(!gameConfig.isProperMove(currentPositions(currentlyMovingUnitIdx), move.position))
      throw new IllegalArgumentException("Move not allowed")
    else if(plannedMoves.size < currentPositions.length -1)
      copy(plannedMoves = move.position +: plannedMoves)
    else
      copy(timeStep = timeStep+1, plannedMoves = Vector(), currentPositions = (plannedMoves :+ move.position))
  }
  override def getPossibleMoves(): ImmutableList[MovingTargetsDefenderMoveMulti] = possibleMoves
  override def getTimeStep(): Int = timeStep

  private lazy val currentlyMovingUnitIdx : Int = plannedMoves.size

  final lazy val possibleMoves = {
    ImmutableList.copyOf(asJavaIterableConverter(gameConfig.possiblePositions(currentPositions(currentlyMovingUnitIdx)).map(MovingTargetsDefenderMoveMulti.apply)).asJava)
  }

}
