package pl.edu.pw.mini.sg.movingtargets.multi

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import pl.edu.pw.mini.sg.movingtargets.DefenderPosition


case class MovingTargetsDefenderMoveMulti(position: DefenderPosition) extends DefenderMove
