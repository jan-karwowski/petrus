package pl.edu.pw.mini.sg.movingtargets.aggregate

import canes.game.{ Depot, MovingTargetsGame, Target }
import java.util.UUID
import org.scalatest.{ Matchers, WordSpec }
import pl.edu.pw.mini.sg.movingtargets.MovingTargetsGameConfig
import scala.collection.JavaConverters._

class AggregateMTGDefenderMoveSpec extends WordSpec with Matchers {
  "GameWithTwoBases and aggr2" when {
    val gameConfig = MovingTargetsGameConfig(MovingTargetsGame(UUID.fromString("123e4567-e89b-12d3-a456-426655440000"),
      List(Depot(1, 5, 5), Depot(2,20,20)),
      List(Target(1, 1, 2, List(2,33,50), 10, -5, -8, 4)),
      0.5, 2, 2, List(1,1), 1, 100, 1, 0), 1)

    val ds = AggregateMTGDefenderState.newGame(gameConfig, 2)

    "time step should be 0" in {
        ds.getTimeStep should be(0)
    }


    "allow 6² defender moves" in {
        ds.getPossibleMoves should have length(13*13)
    }

    "allow playing (-2,0),(0,2) move" in {
      ds.advance(new ShiftMove(Vector((-2,0), (0,2))))
    }

    "advanced state should have single move allowed" in {
      ds.advance(new ShiftMove(Vector((-2,0), (0,2)))).getPossibleMoves.asScala should contain(NoopMove)
      ds.advance(new ShiftMove(Vector((-2,0), (0,2)))).getPossibleMoves.asScala should have length(1)
    }

    "advanced state should have timeStep 1" in {
      ds.advance(new ShiftMove(Vector((-2,0), (0,2)))).getTimeStep should equal(1)
    }
  }
}
