package pl.edu.pw.mini.sg.movingtargets.coord

import canes.game.{ AllUnitsPositions, Depot, MixedStrategyEntry, MovingTargetsGame, PureDefenderStrategy, Strategy, Target, DefenderPosition }
import java.util.UUID
import org.scalamock.scalatest.MockFactory
import org.scalatest._
import pl.edu.pw.mini.sg.movingtargets.MovingTargetsGameConfig

import scala.collection.JavaConverters._


class CoordNormalizableFactorySpec extends WordSpec with Matchers with MockFactory {
  "CoordNormalizableFactory.toNativeStrategy" should {
    val singleStepStrategy = Vector[CoordMove](XMove(1), YMove(1), XMove(2), YMove(2))
    val twoStepStrategy = Vector[CoordMove](XMove(1), YMove(1), XMove(2), YMove(2),
    XMove(3), YMove(3), XMove(4), YMove(4))

    val gameConfig = MovingTargetsGameConfig(MovingTargetsGame(UUID.fromString("123e4567-e89b-12d3-a456-426655440000"),
        List(Depot(1, 5, 5), Depot(2,20,20)),
        List(Target(1, 1, 2, List(2,33,50), 10, -5, -8, 4)),
        0.5, 2, 2, List(1,1), 1, 100, 1, 0), 1)


    val factory = new CoordNormalizableFactory(gameConfig)

    "Properly transform single-step strategy" in {
      val s =factory.toNativeStrategy(Map(singleStepStrategy.toList.asJava -> new java.lang.Double(1.0)).asJava)
      s should equal(Strategy(gameConfig.game.id,Vector(
        MixedStrategyEntry(1.0, PureDefenderStrategy(Vector(AllUnitsPositions(List(DefenderPosition(6,6), DefenderPosition(7,7))))))
      )))
    }

    "Properly transform two-step strategy" in {
      val s =factory.toNativeStrategy(Map(twoStepStrategy.toList.asJava -> new java.lang.Double(1.0)).asJava)
      s should equal(Strategy(gameConfig.game.id,Vector(
        MixedStrategyEntry(1.0, PureDefenderStrategy(Vector(
          AllUnitsPositions(List(DefenderPosition(6,6), DefenderPosition(7,7))),
          AllUnitsPositions(List(DefenderPosition(8,8), DefenderPosition(9,9)))
        )))
      )))
    }
  }
}
