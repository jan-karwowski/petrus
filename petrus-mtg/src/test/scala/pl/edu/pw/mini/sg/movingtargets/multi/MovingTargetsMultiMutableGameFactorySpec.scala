package pl.edu.pw.mini.sg.movingtargets.multi

import canes.game.{ AllUnitsPositions, DefenderPosition, Depot, MixedStrategyEntry, MovingTargetsGame, PureDefenderStrategy, Strategy, Target }
import java.util.UUID
import org.scalatest._
import pl.edu.pw.mini.sg.movingtargets.{ MovingTargetsGameConfig, DefenderPosition => MDP }

import scala.collection.JavaConverters._

class MovingTargetsMultiMutableGameFactorySpec extends WordSpec with Matchers {
  "event length sequences" should {
    "divide sequence wrt order" in {
      MovingTargetsMultiMutableGameFactory.evenLengthSequences(2)(List(3,4,5,6,7,8)) should contain inOrderOnly(Vector(3,4), Vector(5,6), Vector(7,8))
    }
  }

  "toNativeFactory" when {
    val singleStepStrategy = Vector[MovingTargetsDefenderMoveMulti](MovingTargetsDefenderMoveMulti(MDP(1,1)), MovingTargetsDefenderMoveMulti(MDP(2,2)))
    val twoStepStrategy = Vector[MovingTargetsDefenderMoveMulti](MovingTargetsDefenderMoveMulti(MDP(1,1)), MovingTargetsDefenderMoveMulti(MDP(2,2)),
      MovingTargetsDefenderMoveMulti(MDP(3,3)), MovingTargetsDefenderMoveMulti(MDP(4,4)))

    val gameConfig = MovingTargetsGameConfig(MovingTargetsGame(UUID.fromString("123e4567-e89b-12d3-a456-426655440000"),
        List(Depot(1, 5, 5), Depot(2,20,20)),
        List(Target(1, 1, 2, List(2,33,50), 10, -5, -8, 4)),
        0.5, 2, 2, List(1,1), 1, 100, 1, 0), 1)

    val factory = new MovingTargetsMultiMutableGameFactory(gameConfig)

    "converting single step strategy" should {
      "generate single move ouput" in {
        val s =factory.toNativeStrategy(Map(singleStepStrategy.toList.asJava -> new java.lang.Double(1.0)).asJava)
        s should equal(Strategy(gameConfig.game.id,Vector(
          MixedStrategyEntry(1.0, PureDefenderStrategy(Vector(AllUnitsPositions(List(DefenderPosition(6,6), DefenderPosition(7,7))))))
      )))

      }
    }


    "Properly transform two-step strategy" in {
      val s =factory.toNativeStrategy(Map(twoStepStrategy.toList.asJava -> new java.lang.Double(1.0)).asJava)
      s should equal(Strategy(gameConfig.game.id,Vector(
        MixedStrategyEntry(1.0, PureDefenderStrategy(Vector(
          AllUnitsPositions(List(DefenderPosition(6,6), DefenderPosition(7,7))),
          AllUnitsPositions(List(DefenderPosition(8,8), DefenderPosition(9,9)))
        )))
      )))
    }

  }
}
