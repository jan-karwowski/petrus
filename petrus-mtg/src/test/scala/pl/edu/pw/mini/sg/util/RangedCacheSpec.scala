package pl.edu.pw.mini.sg.util

import org.scalatest._

class RangedCacheSpec extends WordSpec with Matchers {
  private case class TC(par: Int)

  "RangedCache" when  {
    "asking for 0 value" should {
      val cache = new RangedCache(TC.apply)
      "return 0" in { cache.get(0) should equal(TC(0)) }
    }

    "asking for -2 value" should {
      val cache = new RangedCache(TC.apply)
      "return -2" in { cache.get(-2) should equal(TC(-2)) }
    }

    "asking for 2 value" should {
      val cache = new RangedCache(TC.apply)
      "return 2" in { cache.get(2) should equal(TC(2)) }
    }

    "asked for -2 value" should {
      val cache = new RangedCache(TC.apply)
      cache.get(-2)

      "return 0 for 0" in {
        cache.get(0) should equal(TC(0))
      }

      "return -1 for -1" in {
        cache.get(-1) should equal(TC(-1))
      }
    }

    "asked in sequence for -2 and 2" should {
      val cache = new RangedCache(TC.apply)
      cache.get(-2)
      cache.get(2)

      "return 1 for 1" in {
        cache.get(1) should equal(TC(1))
      }

      "return 2 for 2" in {
        cache.get(2) should equal(TC(2))
      }

      "return 0 for 0" in {
        cache.get(0) should equal(TC(0))
      }

      "return -1 for -1" in {
        cache.get(-1) should equal(TC(-1))
      }
    }

    "asked in sequence for 2 and -2" should {
      val cache = new RangedCache(TC.apply)
      cache.get(2)
      cache.get(-2)

      "return 1 for 1" in {
        cache.get(1) should equal(TC(1))
      }

      "return 2 for 2" in {
        cache.get(2) should equal(TC(2))
      }

      "return 0 for 0" in {
        cache.get(0) should equal(TC(0))
      }

      "return -1 for -1" in {
        cache.get(-1) should equal(TC(-1))
      }
    }
  }
}
