#!/bin/sh

rm mean.csv res.csv

for a in *superdiv*; do
    if cat /home/jan/research/security_games/petrus/games/superdiv*b.set | grep -q $(echo $a | cut -f1,2 -d.); then
	true
    else
	rm $a
    fi
done
