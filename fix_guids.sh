#!/bin/sh

for a in $(cat ~/research/security_games/petrus/games/superdiv*[ab].set); do
    UID=$(jq .id ~/research/security_games/petrus/games/$a)
    for f in ${a}*.result; do
	jq '.game.id|='$UID < $f > $f.new
	mv $f.new $f
    done
done


for a in $(cat ~/research/security_games/petrus/search-games/searchgames.set); do
    UID=$(jq .id ~/research/security_games/petrus/search-games/$a)
    for f in $(basename ${a} .sgame)*.result; do
	jq '.game.id|='$UID < $f > $f.new
	mv $f.new $f
    done
done
