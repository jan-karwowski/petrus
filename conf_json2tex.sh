#!/bin/bash

jq -r '"\\uctconfig{"+(.uctDepth|tostring)+"}{"+(.epochLength|tostring)+"}{"+(.gameLength|tostring)+"}{"+(.explorationCoefficient|tostring)+"}{"+(.updaterLoss|tostring)+"}{"+(.updaterEps|tostring)+"}{"+(.updaterLimit|tostring)+"}{"+(.uctRoots|tostring)+"}"'

