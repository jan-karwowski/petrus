#!/bin/bash
set -e

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


export GUROBI_ADDITIONAL_SETTINGS='setParam("FeasibilityTol", 1e-9)
setParam("IntFeasTol", 1e-9)
'

$MY_DIR/test_gurobi.base.sh  "$@"



