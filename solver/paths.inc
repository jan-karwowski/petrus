# Paths when running from pack
GRAPH_GAME_PATH=$MYDIR/../games
MIXED_STRATEGY_CONFIGS_PATH=$MY_DIR/../configs
EXPERIMENT_OUT_PATH=$MY_DIR/../experiments
PETRUS_BIN_PATH=$MY_DIR/../bin
SOLVER_RESULTS_PATH=/dev/null
MTG_TEST_PATH=/dev/null

# Overwrite paths it from git 
if [ -d $MY_DIR/../.git ]; then
    MTG_TEST_PATH=/home/jan/git/securitygamesmovingtargets/TestSets/
    SOLVER_RESULTS_PATH=/home/jan/research/security_games/petrus/gtres11
    EXPERIMENT_OUT_PATH=$MY_DIR/../../experiments
    PETRUS_BIN_PATH=$MY_DIR/../target/pack/bin
fi

# Overwrite default paths
if [ ! -z "$PETRUS_PATHS_FILE" ]; then
    . ${PETRUS_PATHS_FILE}
fi
