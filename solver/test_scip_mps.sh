#!/bin/bash
set -e

GAME=$1
MOVES=$2

MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $MY_DIR/paths.inc


if [ -z "$SOLVERCONF" ] ; then
    if [ -z "$SCIP" ] ; then
	if [ -e ~/.config/petrus/solver.sh ] ; then
	    . ~/.config/petrus/solver.sh
	else
	    echo 'Scip configuration is required !'; exit 1;
	fi 
    fi
else
    . "$SOLVERCONF"
fi

GTOPT_PATH=$PETRUS_BIN_PATH/petrus-gtopt

case $GAME in
     *.json) 
	 PROBPREFIX=$(basename $GAME .json)-$MOVES
	 ;;
     *.sgmt)
	 PROBPREFIX=$(basename $GAME)-$MOVES
	 ;;
esac

if [ \! -x ${PROBPREFIX}.mps.gz ]; then 
   $GTOPT_PATH --game-config $GAME --round-limit $MOVES --move-codes ${PROBPREFIX}.codes   --output-file ${PROBPREFIX}.mps.gz
else
    NODEL=1
fi


$SCIP <<EOF
read ${PROBPREFIX}.mps.gz
optimize
write solution ${PROBPREFIX}.sol
write statistics ${PROBPREFIX}.stat
EOF

if [ -z $NODEL ] ; then 
    rm ${PROBPREFIX}.mps.gz
fi 

