#!/bin/bash
set -e

GAME=$1
MOVES=$2
TRANS=$3


MY_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $MY_DIR/paths.inc

if [ $TRANS == NONE ]; then
    TRANSOPT=""
    TRANSSUFFIX=""
else
    TRANSOPT="--game-transformer ${MIXED_STRATEGY_CONFIGS_PATH}/${TRANS}.json"
    TRANSSUFFIX="tr$TRANS"
fi


if [ -z "$SOLVERCONF" ] ; then
    if [ -z "$SCIP" ] ; then
	if [ -e ~/.config/petrus/solver.sh ] ; then
	    . ~/.config/petrus/solver.sh
	else
	    echo 'Scip configuration is required !'; exit 1;
	fi 
    fi
else
    . "$SOLVERCONF"
fi

GTOPT_PATH=$PETRUS_BIN_PATH/petrus-gtopt

case $GAME in
     *.json) 
	 PROBPREFIX=$(basename $GAME .json)$TRANSSUFFIX-$MOVES
	 ;;
     *.sgmt)
	 PROBPREFIX=$(basename $GAME)$TRANSSUFFIX-$MOVES
	 ;;
     *.nfgame)
	 PROBPREFIX=$(basename $GAME)$TRANSSUFFIX-$MOVES
	 ;;
esac

JAVA_OPTS="-Xmx4g -XX:+HeapDumpOnOutOfMemoryError" $GTOPT_PATH $TRANSOPT --game-config $GAME --round-limit $MOVES --move-codes ${PROBPREFIX}.codes  --output-file ${PROBPREFIX}.zpl --zimpl

$SCIP <<EOF
read ${PROBPREFIX}.zpl
optimize
write solution ${PROBPREFIX}.sol
write statistics ${PROBPREFIX}.stat
EOF

rm ${PROBPREFIX}.zpl

