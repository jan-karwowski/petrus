package pl.edu.pw.mini.sg.mixed.tree

import org.scalatest.{ Matchers, WordSpec }
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import pl.edu.pw.mini.sg.game.util.MoveStateKey



final class MixedStrategyNodeSpec extends WordSpec with Matchers {
  private final case class DummyState(
    st: Int
  ) extends PlayerVisibleState[Int] {
    override def possibleMoves = ???
  }

  "successors in tree" when {
    "there are two successors of single move" should {
      val succ1 = new MixedStrategyNode[Int, DummyState, Unit](DummyState(1))(MixedStrategyNode.unitST)
      val succ2 = new MixedStrategyNode[Int, DummyState, Unit](DummyState(2))(MixedStrategyNode.unitST)
      val node = new MixedStrategyNode[Int, DummyState, Unit](DummyState(0))(MixedStrategyNode.unitST)
      node.addSuccessor(new MoveStateKey(1, succ1.state), succ1)
      node.addSuccessor(new MoveStateKey(1, succ2.state), succ2)

      "have two successors in tree" in {
        node.successorsInTree should contain only((1, succ1.state), (1, succ2.state))
      }
    }
  }
}
