package pl.edu.pw.mini.sg.game.boundedrationality

import org.scalacheck.Gen
import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.SimplifiedAnchoringTheory
import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.NeutralPayoffChanger


object ModifiedAnchoringPayoffCohereceCheck extends PayoffCoherenceCheck(
  Gen.const(BoundedRationality(
    NeutralPayoffChanger, SimplifiedAnchoringTheory(0.5)
  )), "modified anchoring theory"
)
