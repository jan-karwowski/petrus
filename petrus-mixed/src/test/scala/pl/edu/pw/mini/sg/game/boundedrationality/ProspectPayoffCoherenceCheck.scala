package pl.edu.pw.mini.sg.game.boundedrationality

import org.scalacheck.Gen
import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.{ NeutralPayoffChanger, ProspectTheoryPayoff }
import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.{ NeutralProbabilityChanger, ProspectTheoryProbability }


object ProspectPayoffCoherenceCheck extends PayoffCoherenceCheck(
  for {
    alpha <- Gen.choose(0.1, 0.9)
    beta <- Gen.choose(0.1, 0.9)
    omega <- Gen.choose(1.0, 3.0)
    gamma <- Gen.choose(0.1, 0.9)
  } yield BoundedRationality(
    ProspectTheoryPayoff(alpha, beta, omega),
    ProspectTheoryProbability(gamma)
  ),
  "prospect theory"
)

object Prospect1PayoffCoherenceCheck extends PayoffCoherenceCheck(
  for {
    alpha <- Gen.choose(0.1, 0.9)
    beta <- Gen.choose(0.1, 0.9)
    omega <- Gen.choose(1.0, 3.0)
  } yield BoundedRationality(
    ProspectTheoryPayoff(alpha, beta, omega),
    NeutralProbabilityChanger
  ),
  "prospect theory payoff only"
)


object Prospect2PayoffCoherenceCheck extends PayoffCoherenceCheck(
    for {
    gamma <- Gen.choose(0.1, 0.9)
  } yield BoundedRationality(
    NeutralPayoffChanger,
    ProspectTheoryProbability(gamma)
  ),
  "prospect theory prob only"
)
