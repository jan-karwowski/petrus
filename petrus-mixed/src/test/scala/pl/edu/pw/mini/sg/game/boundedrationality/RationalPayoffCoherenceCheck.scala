package pl.edu.pw.mini.sg.game.boundedrationality

import org.scalacheck.Gen
import pl.edu.pw.mini.sg.game.boundedrationality.payoffchanger.NeutralPayoffChanger
import pl.edu.pw.mini.sg.game.boundedrationality.probabilitieschanger.NeutralProbabilityChanger


object RationalPayoffCohereceCheck extends PayoffCoherenceCheck(
  Gen.const(BoundedRationality(
    NeutralPayoffChanger, NeutralProbabilityChanger
  )), "rational"
)
