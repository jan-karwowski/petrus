package pl.edu.pw.mini.sg.game.boundedrationality

import org.scalacheck.Gen
import pl.edu.pw.mini.sg.game.deterministic.single.LinearizedAdvanceableGame
import pl.edu.pw.mini.sg.game.deterministic.{ AdvanceablePlayerVisibleState, DeterministicGame, PlayerVisibleState, PureStrategy }


object PureStrategyGen {
  def genPureStrategy[DM, AM,
    DS <: PlayerVisibleState[DM],
    AS <: AdvanceablePlayerVisibleState[AM, AS],
    G <: DeterministicGame[DM, AM, DS, AS, G]
  ](game : G) : Gen[PureStrategy[AM, AS]] = {
    val lag = LinearizedAdvanceableGame.create[AM,AS](game.attackerState)

    def lagMakeMove(lag: LinearizedAdvanceableGame[AM, AS]) : Gen[LinearizedAdvanceableGame[AM, AS]] =
      if(lag.possibleMoves.isEmpty) Gen.const(lag)
      else for {
        move <- Gen.oneOf(lag.possibleMoves)
        res <- lagMakeMove(lag.makeMove(move))
      } yield res


    lagMakeMove(lag).map(_.pureStrategy)
  }
}
