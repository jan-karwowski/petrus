package pl.edu.pw.mini.sg.mixed

final case class IterationInfo(
  feasibilityIteration: Int,
  improvementIteration: Int
)

object IterationInfo {
  val dummyIterationInfo = IterationInfo(0,0)
}
