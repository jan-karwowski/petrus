package pl.edu.pw.mini.sg.mixed.stchooser

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState



trait SubtreeChooser {
  def chooseInSubtrees[M, S <: PlayerVisibleState[M]](
    desiredAttacker: InnerResultNode[M, S],
    optimalAttacker: InnerResultNode[M, S],
    strategy: MixedStrategyNode[M, S,_]
  )(
    implicit rng: RandomGenerator
  ): List[M]
  
  def chooseOutSubtrees[M, S <: PlayerVisibleState[M]](
    optimalAttacker: InnerResultNode[M, S],
    strategy: MixedStrategyNode[M, S,_]
  )(
    implicit rng: RandomGenerator
  ) : List[M]


  def choosePositivePhaseSubtrees[M, S <: PlayerVisibleState[M]](
    gameResult: InnerResultNode[M, S],
    strategy: MixedStrategyNode[M, S,_]
  )(
    implicit rng: RandomGenerator
  ) : List[M]

}
