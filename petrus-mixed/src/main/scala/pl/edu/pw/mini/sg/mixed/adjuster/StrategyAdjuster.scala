package pl.edu.pw.mini.sg.mixed.adjuster

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.tree.InnerResultNode
import pl.edu.pw.mini.sg.mixed.tree.MixedStrategyNode
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState

trait StrategyAdjuster {
  def adjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, Unit],
    desiredAttackerPayoff: InnerResultNode[DM, DS],
    optimalAttackerPayoff: InnerResultNode[DM, DS],
    rng: RandomGenerator
  )(
    implicit iterationInfo: IterationInfo
  ) : Boolean
}
