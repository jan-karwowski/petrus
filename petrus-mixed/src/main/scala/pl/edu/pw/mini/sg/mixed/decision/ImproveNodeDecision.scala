package pl.edu.pw.mini.sg.mixed.decision

import pl.edu.pw.mini.sg.game.boundedrationality.DistortedPayoff
import pl.edu.pw.mini.sg.rng.RandomGenerator



trait ImproveNodeDecision {
  def inTreeImproveDecision(optimalAttackerPayoff: DistortedPayoff, desiredAttackerPayoff: DistortedPayoff)(implicit rng: RandomGenerator) : Boolean
}
