package pl.edu.pw.mini.sg.mixed.adjuster.coorddamping

final class CoordDamping(
  // (sum (x), sum(|x|)
  val sums: Vector[(Double, Double)]
){
  def addAssesments(assesments: Seq[Double]) : CoordDamping = {
    require(sums.length <= assesments.length)
    new CoordDamping(
      sums.view.zipAll(assesments, (0.0,0.0), 0.0)
        .map{case ((sum,abssum), assesment) => (sum+assesment, sum+Math.abs(assesment))}.toVector
    )
  }

  def normalizers : Vector[Double] = sums.map{case (sum, abssum) => if(abssum>0) Math.abs(sum)/abssum else 1.0}
}
