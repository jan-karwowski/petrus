package pl.edu.pw.mini.sg.mixed.response.provider

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mixed.response.AttackerResponseFinder
import pl.edu.pw.mini.sg.mixed.response.OptimalAttackerResponse
import pl.edu.pw.mini.sg.mixed.response.HistorySkipConfig

final case class ExactResponseProvider(
  historySkipConfig: HistorySkipConfig
) extends AttackerResponseProvider {
  def attackerResponse(iterationNumber: Int)(implicit rng: RandomGenerator) : AttackerResponseFinder = OptimalAttackerResponse(historySkipConfig)
}
