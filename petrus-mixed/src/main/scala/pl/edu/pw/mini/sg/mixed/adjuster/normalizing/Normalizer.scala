package pl.edu.pw.mini.sg.mixed.adjuster.normalizing


final class Normalizer(
  val positiveSum: Vector[Double],
  val negativeSum: Vector[Double]
) {
  require(positiveSum.length == negativeSum.length)
  def registerUpdate(upd: Vector[Double]) : Normalizer = {
    require(positiveSum.length <= upd.length)
    new Normalizer(
      positiveSum.zipAll(upd, 0.0, 0.0).map{case (ps, u) => ps+Math.max(0.0, u)},
      negativeSum.zipAll(upd, 0.0, 0.0).map{case (ps, u) => ps+Math.min(0.0, u)}
    )
  }

  def normalizers : Vector[Double] = positiveSum.zip(negativeSum).map{case (pos, neg) =>
    if(pos-neg > 0) Math.abs((pos+neg)/(pos-neg)) else 1
  }
}

