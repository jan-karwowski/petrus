package pl.edu.pw.mini.sg.mixed.json

import argonaut.CodecJson
import pl.edu.pw.mini.sg.json.{ FlatTraitJsonCodec, TraitSubtypeCodec }
import pl.edu.pw.mini.sg.mixed.prune.{ IterationLimitPrune, PruneDecision, NoPrune }


object PruneDecisionJson {
  implicit val iterationLimitCodec : CodecJson[IterationLimitPrune] =
    CodecJson.casecodec1(IterationLimitPrune.apply, IterationLimitPrune.unapply)("iterations")

  implicit val pruneDecisionCodec : FlatTraitJsonCodec[PruneDecision] = new FlatTraitJsonCodec[PruneDecision](
    List(
      new TraitSubtypeCodec(iterationLimitCodec, iterationLimitCodec, "iterationLimit"),
      TraitSubtypeCodec.singletonCodec(NoPrune, "noPrune")
    )
  )
}
