package pl.edu.pw.mini.sg.mixed.adjuster.coorddamping

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType


object CoordDampingStateType extends OptimizerStateType[CoordDamping] {
  override def afterIteration(prev: CoordDamping): CoordDamping = prev
  override def zero: CoordDamping = new CoordDamping(Vector())
  override def pruneMoves(st: CoordDamping, toDelete: Set[Int]) : CoordDamping =
    new CoordDamping(OptimizerStateType.pruneVector(st.sums, toDelete))
}
