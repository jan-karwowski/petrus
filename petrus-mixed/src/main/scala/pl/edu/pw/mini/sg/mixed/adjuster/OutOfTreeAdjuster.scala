package pl.edu.pw.mini.sg.mixed.adjuster

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState



trait OutOfTreeAdjuster {
  // Czy chcę tu jakąś docelową wartość?
  // -- Raczej nie. Po prostu chcę minimalizować
  def adjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, Unit],
    optimalAttackerPayoff: InnerResultNode[DM, DS],
    rng: RandomGenerator
  )(
    implicit iterationInfo: IterationInfo
  ) : Boolean

  def apply[M, S <: PlayerVisibleState[M]](
    mixedStrategy: MixedStrategyNode[M, S, Unit],
    optimalAttackerPayoff: InnerResultNode[M, S],
    rng: RandomGenerator
  )(
    implicit iterationInfo: IterationInfo
  ) : Boolean = adjust(mixedStrategy, optimalAttackerPayoff, rng)
}
