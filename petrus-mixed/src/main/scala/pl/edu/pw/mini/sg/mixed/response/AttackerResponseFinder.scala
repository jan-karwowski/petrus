package pl.edu.pw.mini.sg.mixed.response

import pl.edu.pw.mini.sg.game.boundedrationality.BoundedRationality
import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.{AdvanceablePlayerVisibleState, DeterministicGame, PlayerVisibleState, PureStrategy}
import pl.edu.pw.mini.sg.mixed.StableAttackerFinderState
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}


trait AttackerResponseFinder {
  val isExact : Boolean

  def findResponse[
    DM, AM,
    DS <: PlayerVisibleState[DM],
    AS <: AdvanceablePlayerVisibleState[AM, AS],
    G <: DeterministicGame[DM, AM, DS, AS, G]
  ](
    gameRoot: G, defenderStrategy: MixedStrategyNode[DM, DS, _],
    desiredAttackerPayoff: InnerResultNode[DM, DS],
    state: StableAttackerFinderState[AM, AS]
  )(
    implicit rng: RandomGenerator,
    boundedRationality: BoundedRationality
  ): (PureStrategy[AM, AS], StableAttackerFinderState[AM, AS])
}
