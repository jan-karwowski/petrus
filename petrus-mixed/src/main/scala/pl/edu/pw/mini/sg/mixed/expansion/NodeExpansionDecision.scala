package pl.edu.pw.mini.sg.mixed.expansion

import pl.edu.pw.mini.sg.game.boundedrationality.BoundedRationality
import pl.edu.pw.mini.sg.game.deterministic.DeterministicGame
import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.{PlayerVisibleState, TreeExpander}
import pl.edu.pw.mini.sg.game.deterministic.single.{GameWithPayoff, SinglePlayerGame}
import pl.edu.pw.mini.sg.mixed.{BothTreesPosition, IterationInfo}
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}



trait NodeExpansionDecision {
  val positiveExpander : TreeExpander[Any, Any, GameWithPayoff]
  val negativeExpander : TreeExpander[Any, Any, GameWithPayoff]
  val initialExpander : TreeExpander[Any, Any, GameWithPayoff]

  def inTreeExpansion[DM, AM, DS <: PlayerVisibleState[DM], AS <: PlayerVisibleState[AM],
  G <: DeterministicGame[DM, AM, DS, AS, G]](
    bothTreesPosition: BothTreesPosition[DM, AM, DS, AS, G],
    defenderMixedStrategy: MixedStrategyNode[DM, DS, _],
    desiredAttackerPayoff: InnerResultNode[DM, DS],
    undesiredAttackerPayoff: InnerResultNode[DM, DS],
    noSubtreeExpanded: Boolean
  )(
    implicit rng: RandomGenerator,
    iterationInfo: IterationInfo,
    br: BoundedRationality
  ): Boolean

  def outTreeExpansion[DM, DS <: PlayerVisibleState[DM], G <: SinglePlayerGame[DM, DS, G] with GameWithPayoff](
    gameState: G,
    defenderMixedStrategy: MixedStrategyNode[DM, DS, _],
    undesiredAttackerPayoff: InnerResultNode[DM, DS],
    noSubtreeExpanded: Boolean
  )(
    implicit rng: RandomGenerator,
    iterationInfo: IterationInfo,
    br: BoundedRationality
  ) : Boolean

  def positivePassExpansion[DM, DS <: PlayerVisibleState[DM], G <: SinglePlayerGame[DM, DS, G]  with GameWithPayoff](
    gameState: G,
    defenderMixedStrategy: MixedStrategyNode[DM, DS, _],
    desiredAttackerPayoff: InnerResultNode[DM, DS],
    noSubtreeExpanded: Boolean
  )(
    implicit rng: RandomGenerator,
    iterationInfo: IterationInfo
  ) : Boolean

}
