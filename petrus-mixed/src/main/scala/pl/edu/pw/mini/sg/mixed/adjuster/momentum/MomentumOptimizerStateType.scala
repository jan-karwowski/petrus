package pl.edu.pw.mini.sg.mixed.adjuster.momentum

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType



final object MomentumOptimizerStateType extends OptimizerStateType[Momentum] {
  override def afterIteration(prev: Momentum): Momentum = prev
  override def zero: Momentum = new Momentum(Vector(), 0)
  override def pruneMoves(st: Momentum, toDelete: Set[Int]) : Momentum = 
    new Momentum(OptimizerStateType.pruneVector(st.momentum, toDelete), st.normalizer)
}
