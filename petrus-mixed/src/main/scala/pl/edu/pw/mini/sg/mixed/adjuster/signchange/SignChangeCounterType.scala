package pl.edu.pw.mini.sg.mixed.adjuster.signchange

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType

final object SignChangeCounterType extends OptimizerStateType[SignChangeCounter] {
  def afterIteration(prev: SignChangeCounter): SignChangeCounter = prev
  def zero: SignChangeCounter = new SignChangeCounter(Vector(0), Vector(0))
  def pruneMoves(st: SignChangeCounter, toDelete: Set[Int]) : SignChangeCounter =
    new SignChangeCounter(OptimizerStateType.pruneVector(st.changeCount, toDelete), OptimizerStateType.pruneVector(st.lastAssesments, toDelete))
}
