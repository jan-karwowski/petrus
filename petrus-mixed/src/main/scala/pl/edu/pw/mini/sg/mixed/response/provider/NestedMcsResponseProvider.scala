package pl.edu.pw.mini.sg.mixed.response.provider

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mcts.NestedMcsConfig
import pl.edu.pw.mini.sg.mixed.response.{AttackerResponseFinder, NestedMcsAttackerResponse, HistorySkipConfig}


final case class NestedMcsResponseProvider(
  config: NestedMcsConfig, defenderEps: Double,
  historySkipConfig: HistorySkipConfig
) extends AttackerResponseProvider{
  def attackerResponse(iterationNumber: Int)(implicit rng: RandomGenerator) : AttackerResponseFinder =
    NestedMcsAttackerResponse(config, defenderEps, historySkipConfig)
}
