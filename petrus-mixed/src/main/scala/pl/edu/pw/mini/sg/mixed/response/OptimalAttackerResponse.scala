package pl.edu.pw.mini.sg.mixed.response

import pl.edu.pw.mini.sg.game.boundedrationality.BoundedRationality
import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.{AdvanceablePlayerVisibleState, DeterministicGame, PlayerVisibleState, PureStrategy}
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}
import pl.edu.pw.mini.sg.game.util.attacker.OptimalAttacker


final case class OptimalAttackerResponse(historySkipConfig : HistorySkipConfig
) extends AttackerResponseWithHistory {
  def calculateAttacker[
    DM, AM, DS <: PlayerVisibleState[DM],
    AS <: AdvanceablePlayerVisibleState[AM,AS],
    G <: DeterministicGame[DM,AM,DS,AS,G]
  ](
    gameRoot: G,
    defenderStrategy: MixedStrategyNode[DM, DS, _],
    desiredAttackerPayoff: InnerResultNode[DM,DS],
  )(
    implicit rng: RandomGenerator,
    boundedRationality: BoundedRationality
  ) : PureStrategy[AM,AS] = OptimalAttacker.calculate[DM, AM, DS, AS, G](gameRoot, defenderStrategy)._1
  val isExact: Boolean = historySkipConfig.startUsingHistoryAfter == -1
}
