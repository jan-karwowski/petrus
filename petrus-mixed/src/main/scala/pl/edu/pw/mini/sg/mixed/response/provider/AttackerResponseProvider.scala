package pl.edu.pw.mini.sg.mixed.response.provider

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mixed.response.AttackerResponseFinder



trait AttackerResponseProvider {
  def attackerResponse(iterationNumber: Int)(implicit rng: RandomGenerator) : AttackerResponseFinder
}
