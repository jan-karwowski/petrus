package pl.edu.pw.mini.sg.mixed

import pl.edu.pw.mini.sg.mixed.tree.{ GameResultNode }


object Assessment {
  def positiveAssessment(resultNode: GameResultNode[_,_]) : Double = resultNode.payoff.real.defender
  def inTreeAssessment(desiredNode: GameResultNode[_,_], optimalNode: GameResultNode[_,_]) : Double =
    desiredNode.payoff.distortedAttacker.attacker - optimalNode.payoff.distortedAttacker.attacker
  def outOfTreeAssessment(optimalNode: GameResultNode[_,_]) : Double = -optimalNode.payoff.distortedAttacker.attacker
}
