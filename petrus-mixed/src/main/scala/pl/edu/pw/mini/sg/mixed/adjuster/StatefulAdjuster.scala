package pl.edu.pw.mini.sg.mixed.adjuster

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.tree.{ InnerResultNode, MixedStrategyNode, OptimizerStateType }


trait StatefulAdjuster {
  type AdjusterState

  implicit val ost: OptimizerStateType[AdjusterState]

  /**
    *  @return Czy była zmiana
    */
  def inTreeAdjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, AdjusterState],
    desiredAttackerPayoff: InnerResultNode[DM, DS],
    optimalAttackerPayoff: InnerResultNode[DM, DS]
  )(
    implicit rng: RandomGenerator,
    iterationInfo: IterationInfo
  ) : Boolean

   /**
    *  @return Czy była zmiana
    */
 def outOfTreeAdjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, AdjusterState],
    optimalAttackerPayoff: InnerResultNode[DM, DS]
  )(
   implicit rng: RandomGenerator,
    iterationInfo: IterationInfo
  ) : Boolean


   /**
    *  @return Czy była zmiana
    */
 def positiveAdjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, AdjusterState],
    gamePayoff: InnerResultNode[DM, DS]
  )(
    implicit rng: RandomGenerator, iterationInfo: IterationInfo
  ) : Boolean


}
