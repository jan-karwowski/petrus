package pl.edu.pw.mini.sg.mixed.adjuster.normalizing

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType

final object NormalizerOptimizerStateType extends OptimizerStateType[Normalizer] {
  override def zero: Normalizer = new Normalizer(Vector(), Vector())
  override def afterIteration(prev: Normalizer) : Normalizer = prev
  override def pruneMoves(st: Normalizer, toDelete: Set[Int]) : Normalizer =
    new Normalizer(OptimizerStateType.pruneVector(st.positiveSum, toDelete), OptimizerStateType.pruneVector(st.negativeSum, toDelete))
}
