package pl.edu.pw.mini.sg.mixed

import pl.edu.pw.mini.sg.game.deterministic.single.LinearizedGameProvider
import pl.edu.pw.mini.sg.mcts.NestedMcsConfig

final case class NestedMcsAttackerSamplerConfig(
  nestedMcsConfig: NestedMcsConfig,
  infeasiblePenalty: Double,
  mixedConfig: MixedMethodConfig,
  linearizedGameProvider: LinearizedGameProvider
)

