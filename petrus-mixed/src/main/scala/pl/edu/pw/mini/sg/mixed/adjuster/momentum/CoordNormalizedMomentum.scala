package pl.edu.pw.mini.sg.mixed.adjuster.momentum


final class CoordNormalizedMomentum(val momentum: Vector[Double], val normalizer: Vector[Double]) extends Immutable {
  require(momentum.length == normalizer.length)

  def normalized = momentum.zip(normalizer).map{case (m,n) => m/n}
  def extend(length: Int) : CoordNormalizedMomentum = {
    require(momentum.length <= length)
    if(momentum.length == length) this
    else new CoordNormalizedMomentum(momentum ++ Vector.fill(length - momentum.length)(0.0), normalizer ++ Vector.fill(length - momentum.length)(0.0))
  }

  def accumulateAssessments(assessments: Vector[Double]) : CoordNormalizedMomentum = {
    require(assessments.length >= momentum.length)

    val newMomentum = assessments.zipAll(momentum, 0.0, 0.0).map{case (a, b) => a+b}
    new CoordNormalizedMomentum(newMomentum, normalizer.zipAll(assessments, 0.0, 0.0).map{case (a,b) => a+Math.abs(b)})
  }
}
