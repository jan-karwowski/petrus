package pl.edu.pw.mini.sg.mixed.adjuster.bisect

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType


final class BisectAdjusterStateType[S, ST <: OptimizerStateType[S]](
  fallbackAdjusterOst: ST
) extends OptimizerStateType[BisectAdjusterState[S]] {
  def afterIteration(prev: BisectAdjusterState[S]): BisectAdjusterState[S] = 
    new BisectAdjusterState[S](
      prev.lastGoodPosition.map(fallbackAdjusterOst.afterIteration),
      prev.infeasibleIterations
    )
  def zero: BisectAdjusterState[S] = new BisectAdjusterState(Right(fallbackAdjusterOst.zero), 0)
  def pruneMoves(st: BisectAdjusterState[S], indices: Set[Int]) : BisectAdjusterState[S] =
    new BisectAdjusterState[S](
      st.lastGoodPosition.left.map(v => v.zipWithIndex.collect{case (v, idx) if !indices.contains(idx) => v})
        .right.map(fallbackAdjusterOst.pruneMoves(_, indices)),
      st.infeasibleIterations
    )
}
