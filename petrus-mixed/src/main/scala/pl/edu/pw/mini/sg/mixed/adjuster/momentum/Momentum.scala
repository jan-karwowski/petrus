package pl.edu.pw.mini.sg.mixed.adjuster.momentum


final class Momentum(val momentum: Vector[Double], val normalizer: Double) extends Immutable {
  def extend(length: Int) : Momentum = {
    require(normalizer >= 0, "Normalizer must be non negative")
    require(momentum.length <= length)
    if(momentum.length == length) this
    else new Momentum(momentum ++ Vector.fill(length - momentum.length)(0.0), normalizer)
  }

  def accumulateAssessments(assessments: Vector[Double]) : Momentum = {
    require(assessments.length >= momentum.length)

    val l1norm = assessments.map(Math.abs).sum
    val newMomentum = assessments.zipAll(momentum, 0.0, 0.0).map{case (a, b) => a+b}
    new Momentum(newMomentum, normalizer+l1norm)
  }
}
