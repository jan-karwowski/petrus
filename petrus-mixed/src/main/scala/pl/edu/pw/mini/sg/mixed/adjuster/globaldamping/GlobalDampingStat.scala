package pl.edu.pw.mini.sg.mixed.adjuster.globaldamping

final class GlobalDampingStat(
  val sum: Double,
  val abssum: Double
) {
  def addAssesments(assesments: Seq[Double]) : GlobalDampingStat = new GlobalDampingStat(sum + assesments.sum, abssum + assesments.map(Math.abs).sum)
  val damping: Double = if(abssum==0) 1.0 else ((Math.abs(sum)/abssum))
}
