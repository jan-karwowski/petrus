package pl.edu.pw.mini.sg.mixed.adjuster.decreasing

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType


final class DecreasingDoubleStateType(decreaseRate: Double) extends OptimizerStateType[Double] {
  override def afterIteration(prev: Double): Double = prev*decreaseRate
  override def zero: Double = 1.0
  override def pruneMoves(st: Double, toDelete: Set[Int]) = st
}
