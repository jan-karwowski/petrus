package pl.edu.pw.mini.sg.mixed.prune

import pl.edu.pw.mini.sg.mixed.IterationInfo


trait PruneDecision {
  def decide(current: IterationInfo, lastNonZero: IterationInfo) : Boolean
}
