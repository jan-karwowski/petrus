package pl.edu.pw.mini.sg.mixed

import argonaut.CodecJson



object NestedMcsAttackerSamplerConfigJson {
  import pl.edu.pw.mini.sg.mcts.json.NestedConfigJson.nestedMcsConfigCodec

  implicit val x =  json.MixedMethodConfigJson.defaultMixedMethodConfigCodec
  import json.MixedMethodConfigJson.linearizedGameProviderDecode
  import json.MixedMethodConfigJson.linearizedGameProviderEncode

  implicit val nestedMcsAttackerSamplerConfigCodec : CodecJson[NestedMcsAttackerSamplerConfig] =
    CodecJson.casecodec4(
      NestedMcsAttackerSamplerConfig.apply,
      NestedMcsAttackerSamplerConfig.unapply
    )(
      "nestedMcsConfig", "infeasiblePenalty", "mixedConfig", "linearizedGame"
    )
}
