package pl.edu.pw.mini.sg.mixed.expansion

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.game.deterministic.TreeExpander
import pl.edu.pw.mini.sg.game.deterministic.single.SinglePlayerGame


object DummyExpander extends TreeExpander[Any, Any, Any] {
  type SubtreeData[MO,ST] = Unit
  def zeroSubtreeData[M, S]: Unit = ()
  def expand[M, S, GG <: SinglePlayerGame[M,S,GG]](
    state: GG, subtreeData: Unit
  )(implicit rng: RandomGenerator) : (M, Unit) = ???
}
