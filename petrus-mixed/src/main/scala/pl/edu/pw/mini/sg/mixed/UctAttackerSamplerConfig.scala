package pl.edu.pw.mini.sg.mixed

import pl.edu.pw.mini.sg.game.deterministic.single.LinearizedGameProvider
import pl.edu.pw.mini.sg.mcts.MctsParameters


final case class UctAttackerSamplerConfig(
  val mixedMethodConfig: MixedMethodConfig,
  infeasiblePenalty: Double,
  intfeasibleAttackerPropagation: InfeasibleAttackerPropagation,
  mctsConfig: MctsParameters,
  descent: Boolean,
  linearizedGameProvider: LinearizedGameProvider
)

