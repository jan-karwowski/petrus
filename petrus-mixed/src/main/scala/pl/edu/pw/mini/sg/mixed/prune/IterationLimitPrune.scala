package pl.edu.pw.mini.sg.mixed.prune

import pl.edu.pw.mini.sg.mixed.IterationInfo

final case class IterationLimitPrune(iterations: Int) extends PruneDecision {
  def decide(current: IterationInfo, lastNonZero: IterationInfo) : Boolean =
    iterations < (current.improvementIteration - lastNonZero.improvementIteration)
}
