package pl.edu.pw.mini.sg.mixed.adjuster.bisectrotate

import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.adjuster.AssesmentBasedAdjuster
import pl.edu.pw.mini.sg.mixed.tree.{InnerResultNode, MixedStrategyNode}

final class BisectRotateAdjuster(
  fallbackAdjuster: AssesmentBasedAdjuster,
  edgeDist: Double
) extends BisectRotateAdjusterBase(fallbackAdjuster, edgeDist) {

  def normalizationAfterRotate[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM,DS, AdjusterState],
    desiredAttackerPayoff: InnerResultNode[DM,DS],
    optimalAttackerPayoff: InnerResultNode[DM,DS]
  )(
    implicit iterationInfo: IterationInfo
  ) : Unit = {
    logger.debug(s"not normalized sum ${mixedStrategy.probabilities.map(_._2).sum}")
    mixedStrategy.normalizeOrEqual()
  }
}
