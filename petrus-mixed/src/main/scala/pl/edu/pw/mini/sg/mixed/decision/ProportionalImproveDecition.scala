package pl.edu.pw.mini.sg.mixed.decision

import pl.edu.pw.mini.sg.game.boundedrationality.DistortedPayoff
import pl.edu.pw.mini.sg.rng.RandomGenerator


final case class ProportionalImproveDecision(
  lowerLimit: Double,
  upperLimit: Double 
) extends ImproveNodeDecision {
  override def inTreeImproveDecision(optimalAttackerPayoff: DistortedPayoff, desiredAttackerPayoff: DistortedPayoff
  )(implicit rng: RandomGenerator) : Boolean = {
    rng.nextDouble() > (Math.min(upperLimit,Math.max(lowerLimit,(optimalAttackerPayoff.distortedAttacker.attacker-desiredAttackerPayoff.distortedAttacker.attacker)))-lowerLimit)/(upperLimit-lowerLimit)
  }
}
