package pl.edu.pw.mini.sg.mixed.adjuster.damping

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType


final class DampingState(
  val currentRate: Double,
  val iterationStartRate: Double
) {
  def dampCurrent(coeff: Double) : DampingState = new DampingState(currentRate*coeff, iterationStartRate)
}

final class DampingStateTypeclass(
  initialRate: Double,
  dampingCoefficient: Double
) extends OptimizerStateType[DampingState] {
  override val zero = new DampingState(initialRate, initialRate)
  override def afterIteration(prev: DampingState) = {
    val newRate = prev.iterationStartRate * dampingCoefficient
    new DampingState(newRate, newRate)
  }
  override def pruneMoves(st: DampingState, toDelete: Set[Int]) = st
}
