package pl.edu.pw.mini.sg.mixed.adjuster.momentum

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType



final object CoordNormalizedMomentumOptimizerStateType extends OptimizerStateType[CoordNormalizedMomentum] {
  override def afterIteration(prev: CoordNormalizedMomentum): CoordNormalizedMomentum = prev
  override def zero: CoordNormalizedMomentum = new CoordNormalizedMomentum(Vector(), Vector())
  override def pruneMoves(st : CoordNormalizedMomentum, toDelete: Set[Int]) : CoordNormalizedMomentum =
    new CoordNormalizedMomentum(OptimizerStateType.pruneVector(st.momentum, toDelete), OptimizerStateType.pruneVector(st.normalizer, toDelete))
}
