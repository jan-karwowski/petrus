package pl.edu.pw.mini.sg.mixed.adjuster.decreasing

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.log4s._
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.adjuster.AssesmentBasedAdjuster
import pl.edu.pw.mini.sg.mixed.tree.{ MixedStrategyNode, OptimizerStateType }


final class DecreasingRateAdjuster(val decreaseRate: Double)
    extends AssesmentBasedAdjuster {
  private val logger = getLogger
  override type AdjusterState = Double

  override def updateState(
    oldRate: Double,
    assesments: Seq[Double]
  )(
    implicit rng: RandomGenerator
  ): Double = oldRate

  override def updateStrategy(
    mixedStrategyNode: MixedStrategyNode[_, _, _],
    rate: Double,
    assesments: Seq[Double]
  )(
    implicit rng: RandomGenerator,
    iterationInfo: IterationInfo
  ): Boolean = {
    logger.debug(s"Current rate ${rate}")
    mixedStrategyNode.updateBy(assesments.map(_*rate))
    true
  }

  override val ost: OptimizerStateType[Double] = new DecreasingDoubleStateType(decreaseRate)
}
