package pl.edu.pw.mini.sg.util.stop

trait StopCriterion {
  def iterationResult(result: Double) : StopCriterion
  def iterationResultS(result: Double)  : (StopCriterion, Boolean) = {
    val r = iterationResult(result)
    (r, r.stop)
  }
  val stop: Boolean
}
