package pl.edu.pw.mini.sg.mixed.adjuster

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.log4s._
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.tree.{ InnerResultNode, MixedStrategyNode }



final case class NaivePositiveAdjuster(rate: Double) extends PositivePhaseAdjuster {
  private val logger = getLogger

  override def adjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM,DS, Unit],
    gamePayoff: InnerResultNode[DM,DS],
    rng: RandomGenerator
  )(
    implicit iterationInfo: IterationInfo
  ): Boolean = {
    if(true) {
      def movePayoff(m: DM) = {
        val ret = gamePayoff.successors(m).payoff.real.defender
        require(ret >= 0)
        ret
      }
      val currPayoff = gamePayoff.payoff.real.defender
      val newWeights = mixedStrategy.probabilities
        .map{case (move, prob) => {
          val x = if (movePayoff(move) - currPayoff > 0)
            rate+(1+rate)*prob
          else
            0
          prob + (x)
        }}
      mixedStrategy.updateProbs(newWeights)
      mixedStrategy.normalizeOrEqual()
      logger.trace(s"Positive adjust result: ${mixedStrategy.probabilities} ${mixedStrategy.probabilities.map{case (m, _) => movePayoff(m)}}")
      true
    } else {
      false
    }
  }
}
