package pl.edu.pw.mini.sg.mixed.adjuster.bisectrotate

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType


final class BisectAdjusterWithOldUpdateStateType[S, ST <: OptimizerStateType[S]](
  fallbackAdjusterOst: ST
) extends OptimizerStateType[BisectAdjusterWithOldUpdateState[S]] {
  def afterIteration(prev: BisectAdjusterWithOldUpdateState[S]): BisectAdjusterWithOldUpdateState[S] = 
    prev match {
      case fallback : BisectFallbackState[S] => new BisectFallbackState(fallbackAdjusterOst.afterIteration(fallback.fallback))
      case found : BisectAlreadyFoundState => found
    }
  def zero: BisectFallbackState[S] = new BisectFallbackState(fallbackAdjusterOst.zero)

  def pruneMoves(st: BisectAdjusterWithOldUpdateState[S], toDelete: Set[Int]) : BisectAdjusterWithOldUpdateState[S] = {
    st match {
      case fallback : BisectFallbackState[S] => ???
      case found : BisectAlreadyFoundState => new BisectAlreadyFoundState(found.lastGoodPosition.zipWithIndex.collect{case (v, idx) if !toDelete.contains(idx) =>  v},
        found.initialNewPosition.map(_.zipWithIndex.collect{case (v, idx) if !toDelete.contains(idx) => v}))
    }
  }

}
