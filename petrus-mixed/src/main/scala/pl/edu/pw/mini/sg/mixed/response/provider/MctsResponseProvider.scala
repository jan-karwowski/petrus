package pl.edu.pw.mini.sg.mixed.response.provider

import pl.edu.pw.mini.sg.rng.RandomGenerator
import pl.edu.pw.mini.sg.mcts.MctsParameters
import pl.edu.pw.mini.sg.mixed.response.{ AttackerResponseFinder, MctsAttackerResponse, HistorySkipConfig }


final case class MctsResponseProvider(
  config: MctsParameters, defenderEps: Double,
  historySkipConfig: HistorySkipConfig
) extends AttackerResponseProvider{
  def attackerResponse(iterationNumber: Int)(implicit rng: RandomGenerator) : AttackerResponseFinder =
    MctsAttackerResponse(config, defenderEps, historySkipConfig)
}
