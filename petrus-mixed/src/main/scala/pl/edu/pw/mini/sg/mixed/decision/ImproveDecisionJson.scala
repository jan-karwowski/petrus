package pl.edu.pw.mini.sg.mixed.decision

import argonaut.{ CodecJson, DecodeJson, DecodeResult, EncodeJson }
import pl.edu.pw.mini.sg.json.{ FlatTraitJsonCodec, TraitSubtypeCodec }


object ImproveDecisionJson {
  implicit val proportionalImproveDecisionCodec : CodecJson[ProportionalImproveDecision] =
    CodecJson.casecodec2(ProportionalImproveDecision.apply, ProportionalImproveDecision.unapply)("lowerLimit", "upperLimit")

  implicit val alwaysUpdateEncode : EncodeJson[AlwaysImproveDecision.type] = EncodeJson.UnitEncodeJson.contramap(x => ())
  implicit val alwaysUpdateDecode : DecodeJson[AlwaysImproveDecision.type] = DecodeJson(x => DecodeResult.ok(AlwaysImproveDecision))

  implicit val improveDecisionCodec : FlatTraitJsonCodec[ImproveNodeDecision] = new FlatTraitJsonCodec[ImproveNodeDecision](List(
    new TraitSubtypeCodec(proportionalImproveDecisionCodec, proportionalImproveDecisionCodec, "proportional"),
    new TraitSubtypeCodec(alwaysUpdateEncode, alwaysUpdateDecode, "always")
  ))
}
