package pl.edu.pw.mini.sg.mixed.expander

import pl.edu.pw.mini.sg.game.deterministic.single.GameWithPayoff


sealed trait GWPEvaluation {
  def apply(gwp: GameWithPayoff) : Double
}


object PositiveAttacker extends GWPEvaluation {
  def apply(gwp: GameWithPayoff) : Double = gwp.attackerPayoff
}

object PositiveDefender extends GWPEvaluation {
  def apply(gwp: GameWithPayoff) : Double = gwp.defenderPayoff
}

object NegativeAttacker extends GWPEvaluation {
  def apply(gwp: GameWithPayoff) : Double = 1 - gwp.attackerPayoff
}

object NegativeDefender extends GWPEvaluation {
  def apply(gwp: GameWithPayoff) : Double = 1 - gwp.defenderPayoff
}


