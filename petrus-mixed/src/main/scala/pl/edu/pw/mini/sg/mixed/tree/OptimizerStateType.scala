package pl.edu.pw.mini.sg.mixed.tree


trait OptimizerStateType[T] {
  def zero: T
  def afterIteration(prev: T) : T
  def pruneMoves(t: T, indices: Set[Int]) : T
}

object OptimizerStateType {
  def pruneVector[T](vector: Vector[T], indices: Set[Int]) : Vector[T] =
    vector.zipWithIndex.collect{case (v, idx) if !indices.contains(idx) => v}
}
