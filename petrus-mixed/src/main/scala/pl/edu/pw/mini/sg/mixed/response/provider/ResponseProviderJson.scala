package pl.edu.pw.mini.sg.mixed.response.provider

import argonaut.CodecJson
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}
import pl.edu.pw.mini.sg.mcts.json.MctsParametersJson
import pl.edu.pw.mini.sg.mixed.response.HistorySkipConfig

object ResponseProviderJson {
  import MctsParametersJson._
  import pl.edu.pw.mini.sg.mcts.json.NestedConfigJson.nestedMcsConfigCodec

  implicit val historySkipConfigCodec = CodecJson.casecodec2(
    HistorySkipConfig.apply, HistorySkipConfig.unapply
  )("startUsingAfter","useInterval")

  val mctsResponseCodec = CodecJson.casecodec3(
    MctsResponseProvider.apply, MctsResponseProvider.unapply
  )("mctsConfig", "defenderEps", "historySkipConfig")

  val nestedMcsResponseCodec = CodecJson.casecodec3(NestedMcsResponseProvider.apply, NestedMcsResponseProvider.unapply)("nestedMcs", "defenderEps", "historySkipConfig")

  val exactResponseCodec = CodecJson.casecodec1(ExactResponseProvider.apply, ExactResponseProvider.unapply)(
    "historySkipConfig"
  )

  implicit val attackerResponseProviderCodec = new FlatTraitJsonCodec[AttackerResponseProvider](List(
    TraitSubtypeCodec(exactResponseCodec, exactResponseCodec, "exactResponse"),
    TraitSubtypeCodec(mctsResponseCodec, mctsResponseCodec, "mctsResponse"),
    TraitSubtypeCodec(nestedMcsResponseCodec, nestedMcsResponseCodec, "nestedMcsResponse")
  ))
}
