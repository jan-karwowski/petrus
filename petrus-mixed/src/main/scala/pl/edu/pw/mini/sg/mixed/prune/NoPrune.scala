package pl.edu.pw.mini.sg.mixed.prune

import pl.edu.pw.mini.sg.mixed.IterationInfo


object NoPrune extends PruneDecision {
  def decide(current: IterationInfo, lastNonZero: IterationInfo) : Boolean = false
}
