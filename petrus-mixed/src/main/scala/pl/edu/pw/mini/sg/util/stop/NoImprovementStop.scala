package pl.edu.pw.mini.sg.util.stop

final case class NoImprovementStop(
  remainingIterations: Int,
  noImprovementIterations: Int, noImprovementEps: Double,
  lastImprovement: Int=0, bestValue: Option[Double]= None
) extends StopCriterion {
  override def iterationResult(result: Double) : StopCriterion = {
    if(bestValue.map(_ >= result - noImprovementEps).getOrElse(false))
      copy(lastImprovement = lastImprovement+1, remainingIterations = remainingIterations-1)
    else
      copy(lastImprovement = 0, bestValue = Some(result), remainingIterations = remainingIterations-1)
  }
  override val stop: Boolean = remainingIterations <= 0 ||  lastImprovement >= noImprovementIterations
}

