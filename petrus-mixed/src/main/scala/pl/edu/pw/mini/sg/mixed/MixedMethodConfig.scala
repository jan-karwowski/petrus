package pl.edu.pw.mini.sg.mixed

import pl.edu.pw.mini.sg.mixed.adjuster.StatefulAdjuster
import pl.edu.pw.mini.sg.mixed.decision.ImproveNodeDecision
import pl.edu.pw.mini.sg.mixed.expansion.NodeExpansionDecision
import pl.edu.pw.mini.sg.mixed.prune.PruneDecision
import pl.edu.pw.mini.sg.mixed.response.provider.AttackerResponseProvider
import pl.edu.pw.mini.sg.mixed.stchooser.SubtreeChooser
import pl.edu.pw.mini.sg.game.boundedrationality.BoundedRationality



// Jak ze schodzeniem w dół?
// Potrzebuję dwóch adjusterów: in-optimal-tree i out-optimal-tree
// Jak będę aktualizował out-of-tree?
// Więcej: Jak mogę rozszerzyć węzeł out of tree???
// W węzłach out-of-tree muszę mieć jakąś wartość wypłaty, do której dążę
// Jaką wybrać? Czy potrzebuję tu kolejny selektor?/

final case class MixedMethodConfig(
  allRefinementIterations: Int,
  feasibilityTryIterations: Int,
  expansionDecision: NodeExpansionDecision,
  strategyAdjuster: StatefulAdjuster,
  subtreeChooser: SubtreeChooser,
  poorImprovementIterations: Int,
  poorImprovementEps: Double,
  improveDecision: ImproveNodeDecision,
  attackerOracleProvider: AttackerResponseProvider,
  treePruneDecision: PruneDecision,
  boundedRationalityStrategy: BoundedRationality
) {
}
