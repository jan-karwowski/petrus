package pl.edu.pw.mini.sg.mixed.response

final case class HistorySkipConfig(
  startUsingHistoryAfter: Int,
  historyUseInterval: Int
)

object HistorySkipConfig {
  val noSkip = HistorySkipConfig(-1, -1)
}
