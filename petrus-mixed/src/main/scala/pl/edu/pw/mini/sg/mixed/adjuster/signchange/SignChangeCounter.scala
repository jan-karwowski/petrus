package pl.edu.pw.mini.sg.mixed.adjuster.signchange

final class SignChangeCounter(
  val changeCount: Vector[Int],
  val lastAssesments: Vector[Double]
) {
  def registerAssesments(asm: Seq[Double]) : SignChangeCounter = {
    require(asm.length >= changeCount.length)
    
    new SignChangeCounter(
      changeCount = changeCount.view.zip(lastAssesments).zipAll(asm, (0,0.0), 0.0)
        .map{case ((ch, old), ne) => if(old*ne < 0) ch+1 else ch}.toVector,
      lastAssesments = asm.toVector
    )
  }

}
