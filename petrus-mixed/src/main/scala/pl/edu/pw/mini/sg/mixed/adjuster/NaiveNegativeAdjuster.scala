package pl.edu.pw.mini.sg.mixed.adjuster

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.log4s._
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.tree.{ InnerResultNode, MixedStrategyNode }

final case class NaiveNegativeAdjuster(
  rate: Double
) extends OutOfTreeAdjuster {
  require(rate > 0)

  private val logger = getLogger

  override def adjust[DM, DS <: PlayerVisibleState[DM]](
    mixedStrategy: MixedStrategyNode[DM, DS, Unit],
    optimalAttackerPayoff: InnerResultNode[DM, DS],
    rng: RandomGenerator
  )(
    implicit iterationInfo: IterationInfo
  ) : Boolean = {
    logger.trace("Negative adjuster")
    def movePayoff(m: DM) = optimalAttackerPayoff.successors(m).payoff.distortedAttacker.attacker
    val myPayoff = optimalAttackerPayoff.payoff.distortedAttacker.attacker
    val newWeights: Seq[Double] = mixedStrategy.probabilities
      .map{case (move, prob) => 
        if(myPayoff > movePayoff(move))
          prob+rate
        else
          prob
      }
    mixedStrategy.updateProbs(newWeights)
    mixedStrategy.normalize()
    true 
  }

}
