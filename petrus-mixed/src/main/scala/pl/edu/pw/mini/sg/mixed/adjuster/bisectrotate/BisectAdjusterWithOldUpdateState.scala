package pl.edu.pw.mini.sg.mixed.adjuster.bisectrotate

sealed trait BisectAdjusterWithOldUpdateState[+FallbackState] {
  def registerFeasible(prob: Vector[Double]) : BisectAdjusterWithOldUpdateState[FallbackState]
}

final class BisectFallbackState[+FallbackState](
  val fallback : FallbackState
) extends BisectAdjusterWithOldUpdateState[FallbackState] {
  def registerFeasible(prob: Vector[Double]) : BisectAlreadyFoundState =
    new BisectAlreadyFoundState(prob, None)
  def registerInfeasible(prob: => Vector[Double]) : BisectFallbackState[FallbackState] = this
}

final class BisectAlreadyFoundState(
  val lastGoodPosition : Vector[Double],
  val initialNewPosition : Option[Vector[Double]]
) extends BisectAdjusterWithOldUpdateState[Nothing] {
  def registerFeasible(prob: Vector[Double]) : BisectAlreadyFoundState =
    new BisectAlreadyFoundState(prob, None)
  def registerInfeasible(prob: => Vector[Double]) : BisectAlreadyFoundState =
    initialNewPosition match {
      case Some(_) => this
      case None => new BisectAlreadyFoundState(lastGoodPosition, Some(prob))
    }
}

