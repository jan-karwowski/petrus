package pl.edu.pw.mini.sg.mixed.adjuster.globaldamping

import pl.edu.pw.mini.sg.mixed.tree.OptimizerStateType



final object GlobalDampingStatType extends OptimizerStateType[GlobalDampingStat] {
  override def zero = new GlobalDampingStat(0,0)
  override def afterIteration(prev: GlobalDampingStat) = prev
  override def pruneMoves(st: GlobalDampingStat, toDelete: Set[Int]) : GlobalDampingStat = st
}
