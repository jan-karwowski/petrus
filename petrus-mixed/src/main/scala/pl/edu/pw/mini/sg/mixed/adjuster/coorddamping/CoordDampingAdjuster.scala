package pl.edu.pw.mini.sg.mixed.adjuster.coorddamping

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.log4s._
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.adjuster.AssesmentBasedAdjuster
import pl.edu.pw.mini.sg.mixed.tree.MixedStrategyNode


final object CoordDampingAdjuster
    extends AssesmentBasedAdjuster {
  override type AdjusterState = CoordDamping
  override val ost = CoordDampingStateType

  override def updateState(
    oldState: AdjusterState,
    assesments: Seq[Double]
  )(implicit
    rng: RandomGenerator
  ): AdjusterState = {
    oldState.addAssesments(assesments)
  }


  override def updateStrategy(
    mixedStrategyNode: MixedStrategyNode[_,_, _],
    adjusterState: AdjusterState,
    assesments: Seq[Double]
  )(implicit
    rng: RandomGenerator, iterationInfo: IterationInfo
  ) : Boolean = {
    val changes = assesments.zip(adjusterState.normalizers).map{case (a,b) => a*Math.sqrt(b)}
    mixedStrategyNode.probabilities.view.zip(changes).zipWithIndex
      .foreach{case (((dm, prob), upd), idx) => {
        mixedStrategyNode.updateProb(idx, Math.max(0, prob+upd))
      }}
    mixedStrategyNode.normalizeOrEqual()
    true
  }

}
