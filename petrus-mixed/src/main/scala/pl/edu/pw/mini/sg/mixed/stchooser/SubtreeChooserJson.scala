package pl.edu.pw.mini.sg.mixed.stchooser

import argonaut.CodecJson
import pl.edu.pw.mini.sg.json.{ FlatTraitJsonCodec, TraitSubtypeCodec }



object SubtreeChooserJson {
  implicit val valueChooserCodec : CodecJson[ValueSubtreeChooser] =
    CodecJson.casecodec1(ValueSubtreeChooser.apply, ValueSubtreeChooser.unapply)(
      "probabilityBoost")


  implicit val chooserCodec : FlatTraitJsonCodec[SubtreeChooser] =
    new FlatTraitJsonCodec(List(
      TraitSubtypeCodec(valueChooserCodec, valueChooserCodec, "value")
    ))
}
