package pl.edu.pw.mini.sg.mixed.adjuster.bisect

final class BisectAdjusterState[FallbackState] (
  val lastGoodPosition : Either[Vector[Double], FallbackState],
  val infeasibleIterations : Int
) {
  def registerFeasible(prob: Vector[Double]) : BisectAdjusterState[FallbackState] = new BisectAdjusterState(Left(prob), 0)
  def registerInfeasible : BisectAdjusterState[FallbackState] = new BisectAdjusterState(lastGoodPosition, infeasibleIterations+1)
}
