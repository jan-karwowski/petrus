package pl.edu.pw.mini.sg.mixed.adjuster.globaldamping

import pl.edu.pw.mini.sg.rng.RandomGenerator
import org.log4s._
import pl.edu.pw.mini.sg.mixed.IterationInfo
import pl.edu.pw.mini.sg.mixed.adjuster.AssesmentBasedAdjuster
import pl.edu.pw.mini.sg.mixed.tree.MixedStrategyNode


final object GlobalDampingAdjuster extends AssesmentBasedAdjuster {
  private val logger = getLogger
  override type AdjusterState = GlobalDampingStat
  override val ost = GlobalDampingStatType


  def updateState(oldState: AdjusterState, assesments: Seq[Double]
  )(
    implicit rng: RandomGenerator
  ) :  AdjusterState = oldState.addAssesments(assesments)

  def updateStrategy(
    mixedStrategyNode: MixedStrategyNode[_, _, _],
    adjusterState: GlobalDampingStat,
    assesments: Seq[Double]
  )(
    implicit rng: RandomGenerator, iterationInfo: IterationInfo
  ) : Boolean = {
    if(rng.nextDouble() > 0.3) {
      mixedStrategyNode.probabilities.view.zip(assesments).zipWithIndex.foreach{ case (((dm, prob), as), idx) => {
        mixedStrategyNode.updateProb(idx, Math.max(0, (prob+as)*adjusterState.damping))
      }}
      mixedStrategyNode.normalizeOrEqual()
      logger.debug(s"Damping: ${adjusterState.damping}")
      true
    } else {
      false
    }
  }
}
