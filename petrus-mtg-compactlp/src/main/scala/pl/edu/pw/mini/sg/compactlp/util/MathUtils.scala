package pl.edu.pw.mini.sg.compactlp.util

import pl.edu.pw.mini.sg.compactlp.model.GridPosition

/**
  * Pomocnicze funkcje matematyczne, jak np. interpolacja
  */
object MathUtils {
  /**
    * Dwuwymiarowa interpolacja liniowa na liczbach calkowitych
    *
    * @param a  - wspolrzedne pierwszego punktu
    * @param b  - wspolrzedne drugiego punktu
    * @param ta - czas poczatkowy
    * @param tb - czas koncowy
    * @param t  - aktualny czas, dla ktorego chcemy wyznaczyc pozycje
    * @return Pozycja pomiedzy a i b dla czasu t
    */
  def interpolation2d(a: GridPosition, b: GridPosition, ta: Int, tb: Int)(t: Int): GridPosition = {
    def interp1d(_a: Int, _b: Int): Int = {
      _a + (_b - _a) * (t - ta) / (tb - ta)
    }

    GridPosition(interp1d(a.x, b.x), interp1d(a.y, b.y))
  }
}
