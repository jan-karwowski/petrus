package pl.edu.pw.mini.sg.compactlp.model

import pl.edu.pw.mini.sg.movingtargets.DefenderPosition

final case class GridPosition(x: Int, y: Int) {
  override def productPrefix: String = ""

  def this(defenderPosition: DefenderPosition) = this(defenderPosition.x, defenderPosition.y)
}

object GridPosition {
  def apply(defenderPosition: DefenderPosition): GridPosition = new GridPosition(defenderPosition.x, defenderPosition.y)
}
