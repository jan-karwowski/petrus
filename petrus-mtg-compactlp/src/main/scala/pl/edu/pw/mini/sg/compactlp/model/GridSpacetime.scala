package pl.edu.pw.mini.sg.compactlp.model

final case class GridSpacetime(time: Int, position: GridPosition) {
  override def productPrefix: String = ""
}
