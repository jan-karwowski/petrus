package pl.edu.pw.mini.sg.compactlp

import pl.edu.pw.mini.sg.compactlp.model.{Flow, GridSpacetime}
import pl.edu.pw.mini.sg.compactlp.util.PureStrategy

trait StrategyGenerator {
  /** Metoda sluzy do odzyskania strategii obroncow z wynikow solwera (a dokladniej z otrzymanego przeplywu)
    *
    * @param flow     Przeplywy miedzy wierzcholkami. Z przeplywu sa generowane sciezki obroncy.
    *                 Wartosci przeplywu odpowiadaja zmiennym f z solwera
    * @param grid     Siatka czasoprzestrzeni
    * @param game     Instancja klasy pomocniczej, ktora jest wrapperem wokol [[pl.edu.pw.mini.sg.movingtargets.MovingTargetsGameConfig]]
    * @param sparsity Parametr agregacji siatki s
    * @return lista [[PureStrategy]] obroncow (pozycje oraz prawdopodobienstwa)
    */

  def getStrategy(flow: Map[Flow, Double],
                  grid: Seq[GridSpacetime],
                  game: CompactGameWrapper,
                  sparsity: Int): List[PureStrategy]

}
