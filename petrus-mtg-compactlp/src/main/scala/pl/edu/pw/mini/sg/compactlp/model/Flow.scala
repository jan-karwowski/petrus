package pl.edu.pw.mini.sg.compactlp.model

final case class Flow(from: GridPosition, to: GridPosition, time: Int) {
  override def productPrefix: String = ""
}