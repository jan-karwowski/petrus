package pl.edu.pw.mini.sg.compactlp

import argonaut.CodecJson
import pl.edu.pw.mini.sg.solver.SolverConfig

final case class CompactLPConfig(sparsity: Int, solverConfig: SolverConfig)


object CompactLPConfigJson {
  import SolverConfig.solverConfigCodec
  val compactLPConfigCodec: CodecJson[CompactLPConfig] = CodecJson.casecodec2(CompactLPConfig.apply, CompactLPConfig.unapply)(
    "sparsity", "solver"
  )
}