package pl.edu.pw.mini.sg.compactlp.model

import pl.edu.pw.mini.sg.compactlp.util.Attacker

final case class DoubleAttacker(attackerA: Attacker, attackerB: Attacker) {
  override def productPrefix: String = ""
}
