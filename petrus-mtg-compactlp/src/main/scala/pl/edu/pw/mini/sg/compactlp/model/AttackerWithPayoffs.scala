package pl.edu.pw.mini.sg.compactlp.model

import pl.edu.pw.mini.sg.compactlp.util.Attacker

final case class AttackerWithPayoffs(attacker: Attacker,
                                     attackerSuccessfulAttackPayoff: Double,
                                     attackerUnsuccessfulAttackPayoff: Double,
                                     defenderSuccessfulAttackPayoff: Double,
                                     defenderUnsuccessfulAttackPayoff: Double)
