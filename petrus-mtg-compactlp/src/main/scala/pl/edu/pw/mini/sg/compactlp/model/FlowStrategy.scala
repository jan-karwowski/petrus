package pl.edu.pw.mini.sg.compactlp.model

import pl.edu.pw.mini.sg.compactlp.util.Attacker

final case class FlowStrategy(flow: Flow, strategy: Attacker) {
  override def productPrefix: String = ""
}
