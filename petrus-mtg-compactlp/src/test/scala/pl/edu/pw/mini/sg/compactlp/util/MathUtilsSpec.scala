package pl.edu.pw.mini.sg.compactlp.util

import org.scalatest.{Matchers, WordSpec}
import pl.edu.pw.mini.sg.compactlp.model.GridPosition

class MathUtilsSpec extends WordSpec with Matchers {
  "The MathUtils object" should {
    "provide an interpolation2d method" which {
      "returns point a for t = ta" in {
        val result = MathUtils.interpolation2d(GridPosition(1, 2), GridPosition(5, 10), 3, 8)(3)
        result should equal (GridPosition(1, 2))
      }
      "returns point b for t = tb" in {
        val result = MathUtils.interpolation2d(GridPosition(1, 2), GridPosition(5, 10), 3, 8)(8)
        result should equal (GridPosition(5, 10))
      }
      "returns halfway point for t = (ta+tb)/2" in {
        val result = MathUtils.interpolation2d(GridPosition(1, 2), GridPosition(5, 10), 2, 8)(5)
        result should equal (GridPosition(3, 6))
      }
      "rounds down when getting non-integer result" in {
        val result = MathUtils.interpolation2d(GridPosition(1, 2), GridPosition(5, 9), 2, 8)(5)
        result should equal (GridPosition(3, 5))
      }
      "returns (-5, -2) for two thirds between (1, 2) and (-8, -4)" in {
        val result = MathUtils.interpolation2d(GridPosition(1, 2), GridPosition(-8, -4), 1, 4)(3)
        result should equal (GridPosition(-5, -2))
      }
    }
  }
}
