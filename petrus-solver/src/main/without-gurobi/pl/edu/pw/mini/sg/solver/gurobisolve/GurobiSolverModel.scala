package pl.edu.pw.mini.sg.solver.gurobisolver

import pl.edu.pw.mini.sg.solver.{Binary, Feasible, Infeasible, Integer, Maximize, Minimize, ObjectiveType, Real, SolverModel, VariableType, _}
import squants.time.Time

class GurobiSolverModel(model: GRBModel) extends SolverModel[GurobiVariable, Nothing, GurobiLinExpr, GurobiConstraint] {

  override def addConstraints(expr: Vector[GurobiLinExpr], constraintType: Array[ConstraintType],
    rhs: Array[Double], name: Array[String]) : Array[GurobiConstraint] = {
    ???
  }

  override def addVariable(lowerBound: Double, upperBound: Double, obj: Double, variableType: VariableType, name: String): GurobiVariable = {
    ???
  }

  override def addVariables(lowerBounds: Seq[Double], upperBounds: Seq[Double], objs: Seq[Double], variableTypes: Seq[VariableType], names: Seq[String]): Array[GurobiVariable] = {
    ???
  }

  override def addVariables(lowerBounds: Double, upperBounds: Double, objs: Double, variableTypes: VariableType, names: Seq[String]): Array[GurobiVariable] = {
    ???
  }

  override def createLinExpr(): GurobiLinExpr = {
    ???
  }

  override def addConstraint(lhsExpr: GurobiLinExpr,
                             constraintType: ConstraintType,
                             rhsExpr: GurobiLinExpr,
                             name: String): GurobiConstraint = {
    ???
  }



  override def addConstraint(lhsExpr: GurobiLinExpr,
                             constraintType: ConstraintType,
                             rhsVariable: GurobiVariable,
                             name: String): GurobiConstraint = {
    ???
  }

  override def addConstraint(lhsExpr: GurobiLinExpr,
                             constraintType: ConstraintType,
                             rhs: Double,
                             name: String): GurobiConstraint = {
    ???
  }

  override def addConstraint(lhsVariable: GurobiVariable, constraintType: ConstraintType, rhsVariable: GurobiVariable, name: String): GurobiConstraint = {
    ???
  }

  override def addConstraint(lhsVariable: GurobiVariable, constraintType: ConstraintType, rhs: Double, name: String): GurobiConstraint = {
    ???
  }

  override def removeConstraint(constraint: GurobiConstraint): Unit = {
    ???
  }

  override def setObjective(solverExpr: GurobiLinExpr): Unit = {
    ???
  }

  override def setObjectiveType(objectiveType: ObjectiveType): Unit = {
    ???
  }

  override def getObjective: Double = {
    ???
  }

  override def optimize(): ModelSolutionType = {
    ???
  }


  override def update(): Unit = {
    ???
  }

  override def close(): Unit = {
    ???
  }
}
