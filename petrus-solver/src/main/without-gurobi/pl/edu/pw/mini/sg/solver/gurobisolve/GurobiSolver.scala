package pl.edu.pw.mini.sg.solver.gurobisolver

import pl.edu.pw.mini.sg.solver.{Solver, SolverInfo}


class GurobiSolver(config: GurobiConfig) extends Solver {
  throw new RuntimeException("Gurobi support not compiled into petrus!!")
  override type VariableType = GurobiVariable
  override protected type InnerLinExprType = Nothinh
  override type LinExprType = GurobiLinExpr
  override type ConstraintType = GurobiConstraint
  override type ModelType = GurobiSolverModel


  override def createModel: ModelType = {
    ???
  }


  override val solverInfo: SolverInfo = ???

  override def close(): Unit = {
    ???
  }

}
