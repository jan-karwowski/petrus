package pl.edu.pw.mini.sg.solver.gurobisolver

import pl.edu.pw.mini.sg.solver.SolverLinExpr

class GurobiLinExpr(override val inner: Nothing) extends SolverLinExpr[GurobiVariable, Nothing] {
  ???
  override def addTerm(coef: Double, variable: GurobiVariable): Unit = {
    ???
  }

  override def addTerms(coefs: Seq[Double], variables: Seq[GurobiVariable]): Unit = {
    ???
  }

  override def addTerms(coefs: Double, variables: Seq[GurobiVariable]): Unit = {
    ???
  }

  override def addConstant(c: Double): Unit = {
    ???
  }

  override def +(other: SolverLinExpr[GurobiVariable, Nothing]): GurobiLinExpr = {
    ???
  }

  override def +=(other: SolverLinExpr[GurobiVariable, Nothing]): Unit = {
    ???
  }
  override def -=(other: SolverLinExpr[GurobiVariable, Nothing]): Unit = {
    ???
  }

  override def -(other: SolverLinExpr[GurobiVariable, Nothing]): GurobiLinExpr = {
    ???
  }

}
