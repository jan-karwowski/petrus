package pl.edu.pw.mini.sg.solver

sealed trait ObjectiveType

final object Minimize extends ObjectiveType
final object Maximize extends ObjectiveType
