package pl.edu.pw.mini.sg.solver


sealed trait ModelSolutionType {
  val hasSolution: Boolean
}

sealed trait ModelWithTimeoutSolutionType extends ModelSolutionType {
  val hasSolution: Boolean
}

final object Feasible extends ModelSolutionType with ModelWithTimeoutSolutionType{
  override val hasSolution = true
}
final object Infeasible extends ModelSolutionType with ModelWithTimeoutSolutionType {
  override val hasSolution = false
}
final object TimeoutSolution extends ModelWithTimeoutSolutionType {
  override val hasSolution = true
}
final object TimeoutNoSolution extends ModelWithTimeoutSolutionType {
  override val hasSolution = false
}
