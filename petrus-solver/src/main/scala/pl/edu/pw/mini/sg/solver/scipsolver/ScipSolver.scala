package pl.edu.pw.mini.sg.solver.scipsolver

import jscip._
import pl.edu.pw.mini.sg.solver.{Solver, SolverInfo}


class ScipSolver(config: ScipConfig) extends Solver {
  override type VariableType = ScipVariable
  protected override type InnerLinExprType = ScipLinExprInner
  override type LinExprType = ScipLinExpr
  override type ConstraintType = ScipConstraint
  override type ModelType = ScipSolverModel

  private lazy val scip: Scip = new Scip

  override def createModel: ModelType = {
    System.loadLibrary("jscip")
    scip.create("scipProgram")
    new ScipSolverModel(scip)
  }

  override val solverInfo : SolverInfo = SolverInfo(
    "Scip", Scip.getVersion
  )

  override def close(): Unit = {
  }

}
