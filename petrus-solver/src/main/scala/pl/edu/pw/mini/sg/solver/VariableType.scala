package pl.edu.pw.mini.sg.solver

sealed trait VariableType

final object Real extends VariableType
final object Integer extends VariableType
final object Binary extends VariableType
