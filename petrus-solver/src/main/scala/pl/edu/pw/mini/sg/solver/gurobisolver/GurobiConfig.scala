package pl.edu.pw.mini.sg.solver.gurobisolver

import pl.edu.pw.mini.sg.solver.{Solver, SolverConfig}

final case class GurobiConfig(threads: Int = 0,
                              numericFocus: Int = 0,
                              intFeasTol: Double = 1e-5,
                              feasibilityTol: Double = 1e-6) extends SolverConfig {

  override def solverFromConfig: Solver = {
    new GurobiSolver(this)
  }


}
