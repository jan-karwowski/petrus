package pl.edu.pw.mini.sg.solver.scipsolver

import argonaut.CodecJson

object ScipConfigJson {
  val scipConfigCodec: CodecJson[ScipConfig] = CodecJson.casecodec1(ScipConfig.apply, ScipConfig.unapply)(
    "threads"
  )
}
