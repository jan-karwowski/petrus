package pl.edu.pw.mini.sg.solver.scipsolver

import scala.collection.mutable.ArrayBuffer

//Sztuczna klasa, potrzebna, bo Scip nie udostepnia wlasnej klasy dla LinearExpression
case class ScipLinExprInner(
  termVars: ArrayBuffer[ScipVariable],
  termConsts: ArrayBuffer[Double],
  constants: ArrayBuffer[Double]
)