package pl.edu.pw.mini.sg.solver.scipsolver

import pl.edu.pw.mini.sg.solver.{Solver, SolverConfig}

final case class ScipConfig(threads: Int = 0) extends SolverConfig {
  override def solverFromConfig: Solver = {
    new ScipSolver(this)
  }

}
