package pl.edu.pw.mini.sg.solver.gurobisolver

import argonaut.CodecJson

object GurobiConfigJson {
  val gurobiConfigCodec: CodecJson[GurobiConfig] = CodecJson.casecodec4(GurobiConfig.apply, GurobiConfig.unapply)(
    "threads",
    "numericFocus",
    "intFeasTol",
    "feasibilityTol"
  )
}
