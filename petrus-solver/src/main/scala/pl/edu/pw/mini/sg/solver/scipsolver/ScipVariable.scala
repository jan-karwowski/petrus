package pl.edu.pw.mini.sg.solver.scipsolver

import jscip._
import pl.edu.pw.mini.sg.solver.SolverVariable


class ScipVariable(val inner: Variable)(implicit scip: Scip) extends SolverVariable {
  override def name(): String = {
    inner.getName
  }

  override def value(): Double = {
    scip.getSolVal(scip.getBestSol, inner)
  }
}
