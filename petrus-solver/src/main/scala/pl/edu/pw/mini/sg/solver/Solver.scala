package pl.edu.pw.mini.sg.solver

/** Podstawowy interfejs dla klas implementujacych solwery
  *
  * Klasa implementujaca ten interfejs jest odpowiedzialna za tworzenie modelu solwera oraz za zwalnianie
  * zasobow po zakonczeniu obliczen (stad dziedziczenie od AutoClosable).
  * Dodatkowo definiuje typy klas dla zmiennych, wyrazen liniowych oraz ograniczen solwera. Otrzymujac obiekt
  * typu Solver mozna z tego skorzystac na przyklad zeby zdefiniowac zmienna reprezentujaca
  * zmienna/wyrazenie liniowe/ograniczenie solwera, jaki otrzymalismy:
  * def fun(Solver solver) = {
  *   val solverModel = solver.createModel
  *   val variable: solver.VariableType = solverModel.addVariable(...)
  * }
  */
trait Solver extends AutoCloseable{
  type VariableType <: SolverVariable
  protected type InnerLinExprType
  type LinExprType <: SolverLinExpr[VariableType, InnerLinExprType]
  type ConstraintType <: SolverConstraint
  type ModelType <: SolverModel[VariableType, InnerLinExprType, LinExprType, ConstraintType]
  def createModel: ModelType
  val solverInfo: SolverInfo 
}

