package pl.edu.pw.mini.sg.solver

import argonaut.Argonaut.casecodec2


final case class SolverInfo(
  val solverName: String,
  val versionInfo : String
)

object SolverInfo {
  val solverInfoCodec = casecodec2(SolverInfo.apply, SolverInfo.unapply)("name", "version")
}
