package pl.edu.pw.mini.sg.solver

import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}
trait SolverConfig {
  def solverFromConfig: Solver
}

object SolverConfig {
  implicit val solverConfigCodec: FlatTraitJsonCodec[SolverConfig] = {
    import pl.edu.pw.mini.sg.solver.gurobisolver.GurobiConfigJson.gurobiConfigCodec
    import pl.edu.pw.mini.sg.solver.scipsolver.ScipConfigJson.scipConfigCodec
    new FlatTraitJsonCodec[SolverConfig](List(
      TraitSubtypeCodec(gurobiConfigCodec, gurobiConfigCodec, "Gurobi"),
      TraitSubtypeCodec(scipConfigCodec, scipConfigCodec, "Scip")
    ))
  }
}
