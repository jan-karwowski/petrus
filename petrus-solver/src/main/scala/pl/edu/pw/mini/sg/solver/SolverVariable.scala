package pl.edu.pw.mini.sg.solver

trait SolverVariable {
  def name(): String
  def value(): Double
}
