package pl.edu.pw.mini.sg.solver

sealed trait ConstraintType

final object LessEqual extends ConstraintType
final object Equal extends ConstraintType
final object GreaterEqual extends ConstraintType
