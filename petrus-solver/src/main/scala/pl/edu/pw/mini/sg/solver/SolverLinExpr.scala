package pl.edu.pw.mini.sg.solver

trait SolverLinExpr[V <: SolverVariable, I] {
  def addTerm(coef: Double, variable: V)
  def addTerms(coefs: Seq[Double], variables: Seq[V])
  def addTerms(coefs: Double, variables: Seq[V])
  def addConstant(c: Double)
  def +(other: SolverLinExpr[V, I]): SolverLinExpr[V, I]
  def +=(other: SolverLinExpr[V, I])
  def -=(other: SolverLinExpr[V, I])
  def -(other: SolverLinExpr[V, I]): SolverLinExpr[V, I]
  val inner: I
}
