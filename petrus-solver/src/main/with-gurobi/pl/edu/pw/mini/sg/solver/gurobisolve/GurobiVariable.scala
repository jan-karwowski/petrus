package pl.edu.pw.mini.sg.solver.gurobisolver

import gurobi.{GRB, GRBVar}
import pl.edu.pw.mini.sg.solver.SolverVariable

class GurobiVariable(val inner: GRBVar) extends SolverVariable {
  override def name(): String = {
    inner.get(GRB.StringAttr.VarName)
  }

  override def value(): Double = {
    inner.get(GRB.DoubleAttr.X)
  }
}
