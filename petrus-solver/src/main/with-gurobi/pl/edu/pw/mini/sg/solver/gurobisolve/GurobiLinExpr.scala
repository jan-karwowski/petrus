package pl.edu.pw.mini.sg.solver.gurobisolver

import gurobi.GRBLinExpr
import pl.edu.pw.mini.sg.solver.SolverLinExpr

class GurobiLinExpr(override val inner: GRBLinExpr) extends SolverLinExpr[GurobiVariable, GRBLinExpr] {

  override def addTerm(coef: Double, variable: GurobiVariable): Unit = {
    inner.addTerm(coef, variable.inner)
  }

  override def addTerms(coefs: Seq[Double], variables: Seq[GurobiVariable]): Unit = {
    inner.addTerms(coefs.toArray, variables.map(v => v.inner).toArray)
  }

  override def addTerms(coefs: Double, variables: Seq[GurobiVariable]): Unit = {
    inner.addTerms(Array.fill(variables.length)(coefs), variables.map(v => v.inner).toArray)
  }

  override def addConstant(c: Double): Unit = {
    inner.addConstant(c)
  }

  override def +(other: SolverLinExpr[GurobiVariable, GRBLinExpr]): GurobiLinExpr = {
    val result = new GurobiLinExpr(inner)
    result.inner.add(other.inner)
    result
  }

  override def +=(other: SolverLinExpr[GurobiVariable, GRBLinExpr]): Unit = {
    inner.add(other.inner)
  }
  override def -=(other: SolverLinExpr[GurobiVariable, GRBLinExpr]): Unit = {
    inner.multAdd(-1.0, other.inner)
  }

  override def -(other: SolverLinExpr[GurobiVariable, GRBLinExpr]): GurobiLinExpr = {
    val result = new GurobiLinExpr(inner)
    result.inner.multAdd(-1.0, other.inner)
    result
  }

}
