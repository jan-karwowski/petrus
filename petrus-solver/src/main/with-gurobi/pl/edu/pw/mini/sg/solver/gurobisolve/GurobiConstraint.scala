package pl.edu.pw.mini.sg.solver.gurobisolver

import gurobi.GRBConstr
import pl.edu.pw.mini.sg.solver.SolverConstraint

class GurobiConstraint(val inner: GRBConstr) extends SolverConstraint{
}
