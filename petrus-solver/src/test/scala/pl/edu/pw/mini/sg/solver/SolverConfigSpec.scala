package pl.edu.pw.mini.sg.solver

import argonaut.Parse
import org.scalatest.{Matchers, WordSpec}
import pl.edu.pw.mini.sg.solver.gurobisolver._
import pl.edu.pw.mini.sg.solver.scipsolver.ScipSolver

class SolverConfigSpec extends WordSpec with Matchers{

  private val fullJson =
    """
      |{
      |   "type": "Gurobi",
      |   "intFeasTol": 1e-4,
      |   "numericFocus": 2,
      |   "feasibilityTol": 1e-3,
      |   "threads": 1
      |}
    """.stripMargin

  private val scipJson =
    """
      |{
      | "type": "Scip",
      | "threads": 1
      |}
    """.stripMargin

  "SolverConfig" should {

    "provide a solverConfigCodec" which {
      import SolverConfig.solverConfigCodec
      "correctly decodes a Gurobi json with all fields" in {
        val config = Parse.decode(fullJson) match {
          case Right(c) => c
        }
        config should equal (GurobiConfig(1,  2, 1e-4, 1e-3))
      }
    }

    "provide a solverFromConfig method" which {
      import SolverConfig.solverConfigCodec
      val config = Parse.decode(fullJson) match {
        case Right(c) => c
      }
      val scipConfig = Parse.decode(scipJson) match {
        case Right(c) => c
      }
      "creates a GurobiSolver for type Gurobi" in {
        val solver = config.solverFromConfig
        solver shouldBe a [GurobiSolver]
      }

      "creates a ScipSolver for type Scip" in {
        val solver = scipConfig.solverFromConfig
        solver shouldBe a [ScipSolver]
      }
    }
  }

}
