package pl.edu.pw.mini.sg.game.util

import argonaut.EncodeJson



object MoveStateKeyJson {
  implicit def moveStateKeyEncode[M, S](implicit me: EncodeJson[M], se: EncodeJson[S]
  ): EncodeJson[MoveStateKey[M, S]] = {
    EncodeJson.jencode2L((msk: MoveStateKey[M, S]) => (msk.move, msk.state))("move", "state")
  }
}
