package pl.edu.pw.mini.sg.game.single

import pl.edu.pw.mini.sg.game.deterministic.single.SinglePlayerGame

final class ForbiddenMoveRootGame[M, S, G <: SinglePlayerGame[M, S, G]](
  game: G,
  moveMask: Option[Set[M]]
) extends SinglePlayerGame[M, S, ForbiddenMoveRootGame[M, S, G]] {
  override def makeMove(move: M): ForbiddenMoveRootGame[M,S,G] = new ForbiddenMoveRootGame(game.makeMove(move), None)
  override def playerState: S = game.playerState
  override def possibleMoves: List[M] = moveMask match {
    case Some(set) => {
      val ret = game.possibleMoves.filter(!set.contains(_))
      require(ret.nonEmpty)
      ret
    }
    case None => game.possibleMoves
  }
}
