package pl.edu.pw.mini.sg.game.util

import pl.edu.pw.mini.sg.rng.RandomGenerator


object RandomHelpers {
  implicit final class RandomVectorElement[T](vector: Vector[T]) {
    def randomElement(implicit rng: RandomGenerator) : T = vector(rng.nextInt(vector.length))
  }

  implicit final class RandomMapElement[V](map : Map[_, V]) {
    def randomValue(implicit rng: RandomGenerator) : V = map.values.drop(rng.nextInt(map.size)).head
  }
}
