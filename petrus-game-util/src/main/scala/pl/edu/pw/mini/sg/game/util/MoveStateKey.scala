package pl.edu.pw.mini.sg.game.util

import scala.runtime.ScalaRunTime
import scala.specialized
import scala.util.hashing.MurmurHash3

@specialized(Int)
final class MoveStateKey[M, S](
  val move: M,
  val state: S
) {
  override def equals(other: Any) = other match {
    case msk: MoveStateKey[M, S] => move.equals(msk.move) && state.equals(msk.state)
    case _ => false
  }
  override val hashCode : Int = MurmurHash3.finalizeHash(MurmurHash3.mix(MurmurHash3.mix(0xcafebabe, move.hashCode), state.hashCode), 2)
//  override val hashCode : Int = (move, state).hashCode
  override def toString = s"($move, $state)"
}

object MoveStateKey {
  def apply[M, S](move: M, state: S) = new MoveStateKey(move, state)
}
