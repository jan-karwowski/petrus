#!/bin/bash

INFILE=$1

gawk -f - $1 <<EOF
BEGIN{iteration=0; print "" > "$INFILE.r"iteration".csv";print "" > "$INFILE.p"iteration".csv"; step=0;found=0;}
match(\$0, /Reduction val: ([0-9., -E]*)/,a){print a[1] >> "$INFILE.r"iteration".csv";found=0;step=step+1;}
match(\$0, /Signchange vec: Vector\\(([0-9., -E]*)\\)/,a){print a[1] >> "$INFILE.r"iteration".csv";found=0;step=step+1;}
match(\$0, /Found: P\\(d: ([0-9.-E]*), a: ([0-9.-E]*)\\)/,a){if(found == 0){print step","a[1]","a[2] >> "$INFILE.p"iteration".csv";found=1}}
/Outer probe iteration/{iteration=iteration+1; print "" > "$INFILE.r"iteration".csv";  print "" > "$INFILE.p"iteration".csv";step=0;found=0;}
EOF
