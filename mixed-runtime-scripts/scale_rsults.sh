#!/bin/sh


if [ 1 -eq "$(echo "$(jq .payoff.defender *.result | sort | head -n 1) < 0" | bc)" ]; then
    echo "Negative values found. Not running!"
else 
    for a in *.result; do
	jq -c '.payoff.defender*=2|.payoff.attacker*=2|.payoff.defender-=1|.payoff.attacker-=1' $a > $a.new
	mv $a.new $a
    done
fi
