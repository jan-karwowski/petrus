#!/bin/bash

INFILE=$1

gawk -f - $1 <<EOF
BEGIN{iteration=0; print "" > "$INFILE.i"iteration".csv"; print "" > "$INFILE.n"iteration".csv";print "" > "$INFILE.p"iteration".csv"; step=0;found=0;}
match(\$0, /Normalized Momentum value: Vector\\(([0-9., -E]*)\\)/,a){print a[1] >> "$INFILE.i"iteration".csv"; step=step+1;found=0;}
match(\$0, /Normalizer: ([0-9., -E]*)/,a){print a[1] >> "$INFILE.n"iteration".csv"}
match(\$0, /Found: P\\(d: ([0-9.-E]*), a: ([0-9.-E]*)\\)/,a){if(found == 0){print step","a[1]","a[2] >> "$INFILE.p"iteration".csv";found=1}}
/Outer probe iteration/{iteration=iteration+1; print "" > "$INFILE.i"iteration".csv"; print "" > "$INFILE.n"iteration".csv"; print "" > "$INFILE.p"iteration".csv";step=0;found=0;}
EOF
