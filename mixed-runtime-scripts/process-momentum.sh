#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

for a in *.out; do
    $SCRIPTPATH/extract-momentum.sh $a
    $SCRIPTPATH/payoff-plot.sh $a
    for x in $a.i*.csv; do	
	Rscript $SCRIPTPATH/momentum-plot.R $x
    done
done
