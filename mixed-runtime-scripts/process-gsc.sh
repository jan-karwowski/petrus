#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

for a in *.out; do
    $SCRIPTPATH/extract-scred.sh $a
    $SCRIPTPATH/payoff-plot.sh $a
done
