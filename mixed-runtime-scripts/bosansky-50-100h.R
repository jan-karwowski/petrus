#!/usr/bin/env Rscript

basedir <- "/home/jan/research/security_games/experiments-new"

methods <- list(
    `mixeduct` = c("search-games6-momentum-ir4-deep-max/out/mean.csv",
                   "search-games-momentum-ir4-deep-max/out/mean.csv"),
    `mixeduct2` = c("searchgames-momentum-deep2-max-mcex2/out/mean.csv"),
    bc2015 = c("search-games-bc15/mean.csv"),
    `bc2015-50` = c("search-games-bosansky-50h/out/mean.csv"),
    `bc205-100` = c("search-games-bosansky-100h/out/mean.csv"),
    `cor` = c("search-games-correlated-ai-milp/mean.csv"),
    `cor50` = c("search-games-correlated-50h/out/mean.csv")
)

read.inputs <- function(files) {
    do.call(rbind, Map(function(file) read.csv(paste(basedir, file, sep="/"), header=TRUE)[,c("game", "defender.payoff", "trainingTime")], files))
}

data <- Map(read.inputs, methods)

mergeFun <- function(df, name) {
    merge(df, data[[name]], by="game", suffixes = c("", name), all=TRUE)
}

dd  <- Reduce(mergeFun, init = data$mixeduct, x = names(methods)[-1])

write.csv(dd, "hourlimit.csv")
