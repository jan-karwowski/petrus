#!/bin/bash

MASTER_FILE=$1

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

for a in $1.p*.csv; do
    Rscript $SCRIPTPATH/payoff-plot.R $a
done
