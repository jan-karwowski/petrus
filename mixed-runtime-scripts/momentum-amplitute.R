#!/usr/bin/env R

require(ggplot2)
require(zoo)

data <- read.csv("/home/jan/matrix-results/long/out/mat-100-20-02-3.nfgame.momentum-longer-fullexpand.1.out.p1.csv",
                 header = FALSE, col.names=c("it", "payoff", "ap"))

data <- data[-nrow(data),]

amplitute100 <-data.frame(
    it = rollapply(data$it, 3000, max),
    amp = rollapply(data$payoff, 3000, function (x) max(x) - min(x))
)

ggplot(data = amplitute100[amplitute100$it >= 7000,], mapping = aes(x=it, y=amp))+geom_point()
