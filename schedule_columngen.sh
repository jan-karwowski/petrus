#!/bin/bash


for targets in 02 03 04; do
    for defenders in 01 02; do
	for steps in 02 04 06; do
	    for density in 10 20; do
		for id in 0000 0001 0002 0003 0004 0005 0006 0007 0008 0009; do
		    tsp ./run_columngen_lp.sh ~/git/securitygamesmovingtargets/TestSets/ParamsTests/ParamsTestsConfig01_steps${steps}targets${targets}defenders${defenders}density${density}_${id}.sgmt
		done
	    done
	done
    done
done
		
