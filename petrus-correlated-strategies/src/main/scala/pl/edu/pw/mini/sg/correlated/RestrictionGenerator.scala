package pl.edu.pw.mini.sg.correlated

import pl.edu.pw.mini.sg.bosansky.{ExtendedMove, InformationSetId}
import pl.edu.pw.mini.sg.game.deterministic.{AdvanceablePlayerVisibleState, PlayerVisibleState}
import pl.edu.pw.mini.sg.solver.{SolverConstraint, SolverLinExpr, SolverModel, SolverVariable}

import scala.collection.mutable

trait RestrictionGenerator {
  def addRestrictions[DM,
    AM,
    DS <: PlayerVisibleState[DM],
    AS <: AdvanceablePlayerVisibleState[AM, AS],
    V <: SolverVariable,
    I,
    L <: SolverLinExpr[V, I],
    C <: SolverConstraint](currentBestRestriction: Restriction[L],
                           currentRestrictions: mutable.PriorityQueue[Restriction[L]],
                           inconsistentInformationSets: List[InformationSetId[AM, AS]],
                           value: Double,
                           solverModel: SolverModel[V, I, L, C],
                           p: Map[(List[ExtendedMove[DM, DS]], List[ExtendedMove[AM, AS]]), V],
                           relevantPairs: List[(List[ExtendedMove[DM, DS]], List[ExtendedMove[AM, AS]])])
}
