package pl.edu.pw.mini.sg.correlated

case class Restriction[L](upperBound: Double, constraints: List[ConstraintParameters[L]]) {

}

object Restriction {
  def restrictionOrder[L] (restriction: Restriction[L]): Double = restriction.upperBound
}
