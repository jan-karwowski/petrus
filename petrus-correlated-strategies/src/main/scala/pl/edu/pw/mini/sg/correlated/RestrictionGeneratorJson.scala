package pl.edu.pw.mini.sg.correlated

import argonaut.{CodecJson, DecodeJson, DecodeResult, EncodeJson}
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}

object RestrictionGeneratorJson {

  implicit val silpRestrictionGeneratorDecoder: DecodeJson[SilpRestrictionGenerator] =
    DecodeJson(_ => DecodeResult.ok(new SilpRestrictionGenerator))

  implicit val silpRestrictionGeneratorEncoder: EncodeJson[SilpRestrictionGenerator] =
    EncodeJson.UnitEncodeJson.contramap { x => Unit }

  implicit val milpRestrictionGeneratorCodec: CodecJson[MilpRestrictionGenerator] = {
    import InformationSetChooserJson.informationSetChooserCodec
    CodecJson.casecodec1(MilpRestrictionGenerator.apply, MilpRestrictionGenerator.unapply)(
      "informationSetChooser"
    )(informationSetChooserCodec, informationSetChooserCodec)
  }

  implicit val restrictionGeneratorCodec: FlatTraitJsonCodec[RestrictionGenerator] =
    new FlatTraitJsonCodec[RestrictionGenerator](List(
      new TraitSubtypeCodec(silpRestrictionGeneratorEncoder, silpRestrictionGeneratorDecoder, "SILP"),
      new TraitSubtypeCodec(milpRestrictionGeneratorCodec, milpRestrictionGeneratorCodec, "MILP")
    ))

}
