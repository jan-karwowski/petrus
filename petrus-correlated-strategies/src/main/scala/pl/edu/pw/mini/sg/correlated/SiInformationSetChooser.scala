package pl.edu.pw.mini.sg.correlated
import pl.edu.pw.mini.sg.bosansky.InformationSetId
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState

class SiInformationSetChooser extends InformationSetChooser {
  override def chooseSets[M, S <: PlayerVisibleState[M]](inconsistentInformationSets: List[InformationSetId[M, S]]): List[InformationSetId[M, S]] = {
    //TODO: Maybe they are already sorted, then we can return the first element
    List(inconsistentInformationSets.minBy(_.precedingMoves.length))
  }
}
