package pl.edu.pw.mini.sg.correlated

import argonaut.{ CodecJson, DecodeJson, EncodeJson }
import pl.edu.pw.mini.sg.solver.SolverConfig
import squants.time.{ Seconds, Time }

object CorrelatedStrategiesConfigJson {
  import SolverConfig.solverConfigCodec
  import RestrictionGeneratorJson.restrictionGeneratorCodec
  implicit val timeEncode = EncodeJson.DoubleEncodeJson.contramap[Time](_.toSeconds)
  implicit val timeDecode = DecodeJson.DoubleDecodeJson.map(Seconds.apply[Double])

  val correlatedStrategiesConfigCodec = CodecJson.casecodec3(CorrelatedStrategiesConfig.apply, CorrelatedStrategiesConfig.unapply)(
    "restrictionGenerator", "solver", "timeLimitSeconds"
  )
}
