package pl.edu.pw.mini.sg.correlated

import argonaut.{DecodeJson, DecodeResult, EncodeJson}
import pl.edu.pw.mini.sg.json.{FlatTraitJsonCodec, TraitSubtypeCodec}

object InformationSetChooserJson {

  implicit val siInformationSetChooserDecoder: DecodeJson[SiInformationSetChooser] =
    DecodeJson(_ => DecodeResult.ok(new SiInformationSetChooser))

  implicit val siInformationSetChooserEncoder: EncodeJson[SiInformationSetChooser] =
    EncodeJson.UnitEncodeJson.contramap { _ => Unit }

  implicit val aiInformationSetChooserDecoder: DecodeJson[AiInformationSetChooser] =
    DecodeJson(_ => DecodeResult.ok(new AiInformationSetChooser))

  implicit val aiInformationSetChooserEncoder: EncodeJson[AiInformationSetChooser] =
    EncodeJson.UnitEncodeJson.contramap { _ => Unit }

  val informationSetChooserCodec: FlatTraitJsonCodec[InformationSetChooser] =
    new FlatTraitJsonCodec[InformationSetChooser](List(
      new TraitSubtypeCodec(siInformationSetChooserEncoder, siInformationSetChooserDecoder, "SI"),
      new TraitSubtypeCodec(aiInformationSetChooserEncoder, aiInformationSetChooserDecoder, "AI")
    ))

}
