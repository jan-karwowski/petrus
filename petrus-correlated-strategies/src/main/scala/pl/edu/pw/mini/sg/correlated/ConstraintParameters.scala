package pl.edu.pw.mini.sg.correlated

import pl.edu.pw.mini.sg.solver.{ConstraintType}

final case class ConstraintParameters[L](linExpr: L, constraintType: ConstraintType, rhs: Double) {
}
