package pl.edu.pw.mini.sg.correlated

import pl.edu.pw.mini.sg.solver.SolverConfig
import squants.time.Time

final case class CorrelatedStrategiesConfig(
  restrictionGenerator: RestrictionGenerator,
  solverConfig: SolverConfig,
  timeLimit: Option[Time]
)
