package pl.edu.pw.mini.sg.correlated

import argonaut.Argonaut._
import argonaut.Json
import pl.edu.pw.mini.sg.results.MethodInfo
import pl.edu.pw.mini.sg.solver.SolverInfo

case class CorrelatedStrategiesMethodInfo(override val configFileName: String,
  override val gitHash: String,
  override val version: String,
  solverInfo: SolverInfo,
  restrictionGenerator: RestrictionGenerator) extends MethodInfo {

  override val methodName: String = "correlated-strategies"

  override val additionalInformation: Json = {
    import SolverInfo.solverInfoCodec
    import RestrictionGeneratorJson.restrictionGeneratorCodec
    ("solverInfo" := solverInfoCodec.encode(solverInfo)) ->:
      ("restrictionGenerator" := restrictionGeneratorCodec.encode(restrictionGenerator)) ->: jEmptyObject
  }
}
