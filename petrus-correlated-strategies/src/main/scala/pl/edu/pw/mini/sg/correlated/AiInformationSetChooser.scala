package pl.edu.pw.mini.sg.correlated
import pl.edu.pw.mini.sg.bosansky.InformationSetId
import pl.edu.pw.mini.sg.game.deterministic.PlayerVisibleState

class AiInformationSetChooser extends InformationSetChooser {
  override def chooseSets[M, S <: PlayerVisibleState[M]](inconsistentInformationSets: List[InformationSetId[M, S]]): List[InformationSetId[M, S]] = {
    inconsistentInformationSets
  }
}
