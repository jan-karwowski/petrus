package pl.edu.pw.mini.jk.sg.game.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class TimedAccidentHistoryTest {
    @Test
    public void testTimeEqualsEq() {
        TimedAccidentHistory tah1 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(2, 2, 2, true)).build(), 5);
        TimedAccidentHistory tah2 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(3, 2, 2, true)).build(), 6);

        assertEquals(tah1, tah2);
        assertEquals(tah1.hashCode(), tah2.hashCode());
    }

    @Test
    public void testTimeEqualsNEq() {
        TimedAccidentHistory tah1 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(2, 2, 2, true)).build(), 5);
        TimedAccidentHistory tah2 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(3, 2, 2, true)).build(), 5);

        assertNotEquals(tah1, tah2);
    }

    @Test
    public void testTimeEqualsEqPerm() {
        TimedAccidentHistory tah1 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(1, 2, 2, true), new Accident(2, 2, 2, true)).build(), 5);
        TimedAccidentHistory tah2 = new TimedAccidentHistory(ImmutableList.<Accident> builder()
                .add(new Accident(3, 2, 2, true), new Accident(2, 2, 2, true)).build(), 6);

        assertEquals(tah1, tah2);
        assertEquals(tah1.hashCode(), tah2.hashCode());
    }

}
