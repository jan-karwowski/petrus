package pl.edu.pw.mini.jk.sg.game.configuration

import org.scalactic.TripleEqualsSupport.Spread
import org.scalatest._

import scala.collection.JavaConverters._

class StaticGameConfigurationSpec extends WordSpec with Matchers {
  "StaticGameConfiguration with simple graph" should {
    val graphDescription = GraphDescription(5,
      List(Edge(0,1), Edge(1,2), Edge(2,3), Edge(0,2))
    )

    val gameConfiguration = new StaticGameConfiguration(
      graphDescription, List[Integer](0,2).asJava, List[Integer](3).asJava, 
      Array(3,4).map(Array(_)), Array(5,8).map(Array(_)),
      Array(5, 1, 7, 1, 1).map(Array(_)),
      Array(2, 1, 5, 1, 1).map(Array(_)),
      1, 1, DiscreteProbabilityFunction, 0, 0)


    "give 0,1,2 possible moves from 0" in {
      gameConfiguration.getPossibleTargetFromVertex.get(0).asScala should contain only(0,1,2)
    }

    "give 4 possible moves from 4" in {
      gameConfiguration.getPossibleTargetFromVertex.get(4).asScala should contain only(4)
    }

    "give 0,1,2 possible moves from 1" in {
      gameConfiguration.getPossibleTargetFromVertex.get(1).asScala should contain only(0,1,2)
    }

    "give 5 defender reward in vertex 0" in {
      gameConfiguration.getVertexDefenderReward(0, 0) should equal(5)
    }

    "give -3 defender penalty in target 0" in {
      gameConfiguration.getDefenderPenalty(0, 0) should equal(-3)
    }

    "give -4 defender penalty in target 1" in {
      gameConfiguration.getDefenderPenalty(1,0) should equal(-4)
    }

    "give -5 attacker penalty in vertex 2" in {
      gameConfiguration.getVertexAttackerPenalty(2, 0) should equal(-5)
    }

    "normalize -4 to -.5" in {
      gameConfiguration.normalizeReward(-4) shouldEqual(Spread(-.5, 1e-7))
    }

    "map vertex 2 to target 1" in {
      gameConfiguration.vertexToTarget(2) shouldEqual(1)
    }

    "map target 1 to vertex 2" in {
      gameConfiguration.targetToVertex(1) shouldEqual(2)
    }
  }
}
