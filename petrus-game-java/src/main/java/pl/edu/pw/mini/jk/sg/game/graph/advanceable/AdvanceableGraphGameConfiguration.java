package pl.edu.pw.mini.jk.sg.game.graph.advanceable;

import com.fasterxml.jackson.annotation.JsonProperty;

import pl.edu.pw.mini.jk.sg.game.configuration.StaticGameConfiguration;

public class AdvanceableGraphGameConfiguration {
	private final StaticGameConfiguration staticGameConfiguration;
	private final int numberOfAttackers;

	@JsonProperty("attackerCount")
	public int getNumberOfAttackers() {
		return numberOfAttackers;
	}

	@JsonProperty("staticGameConfiguration")
	public StaticGameConfiguration getStaticGameConfiguration() {
		return staticGameConfiguration;
	}

	public AdvanceableGraphGameConfiguration(
			@JsonProperty(value = "staticGameConfiguration", required = true) StaticGameConfiguration staticGameConfiguration,
			@JsonProperty(value = "attackerCount", required = true) int numberOfAttackers) {
		super();
		this.staticGameConfiguration = staticGameConfiguration;
		this.numberOfAttackers = numberOfAttackers;
	}
}
