package pl.edu.pw.mini.jk.sg.game.graph.moves.many.advanceable;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;

public class SingleUnitDestination implements DefenderMove {
    private static final long serialVersionUID = 1L;
    
    private final int destination;

    public SingleUnitDestination(int destination) {
        super();
        this.destination = destination;
    }

    public int getDestination() {
        return destination;
    }

    @Override
    public int hashCode() {
        return destination;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SingleUnitDestination other = (SingleUnitDestination) obj;
        if (destination != other.destination)
            return false;
        return true;
    }

	@Override
	public String toString() {
		return Integer.toString(destination);
	}
    
    
    
    

}
