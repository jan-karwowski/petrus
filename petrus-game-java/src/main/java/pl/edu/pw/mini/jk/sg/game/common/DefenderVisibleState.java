package pl.edu.pw.mini.jk.sg.game.common;

import java.io.Serializable;

public interface DefenderVisibleState extends Serializable {
    int getTimeStep();
    int hashCode();
    boolean equals(Object obj);
}
