package pl.edu.pw.mini.jk.sg.game.common.advanceable;

import pl.edu.pw.mini.jk.sg.game.common.AttackerVisibleState;

public interface AdvanceableAttackerState<M> extends AdvanceableState<M>, AttackerVisibleState {

}
