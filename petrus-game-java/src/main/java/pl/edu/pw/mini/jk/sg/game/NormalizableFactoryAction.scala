package pl.edu.pw.mini.jk.sg.game

import pl.edu.pw.mini.jk.sg.game.common.{ Cloneable, DefenderMove, SecurityGame }
import pl.edu.pw.mini.jk.sg.game.common.advanceable.{ AdvanceableAttackerState, AdvanceableDefenderState }


trait NormalizableFactoryAction[R] {
  def runWithNormalizableFactory[AM, DM <: DefenderMove,
    AS <: AdvanceableAttackerState[AM], DS <: AdvanceableDefenderState[DM],
    G <: SecurityGame with Cloneable[G] with DefenderAttackerGame[AM, DM, AS, DS, _]](factory: NormalizableFactory[AM,DM,G], gameLength: Int) : R
}
