package pl.edu.pw.mini.jk.sg.game.configuration;

import scala.collection.JavaConverters._

final case class GraphDescription(vertexCount: Int, edges: List[Edge]) {
  def this(vertexCount: Int, edges: java.util.List[Edge]) = this(vertexCount, edges.asScala.toList)

  final def getEdges = edges.asJava
}
