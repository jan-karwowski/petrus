package pl.edu.pw.mini.jk.sg.game.graph.moves.many.advanceable;

import java.util.Optional;

import com.google.common.cache.Cache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState;

public class AdvanceableMultiMoveAttackerState extends AdvanceableMultiMoveSideState
        implements AdvanceableAttackerState<SingleUnitDestination> {

    public AdvanceableMultiMoveAttackerState(int timeStep, ImmutableList<Integer> unitPositions,
            ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex, int currentUnit, int maxUnitsInGame,
            Optional<Cache<Integer, ImmutableList<SingleUnitDestination>>> moveCache) {
        super(timeStep, unitPositions, possibleDestinationFromVertex, currentUnit, maxUnitsInGame, moveCache);
    }

    @Override
    public AdvanceableMultiMoveAttackerState advance(SingleUnitDestination move) {
        return new AdvanceableMultiMoveAttackerState(isLastMoveInRound() ? getTimeStep()+1 : getTimeStep(),
						     advanceUnitPositinos(move), getPossibleDestinationFromVertex(),
                advanceCurrentUnit(), getMaxUnitsInGame(), getMoveCache());
    }


	@Override
	public String toString() {
		return "AdvanceableMultiMoveAttackerState [getUnitPositions()=" + getUnitPositions() + ", getCurrentUnit()="
				+ getCurrentUnit() + "]";
	}

    
    
}
