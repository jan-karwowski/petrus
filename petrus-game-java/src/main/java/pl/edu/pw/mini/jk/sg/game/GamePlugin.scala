package pl.edu.pw.mini.jk.sg.game

import java.io.File

trait GamePlugin {
  def matchFile(file: File) : Boolean
  def fileToGameVariantRunner(file: File): GameVariantRunner
  def fileToGameVariantRunner(file: File, transformer: File) : GameVariantRunner
}
