package pl.edu.pw.mini.jk.sg.game.common;

import java.util.Collection;

public interface AccidentHistory {
	public void registerAccident(Accident accident);
	public void setCurrentTimestep(int timeStep);
	public Collection<Accident> getAccidents(int timeHorizon); 
	public AccidentHistory typedClone();
}
