package pl.edu.pw.mini.jk.sg.game.common

case class TwoPlayerPayoffInfo(defenderPayoff: Double, attackerPayoff: Double,
  catchAccidentsCount: Int, attackAccidentsCount: Int) 

