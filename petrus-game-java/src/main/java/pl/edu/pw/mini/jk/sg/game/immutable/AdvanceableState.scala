package pl.edu.pw.mini.jk.sg.game.immutable

import com.google.common.collect.ImmutableList


trait AdvanceableState[M, S <: AdvanceableState[M, S]] {
  type Move = M 

  def advance(move: M) : S
  def getPossibleMoves : ImmutableList[M]
}
