package pl.edu.pw.mini.jk.sg.game.common;

final case class Accident(
  timeStep: Int,
  attackerId: Int,
  location: Int,
  attackerSuccess: Boolean 
)

