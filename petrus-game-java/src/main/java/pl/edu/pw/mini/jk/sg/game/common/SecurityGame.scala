package pl.edu.pw.mini.jk.sg.game.common

import java.util.List;

trait SecurityGame {
    def getTimeStep() : Int;
    def getPreviousAccidents() : List[Accident];
    def accidentOccured(): Boolean
}
