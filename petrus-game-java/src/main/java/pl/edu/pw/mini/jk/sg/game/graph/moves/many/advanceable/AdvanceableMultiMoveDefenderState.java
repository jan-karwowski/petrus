package pl.edu.pw.mini.jk.sg.game.graph.moves.many.advanceable;

import java.util.Optional;

import com.google.common.cache.Cache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableDefenderState;

public class AdvanceableMultiMoveDefenderState extends AdvanceableMultiMoveSideState
        implements AdvanceableDefenderState<SingleUnitDestination> {
    private static final long serialVersionUID = 1L;

    public AdvanceableMultiMoveDefenderState(ImmutableList<Integer> unitPositions,
            ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex, int currentUnit, int maxUnitsInGame,
            int timeStep, Optional<Cache<Integer, ImmutableList<SingleUnitDestination>>> moveCache) {
        super(timeStep, unitPositions, possibleDestinationFromVertex, currentUnit, maxUnitsInGame, moveCache);
    }

    @Override
    public AdvanceableMultiMoveDefenderState advance(SingleUnitDestination move) {
        return new AdvanceableMultiMoveDefenderState(advanceUnitPositinos(move), getPossibleDestinationFromVertex(),
                advanceCurrentUnit(), getMaxUnitsInGame(), isLastMoveInRound() ? getTimeStep() + 1 : getTimeStep(),
                getMoveCache());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AdvanceableMultiMoveDefenderState other = (AdvanceableMultiMoveDefenderState) obj;
        return true;
    }

	@Override
	public String toString() {
	    return "AdvanceableMultiMoveDefenderState [timeStep=" + getTimeStep() + ", getUnitPositions()=" + getUnitPositions()
				+ ", getCurrentUnit()=" + getCurrentUnit() + "]";
	}
    
    
}
