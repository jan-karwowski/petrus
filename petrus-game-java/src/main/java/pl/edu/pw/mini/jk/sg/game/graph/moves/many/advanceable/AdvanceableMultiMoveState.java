package pl.edu.pw.mini.jk.sg.game.graph.moves.many.advanceable;

import java.util.List;
import java.util.Optional;

import com.google.common.cache.Cache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import pl.edu.pw.mini.jk.sg.game.common.State;

public class AdvanceableMultiMoveState
        implements State<AdvanceableMultiMoveAttackerState, AdvanceableMultiMoveDefenderState> {
    private final AdvanceableMultiMoveAttackerState attackerVisibleState;
    private final AdvanceableMultiMoveDefenderState defenderVisibleState;

    public AdvanceableMultiMoveState(List<Integer> defenderPositions, List<Integer> attackerPositions, int timeStep,
            int currentUnitMove, ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex,
            Optional<Cache<Integer, ImmutableList<SingleUnitDestination>>> moveCache) {
        final int maxUnitCount = Math.max(defenderPositions.size(), attackerPositions.size());
        attackerVisibleState = new AdvanceableMultiMoveAttackerState(timeStep, ImmutableList.copyOf(attackerPositions),
                possibleDestinationFromVertex, currentUnitMove, maxUnitCount, moveCache);
        defenderVisibleState = new AdvanceableMultiMoveDefenderState(ImmutableList.copyOf(defenderPositions),
                possibleDestinationFromVertex, currentUnitMove, maxUnitCount, timeStep, moveCache);
    }

    @Override
    public AdvanceableMultiMoveDefenderState getDefenderVisibleState() {
        return defenderVisibleState;
    }

    @Override
    public AdvanceableMultiMoveAttackerState getAttackerVisibleState() {
        return attackerVisibleState;
    }

}
