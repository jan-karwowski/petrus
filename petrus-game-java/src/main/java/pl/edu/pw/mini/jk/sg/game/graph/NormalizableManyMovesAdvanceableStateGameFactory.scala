package pl.edu.pw.mini.jk.sg.game.graph;

import argonaut.Json
import com.google.common.cache.Cache
import com.google.common.collect.ImmutableList
import java.util.Optional
import pl.edu.pw.mini.jk.sg.game.graph.advanceable.AdvanceableGraphGameConfiguration
import pl.edu.pw.mini.jk.sg.game.graph.moves.many.advanceable.{ ManyMovesAdvanceableStateGame, SingleUnitDestination }
import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import squants.time.Time



case class NormalizableManyMovesAdvanceableStateGameFactory(
  agc: AdvanceableGraphGameConfiguration,
  cache: Cache[Integer, ImmutableList[SingleUnitDestination]],
  normalized: Boolean = true
) extends NormalizableFactory[SingleUnitDestination, SingleUnitDestination ,ManyMovesAdvanceableStateGame] {
  override def create(): ManyMovesAdvanceableStateGame =
    new ManyMovesAdvanceableStateGame(agc, if(cache == null) Optional.empty() else Optional.of(cache), normalized)
  override def denormalize(): NormalizableFactory[SingleUnitDestination, SingleUnitDestination, ManyMovesAdvanceableStateGame] = copy(normalized=false)
  override def normalize(): NormalizableFactory[SingleUnitDestination, SingleUnitDestination, ManyMovesAdvanceableStateGame] = copy(normalized=true)

  override def defenderResultAsJson(strategy: java.util.Map[_ <: java.util.List[SingleUnitDestination], java.lang.Double], trainingTimeS: Double, methodName: String, methodConfig: Json, preprocessingTime: Time) = ???
  override def defenderStrategyToJson(strategy: java.util.Map[_ <: java.util.List[SingleUnitDestination], java.lang.Double]) = ???

}
