package pl.edu.pw.mini.jk.sg.game.graph.moves.single;

import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.stream.Collectors;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;

public class DesiredConfigurationMove implements DefenderMove{
    private static final long serialVersionUID = 2L;
    private final ImmutableList<Integer> targetVertices;
    private final int hash;

    public DesiredConfigurationMove(final int[] targetVertices) {
        this(ImmutableList.copyOf(Arrays.stream(targetVertices).boxed().iterator()));
    }
    
    public DesiredConfigurationMove(final ImmutableList<Integer> targetVertices) {
        this.targetVertices = targetVertices;
        hash = calculateHash();
    }

    public ImmutableList<Integer> getTargetVertices() {
        return targetVertices;
    }
    
    
    
    public int getTargetFor(int unit){
        return targetVertices.get(unit);
    }
    
    public int unitCount(){
        return targetVertices.size();
    }

    @Override
    public String toString() {
    	if(targetVertices.size()==1){
    		return Integer.toString(targetVertices.get(0));
    	} else {
    		return "["+targetVertices.stream().map(i -> i.toString()).collect(Collectors.joining(","))+"]";
    	}
    }

    @Override
    public int hashCode() {
        return hash;
    }

    private int calculateHash() {
        return targetVertices.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DesiredConfigurationMove other = (DesiredConfigurationMove) obj;
        if (!this.targetVertices.equals(other.targetVertices))
            return false;
        return true;
    }
}
