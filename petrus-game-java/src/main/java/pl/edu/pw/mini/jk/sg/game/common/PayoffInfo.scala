package pl.edu.pw.mini.jk.sg.uct

final case class PayoffInfo(
  payoff: Double,
  catchAccidents: Int,
  attackAccidents: Int
)

object PayoffInfo {
  def sum(pi1: PayoffInfo, pi2: PayoffInfo) : PayoffInfo =
    PayoffInfo(pi1.payoff + pi2.payoff, pi1.catchAccidents + pi2.catchAccidents,
      pi1.attackAccidents + pi2.attackAccidents)
}
