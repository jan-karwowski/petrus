package pl.edu.pw.mini.jk.sg.game.graph;

import argonaut.Json
import com.google.common.cache.Cache
import com.google.common.collect.ImmutableList
import java.util.Optional
import pl.edu.pw.mini.jk.sg.game.graph.advanceable.{ AdvanceableGraphGameConfiguration, AdvanceableStateGraphGame }
import pl.edu.pw.mini.jk.sg.game.graph.moves.single.DesiredConfigurationMove
import pl.edu.pw.mini.jk.sg.game.NormalizableFactory
import squants.time.Time



case class NormalizableAdvanceableStateGraphGameFactory(
  sgc: AdvanceableGraphGameConfiguration,
  moveCache: Option[Cache[Object, ImmutableList[DesiredConfigurationMove]]],
  normalized: Boolean = true
) extends NormalizableFactory[DesiredConfigurationMove, DesiredConfigurationMove, AdvanceableStateGraphGame] {
  override def create(): AdvanceableStateGraphGame = new AdvanceableStateGraphGame(sgc, moveCache match {case Some(x) => Optional.of(x); case None => Optional.empty[Cache[Object, ImmutableList[DesiredConfigurationMove]]]() }, normalized)
  override def denormalize(): NormalizableFactory[DesiredConfigurationMove,DesiredConfigurationMove,AdvanceableStateGraphGame] = copy(normalized=false)
  override def normalize(): NormalizableFactory[DesiredConfigurationMove, DesiredConfigurationMove, AdvanceableStateGraphGame] = copy(normalized=true)

  override def defenderResultAsJson(strategy: java.util.Map[_ <: java.util.List[DesiredConfigurationMove], java.lang.Double], trainingTimeS: Double, methodName: String, methodConfig: Json, preprocessingTime: Time) = ??? 
  override def defenderStrategyToJson(strategy: java.util.Map[_ <: java.util.List[DesiredConfigurationMove], java.lang.Double]) = ???

}
