package pl.edu.pw.mini.jk.sg.game.graph.advanceable;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import com.google.common.cache.Cache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import pl.edu.pw.mini.jk.sg.game.common.advanceable.AdvanceableAttackerState;
import pl.edu.pw.mini.jk.sg.game.graph.moves.single.DesiredConfigurationMove;

public class AttackerAdvaceableGraphState extends AdvanceableSideGraphState
		implements AdvanceableAttackerState<DesiredConfigurationMove> {

    public AttackerAdvaceableGraphState(int timeStep, ImmutableList<Integer> unitPositions,
			ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex) {
	    super(timeStep, unitPositions, possibleDestinationFromVertex);
	}

    public AttackerAdvaceableGraphState(int timeStep, ImmutableList<Integer> unitPositions,
			ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex,
			final Cache<Object, ImmutableList<DesiredConfigurationMove>> moveCache) {
	super(timeStep, unitPositions, possibleDestinationFromVertex, moveCache);
	}

	
	@Override
	public AttackerAdvaceableGraphState advance(DesiredConfigurationMove move) {
		if (!checkLegalMove(move)) {
			throw new IllegalArgumentException("Illegal move!");
		} else {
		    return new AttackerAdvaceableGraphState(getTimeStep()+1, ImmutableList.copyOf(IntStream.range(0, move.unitCount())
					.mapToObj(i -> move.getTargetFor(i)).iterator()), possibleDestinationFromVertex);
		}
	}

	@Override
	public ImmutableList<DesiredConfigurationMove> getPossibleMoves() {
		return super.getPossibleMoves();
	}
}
