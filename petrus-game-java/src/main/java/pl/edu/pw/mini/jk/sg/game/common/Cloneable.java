package pl.edu.pw.mini.jk.sg.game.common;

public interface Cloneable<T extends Cloneable<T>> {
    T typedClone();
}
