package pl.edu.pw.mini.jk.sg.game.configuration

final case class Edge(from: Int, to: Int)
