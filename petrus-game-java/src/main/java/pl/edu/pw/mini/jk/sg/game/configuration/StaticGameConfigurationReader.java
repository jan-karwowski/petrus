package pl.edu.pw.mini.jk.sg.game.configuration;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import org.apache.commons.io.IOUtils;
import pl.edu.pw.mini.jk.sg.game.graph.advanceable.AdvanceableGraphGameConfiguration;



public class StaticGameConfigurationReader {
    public static StaticGameConfiguration readSgcFromFile(File file) throws IOException {
	try (Reader reader = new FileReader(file)) {
	    String contents = IOUtils.toString(reader);
	    return DecodeAdvanceableGraphGameConfiguraionJson.readSgc(contents);
	}
    }

    public static AdvanceableGraphGameConfiguration readAagcFromFile(File file)
	throws IOException {
	try (Reader reader = new FileReader(file)) {
	    return readFromStream(reader);
	}
    }
	
    public static AdvanceableGraphGameConfiguration readFromStream(Reader file)
	throws IOException {
	String contents = IOUtils.toString(file);
	return DecodeAdvanceableGraphGameConfiguraionJson.readAggc(contents);
    }

    public static AdvanceableGraphGameConfiguration readFromStream(InputStream file)
	throws IOException {
	try (Reader rs = new InputStreamReader(file)) {
	    return readFromStream(rs);
	}
    }

    public static void writeToFile(AdvanceableGraphGameConfiguration aggc, File output)  throws IOException {
	try (Writer writer = new FileWriter(output)) {
	    writer.write(DecodeAdvanceableGraphGameConfiguraionJson.encodeAggc(aggc));
	}
    }
}
