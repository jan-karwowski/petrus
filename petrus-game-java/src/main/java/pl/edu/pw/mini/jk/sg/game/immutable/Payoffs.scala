package pl.edu.pw.mini.jk.sg.game.immutable

case class Payoffs(defenderPayoff: Double, attackerPayoff: Double, successfulAttacks: Int, successfulDefenses: Int)
