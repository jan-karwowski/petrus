package pl.edu.pw.mini.jk.sg.game.common.advanceable;

import pl.edu.pw.mini.jk.sg.game.common.DefenderMove;
import pl.edu.pw.mini.jk.sg.game.common.DefenderVisibleState;

public interface AdvanceableDefenderState<M extends DefenderMove> extends AdvanceableState<M>, DefenderVisibleState {

}
