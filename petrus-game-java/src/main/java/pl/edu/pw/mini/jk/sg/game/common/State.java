package pl.edu.pw.mini.jk.sg.game.common;

public interface State<A extends AttackerVisibleState, D extends DefenderVisibleState> {
	public D getDefenderVisibleState();
	public A getAttackerVisibleState();
}
