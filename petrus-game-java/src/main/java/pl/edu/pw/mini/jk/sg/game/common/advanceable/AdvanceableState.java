package pl.edu.pw.mini.jk.sg.game.common.advanceable;


import com.google.common.collect.ImmutableList;
import org.apache.commons.math3.random.RandomGenerator;

public interface AdvanceableState<M> {
	AdvanceableState<M> advance(M move);
	
	ImmutableList<M> getPossibleMoves();
    int getTimeStep();

    public default M getRandomMove(RandomGenerator rng) {
	final ImmutableList<M> moves = getPossibleMoves();
	return moves.get(rng.nextInt(moves.size()));
    }
}
