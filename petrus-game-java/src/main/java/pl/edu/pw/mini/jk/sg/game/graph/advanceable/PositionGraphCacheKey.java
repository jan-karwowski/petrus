package pl.edu.pw.mini.jk.sg.game.graph.advanceable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

public class PositionGraphCacheKey {
    private final ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex;
    private final ImmutableList<Integer> unitPositions;

    public PositionGraphCacheKey(ImmutableList<ImmutableSet<Integer>> possibleDestinationFromVertex,
            ImmutableList<Integer> unitPositions) {
        super();
        this.possibleDestinationFromVertex = possibleDestinationFromVertex;
        this.unitPositions = unitPositions;
    }

    public ImmutableList<ImmutableSet<Integer>> getPossibleDestinationFromVertex() {
        return possibleDestinationFromVertex;
    }

    public ImmutableList<Integer> getUnitPositions() {
        return unitPositions;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((possibleDestinationFromVertex == null) ? 0 : possibleDestinationFromVertex.hashCode());
        result = prime * result + ((unitPositions == null) ? 0 : unitPositions.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PositionGraphCacheKey other = (PositionGraphCacheKey) obj;
        if (possibleDestinationFromVertex == null) {
            if (other.possibleDestinationFromVertex != null)
                return false;
        } else if (!possibleDestinationFromVertex.equals(other.possibleDestinationFromVertex))
            return false;
        if (unitPositions == null) {
            if (other.unitPositions != null)
                return false;
        } else if (!unitPositions.equals(other.unitPositions))
            return false;
        return true;
    }
}
