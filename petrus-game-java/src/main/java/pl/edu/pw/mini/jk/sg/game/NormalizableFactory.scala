package pl.edu.pw.mini.jk.sg.game

import argonaut.Json
import pl.edu.pw.mini.jk.sg.game.common.DefenderMove
import squants.time.Time


trait NormalizableFactory[AM, DM <: DefenderMove, G <: DefenderAttackerGame[AM, DM, _, _, _]] {
  final type Game = G
  final type AttackerMove = AM
  final type DefenderMove = DM 

  def create: G
  def normalize : NormalizableFactory[AM, DM, G]
  def denormalize: NormalizableFactory[AM, DM, G]

  def defenderStrategyToJson(strategy: java.util.Map[_ <: java.util.List[DM], java.lang.Double]) : Json
  def defenderResultAsJson(strategy: java.util.Map[_ <: java.util.List[DM], java.lang.Double], trainingTimeS: Double, methodName: String, methodConfig: Json, preprocessingTime: Time) : Json
}

