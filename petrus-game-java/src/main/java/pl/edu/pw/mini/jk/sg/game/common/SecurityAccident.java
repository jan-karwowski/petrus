package pl.edu.pw.mini.jk.sg.game.common;

public interface SecurityAccident {
	int getTimeStep();
	int getAttacker();
	boolean hasSucceed();
}
