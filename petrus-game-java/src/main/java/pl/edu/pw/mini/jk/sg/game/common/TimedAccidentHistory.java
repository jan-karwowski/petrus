package pl.edu.pw.mini.jk.sg.game.common;

import java.io.Serializable;
import java.util.Collection;

import com.google.common.collect.ImmutableSet;

public class TimedAccidentHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    private final ImmutableSet<Accident> accidentHistory;

    public TimedAccidentHistory(Collection<Accident> accidentHistory, int timeStep) {

        this.accidentHistory = ImmutableSet.copyOf(accidentHistory
                .stream()
                .map(a -> new Accident(a.timeStep() - timeStep, a.attackerId(), a.location(), a
                        .attackerSuccess())).iterator());
    }

    public ImmutableSet<Accident> getAccidentHistory() {
        return accidentHistory;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accidentHistory == null) ? 0 : accidentHistory.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TimedAccidentHistory other = (TimedAccidentHistory) obj;
        if (accidentHistory == null) {
            if (other.accidentHistory != null)
                return false;
        } else if (!accidentHistory.equals(other.accidentHistory))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TAH " + accidentHistory + "";
    }

    

}
