package pl.edu.pw.mini.jk.sg.game.configuration

@FunctionalInterface
trait DefenseProbabilityFunction {
    def apply(vertex: Int, defenderCount : Int) : Double
}
      

class DiscreteProbabilityFunction

object DiscreteProbabilityFunction extends DefenseProbabilityFunction {
  final def apply(vertex: Int, defenderCount : Int) : Double =
    if (defenderCount > 0) 1 else 0

  final val instance = this
}
