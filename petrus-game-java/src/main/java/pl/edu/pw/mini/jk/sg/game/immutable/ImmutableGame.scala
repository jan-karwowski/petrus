package pl.edu.pw.mini.jk.sg.game.immutable

import pl.edu.pw.mini.jk.sg.game.common.{DefenderMove => CDM}



trait ImmutableGame[DM <: CDM, AM, DS <: AdvanceableState[DM,DS], AS <: AdvanceableState[AM,AS], G <: ImmutableGame[DM, AM, DS, AS, G]] {
  this: G => 

  // type DefenderMove = DS#Move
  // type AttackerMove = AS#Move
  // type AttackerState = AS
  // type DefenderState = DS

  def makeMoves(defenderMove: DM, attackerMove: AM) : (Payoffs, G)

  def defenderVisibleState : DS
  def attackerVisibleState : AS
  def timeStep: Int

  def defenderPayoff : Double
  def attackerPayoff : Double
}
