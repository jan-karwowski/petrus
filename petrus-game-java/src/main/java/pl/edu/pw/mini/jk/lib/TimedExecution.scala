package pl.edu.pw.mini.jk.lib

import com.google.common.base.Stopwatch
import java.util.concurrent.TimeUnit
import squants.Time
import squants.time.Milliseconds

class TimedExecution

object TimedExecution {
  def apply[T](proc: => T) : (T, Time) = {
    val sw = Stopwatch.createStarted()
    val out: T = proc 
    sw.stop()
    (out, Milliseconds(sw.elapsed(TimeUnit.MILLISECONDS).toDouble))
  }
}
