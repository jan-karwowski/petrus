#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $DIR/el_config.sh
echo '\begin{longtable}{l|l|l|rrrrr|rrr|r}'
echo '\caption{Wyniki: liczba udanych ataków przy różnych konfiguracjach. Numery gier i konfiguracji oznaczają odpowiednie gry i konfiguracje zdefiniowane w poprzednich sekcjach. Litera przy numerze gry oznacza modelowanie ruchów. A --- opisane w \ref{sec:mov-mul-enum}, B --- \ref{mov:single}}'
echo '\label{tab:attacks}\\'

echo '&&&\multicolumn{5}{c}{Attacks}&\multicolumn{3}{c}{Payoff}&Random\\'
echo 'Game&Config&Attacker&min&mean&median&max&stddev&mean&median&stddev&mean\\'
echo '\hline\hline'
echo '\endfirsthead'
echo '&&&\multicolumn{5}{c}{Attacks}&\multicolumn{3}{c}{Payoff}&Random\\'
echo 'Game&Config&Attacker&min&mean&median&max&stddev&mean&median&stddev&mean\\'
echo '\hline\hline'
echo '\endhead'
echo '\endfoot\endlastfoot'



for game in $games; do
    for config in $configs; do
	if test $config '!=' RANDOM; then
	    for attacker in ema1 cea1; do
		echo -n \\gameinfo\{${game}\}\&\\config\{$(echo ${config}|sed 's/_/-/g')\}\&${attacker}\&
		grep '"uctattacks"' game${game}_${config}_${attacker}.agg.csv | cut -d, -f2- --output-delimiter=\&
		echo '&'
		grep '"uctpayoff"' game${game}_${config}_${attacker}.agg.csv | cut -d, -f3,4,6 --output-delimiter=\&
		echo '&'
		grep '"uctpayoff"' game${game}_RANDOM_${attacker}.agg.csv | cut -d, -f3
		echo \\\\
		echo '\nopagebreak'
	    done
	fi
    done
    echo \\hline
	echo '\pagebreak[1]'
done

echo '\end{longtable}'

