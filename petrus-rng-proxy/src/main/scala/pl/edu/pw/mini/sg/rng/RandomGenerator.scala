package pl.edu.pw.mini.sg.rng

trait RandomGenerator {
  def nextInt(upperBoundExcl: Int) : Int
  def nextDouble() : Double
  def nextBoolean(): Boolean
}
